//******************************************************************************
// file: LTPI.cc
// desc: library for LTPI module
// auth: 08-JUL-2013, R. Spiwoks (rewrite from exisiting RCDLtpi.cc)
//******************************************************************************

#include "RCDLtpi/LTPI.h"
#include "RCDUtilities/RCDUtilities.h"

#include <sstream>
#include <fstream>

using namespace RCD;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const std::string LTPI::MODE_SELECTION_NAME[MODE_SELECTION_NUMBER] = {
    "DISABLED", "FROM CTP", "FROM LTP", "FROM NIM", "PARALLEL", "PARALLEL FROM NIM"
};

const std::string LTPI::SIGNAL_NAME[SIGNAL_NUMBER] = {
    "ORB", "L1A", "TTRG1", "TTRG2", "TTRG3", "BGO0", "BGO1", "BGO2", "BGO3"
};

const std::string LTPI::SELECT_SIGNAL_NAME[SELECT_SIGNAL_NUMBER] = {
    "BC", "ORB", "L1A", "TTRG", "BGO"
};

const std::string LTPI::SIGNAL_SELECTION_NAME[SIGNAL_SELECTION_NUMBER] = {
    "DISABLED", "PARALLEL", "FROM CTP", "FROM LTP", "FROM NIM", "PARALLEL FROM NIM"
};

const std::string LTPI::SIGNAL_SELECTION_DESC[SIGNAL_SELECTION_NUMBER] = {
    "",
    "CTP -> CTP,NIM  and  LTP -> LTP",
    "CTP -> CTP,NIM  and  CTP -> LTP",
    "LTP -> CTP,NIM  and  LTP -> LTP",
    "NIM -> CTP,NIM  and  NIM -> LTP",
    "NIM -> CTP,NIM  and  LTP -> LTP"
};

const std::string LTPI::TTYP_SELECTION_NAME[TTYP_SELECTION_NUMBER] = {
    "DISABLED", "PARALLEL", "FROM CTP", "FROM LTP", "FROM LOCAL", "PARALLEL FROM LOCAL"
};

const std::string LTPI::TTYP_SELECTION_DESC[TTYP_SELECTION_NUMBER] = {
    "",
    "CTP -> CTP  and  LTP -> LTP",
    "CTP -> CTP  and  CTP -> LTP",
    "LTP -> CTP  and  LTP -> LTP",
    "LOC -> CTP  and  LOC -> LTP",
    "LOC -> CTP  and  LTP -> LTP"
};

const std::string LTPI::LOCTTYP_SELECTION_NAME[LOCTTYP_SELECTION_NUMBER] = {
    "FROM TTYP[0]", "FROM CTP TTRG", "FROM LTP TTRG", "FROM NIM TTRG"
};

const std::string LTPI::CALREQ_SELECTION_NAME[CALREQ_SELECTION_NUMBER] = {
    "DISABLED", "PARALLEL", "FROM CTP", "FROM LTP"
};

const std::string LTPI::CALREQ_SELECTION_DESC[CALREQ_SELECTION_NUMBER] = {
    "",
    "CTP <- CTP  and  LTP <- LTP",
    "CTP <- CTP  and  LTP <- CTP",
    "CTP <- LTP  and  LTP <- LTP",
};

const std::string LTPI::LINK_NAME[LINK_NUMBER] = {
    "CTP", "LTP", "NIM"
};

const std::string LTPI::BUSY_SELECTION_NAME[BUSY_SELECTION_NUMBER] = {
    "DISABLED", "FROM CTP", "FROM LTP", "FROM CTP OR LTP"
};

const std::string LTPI::BUSY_SELECTION_DESC[BUSY_SELECTION_NUMBER] = {
    "", "<- CTP", "<- LTP", "<- CTP+LTP"
};

const std::string LTPI::DELAY_SIGNAL_NAME[DELAY_SIGNAL_NUMBER] = {
    "ORB", "L1A", "TTRG1", "TTRG2", "TTRG3", "BGO0",  "BGO1",  "BGO2",  "BGO3",
    "TTYP0", "TTYP1", "TTYP2", "TTYP3", "TTYP4", "TTYP5", "TTYP6", "TTYP7"
};

const std::string LTPI::DELAY_GROUP_NAME[DELAY_GROUP_NUMBER] = {
    "ORB", "L1A", "TTRG", "BGO", "TTYP"
};

const unsigned int LTPI::DELAY_GROUP_LO[DELAY_GROUP_NUMBER] = {
    DELAY_ORB, DELAY_L1A, DELAY_TTRG1, DELAY_BGO0, DELAY_TTYP0
};

const unsigned int LTPI::DELAY_GROUP_HI[DELAY_GROUP_NUMBER] = {
    DELAY_ORB+1, DELAY_L1A+1, DELAY_TTRG1+TTRG_NUMBER, DELAY_BGO0+BGO_NUMBER, DELAY_TTYP0+TTYP_NUMBER
};

const double LTPI::DELAY_STEP = 0.5;

const std::string LTPI::STATUS_SIGNAL_NAME[STATUS_SIGNAL_NUMBER] = {
    "BC", "ORB", "L1A", "TTRG1", "TTRG2", "TTRG3", "BGO0",  "BGO1",  "BGO2",  "BGO3",
    "TTYP0", "TTYP1", "TTYP2", "TTYP3", "TTYP4", "TTYP5", "TTYP6", "TTYP7",
    "CALREQ0", "CALREQ1", "CALRQE2", "BUSY"
};

const double LTPI::I2C_DEFAULT_FREQUENCY = 100.0;

const std::string LTPI::EQUAL_LINK_NAME[EQUAL_LINK_NUMBER] = {
    "CTPin", "LTPin"
};

const double LTPI::EQUAL_LENGTH = 20.0;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const std::string LTPI::VMM_CODE = "A24";

const u_short LTPI::SELECT_ADDR[SELECT_SIGNAL_NUMBER] = {
    CSR1_ADDR, CSR1_ADDR, CSR1_ADDR, CSR1_ADDR, CSR2_ADDR
};

const u_short LTPI::SELECT_MASK[SELECT_SIGNAL_NUMBER] = {
    CSR1_MASK_BC, CSR1_MASK_ORB, CSR1_MASK_L1A, CSR1_MASK_TTRG, CSR2_MASK_BGO
};

const u_short LTPI::SELECT_SHFT[SELECT_SIGNAL_NUMBER] = {
    CSR1_SHFT_BC, CSR1_SHFT_ORB, CSR1_SHFT_L1A, CSR1_SHFT_TTRG, CSR2_SHFT_BGO
};

const u_short LTPI::SELECT_DATA[SIGNAL_SELECTION_NUMBER] = {
    0x0000, 0x0001, 0x0002, 0x0003, 0x0004, 0x0005
};

const u_short LTPI::TTYP_DATA[TTYP_SELECTION_NUMBER] = {
    0x0000, 0x0001, 0x0002, 0x0003, 0x0004, 0x0005
};

const u_short LTPI::LOCTTYP_DATA[LOCTTYP_SELECTION_NUMBER] = {
    0x0000, 0x0001, 0x0002, 0x0003
};

const u_short LTPI::CALREQ_DATA[CALREQ_SELECTION_NUMBER] = {
    0x0000, 0x0001, 0x0002, 0x0003
};

const u_short LTPI::BUSY_ADDR[LINK_NUMBER] = {
    CSR3_ADDR, CSR3_ADDR, CSR3_ADDR
};

const u_short LTPI::BUSY_MASK[LINK_NUMBER] = {
    CSR3_MASK_BUSY_CTP, CSR3_MASK_BUSY_LTP, CSR3_MASK_BUSY_NIM
};

const u_short LTPI::BUSY_SHFT[LINK_NUMBER] = {
    CSR3_SHFT_BUSY_CTP, CSR3_SHFT_BUSY_LTP, CSR3_SHFT_BUSY_NIM
};

const u_short LTPI::BUSY_DATA[BUSY_SELECTION_NUMBER] = {
    0x0000, 0x0001, 0x0003, 0x0002
};

const u_short LTPI::STATUS_ADDR[LINK_NUMBER][STATUS_SIGNAL_NUMBER] = {
    { STAT1_ADDR, STAT1_ADDR, STAT1_ADDR,
      STAT2_ADDR, STAT2_ADDR, STAT2_ADDR,
      STAT3_ADDR, STAT3_ADDR, STAT3_ADDR, STAT3_ADDR,
      STAT4_ADDR, STAT4_ADDR, STAT4_ADDR, STAT4_ADDR,
      STAT4_ADDR, STAT4_ADDR, STAT4_ADDR, STAT4_ADDR,
      STAT2_ADDR, STAT2_ADDR, STAT2_ADDR, STAT1_ADDR },
    { STAT1_ADDR, STAT1_ADDR, STAT1_ADDR,
      STAT2_ADDR, STAT2_ADDR, STAT2_ADDR,
      STAT3_ADDR, STAT3_ADDR, STAT3_ADDR, STAT3_ADDR,
      STAT4_ADDR, STAT4_ADDR, STAT4_ADDR, STAT4_ADDR,
      STAT4_ADDR, STAT4_ADDR, STAT4_ADDR, STAT4_ADDR,
      STAT3_ADDR, STAT3_ADDR, STAT3_ADDR, STAT1_ADDR },
    { STAT1_ADDR, STAT1_ADDR, STAT1_ADDR,
      STAT2_ADDR, STAT2_ADDR, STAT2_ADDR,
      STAT3_ADDR, STAT3_ADDR, STAT3_ADDR, STAT3_ADDR,
      0x00000000, 0x00000000, 0x00000000, 0x0000000,
      0x00000000, 0x00000000, 0x00000000, 0x0000000,
      0x00000000, 0x00000000, 0x00000000, 0x0000000 }
};

const u_short LTPI::STATUS_MASK[LINK_NUMBER][STATUS_SIGNAL_NUMBER] = {
    { STAT1_MASK_BC_CTP, STAT1_MASK_ORB_CTP, STAT1_MASK_L1A_CTP,
      STAT2_MASK_TTRG1_CTP, STAT2_MASK_TTRG2_CTP, STAT2_MASK_TTRG3_CTP,
      STAT3_MASK_BGO0_CTP, STAT3_MASK_BGO1_CTP, STAT3_MASK_BGO2_CTP, STAT3_MASK_BGO3_CTP,
      STAT4_MASK_TTYP0_CTP, STAT4_MASK_TTYP1_CTP, STAT4_MASK_TTYP2_CTP, STAT4_MASK_TTYP3_CTP,
      STAT4_MASK_TTYP4_CTP, STAT4_MASK_TTYP5_CTP, STAT4_MASK_TTYP6_CTP, STAT4_MASK_TTYP7_CTP,
      STAT2_MASK_CALREQ0_CTP, STAT2_MASK_CALREQ1_CTP, STAT2_MASK_CALREQ2_CTP, STAT1_MASK_BUSY_CTP },
    { STAT1_MASK_BC_LTP, STAT1_MASK_ORB_LTP, STAT1_MASK_L1A_LTP,
      STAT2_MASK_TTRG1_LTP, STAT2_MASK_TTRG2_LTP, STAT2_MASK_TTRG3_LTP,
      STAT3_MASK_BGO0_LTP, STAT3_MASK_BGO1_LTP, STAT3_MASK_BGO2_LTP, STAT3_MASK_BGO3_LTP,
      STAT4_MASK_TTYP0_LTP, STAT4_MASK_TTYP1_LTP, STAT4_MASK_TTYP2_LTP, STAT4_MASK_TTYP3_LTP,
      STAT4_MASK_TTYP4_LTP, STAT4_MASK_TTYP5_LTP, STAT4_MASK_TTYP6_LTP, STAT4_MASK_TTYP7_LTP,
      STAT3_MASK_CALREQ0_LTP, STAT3_MASK_CALREQ1_LTP, STAT3_MASK_CALREQ2_LTP, STAT1_MASK_BUSY_LTP },
    { STAT1_MASK_BC_NIM, STAT1_MASK_ORB_NIM, STAT1_MASK_L1A_NIM,
      STAT2_MASK_TTRG1_NIM, STAT2_MASK_TTRG2_NIM, STAT2_MASK_TTRG3_NIM,
      STAT3_MASK_BGO0_NIM, STAT3_MASK_BGO1_NIM, STAT3_MASK_BGO2_NIM, STAT3_MASK_BGO3_NIM,
      0x00000000, 0x00000000, 0x00000000, 0x0000000,
      0x00000000, 0x00000000, 0x00000000, 0x0000000,
      0x00000000, 0x00000000, 0x00000000, 0x0000000 }
};

const u_short LTPI::STATUS_INVT[LINK_NUMBER][STATUS_SIGNAL_NUMBER] = {
    { false, false, false,
      false, false, false,
      false, false, false, false,
      false, false, false, false,
      false, false, false, false,
      false, false, false, false },
    { false, false, false,
      false, false, false,
      false, false, false, false,
      false, false, false, false,
      false, false, false, false,
      false, false, false, false },
    { false, false, false,
      false, false, false,
      false, false, false, false,
      false, false, false, false,
      false, false, false, false,
      false, false, false, false },
};

const u_short LTPI::DELAY_ADDR[DELAY_SIGNAL_NUMBER] = {
    0x003a, 0x0038, 0x0028, 0x0029, 0x002a, 0x0030, 0x0031, 0x0032, 0x0033,
    0x0010, 0x0011, 0x0012, 0x0013, 0x0020, 0x0021, 0x0022, 0x0023
};

const double LTPI::I2C_PRE_FACT = 8000.0;

const u_short LTPI::EQUAL_GAIN_ADDR[EQUAL_LINK_NUMBER] = {
    0x0010, 0x0012
};

const u_short LTPI::EQUAL_CTRL_ADDR[EQUAL_LINK_NUMBER] = {
    0x0014, 0x0016
};

const std::string LTPI::EQUAL_SELECTION_NAME[EQUAL_SELECTION_NUMBER] = {
    "DISABLED", "SHORT CABLE", "LONG CABLE"
};

const u_short LTPI::EQUAL_GAIN_DATA[EQUAL_SELECTION_NUMBER] = {
    0x00, 0x80, 0xa0
};

const u_short LTPI::EQUAL_CTRL_DATA[EQUAL_SELECTION_NUMBER] = {
    0x00, 0x00, 0x10
};

const u_short LTPI::EEPROM_MAN_ADDR[EEPROM_MAN_NUMBER] = {
    0x26, 0x2a, 0x2e
};

const u_short LTPI::EEPROM_BRD_ADDR[EEPROM_BRD_NUMBER] = {
    0x32, 0x36, 0x3a, 0x3e
};

const u_short LTPI::EEPROM_REV_ADDR[EEPROM_REV_NUMBER] = {
    0x42, 0x46, 0x4a, 0x4e
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

LTPI::Configuration::Configuration() {

    reset();
}

//------------------------------------------------------------------------------

LTPI::Configuration::~Configuration() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void LTPI::Configuration::reset() {

    unsigned int sig, typ, lnk, del;

    // link equalization
    for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) {
        equal_enable[lnk] = false;
        equal_short_cable[lnk] = true;
    }

    // signal selection
    for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) {
        signal_select[sig] = SIGNAL_DISABLED;
    }
    ttyp_select = TTYP_DISABLED;
    locttyp_select = LOCTTYP_FROM_TTYP_WORD0;
    for(typ=0; typ<LOCTTYP_NUMBER; typ++) {
        locttyp_word[typ] = 0x00;
    }
    calreq_select = CALREQ_DISABLED;
    for(lnk=0; lnk<LINK_NUMBER; lnk++) {
        busy_select[lnk] = BUSY_DISABLED;
    }

    // enable/disable selection
    for(del=0; del<DELAY_SIGNAL_NUMBER; del++) {
        ctpout_enable[del] = false;
        ctpout_delay[del] = 0;
    }
}

//------------------------------------------------------------------------------

void LTPI::Configuration::print(std::ostream& s) const {

    unsigned int lnk, sig, idx, del;
    double stp;
    std::string nam, nbm, ncm;
    char os[STRING_LENGTH];

    s << "============================================================================" << std::endl;
    s << "LTPI::Configuration:" << std::endl;

    // link equalization
    s << "----------------------------------------------------------------------------" << std::endl;
    s << "LINK EQUALIZATION [CTPin,LTPin]:" << std::endl;
    s << "----------------------------------------------------------------------------" << std::endl;
    for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) {
        nam = equal_enable[lnk]?"\"ENABLED\" ":"\"DISABLED\"";
        nbm = "\""+(equal_short_cable[lnk]?EQUAL_SELECTION_NAME[EQUAL_SHORT_CABLE]:EQUAL_SELECTION_NAME[EQUAL_LONG_CABLE]) + "\"";
        std::sprintf(os,"Link \"%s\"      : %s - %s\n",EQUAL_LINK_NAME[lnk].c_str(),nam.c_str(),nbm.c_str()); s << os;
    }

    // signal selection
    s << "----------------------------------------------------------------------------" << std::endl;
    s << "SIGNAL SELECTION:" << std::endl;
    s << "----------------------------------------------------------------------------" << std::endl;
    for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) {
        nam = "\"" + SELECT_SIGNAL_NAME[sig] + "\"";
        nbm = "\"" + SIGNAL_SELECTION_NAME[signal_select[sig]] + "\"";
        ncm = (!SIGNAL_SELECTION_DESC[signal_select[sig]].empty()) ? ("  (" + SIGNAL_SELECTION_DESC[signal_select[sig]] + ")") : "";
        std::sprintf(os,"Signal %-6s     : %-21s%s\n",nam.c_str(),nbm.c_str(),ncm.c_str()); s << os;
    }
    nam = "\"" + TTYP_SELECTION_NAME[ttyp_select] + "\"";
    nbm = (!TTYP_SELECTION_DESC[ttyp_select].empty()) ? ("  (" + TTYP_SELECTION_DESC[ttyp_select] + ")") : "";
    std::sprintf(os,"Signal \"TTYP\"     : %-21s%s\n",nam.c_str(),nbm.c_str()); s << os;
    nam = "\"" + LOCTTYP_SELECTION_NAME[locttyp_select] + "\"";
    nbm = ((ttyp_select==TTYP_FROM_LOCAL)||(ttyp_select==TTYP_PARALLEL_FROM_LOCAL)) ? "" : "  [NOT USED]";
    std::sprintf(os,"\"LOCAL TTYP\"      : %-21s%s\n",nam.c_str(),nbm.c_str()); s << os;
    for(idx=0; idx<LOCTTYP_NUMBER; idx++) {
        std::sprintf(os,"\"LOCAL TTYP[%d]\"   : 0x%02x                 %s\n",idx,locttyp_word[idx],nbm.c_str()); s<< os;
    }
    nam = "\"" + CALREQ_SELECTION_NAME[calreq_select] + "\"";
    nbm = (!CALREQ_SELECTION_DESC[calreq_select].empty()) ? ("  (" + CALREQ_SELECTION_DESC[calreq_select] + ")") : "";
    std::sprintf(os,"Signal \"CALREQ\"   : %-21s%s\n",nam.c_str(),nbm.c_str()); s << os;
    for(lnk=0; lnk<LINK_NUMBER; lnk++) {
        nam = "\"" + BUSY_SELECTION_NAME[busy_select[lnk]] + "\"";
        nbm = (!BUSY_SELECTION_DESC[busy_select[lnk]].empty()) ? ("  (" + LINK_NAME[lnk] + " " + BUSY_SELECTION_DESC[busy_select[lnk]] + ")") : "";
        std::sprintf(os,"Signal \"BUSY[%s]\": %-21s%s\n",LINK_NAME[lnk].c_str(),nam.c_str(),nbm.c_str()); s << os;
    }

    // enable/delay selection
    s << "----------------------------------------------------------------------------" << std::endl;
    s << "SIGNAL DELAY/ENABLE SETTING [CTPout]:" << std::endl;
    s << "----------------------------------------------------------------------------" << std::endl;
    for(del=0; del<DELAY_SIGNAL_NUMBER; del++) {
        nam = "\"" + DELAY_SIGNAL_NAME[del] + "\"";
        nbm = ctpout_enable[del]?"\"ENABLED\" ":"\"DISABLED\"";
        stp = static_cast<double>(ctpout_delay[del])*DELAY_STEP;
        std::sprintf(os,"Signal %-8s   : %s - %2d (%4.1f nsec)\n",nam.c_str(),nbm.c_str(),ctpout_delay[del],stp); s << os;
    }
    s << "============================================================================" << std::endl;

}

//------------------------------------------------------------------------------

int LTPI::Configuration::read(const std::string& fn) {

    std::ifstream s(fn.c_str()); if(!s) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        return(ERROR);
    }

    unsigned int lnk, sig, typ, del, i;
    std::string key, val, nam;
    std::ostringstream buf;
    int rtnv(0);
    bool flag;

    // link equalization
    for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) {
        nam = "ENABLE_EQUAL_" + EQUAL_LINK_NAME[lnk];
        if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
            return(rtnv);
        }
        if(key != nam) {
            CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
            return(ERROR);
        }
        if(val == "ENABLED") {
            equal_enable[lnk] = true;
        }
        else if(val == "DISABLED") {
            equal_enable[lnk] = false;
        }
        else {
            CERR("error in fixed format: scanning %s word %d \"%s\"",nam.c_str(),val.c_str());
            return(ERROR);
        }
        nam = "CABLE_EQUAL_" + EQUAL_LINK_NAME[lnk];
        if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
            return(rtnv);
        }
        if(key != nam) {
            CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
            return(ERROR);
        }
        if(val == "SHORT CABLE") {
            equal_short_cable[lnk] = true;
        }
        else if(val == "LONG CABLE") {
            equal_short_cable[lnk] = false;
        }
        else {
            CERR("error in fixed format: for key \"%s\" got unknown value  \"%s\"",nam.c_str(),val.c_str());
            return(ERROR);
        }
    }

    // signal selection
    for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) {
        nam = "SELECTION_" + SELECT_SIGNAL_NAME[sig];
        if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
            return(rtnv);
        }
        if(key != nam) {
            CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
            return(ERROR);
        }
        for(flag=false, i=0; i<SIGNAL_SELECTION_NUMBER; i++) {
            if(val == SIGNAL_SELECTION_NAME[i]) {
                signal_select[sig] = static_cast<SIGNAL_SELECTION>(i);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("error in fixed format: for key \"%s\" got unknown value  \"%s\"",nam.c_str(),val.c_str());
            return(ERROR);
        }
    }
    nam = "SELECTION_TTYP";
    if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
        return(rtnv);
    }
    if(key != nam) {
        CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
        return(ERROR);
    }
    for(flag=false, i=0; i<TTYP_SELECTION_NUMBER; i++) {
        if(val == TTYP_SELECTION_NAME[i]) {
            ttyp_select = static_cast<TTYP_SELECTION>(i);
            flag = true;
            break;
        }
    }
    if(!flag) {
        CERR("error in fixed format: for key \"%s\" got unknown value  \"%s\"",nam.c_str(),val.c_str());
        return(ERROR);
    }
    nam = "SELECTION_LOCTTYP";
    if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
        return(rtnv);
    }
    if(key != nam) {
        CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
        return(ERROR);
    }
    for(flag=false, i=0; i<LOCTTYP_SELECTION_NUMBER; i++) {
        if(val == LOCTTYP_SELECTION_NAME[i]) {
            locttyp_select = static_cast<LOCTTYP_SELECTION>(i);
            flag = true;
            break;
        }
    }
    if(!flag) {
        CERR("error in fixed format: for key \"%s\" got unknown value  \"%s\"",nam.c_str(),val.c_str());
        return(ERROR);
    }
    for(typ=0; typ<LOCTTYP_NUMBER; typ++) {
        buf.str(""); buf << "SELECTION_TTYP[" << typ << "]";
        if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"",buf.str().c_str(),fn.c_str());
            return(rtnv);
        }
        if(key != buf.str()) {
            CERR("error in fixed format: expected key \"%s\" got key \"%s\"",buf.str().c_str(),key.c_str());
            return(ERROR);
        }
        if(std::sscanf(val.c_str(),"%hx",&locttyp_word[typ]) != 1) {
            CERR("error in fixed format: scanning TTYP word %d \"%s\"",typ,val.c_str());
            return(ERROR);
        }
    }
    nam = "SELECTION_CALREQ";
    if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
        return(rtnv);
    }
    if(key != nam) {
        CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
        return(ERROR);
    }
    for(flag=false, i=0; i<CALREQ_SELECTION_NUMBER; i++) {
        if(val == CALREQ_SELECTION_NAME[i]) {
            calreq_select = static_cast<CALREQ_SELECTION>(i);
            flag = true;
            break;
        }
    }
    if(!flag) {
        CERR("error in fixed format: for key \"%s\" got unknown value  \"%s\"",nam.c_str(),val.c_str());
        return(ERROR);
    }
    for(lnk=0; lnk<LINK_NUMBER; lnk++) {
        nam = "SELECTION_BUSY[" + LINK_NAME[lnk] + "]";
        if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
            return(rtnv);
        }
        if(key != nam) {
            CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
            return(ERROR);
        }
        for(flag=false, i=0; i<CALREQ_SELECTION_NUMBER; i++) {
            if(val == BUSY_SELECTION_NAME[i]) {
                busy_select[lnk] = static_cast<BUSY_SELECTION>(i);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("error in fixed format: for key \"%s\" got unknown value  \"%s\"",nam.c_str(),val.c_str());
            return(ERROR);
        }
    }

    // enable/delay selection
    for(del=0; del<DELAY_SIGNAL_NUMBER; del++) {
        nam = "ENABLE_CTPOUT_" + DELAY_SIGNAL_NAME[del];
        if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
            return(rtnv);
        }
        if(key != nam) {
            CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
            return(ERROR);
        }
        if(val == "ENABLED") {
            ctpout_enable[del] = true;
        }
        else if(val == "DISABLED") {
            ctpout_enable[del] = false;
        }
        else {
            CERR("error in fixed format: scanning %s word %d \"%s\"",nam.c_str(),val.c_str());
            return(ERROR);
        }
        nam = "DELAY_CTPOUT_" + DELAY_SIGNAL_NAME[del];
        if((rtnv = readNextRecord(s,key,val)) != SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"",nam.c_str(),fn.c_str());
            return(rtnv);
        }
        if(key != nam) {
            CERR("error in fixed format: expected key \"%s\" got key \"%s\"",nam.c_str(),key.c_str());
            return(ERROR);
        }
        if(std::sscanf(val.c_str(),"%hu",&ctpout_delay[del]) != 1) {
            CERR("error in fixed format: scanning %s word %d \"%s\"",nam.c_str(),val.c_str());
            return(ERROR);
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::Configuration::readNextRecord(std::istream& s, std::string& key, std::string& val) {

    char os[STRING_LENGTH];
    std::string line;

    while(true) {
        if(!s.getline(os,STRING_LENGTH)) return (ERROR);
        line = os;
        if(!line.empty() && !(line.substr(0,1) == "#") && !(line.substr(0,2) == "//")) break;
    }

    std::string::size_type pos = line.find_first_of("=",0);
    key = line.substr(0,pos);
    key.erase(0,key.find_first_not_of(' '));
    key.erase(key.find_last_not_of(' ')+1);
    val = line.substr(pos+1);
    val.erase(0,val.find_first_not_of(' '));
    val.erase(val.find_last_not_of(' ')+1);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::Configuration::write(const std::string& fn) const {

    std::ofstream s(fn.c_str()); if(!s) {
        CERR("cannot open output file \"%s\"",fn.c_str());
        return(ERROR);
    }

    unsigned int lnk, sig, typ, del;
    std::string nam;
    std::ostringstream buf;
    char os[STRING_LENGTH];

    // link equalization
    for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) {
        nam = "ENABLE_EQUAL_" + EQUAL_LINK_NAME[lnk];
        std::sprintf(os,"%-20s= %s\n",nam.c_str(),equal_enable[lnk]?"ENABLED":"DISABLED"); s << os;
        nam = "CABLE_EQUAL_" + EQUAL_LINK_NAME[lnk];
        std::sprintf(os,"%-20s= %s\n",nam.c_str(),equal_short_cable[lnk]?"SHORT CABLE":"LONG CABLE"); s << os;
    }

    // signal selection
    for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) {
        nam = "SELECTION_" + SELECT_SIGNAL_NAME[sig];
        std::sprintf(os,"%-20s= %s\n",nam.c_str(),SIGNAL_SELECTION_NAME[signal_select[sig]].c_str()); s << os;
    }
    nam = "SELECTION_TTYP";
    std::sprintf(os,"%-20s= %s\n",nam.c_str(),TTYP_SELECTION_NAME[ttyp_select].c_str()); s << os;
    nam = "SELECTION_LOCTTYP";
    std::sprintf(os,"%-20s= %s\n",nam.c_str(),LOCTTYP_SELECTION_NAME[locttyp_select].c_str()); s << os;
    for(typ=0; typ<LOCTTYP_NUMBER; typ++) {
        buf.str(""); buf << "SELECTION_TTYP[" << typ << "]";
        std::sprintf(os,"%-20s= 0x%02x\n",buf.str().c_str(),locttyp_word[typ]); s << os;
    }
    nam = "SELECTION_CALREQ";
    std::sprintf(os,"%-20s= %s\n",nam.c_str(),CALREQ_SELECTION_NAME[calreq_select].c_str()); s << os;
    for(lnk=0; lnk<LINK_NUMBER; lnk++) {
        nam = "SELECTION_BUSY[" + LINK_NAME[lnk] + "]";
        std::sprintf(os,"SELECTION_BUSY[%s] = %s\n",LINK_NAME[lnk].c_str(),BUSY_SELECTION_NAME[busy_select[lnk]].c_str()); s << os;
    }

    // enable/delay selection
    for(del=0; del<DELAY_SIGNAL_NUMBER; del++) {
        nam = "ENABLE_CTPOUT_" + DELAY_SIGNAL_NAME[del];
        std::sprintf(os,"%-20s= %s\n",nam.c_str(),ctpout_enable[del]?"ENABLED":"DISABLED"); s << os;
        nam = "DELAY_CTPOUT_" + DELAY_SIGNAL_NAME[del];
        std::sprintf(os,"%-20s= %2d\n",nam.c_str(),ctpout_delay[del]); s << os;
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void LTPI::Configuration::setMode(const MODE_SELECTION sel) {

    unsigned int lnk, sig, del;

    switch(sel) {
    case MODE_DISABLED:
        for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) equal_enable[lnk] = false;
        for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) signal_select[sig] = SIGNAL_DISABLED;
        ttyp_select = TTYP_DISABLED;
        calreq_select = CALREQ_DISABLED;
        for(lnk=0; lnk<LINK_NUMBER; lnk++) busy_select[lnk] = BUSY_DISABLED;
        for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_enable[del] = false;
        break;

    case MODE_FROM_CTP:
        equal_enable[EQUAL_CTP] = true;
        equal_enable[EQUAL_LTP] = false;
        for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) signal_select[sig] = SIGNAL_FROM_CTP;
        ttyp_select = TTYP_FROM_CTP;
        calreq_select = CALREQ_FROM_CTP;
        for(lnk=0; lnk<LINK_NUMBER; lnk++) busy_select[lnk] = BUSY_FROM_CTP;
        for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_enable[del] = true;
        break;

    case MODE_FROM_LTP:
        equal_enable[EQUAL_CTP] = false;
        equal_enable[EQUAL_LTP] = true;
        for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) signal_select[sig] = SIGNAL_FROM_LTP;
        ttyp_select = TTYP_FROM_LTP;
        calreq_select = CALREQ_FROM_LTP;
        for(lnk=0; lnk<LINK_NUMBER; lnk++) busy_select[lnk] = BUSY_FROM_LTP;
        for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_enable[del] = true;
        break;

    case MODE_FROM_NIM:
        for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) equal_enable[lnk] = false;
        for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) signal_select[sig] = SIGNAL_FROM_NIM;
        ttyp_select = TTYP_FROM_LOCAL;
        locttyp_select = LOCTTYP_FROM_NIM_TTRG;
        calreq_select = CALREQ_DISABLED;
        for(lnk=0; lnk<LINK_NUMBER; lnk++) busy_select[lnk] = BUSY_DISABLED;
        for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_enable[del] = true;
        break;

    case MODE_PARALLEL:
        for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) equal_enable[lnk] = true;
        for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) signal_select[sig] = SIGNAL_PARALLEL;
        ttyp_select = TTYP_PARALLEL;
        calreq_select = CALREQ_PARALLEL;
        busy_select[CTP] = BUSY_FROM_CTP;
        busy_select[LTP] = BUSY_FROM_LTP;
        busy_select[NIM] = BUSY_FROM_CTP_OR_LTP;
        for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_enable[del] = true;
        break;

    case MODE_PARALLEL_FROM_NIM:
        equal_enable[EQUAL_CTP] = false;
        equal_enable[EQUAL_LTP] = true;
        for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) signal_select[sig] = SIGNAL_PARALLEL_FROM_NIM;
        ttyp_select = TTYP_PARALLEL_FROM_LOCAL;
        locttyp_select = LOCTTYP_FROM_NIM_TTRG;
        calreq_select = CALREQ_PARALLEL;
        busy_select[CTP] = BUSY_FROM_CTP;
        busy_select[LTP] = BUSY_FROM_LTP;
        busy_select[NIM] = BUSY_FROM_CTP_OR_LTP;
        for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_enable[del] = true;
        break;
    }
}

//------------------------------------------------------------------------------

void LTPI::Configuration::setSignalDelay(const DELAY_GROUP grp, const u_short val) {

    unsigned int del;

    for(del=DELAY_GROUP_LO[grp]; del<DELAY_GROUP_HI[grp]; del++) ctpout_delay[del] = val;
}


//------------------------------------------------------------------------------

void LTPI::Configuration::setSignalDelay(const u_short val) {

    unsigned int del;

    for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_delay[del] = val;
}

//------------------------------------------------------------------------------

void LTPI::Configuration::setSignalEnable(const DELAY_GROUP grp, const bool ena) {

    unsigned int del;

    for(del=DELAY_GROUP_LO[grp]; del<DELAY_GROUP_HI[grp]; del++) ctpout_enable[del] = ena;
}

//------------------------------------------------------------------------------

void LTPI::Configuration::setSignalEnable(const bool ena) {

    unsigned int del;

    for(del=0; del<DELAY_SIGNAL_NUMBER; del++) ctpout_enable[del] = ena;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

LTPI::LTPI(RCD::VME* vme, unsigned int base) : m_vme(vme), m_vmm(0) {

    // set base
    m_base = base + VMM_ADDR;

    // open VME master mapping
    m_vmm = m_vme->MasterMap(m_base,VMM_SIZE,VMM_AMOD,0);
    if((m_status = ((*m_vmm)()))) {
        CERR("LTPI: creating VME master mapping at addr = 0x%08x, size = 0x%08x, type = \"%s\"",m_base,VMM_SIZE,VMM_CODE.c_str());
        return;
    }
}

//------------------------------------------------------------------------------

LTPI::~LTPI() {

    // close VME master mapping
    if(m_vmm) m_vme->MasterUnmap(m_vmm);
}

//------------------------------------------------------------------------------

void LTPI::printSignalSelectionLegend(const PRINT_SELECTION sel, std::ostream& s) {

    bool flag(false);

    s << std::endl;
    s << "=======================================" << std::endl;
    s << "Legend for signal selection:" << std::endl;
    s << "=======================================" << std::endl;
    if((sel == PRINT_SIG) || (sel == PRINT_TTYP) || (sel == PRINT_ALL)) {
        s << "PARALLEL:                CTP -> CTP" << ((sel==PRINT_TTYP)?"":",NIM") << std::endl;
        s << "                         LTP -> LTP" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "FROM CTP:                CTP -> CTP" << ((sel==PRINT_TTYP)?"":",NIM") << std::endl;
        s << "                              \\ LTP" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "FROM LTP:                     / CTP" << ((sel==PRINT_TTYP)?"":",NIM") << std::endl;
        s << "                         LTP -> LTP" << std::endl;
        flag = true;
    }
    if((sel == PRINT_SIG) || (sel == PRINT_ALL)) {
        if(flag) s << "---------------------------------------" << std::endl;
        s << "FROM NIM:                NIM -> CTP,NIM" << std::endl;
        s << "                              \\ LTP" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "PARALLEL FROM NIM:       NIM -> CTP,NIM" << std::endl;
        s << "                         LTP -> LTP" << std::endl;
        flag = true;
    }
    if((sel == PRINT_TTYP) || (sel == PRINT_ALL)) {
        if(flag) s << "---------------------------------------" << std::endl;
        s << "FROM LOCAL:              LOC -> CTP" << std::endl;
        s << "                              \\ LTP" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "PARALLEL FROM LOCAL:     LOC -> CTP" << std::endl;
        s << "                         LTP -> LTP" << std::endl;
        flag = true;
    }
    if((sel == PRINT_CALREQ) || (sel == PRINT_ALL)) {
        if(flag) s << "---------------------------------------" << std::endl;
        s << "CALREQ - PARALLEL:       CTP <- CTP" << std::endl;
        s << "                         LTP <- LTP" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "CALREQ - FROM CTP:       CTP <- CTP" << std::endl;
        s << "                         LTP /" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "CALREQ - FROM LTP:       CTP \\" << std::endl;
        s << "                         LTP <- LTP" << std::endl;
        flag = true;
    }
    if((sel == PRINT_BUSY) || (sel == PRINT_ALL)) {
        if(flag) s << "---------------------------------------" << std::endl;
        s << "BUSY - FROM CTP:         LNK <- CTP" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "BUSY - FROM LTP:         LNK <- LTP" << std::endl;
        s << "---------------------------------------" << std::endl;
        s << "BUSY - FROM CTP OR LTP:  LNK <- CTP" << std::endl;
        s << "                             \\  LTP" << std::endl;
        flag = true;
    }
    if(flag) s << "=======================================" << std::endl;
    s << std::endl;
}

//------------------------------------------------------------------------------

int LTPI::reset() {

    if((m_status = m_vmm->WriteSafe(SWRST_ADDR,SWRST_DATA)) != SUCCESS) {
        CERR("writing to reset register at 0x%02x",SWRST_ADDR);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::ready(bool& ready) {

    if((m_status = readyI2C(ready)) != SUCCESS) {
        CERR("initializing I2C","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::init() {

    if((m_status = initI2C()) != SUCCESS) {
        CERR("initializing I2C","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::read(Configuration& cfg) {

    // link equalization
    if((m_status = readLinkEqualization(cfg)) != SUCCESS) {
        CERR("reading link equalization configuration","");
        return(m_status);
    }

    // signal selection
    if((m_status = readSignalSelection(cfg)) != SUCCESS) {
        CERR("reading signal selection configuration","");
        return(m_status);
    }

    // signal delay
    if((m_status = readSignalDelay(cfg)) != SUCCESS) {
        CERR("reading signal delay configuration","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readLinkEqualization(Configuration& cfg) {

    unsigned int lnk;

    // link equalization
    for(lnk=0; lnk<LTPI::EQUAL_LINK_NUMBER; lnk++) {
        if((m_status = readLinkEqualization(static_cast<EQUAL_LINK>(lnk),cfg.equal_enable[lnk],cfg.equal_short_cable[lnk])) != SUCCESS) {
            CERR("reading link equalization of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readSignalSelection(Configuration& cfg) {

    unsigned int sig, typ, lnk;

    // signal selection
    for(sig=0; sig<LTPI::SELECT_SIGNAL_NUMBER; sig++) {
        if((m_status = readSignalSelection(static_cast<SELECT_SIGNAL>(sig),cfg.signal_select[sig])) != SUCCESS) {
            CERR("reading signal selection of signal \"%s\"",SELECT_SIGNAL_NAME[sig].c_str());
            return(m_status);
        }
    }
    if((m_status = readTriggerTypeSelection(cfg.ttyp_select,cfg.locttyp_select)) != SUCCESS) {
        CERR("reading signal selection of signal \"TTYP\"","");
        return(m_status);
    }
    for(typ=0; typ<LTPI::LOCTTYP_NUMBER; typ++) {
        if((m_status = readLocalTriggerTypeWord(typ,cfg.locttyp_word[typ])) != SUCCESS) {
            CERR("reading local trigger type word %d",typ);
            return(m_status);
        }
    }
    if((m_status = readCalibrationRequestSelection(cfg.calreq_select)) != SUCCESS) {
        CERR("reading signal selection of signal \"CALREQ\"","");
        return(m_status);
    }
    for(lnk=0; lnk<LTPI::LINK_NUMBER; lnk++) {
        if((m_status = readBusySelection(static_cast<LINK>(lnk),cfg.busy_select[lnk])) != SUCCESS) {
            CERR("reading busy selection of link \"%s\"",LINK_NAME[lnk].c_str());
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readSignalDelay(Configuration& cfg) {

    unsigned int del;

    // signal delay
    for(del=0; del<LTPI::DELAY_SIGNAL_NUMBER; del++) {
        if((m_status = readSignalDelay(static_cast<DELAY_SIGNAL>(del),cfg.ctpout_delay[del],cfg.ctpout_enable[del])) != SUCCESS) {
            CERR("reading signal delay of signal \"%s\"",DELAY_SIGNAL_NAME[del].c_str());
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::write(const Configuration& cfg) {

    // link equalization
    if((m_status = writeLinkEqualization(cfg)) != SUCCESS) {
        CERR("writing link equalization configuration","");
        return(m_status);
    }

    // signal selection
    if((m_status = writeSignalSelection(cfg)) != SUCCESS) {
        CERR("writing signal selection configuration","");
        return(m_status);
    }

    // signal delay
    if((m_status = writeSignalDelay(cfg)) != SUCCESS) {
        CERR("writing signal delay configuration","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeLinkEqualization(const Configuration& cfg) {

    unsigned int lnk;

    // link equalization
    for(lnk=0; lnk<LTPI::EQUAL_LINK_NUMBER; lnk++) {
        if((m_status = writeLinkEqualization(static_cast<EQUAL_LINK>(lnk),cfg.equal_enable[lnk],cfg.equal_short_cable[lnk])) != SUCCESS) {
            CERR("writing link equalization of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalSelection(const Configuration& cfg) {

    unsigned int sig, typ, lnk;

    // signal selection
    for(sig=0; sig<LTPI::SELECT_SIGNAL_NUMBER; sig++) {
        if((m_status = writeSignalSelection(static_cast<SELECT_SIGNAL>(sig),cfg.signal_select[sig])) != SUCCESS) {
            CERR("writing signal selection of signal \"%s\"",SELECT_SIGNAL_NAME[sig].c_str());
            return(m_status);
        }
    }
    if((m_status = writeTriggerTypeSelection(cfg.ttyp_select,cfg.locttyp_select)) != SUCCESS) {
        CERR("writing signal selection of signal \"TTYP\"","");
        return(m_status);
    }
    for(typ=0; typ<LTPI::LOCTTYP_NUMBER; typ++) {
        if((m_status = writeLocalTriggerTypeWord(typ,cfg.locttyp_word[typ])) != SUCCESS) {
            CERR("writing local trigger type word %d",typ);
            return(m_status);
        }
    }
    if((m_status = writeCalibrationRequestSelection(cfg.calreq_select)) != SUCCESS) {
        CERR("writing signal selection of signal \"CALREQ\"","");
        return(m_status);
    }
    for(lnk=0; lnk<LTPI::LINK_NUMBER; lnk++) {
        if((m_status = writeBusySelection(static_cast<LINK>(lnk),cfg.busy_select[lnk])) != SUCCESS) {
            CERR("writing busy selection of link \"%s\"",LINK_NAME[lnk].c_str());
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalDelay(const Configuration& cfg) {

    unsigned int del;

    // signal delay
    for(del=0; del<LTPI::DELAY_SIGNAL_NUMBER; del++) {
        if((m_status = writeSignalDelay(static_cast<DELAY_SIGNAL>(del),cfg.ctpout_delay[del],cfg.ctpout_enable[del])) != SUCCESS) {
            CERR("writing signal delay of signal \"%s\"",DELAY_SIGNAL_NAME[del].c_str());
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPI::readSignalSelection(const SELECT_SIGNAL sig, SIGNAL_SELECTION& sel) {

    u_short data;

    if((m_status = readBitField(SELECT_ADDR[sig],SELECT_MASK[sig],SELECT_SHFT[sig],data)) != SUCCESS) {
        CERR("reading selection of signal \"%s\"",SELECT_SIGNAL_NAME[sig].c_str());
        return(m_status);
    }

    u_short isel;
    for(isel=0; isel<SIGNAL_SELECTION_NUMBER; isel++) {
        if(data == SELECT_DATA[isel]) {
            sel = static_cast<SIGNAL_SELECTION>(isel);
            return(SUCCESS);
        }
    }

    return(ERROR);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalSelection(const SELECT_SIGNAL sig, const SIGNAL_SELECTION sel) {

    if((m_status = writeBitField(SELECT_ADDR[sig],SELECT_MASK[sig],SELECT_SHFT[sig],SELECT_DATA[sel])) != SUCCESS) {
        CERR("writing selection of signal \"%s\"",SELECT_SIGNAL_NAME[sig].c_str());
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalSelection(const SIGNAL_SELECTION sel) {

    unsigned int sig;

    for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) {
        if((m_status = writeSignalSelection(static_cast<SELECT_SIGNAL>(sig),sel)) != SUCCESS) return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readTriggerTypeSelection(TTYP_SELECTION& typ, LOCTTYP_SELECTION& loc) {

    u_short data;

    if((m_status = readBitField(CSR2_ADDR,CSR2_MASK_TTYP,CSR2_SHFT_TTYP,data)) != SUCCESS) {
        CERR("reading selection of signal \"TTYP\"","");
        return(m_status);
    }

    u_short ityp;
    bool flag(false);
    for(ityp=0; ityp<TTYP_SELECTION_NUMBER; ityp++) {
        if(data == TTYP_DATA[ityp]) {
            typ = static_cast<TTYP_SELECTION>(ityp);
            flag = true;
            break;
        }
    }
    if(!flag) return(ERROR);

    if((m_status = readBitField(CSR2_ADDR,CSR2_MASK_LOCTTYP,CSR2_SHFT_LOCTTYP,data)) != SUCCESS) {
        CERR("reading selection of local TTYP word","");
        return(m_status);
    }

    u_short iloc;
    for(iloc=0; iloc<LOCTTYP_SELECTION_NUMBER; iloc++) {
        if(data == LOCTTYP_DATA[iloc]) {
            loc = static_cast<LOCTTYP_SELECTION>(iloc);
            return(SUCCESS);
        }
    }

    return(ERROR);
}

//------------------------------------------------------------------------------

int LTPI::writeTriggerTypeSelection(const TTYP_SELECTION typ, const LOCTTYP_SELECTION loc) {

    if((m_status = writeBitField(CSR2_ADDR,CSR2_MASK_LOCTTYP,CSR2_SHFT_LOCTTYP,LOCTTYP_DATA[loc])) != SUCCESS) {
        CERR("writing selection of local TTYP word","");
        return(m_status);
    }

    if((m_status = writeBitField(CSR2_ADDR,CSR2_MASK_TTYP,CSR2_SHFT_TTYP,TTYP_DATA[typ])) != SUCCESS) {
        CERR("writing selection of signal \"TTYP\"","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readLocalTriggerTypeWord(const unsigned int idx, u_short& word) {

    if(idx >= LOCTTYP_NUMBER) {
        CERR("local TTYP index %d out of allowed range ([0..%d])",idx,LOCTTYP_NUMBER-1);
        return(ERROR_INVALID);
    }

    u_short addr = LOCTTYP1_ADDR + (idx/LOCTTYP_NMOD)*sizeof(u_short);
    u_short data;

    if((m_status = m_vmm->ReadSafe(addr,&data)) != SUCCESS) {
        CERR("reading local TTYP index %d at addr 0x%08x",idx,addr);
        return(m_status);
    }
    word = (data >> ((idx%LOCTTYP_NMOD)*LOCTTYP_SHFT)) & LOCTTYP_MASK;

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeLocalTriggerTypeWord(const unsigned int idx, const u_short word) {

    if(idx >= LOCTTYP_NUMBER) {
        CERR("local TTYP index %d out of allowed range ([0..%d])",idx,LOCTTYP_NUMBER-1);
        return(ERROR_INVALID);
    }

    u_short addr = LOCTTYP1_ADDR + (idx/LOCTTYP_NMOD)*sizeof(u_short);
    u_short data, shft;

    // READ
    if((m_status = m_vmm->ReadSafe(addr,&data)) != SUCCESS) {
        CERR("reading local TTYP index %d at addr 0x%08x",idx,addr);
        return(m_status);
    }

    // MODIFY
    shft = ((idx%LOCTTYP_NMOD)*LOCTTYP_SHFT);
    data = (data & (~(LOCTTYP_MASK << shft))) | ((word & LOCTTYP_MASK) << shft);

    // WRITE
    if((m_status = m_vmm->WriteSafe(addr,data)) != SUCCESS) {
        CERR("writing local TTYP index %d at addr 0x%08x",idx,addr);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readCalibrationRequestSelection(CALREQ_SELECTION& req) {

    u_short data;

    if((m_status = readBitField(CSR2_ADDR,CSR2_MASK_CALREQ,CSR2_SHFT_CALREQ,data)) != SUCCESS) {
        CERR("reading selection of signal \"CALREQ\"","");
        return(m_status);
    }

    u_short ireq;
    for(ireq=0; ireq<CALREQ_SELECTION_NUMBER; ireq++) {
        if(data == CALREQ_DATA[ireq]) {
            req = static_cast<CALREQ_SELECTION>(ireq);
            return(SUCCESS);
        }
    }

    return(ERROR);
}

//------------------------------------------------------------------------------

int LTPI::writeCalibrationRequestSelection(const CALREQ_SELECTION req) {

    if((m_status = writeBitField(CSR2_ADDR,CSR2_MASK_CALREQ,CSR2_SHFT_CALREQ,CALREQ_DATA[req])) != SUCCESS) {
        CERR("writing selection of signal \"CALREQ\"","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readBusySelection(const LINK lnk, BUSY_SELECTION& bsy) {

    u_short data;

    if((m_status = readBitField(BUSY_ADDR[lnk],BUSY_MASK[lnk],BUSY_SHFT[lnk],data)) != SUCCESS) {
        CERR("reading selection of signal \"BUSY\" of link \"%s\"",LINK_NAME[lnk].c_str());
        return(m_status);
    }

    u_short ibsy;
    for(ibsy=0; ibsy<BUSY_SELECTION_NUMBER; ibsy++) {
        if(data == BUSY_DATA[ibsy]) {
            bsy = static_cast<BUSY_SELECTION>(ibsy);
            return(SUCCESS);
        }
    }

    return(ERROR);
}

//------------------------------------------------------------------------------

int LTPI::writeBusySelection(const LINK lnk, const BUSY_SELECTION bsy) {

    if((m_status = writeBitField(BUSY_ADDR[lnk],BUSY_MASK[lnk],BUSY_SHFT[lnk],BUSY_DATA[bsy])) != SUCCESS) {
        CERR("writing selection of signal \"BUSY\" of link \"%s\"",LINK_NAME[lnk].c_str());
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::printSignalSelection(std::ostream& s) {

    unsigned int sig, idx;
    LTPI::SIGNAL_SELECTION sel;
    std::string nam, nbm, ncm;
    u_short word;
    char os[STRING_LENGTH];

    // print header
    s << "----------------------------------------------------------------------------" << std::endl;
    s << "SIGNAL SELECTION:" << std::endl;
    s << "----------------------------------------------------------------------------" << std::endl;

    // BC/ORB/L1A/TTRG/BGO
    for(sig=0; sig<SELECT_SIGNAL_NUMBER; sig++) {
        if((m_status = readSignalSelection(static_cast<SELECT_SIGNAL>(sig),sel)) != SUCCESS) {
            CERR("reading selection of signal \"%s\"",SELECT_SIGNAL_NAME[sig].c_str());
            return(m_status);
        }
        nam = "\"" + SELECT_SIGNAL_NAME[sig] + "\"";
        nbm = "\"" + SIGNAL_SELECTION_NAME[sel] + "\"";
        ncm = (!SIGNAL_SELECTION_DESC[sel].empty()) ? ("  (" + SIGNAL_SELECTION_DESC[sel] + ")") : "";
        std::sprintf(os,"Signal %-6s     : %-21s%s\n",nam.c_str(),nbm.c_str(),ncm.c_str()); s << os;
    }

    TTYP_SELECTION typ;
    LOCTTYP_SELECTION loc;

    // TTYP and LOCTTYP
    if((m_status = readTriggerTypeSelection(typ,loc)) != SUCCESS) {
        CERR("reading selection of signal \"TTYP\"","");
        return(m_status);
    }
    nam = "\"" + TTYP_SELECTION_NAME[typ] + "\"";
    nbm = (!TTYP_SELECTION_DESC[typ].empty()) ? ("  (" + TTYP_SELECTION_DESC[typ] + ")") : "";
    std::sprintf(os,"Signal \"TTYP\"     : %-21s%s\n",nam.c_str(),nbm.c_str()); s << os;

    nam = "\"" + LOCTTYP_SELECTION_NAME[loc] + "\"";
    nbm = ((typ==TTYP_FROM_LOCAL)||(typ==TTYP_PARALLEL_FROM_LOCAL)) ? "" : "  [NOT USED]";
    std::sprintf(os,"\"LOCAL TTYP\"      : %-21s%s\n",nam.c_str(),nbm.c_str()); s << os;
    for(idx=0; idx<LOCTTYP_NUMBER; idx++) {
        if((m_status = readLocalTriggerTypeWord(idx,word)) != SUCCESS) {
            CERR("reading local TTYP word index %d",idx);
            return(m_status);
        }
        std::sprintf(os,"\"LOCAL TTYP[%d]\"   : 0x%02x                 %s\n",idx,word,nbm.c_str()); s<< os;
    }

    CALREQ_SELECTION req;

    // CALREQ
    if((m_status = readCalibrationRequestSelection(req)) != SUCCESS) {
        CERR("reading selection of signal \"CALREQ\"","");
        return(m_status);
    }
    nam = "\"" + CALREQ_SELECTION_NAME[req] + "\"";
    nbm = (!CALREQ_SELECTION_DESC[req].empty()) ? ("  (" + CALREQ_SELECTION_DESC[req] + ")") : "";
    std::sprintf(os,"Signal \"CALREQ\"   : %-21s%s\n",nam.c_str(),nbm.c_str()); s << os;

    unsigned int lnk;
    BUSY_SELECTION bsy;

    // BUSY
    for(lnk=0; lnk<LINK_NUMBER; lnk++) {
        if((m_status = readBusySelection(static_cast<LINK>(lnk),bsy)) != SUCCESS) {
            CERR("reading selection of signal \"BUSY\" of link \"%s\"",LINK_NAME[lnk].c_str());
            return(m_status);
        }
        nam = "\"" + BUSY_SELECTION_NAME[bsy] + "\"";
        nbm = (!BUSY_SELECTION_DESC[bsy].empty()) ? ("  (" + LINK_NAME[lnk] + " " + BUSY_SELECTION_DESC[bsy] + ")") : "";
        std::sprintf(os,"Signal \"BUSY[%s]\": %-21s%s\n",LINK_NAME[lnk].c_str(),nam.c_str(),nbm.c_str()); s << os;
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readSignalStatus(const STATUS_SIGNAL sig, const LINK lnk, bool& act) {

    if((!STATUS_ADDR[lnk][sig]) || (!STATUS_MASK[lnk][sig])) return(ERROR_NOTEXIST);

    if((m_status = readBitMask(STATUS_ADDR[lnk][sig],STATUS_MASK[lnk][sig],STATUS_INVT[lnk][sig],act)) != SUCCESS) {
        CERR("reading selection of signal \"%s\"",SELECT_SIGNAL_NAME[sig].c_str());
        return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::readSignalDelay(const DELAY_SIGNAL sig, u_short& del, bool& ena) {

    u_short data;

    if((m_status = readI2CDEL(DELAY_ADDR[sig],data)) != SUCCESS) {
        CERR("reading delay of signal \"%s\"",DELAY_SIGNAL_NAME[sig].c_str());
        return(m_status);
    }

    del = data & DELAY_MASK_VAL;
    ena = (data & DELAY_MASK_ENA);
    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalDelay(const DELAY_SIGNAL sig, const u_short del, const bool ena) {

    u_short data;

    data = del & DELAY_MASK_VAL;
    data = (ena) ? (data | DELAY_MASK_ENA) : (data & (~DELAY_MASK_ENA));
    if((m_status = writeI2CDEL(DELAY_ADDR[sig],data)) != SUCCESS) {
        CERR("writing delay of signal \"%s\"",DELAY_SIGNAL_NAME[sig].c_str());
        return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalDelay(const DELAY_GROUP grp, const u_short del, const bool ena) {

    unsigned int sig;

    for(sig=DELAY_GROUP_LO[grp]; sig<DELAY_GROUP_HI[grp]; sig++) {
        if((m_status = writeSignalDelay(static_cast<DELAY_SIGNAL>(sig),del,ena)) != SUCCESS) return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalDelay(const u_short del, const bool ena) {

    unsigned int sig;

    for(sig=0; sig<DELAY_SIGNAL_NUMBER; sig++) {
        if((m_status = writeSignalDelay(static_cast<DELAY_SIGNAL>(sig),del,ena)) != SUCCESS) return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalEnable(const DELAY_SIGNAL sig, const bool ena) {

    u_short data;

    // READ
    if((m_status = readI2CDEL(DELAY_ADDR[sig],data)) != SUCCESS) {
        CERR("reading delay of signal \"%s\"",DELAY_SIGNAL_NAME[sig].c_str());
        return(m_status);
    }

    // MODIFY
    data = (ena) ? (data | DELAY_MASK_ENA) : (data & (~DELAY_MASK_ENA));

    // WRITE
    if((m_status = writeI2CDEL(DELAY_ADDR[sig],data)) != SUCCESS) {
        CERR("writing selection of signal \"%s\"",DELAY_SIGNAL_NAME[sig].c_str());
        return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalEnable(const DELAY_GROUP grp, const bool ena) {

    unsigned int sig;

    for(sig=DELAY_GROUP_LO[grp]; sig<DELAY_GROUP_HI[grp]; sig++) {
        if((m_status = writeSignalEnable(static_cast<DELAY_SIGNAL>(sig),ena)) != SUCCESS) return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::writeSignalEnable(const bool ena) {

    unsigned int sig;

    for(sig=0; sig<DELAY_SIGNAL_NUMBER; sig++) {
        if((m_status = writeSignalEnable(static_cast<DELAY_SIGNAL>(sig),ena)) != SUCCESS) return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::printSignalDelay(std::ostream& s) {

    unsigned int sig;
    u_short del;
    bool ena;
    std::string nam, nbm;
    double stp;
    char os[STRING_LENGTH];

    // print header
    s << "----------------------------------------------------------------------------" << std::endl;
    s << "SIGNAL DELAY/ENABLE SETTING [CTPout]:" << std::endl;
    s << "----------------------------------------------------------------------------" << std::endl;

    for(sig=0; sig<DELAY_SIGNAL_NUMBER; sig++) {
        if((m_status = readSignalDelay(static_cast<DELAY_SIGNAL>(sig),del,ena)) != SUCCESS) {
             CERR("reading delay of signal \"%s\"",DELAY_SIGNAL_NAME[sig].c_str());
             return(m_status);
        }
        nam = "\"" + DELAY_SIGNAL_NAME[sig] + "\"";
        nbm = ena?"\"ENABLED\" ":"\"DISABLED\"";
        stp = static_cast<double>(del)*DELAY_STEP;
        std::sprintf(os,"Signal %-8s   : %s - %2d (%4.1f nsec)\n",nam.c_str(),nbm.c_str(),del,stp); s << os;
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readLinkEqualization(const EQUAL_LINK lnk, u_short& gain, u_short& ctrl) {

    // GAIN
    if((m_status = readI2CDAC(EQUAL_GAIN_ADDR[lnk],gain)) != SUCCESS) {
        CERR("reading equalization gain of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
        return(m_status);
    }

    // CTRL
    if((m_status = readI2CDAC(EQUAL_CTRL_ADDR[lnk],ctrl)) != SUCCESS) {
        CERR("reading equalization ctrl of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
        return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::writeLinkEqualization(const EQUAL_LINK lnk, const u_short gain, const u_short ctrl) {

    // GAIN
    if((m_status = writeI2CDAC(EQUAL_GAIN_ADDR[lnk],gain)) != SUCCESS) {
        CERR("writing equalization gain of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
        return(m_status);
    }

    // CTRL
    if((m_status = writeI2CDAC(EQUAL_CTRL_ADDR[lnk],ctrl)) != SUCCESS) {
        CERR("writing equalization ctrl of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
        return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::readLinkEqualization(const EQUAL_LINK lnk, bool& ena, bool& sho) {

    u_short gain, ctrl;

    if((m_status = readLinkEqualization(lnk,gain,ctrl)) != SUCCESS) {
        CERR("reading equalization of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
        return(m_status);
    }

    unsigned int isel;
    for(isel=0; isel<EQUAL_SELECTION_NUMBER; isel++) {
        if((gain == EQUAL_GAIN_DATA[isel]) && (ctrl == EQUAL_CTRL_DATA[isel])) {
            ena = !(static_cast<EQUAL_SELECTION>(isel) == EQUAL_DISABLED);
            if(ena) sho = (static_cast<EQUAL_SELECTION>(isel) == EQUAL_SHORT_CABLE);
            return(SUCCESS);
        }
    }

    return(ERROR_INVALID);
}

//------------------------------------------------------------------------------

int LTPI::writeLinkEqualization(const EQUAL_LINK lnk, const bool ena, const bool sho) {

    u_short gain, ctrl;

    gain = EQUAL_GAIN_DATA[ena ? (sho?EQUAL_SHORT_CABLE:EQUAL_LONG_CABLE) : EQUAL_DISABLED];
    ctrl = EQUAL_CTRL_DATA[ena ? (sho?EQUAL_SHORT_CABLE:EQUAL_LONG_CABLE) : EQUAL_DISABLED];
    if((m_status = writeLinkEqualization(lnk,gain,ctrl)) != SUCCESS) {
        CERR("writing equalization of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeLinkEqualization(const EQUAL_LINK lnk, const bool ena, const double lgt) {

    bool sho = (lgt < EQUAL_LENGTH);
    if((m_status = writeLinkEqualization(lnk,ena,sho)) != SUCCESS) {
        CERR("writing equalization gain of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::printLinkEqualization(std::ostream& s) {

    unsigned int lnk;
    u_short gain, ctrl;
    bool ena, sho;
    std::string nam, nbm;
    char os[STRING_LENGTH];

    // print header
    s << "----------------------------------------------------------------------------" << std::endl;
    s << "LINK EQUALIZATION [CTPin,LTPin]:" << std::endl;
    s << "----------------------------------------------------------------------------" << std::endl;

    // LINK EQUALIZATION
    for(lnk=0; lnk<EQUAL_LINK_NUMBER; lnk++) {
        if((m_status = readLinkEqualization(static_cast<EQUAL_LINK>(lnk),gain,ctrl)) != SUCCESS) {
            CERR("reading equalization of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
            return(m_status);
        }
        if((m_status = readLinkEqualization(static_cast<EQUAL_LINK>(lnk),ena,sho)) != SUCCESS) {
            if(m_status == ERROR_INVALID) {
                std::sprintf(os,"Link \"%s\"   : gain = 0x%02x, ctrl = 0x%02x => <INVALID VALUES>\n",EQUAL_LINK_NAME[lnk].c_str(),gain,ctrl); s << os;
                m_status = SUCCESS;
                continue;
            }
            CERR("reading equalization of link \"%s\"",EQUAL_LINK_NAME[lnk].c_str());
            return(m_status);
        }
        nam = ena?"\"ENABLED\" ":"\"DISABLED\"";
        nbm = ena ? (" - \""+(sho?EQUAL_SELECTION_NAME[EQUAL_SHORT_CABLE]:EQUAL_SELECTION_NAME[EQUAL_LONG_CABLE]) + "\"") : "";
        std::sprintf(os,"Link \"%s\"      : gain = 0x%02x, ctrl = 0x%02x => %s%s\n",EQUAL_LINK_NAME[lnk].c_str(),gain,ctrl,nam.c_str(),nbm.c_str()); s << os;
    }

    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPI::readBitField(const u_short addr, const u_short mask, const u_short shft, u_short& data) {

    u_short dats;

    if((m_status = m_vmm->ReadSafe(addr,&dats)) != SUCCESS) {
        CERR("reading from register 0x%08x",addr);
        return(m_status);
    }

    data = (dats & mask) >> shft;
    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeBitField(const u_short addr, const u_short mask, const u_short shft, const u_short data) {

    u_short dats;

    // READ
    if((m_status = m_vmm->ReadSafe(addr,&dats)) != SUCCESS) {
        CERR("reading from register 0x%08x",addr);
        return(m_status);
    }

    // MODIFY
    dats = (dats & (~mask)) | ((data << shft) & mask);

    // WRITE
    if((m_status = m_vmm->WriteSafe(addr,dats)) != SUCCESS) {
        CERR("writing to register 0x%08x",addr);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readBitMask(const u_short addr, const u_short mask, const bool invt, bool& data) {

    u_short dats;

    if((m_status = m_vmm->ReadSafe(addr,&dats)) != SUCCESS) {
        CERR("reading from register 0x%08x",addr);
        return(m_status);
    }

    data = invt ? (!(dats & mask)) : (dats & mask);
    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPI::resetI2C() {

    if((m_status = m_vmm->WriteSafe(I2C_RST_ADDR,static_cast<u_short>(0))) != SUCCESS) {
        CERR("reseting to I2C register 0x%02x",I2C_RST_ADDR);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::initI2C() {

    u_short idat, iadd;

    // WRITE TO CTR: disable I2C, disable interrupts
    idat = 0;
    iadd = I2C_CTR_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // write default frequency
    if((m_status = writeI2CFrequency(I2C_DEFAULT_FREQUENCY)) != SUCCESS) {
        CERR("writing I2C frequency","");
        return(m_status);
    }

    // read default frequency
    double freq;
    if((m_status = readI2CFrequency(freq)) != SUCCESS) {
        CERR("reading I2C frequency","");
        return(m_status);
    }
    if(freq != I2C_DEFAULT_FREQUENCY) {
        CERR("I2C frequency (%8.2f kHz) not equal to default frequency (%f8.2f kHz)",freq,I2C_DEFAULT_FREQUENCY);
        return(ERROR);
    }
    COUT("I2C frequency set to default frequency (%8.2f kHz)",I2C_DEFAULT_FREQUENCY);

    // WRITE TO CTR: enable I2C
    idat = I2C_CTR_MASK_EN;
    iadd = I2C_CTR_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }
    COUT("I2C enabled","");

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readyI2C(bool& ready) {

    double freq;

    ready = false;

    if((m_status = readI2CFrequency(freq)) != SUCCESS) {
        CERR("reading I2C frequency","");
        return(m_status);
    }
    if(freq != I2C_DEFAULT_FREQUENCY) {
        COUT("I2C frequency %8.2f kHz not equal to default frequency (%8.2f kHz)",freq,I2C_DEFAULT_FREQUENCY);
        return(SUCCESS);
    }

    u_short iadd, idat;

    iadd = I2C_CTR_ADDR;
    if((m_status = m_vmm->ReadSafe(iadd,&idat)) != SUCCESS) {
        CERR("reading from I2C register 0x%02x",iadd);
        return(m_status);
    }
    if(!(idat & I2C_CTR_MASK_EN)) {
        COUT("I2C not enabled","");
        return(SUCCESS);
    }

    ready = true;
    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::readI2CFrequency(double& freq) {

    u_short dtlo, dthi, data;

    if((m_status = m_vmm->ReadSafe(I2C_PLO_ADDR,&dtlo)) != SUCCESS) {
        CERR("reading from I2C register 0x%02x",I2C_PLO_ADDR);
        return(m_status);
    }

    if((m_status = m_vmm->ReadSafe(I2C_PHI_ADDR,&dthi)) != SUCCESS) {
        CERR("reading from I2C register 0x%02x",I2C_PHI_ADDR);
        return(m_status);
    }

    data = ((dthi&I2C_PRE_MASK) << I2C_PRE_SHFT | (dtlo & I2C_PRE_MASK)) + 1;
    freq = I2C_PRE_FACT / static_cast<double>(data);

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeI2CFrequency(const double freq) {

    u_short dtlo, dthi, data;

    data = static_cast<u_short>(static_cast<double>(I2C_PRE_FACT) / freq) - 1;
    dtlo = data & I2C_PRE_MASK;
    dthi = (data >> I2C_PRE_SHFT) & I2C_PRE_MASK;

    if((m_status = m_vmm->WriteSafe(I2C_PLO_ADDR,dtlo)) != SUCCESS) {
        CERR("reading from I2C register 0x%02x",I2C_PLO_ADDR);
        return(m_status);
    }

    if((m_status = m_vmm->WriteSafe(I2C_PHI_ADDR,dthi)) != SUCCESS) {
        CERR("reading from I2C register 0x%02x",I2C_PHI_ADDR);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPI::readBaseAddress(u_short& addr) {

    u_short idat;

    // read base address low byte
    if((m_status = readFlash(SFL_ADDR_MASK_BASE_LO,idat)) != SUCCESS) {
        CERR("reading base address low byte from serial flash address 0x%02x",SFL_ADDR_MASK_BASE_LO);
        return(m_status);
    }
    addr = idat;

    // read base address high byte
    if((m_status = readFlash(SFL_ADDR_MASK_BASE_HI,idat)) != SUCCESS) {
        CERR("reading base address high byte from serial flash address 0x%02x",SFL_ADDR_MASK_BASE_HI);
        return(m_status);
    }
    addr |= (idat << SFL_ADDR_SHFT_BASE);

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeBaseAddress(const u_short addr) {

    u_short iadd, idat;

    // read base address
    if((m_status = readBaseAddress(iadd)) != SUCCESS) {
        CERR("reading base address from serial flash","");
        return(m_status);
    }
    if(iadd == addr) return(m_status);

    // erase serial flash
    if((m_status = eraseFlash()) != SUCCESS) {
        CERR("erasing serial flash","");
        return(m_status);
    }

    // read base address low byte
    idat = addr;
    if((m_status = writeFlash(SFL_ADDR_MASK_BASE_LO,idat)) != SUCCESS) {
        CERR("writing base address low byte from serial flash address 0x%02x",SFL_ADDR_MASK_BASE_LO);
        return(m_status);
    }

    // read base address high byte
    idat = addr >> SFL_ADDR_SHFT_BASE;
    if((m_status = writeFlash(SFL_ADDR_MASK_BASE_HI,idat)) != SUCCESS) {
        CERR("writing base address high byte from serial flash address 0x%02x",SFL_ADDR_MASK_BASE_HI);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPI::readEEPROM(unsigned int& manuf, unsigned int& board, unsigned int& revision) {

    u_short idat;
    unsigned int i;

    // MANUFACTURER ID
    manuf = 0;
    for(i=0; i<EEPROM_MAN_NUMBER; i++) {
        if((m_status = m_vmm->ReadSafe(EEPROM_MAN_ADDR[i],&idat)) != SUCCESS) {
            CERR("reading manufacturer ID from EEPROM at addr 0x%02x",EEPROM_MAN_ADDR[i]);
            return(m_status);
        }
        manuf = (manuf << EEPROM_SHFT) | (idat & EEPROM_MASK);
    }

    // BOARD ID
    board = 0;
    for(i=0; i<EEPROM_BRD_NUMBER; i++) {
        if((m_status = m_vmm->ReadSafe(EEPROM_BRD_ADDR[i],&idat)) != SUCCESS) {
            CERR("reading board ID from EEPROM at addr 0x%02x",EEPROM_BRD_ADDR[i]);
            return(m_status);
        }
        board = (board << EEPROM_SHFT) | (idat & EEPROM_MASK);
    }

    // REVISION number
    revision = 0;
    for(i=0; i<EEPROM_REV_NUMBER; i++) {
        if((m_status = m_vmm->ReadSafe(EEPROM_REV_ADDR[i],&idat)) != SUCCESS) {
            CERR("reading revision number from EEPROM at addr 0x%02x",EEPROM_REV_ADDR[i]);
            return(m_status);
        }
        revision = (revision << EEPROM_SHFT) | (idat & EEPROM_MASK);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeEEPROM(const unsigned int manuf, const unsigned int board, const unsigned int revision) {

    u_short idat;
    unsigned int data, i;

    // MANUFACTURER ID
    data = manuf;
    for(i=EEPROM_MAN_NUMBER; i!=0; i--) {
        idat = data & EEPROM_MASK;
        if((m_status = m_vmm->WriteSafe(EEPROM_MAN_ADDR[i-1],idat)) != SUCCESS) {
            CERR("writing manufacturer ID to EEPROM at addr 0x%02x",EEPROM_MAN_ADDR[i]);
            return(m_status);
        }
        data = (data >> EEPROM_SHFT);
    }

    // BOARD ID
    data = board;
    for(i=EEPROM_BRD_NUMBER; i!=0; i--) {
        idat = data & EEPROM_MASK;
        if((m_status = m_vmm->WriteSafe(EEPROM_BRD_ADDR[i-1],idat)) != SUCCESS) {
            CERR("writing board ID to EEPROM at addr 0x%02x",EEPROM_BRD_ADDR[i]);
            return(m_status);
        }
        data = (data >> EEPROM_SHFT);
    }

    // REVISION number
    data = revision;
    for(i=EEPROM_REV_NUMBER; i!=0; i--) {
        idat = data & EEPROM_MASK;
        if((m_status = m_vmm->WriteSafe(EEPROM_REV_ADDR[i-1],idat)) != SUCCESS) {
            CERR("writing revision number to EEPROM at addr 0x%02x",EEPROM_REV_ADDR[i]);
            return(m_status);
        }
        data = (data >> EEPROM_SHFT);
    }

    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPI::readI2CDEL(const u_short addr, u_short& data) {

    u_short idat, iadd;

    // WRITE TO TRANSMIT
    idat = ((addr & I2C_TXT_MASK_ADDR) << I2C_TXT_SHFT_ADDR) | I2C_TXT_MASK_READ;
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STA | I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STO | I2C_CMD_MASK_RD | I2C_CMD_MASK_ACK;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // READ FROM RECEIVE: read data
    iadd = I2C_RCV_ADDR;
    if((m_status = m_vmm->ReadSafe(iadd,&idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }
    data = idat;

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeI2CDEL(const u_short addr, const u_short data) {

    u_short idat, iadd;

    // WRITE TO TRANSMIT
    idat = ((addr & I2C_TXT_MASK_ADDR) << I2C_TXT_SHFT_ADDR) & (~I2C_TXT_MASK_READ);
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STA | I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO TRANSMIT: write data
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,data)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STO | I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::readI2CDAC(const u_short addr, u_short& data) {

    u_short idat, iadd;

    // WRITE TO TRANSMIT: device address
    idat = DAC_ADDR & (~I2C_TXT_MASK_READ);
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STA | I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO TRANSMIT: register address
    idat = addr;
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO TRANSMIT
    idat = DAC_ADDR | I2C_TXT_MASK_READ;
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STA | I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_RD;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // READ FROM RECEIVE: read data
    iadd = I2C_RCV_ADDR;
    if((m_status = m_vmm->ReadSafe(iadd,&idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }
    data = idat & DAC_MASK;

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STO | I2C_CMD_MASK_RD | I2C_CMD_MASK_ACK;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // READ FROM RECEIVE: read data
    iadd = I2C_RCV_ADDR;
    if((m_status = m_vmm->ReadSafe(iadd,&idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }
    data = data | ((idat & DAC_MASK) << DAC_SHFT);

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeI2CDAC(const u_short addr, const u_short data) {

    u_short idat, iadd;

    // WRITE TO TRANSMIT: device address
    idat = DAC_ADDR & (~I2C_TXT_MASK_READ);
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STA | I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO TRANSMIT: register address
    idat = addr;
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO TRANSMIT: data
    idat = data;
    iadd = I2C_TXT_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    // WRITE TO COMMAND
    idat = I2C_CMD_MASK_STO | I2C_CMD_MASK_WR;
    iadd = I2C_CMD_ADDR;
    if((m_status = m_vmm->WriteSafe(iadd,idat)) != SUCCESS) {
        CERR("writing to I2C register 0x%02x",iadd);
        return(m_status);
    }

    // READ FROM STATUS: wait for TIP to clear
    if((m_status = waitI2C()) != SUCCESS) {
        CERR("waiting for I2C transfer","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::waitI2C() {

    u_short idat, iadd;
    unsigned int loop(0);

    while(true) {

        // READ FROM STATUS
        iadd = I2C_STA_ADDR;
        if((m_status = m_vmm->ReadSafe(iadd,&idat)) != SUCCESS) {
            CERR("reading I2C register 0x%02x",iadd);
            return(m_status);
        }

        // check TIP (transfer in progress)
        if(!(idat & I2C_STA_MASK_TIP)) break;

        // check for timeout
        if((++loop) == TIMEOUT) {
            CERR("timeout waiting for transfer to complete","");
            return(ERROR_TIMEOUT);
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int LTPI::checkI2C() {

    u_short idat, iadd;

    // 09-JUL-2013, R. Spiwoks, method not needed any longer
    // 09-JUL-2013, R. Spiwoks, method not needed any longer
    // 09-JUL-2013, R. Spiwoks, method not needed any longer

    // READ FROM STATUS: check RxACK bit
    iadd = I2C_STA_ADDR;
    if((m_status = m_vmm->ReadSafe(iadd,&idat)) != SUCCESS) {
        CERR("reading I2C register 0x%02x",iadd);
        return(m_status);
    }
    if(idat & I2C_STA_MASK_RXACK) {
        CERR("I2C STATUS/RxACK not cleared after address cycle to 0x%02x - STEP 1",I2C_CMD_ADDR);
        return(ERROR);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::readFlash(const u_short addr, u_short& data) {

    u_short idat;

    idat = addr & SFL_ADDR_MASK;
    if((m_status = m_vmm->WriteSafe(SFL_ADDR_ADDR,addr)) != SUCCESS) {
        CERR("writing serial flash address register 0x%02x",SFL_ADDR_ADDR);
        return(m_status);
    }

    if((m_status = m_vmm->WriteSafe(SFL_CSR_ADDR,SFL_CSR_MASK_READ)) != SUCCESS) {
        CERR("writing serial flash CSR register 0x%02x",SFL_CSR_ADDR);
        return(m_status);
    }

    if((m_status = waitFlash()) != SUCCESS) {
        CERR("waiting for serial flash transfer","");
        return(m_status);
    }

    if((m_status = m_vmm->ReadSafe(SFL_RDAT_ADDR,&idat)) != SUCCESS) {
        CERR("reading serial flash read register 0x%02x",SFL_RDAT_ADDR);
        return(m_status);
    }
    data = idat & SFL_DATA_MASK;

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::writeFlash(const u_short addr, const u_short data) {

    u_short idat;

    if((m_status = m_vmm->WriteSafe(SFL_ADDR_ADDR,addr)) != SUCCESS) {
        CERR("writing serial flash address register 0x%02x",SFL_ADDR_ADDR);
        return(m_status);
    }

    idat = data & SFL_DATA_MASK;
    if((m_status = m_vmm->WriteSafe(SFL_WDAT_ADDR,idat)) != SUCCESS) {
        CERR("writing serial flash write register 0x%02x",SFL_RDAT_ADDR);
        return(m_status);
    }

    if((m_status = m_vmm->WriteSafe(SFL_CSR_ADDR,SFL_CSR_MASK_WRITE)) != SUCCESS) {
        CERR("writing serial flash CSR register 0x%02x",SFL_CSR_ADDR);
        return(m_status);
    }

    if((m_status = waitFlash()) != SUCCESS) {
        CERR("waiting for serial flash transfer","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::eraseFlash() {

    if((m_status = m_vmm->WriteSafe(SFL_CSR_ADDR,SFL_CSR_MASK_ERASE)) != SUCCESS) {
        CERR("writing serial flash CSR register 0x%02x",SFL_CSR_ADDR);
        return(m_status);
    }

    if((m_status = waitFlash()) != SUCCESS) {
        CERR("waiting for serial flash transfer","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int LTPI::waitFlash() {

    u_short idat;
    unsigned int loop(0);

    while(true) {

        // read from serial flash CSR
        if((m_status = m_vmm->ReadSafe(SFL_CSR_ADDR,&idat)) != SUCCESS) {
            CERR("reading serial flash CSR register 0x%02x",SFL_CSR_ADDR);
            return(m_status);
        }

        // check BUSY
        if(!(idat & SFL_CSR_MASK_BUSY)) break;

        // check for timeout
        if((++loop) == TIMEOUT) {
            CERR("timeout waiting for transfer to complete","");
            return(ERROR_TIMEOUT);
        }
    }

    return(SUCCESS);
}
