///*****************************************************************************
// file: RCDLtpi.cc
// desc: library for LTPi Module
///*****************************************************************************

#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <errno.h>
#include <iomanip>
#include <utility>
#include <list>
#include <vector>
#include <fstream>
#include <string>

#include "RCDLtpi/RCDLtpi.h"

#include "rcc_error/rcc_error.h"

#include <time.h>

using namespace RCD;
using namespace std;

//------------------------------------------------------------------------------

bool LTPi::CheckCERNID() {

  u_int man, rev, id;
  this->ReadConfigurationEEPROM(man, rev, id);
  return ( 0x00080030 == man)? true : false;
}

//------------------------------------------------------------------------------

int LTPi::ReadConfigurationEEPROM(u_int& board_man, u_int& board_revision, u_int& board_id) {

  int status = 0;
  u_short rev1, rev2, rev3, rev4;
  u_short id1, id2, id3, id4;
  u_short man1, man2, man3;

  status |= m_vmm->ReadSafe(0x4e,&rev1);
  status |= m_vmm->ReadSafe(0x4a,&rev2);
  status |= m_vmm->ReadSafe(0x46,&rev3);
  status |= m_vmm->ReadSafe(0x42,&rev4);

  status |= m_vmm->ReadSafe(0x3e,&id1);
  status |= m_vmm->ReadSafe(0x3a,&id2);
  status |= m_vmm->ReadSafe(0x36,&id3);
  status |= m_vmm->ReadSafe(0x32,&id4);

  status |= m_vmm->ReadSafe(0x2e,&man1);
  status |= m_vmm->ReadSafe(0x2a,&man2);
  status |= m_vmm->ReadSafe(0x26,&man3);

  u_int urev1 = static_cast<u_int>(rev1 & 0xff);
  u_int urev2 = (static_cast<u_int>(rev2 & 0xff) << 8) & 0xff00;
  u_int urev3 = (static_cast<u_int>(rev3 & 0xff) << 16) & 0xff0000;
  u_int urev4 = (static_cast<u_int>(rev4 & 0xff) << 24) & 0xff000000;
  board_revision = urev4 | urev3 | urev2 | urev1;

  u_int uid1 = static_cast<u_int>(id1 & 0xff);
  u_int uid2 = (static_cast<u_int>(id2 & 0xff) << 8) & 0xff00;
  u_int uid3 = (static_cast<u_int>(id3 & 0xff) << 16) & 0xff0000;
  u_int uid4 = (static_cast<u_int>(id4 & 0xff) << 24) & 0xff000000;
  board_id = uid4 | uid3 | uid2 | uid1;

  u_int uman1 = static_cast<u_int>(man1 & 0xff);
  u_int uman2 = (static_cast<u_int>(man2 & 0xff) << 8) & 0xff00;
  u_int uman3 = (static_cast<u_int>(man3 & 0xff) << 16) & 0xff0000;
  board_man = uman3 | uman2 | uman1;

  //   printf(" Manufacturer: 0x%08x \n", board_man);
  //   printf(" Revision    : 0x%08x \n", board_revision);
  //   printf(" Id          : 0x%08x \n", board_id);

  return status;
}

//------------------------------------------------------------------------------

std::string LTPi::GetError(int errcode) {

  char message1[100];
  sprintf(message1,"Error %d in package RCDLtp => ",errcode);
  std::string errstring = message1;
  //   if (errcode == ERROR_INVALID_INTERRUPT_VECTOR ) {
  //     errstring += "Invalid interrupt vector.";
  //   }
  //   else {
  //     m_vme->ErrorString(errcode, &errstring);
  //   }

  return errstring;
}

//------------------------------------------------------------------------------

int LTPi::Reset() {
  u_short data = 1;
  u_short reg = REG_SW_RST;

  // write register
  return m_vmm->WriteSafe(reg,data);

}

//------------------------------------------------------------------------------

int LTPi::pow(int a, int k) {

  int ret = 1;
  for (int i = 0; i < k; ++i) {
    ret *= a;
  }
  return(ret);
}

//------------------------------------------------------------------------------

int LTPi::bintoint(char* a) {

  int r=0;
  for (int k = 0; k<4; ++k) {
    if (a[k]=='1') r+=this->pow(2u,3u-k);
    else if (a[k]=='0') ;
    else r = -1;
  }
  return r;
}

//------------------------------------------------------------------------------

int LTPi::bin4toint(char* a1,char* a2,char* a3,char* a4) {

  return 0x1000*bintoint(a1)
    + 0x0100*bintoint(a2)
    + 0x0010*bintoint(a3)
    + 0x0001*bintoint(a4);
}

//------------------------------------------------------------------------------

int LTPi::Open(u_int base) {

  // open VME library
  m_vme = VME::Open();

  // create VME master mapping
  m_vmm = m_vme->MasterMap(base,VME_SIZE,VME_A24,0);
  return (*m_vmm)();
}

//------------------------------------------------------------------------------

int LTPi::Close(void) {

  // unmap VME master mapping
  int status = m_vme->MasterUnmap(m_vmm);

  // close VME library
  VME::Close();

  return status;
}

//------------------------------------------------------------------------------

int LTPi::setBits(u_int reg, u_short mask, u_short bits) {

  u_short data = 0;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
    return status;
  }

  // set bits
  data = (data & ~mask) | ( bits & mask);

  // write register
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------

int LTPi::testBits(u_int reg, u_short mask, bool* result) {
  u_short data;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
    return status;
  }

  // apply mask
  data = (data & mask);
  if (data==mask) *result=true;
  else *result= false;

  return(status);
}

//------------------------------------------------------------------------------

string LTPi::printBinary16(u_short number) {

  int n = number;
  string s;
  for (int i = 15; i >= 0; --i) {
    if ( ((i+1)%4) == 0 )
      s = s + " ";
    n = n >> i;
    int bit = n & 0x1;
    if (bit==0) s=s+"0";
    if (bit==1) s=s+"1";
    n = number;
  }

  return s;
}

//------------------------------------------------------------------------------

int LTPi::getBitsAndPrint(u_int reg, u_short mask, u_short* data) {

  u_short d;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&d)) != VME_SUCCESS) {
    return status;
  }

  //  apply read mask
  *data = (d & mask);
  std::cout <<"Read value "
            << std::hex << std::setfill('0') << std::setw(4) << *data
            << " = " << printBinary16(*data)
            << " from register "
            << std::hex << std::setfill('0') << std::setw(4) << reg
            << std::dec << std::endl;

  return status;
}

//------------------------------------------------------------------------------

int LTPi::getBits(u_int reg, u_short mask, u_short* data) {

  u_short d;
  int status = 0;
  // read register
  status = m_vmm->ReadSafe(reg,&d);

  //  apply read mask
  *data = (d & mask);
  return status;
}

//------------------------------------------------------------------------------

int LTPi::resetI2C() {
  // reset
  return(m_vmm->WriteSafe(REG_I2C_RESET, static_cast<u_short>(0)));
}

//------------------------------------------------------------------------------

bool LTPi::I2C_frequency_OK() {

  int status = 0;
  u_short reg;
  // read back:
  reg = REG_I2C_PER_LOW;
  u_short pre_low;
  status |= m_vmm->ReadSafe(reg, &pre_low);

  reg = REG_I2C_PER_HIGH;
  u_short pre_high;
  status |= m_vmm->ReadSafe(reg, &pre_high);

  bool ok(false);
//   cout << pre_high << " " << pre_low << endl;
  if ((pre_high==0) && (pre_low==79)) {
    ok = true;
  }

  return(ok);

}

//------------------------------------------------------------------------------

float LTPi::readI2Cfreq() {

  int status = 0;
  u_short reg;
  // read back:
  reg = REG_I2C_PER_LOW;
  u_short pre_low;
  status |= m_vmm->ReadSafe(reg, &pre_low);

  reg = REG_I2C_PER_HIGH;
  u_short pre_high;
  status |= m_vmm->ReadSafe(reg, &pre_high);

  u_int pre = 0x1000*pre_high + pre_low;
  float freq = 40000./(5.*(static_cast<float>(pre)+1.));
  //printf("I2C prescaled clock set to 0x%04x %04x = %5.1f kHz \n", pre_high, pre_low, freq);
  return freq;
}

//------------------------------------------------------------------------------

int LTPi::setupI2C() {
  u_short data;
  u_short reg;
  int status = 0;

  //status |= this->resetI2C();

  // clear enable I2C core
  // send stop command first
  //reg = REG_I2C_TRANS_RECEIVE;
  //data = 0x0080; // stop
  //status |= m_vmm->WriteSafe(reg,data);

  reg = REG_I2C_CTR;
  data = 0x0000; // disable I2C, disable interupts
  status |= m_vmm->WriteSafe(reg,data);

  // set prescale clock
  reg = REG_I2C_PER_LOW;
  data = 0x004f; // 40MHz/(5*100kHz)-1 = 0x4f
  status |= m_vmm->WriteSafe(reg,data);

  reg = REG_I2C_PER_HIGH;
  data = 0x0000;
  status |= m_vmm->WriteSafe(reg,data);

  // read back:
  reg = REG_I2C_PER_LOW;
  u_short pre_low;
  status |= m_vmm->ReadSafe(reg, &pre_low);

  reg = REG_I2C_PER_HIGH;
  u_short pre_high;
  status |= m_vmm->ReadSafe(reg, &pre_high);

  u_int pre = 0x1000*pre_high + pre_low;
  float freq = 40000./(5.*(static_cast<float>(pre)+1.));
  if (freq != 100.0) {
    status|=-1;
  }
//   printf("I2C prescaled clock set to 0x%04x %04x = %5.1f kHz \n", pre_high, pre_low, freq);

  // clear enable I2C core
  // send stop command first
  //reg = REG_I2C_TRANS_RECEIVE;
  //data = 0x0080; // stop
  //status |= m_vmm->WriteSafe(reg,data);

  reg = REG_I2C_CTR;
  data = 0x0080; // enable I2C, disable interupts
  status |= m_vmm->WriteSafe(reg,data);

  return status;
}

//------------------------------------------------------------------------------

int LTPi::printI2Cfreq() {

  u_short reg;
  //u_short data;
  int ierr = 0;

  // read
  reg = REG_I2C_PER_LOW;
  u_short pre_low;
  ierr |= m_vmm->ReadSafe(reg, &pre_low);

  reg = REG_I2C_PER_HIGH;
  u_short pre_high;
  ierr |= m_vmm->ReadSafe(reg, &pre_high);

  u_int pre = 0x1000*pre_high + pre_low;
  float freq = 40000./(5.*(static_cast<float>(pre)+1.));
//   printf("\n");
   printf("I2C prescaled clock set to 0x%04x %04x = %5.1f kHz \n", pre_high, pre_low, freq);

  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::printDACsettings() {

  int ierr = 0;

  u_short gain;
  ierr |= getDACCTPgain(gain);
  printf("CTP gain        0x%02x\n", gain);
  ierr |= getDACCTPequ(gain);
  printf("CTP equalizer   0x%02x\n", gain);
  ierr |= getDACLTPgain(gain);
  printf("LTP gain        0x%02x\n", gain);
  ierr |= getDACLTPequ(gain);
  printf("LTP equalizer   0x%02x\n", gain);
  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::setupDAC() {

  int status = 0;

  // dac addresses for addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  u_short daccr[4] = {addGain_CTP, addGain_LTP, addCTRL_CTP, addCTRL_LTP};
  // default values: 1.7V, 1.7V, 0.05V, 0.05V
  u_short dacval[4] = {VGain_shortCTP, VGain_shortLTP, VCTRL_shortCTP, VCTRL_shortLTP}; // Correct values
  // loop over addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  for (int i = 0; i < 4; ++i) {
    status |= i2c_writeDAC(daccr[i], dacval[i]);
  }

  return status;
}

//------------------------------------------------------------------------------

bool LTPi::checkDAC4longCTPCable() {

  bool isSet = false;
  u_short read;
  getDACCTPgain(read);
  if (read == VGain_longCTP) {
    getDACCTPequ(read);
      if (read == VCTRL_longCTP) isSet = true;
  }
  return isSet;
}

//------------------------------------------------------------------------------

bool LTPi::checkDAC4longLTPCable() {

  bool isSet = false;
  u_short read;
  getDACLTPgain(read);
  if (read == VGain_longLTP) {
    getDACLTPequ(read);
      if (read == VCTRL_longLTP) isSet = true;
  }
  return isSet;
}

//------------------------------------------------------------------------------

bool LTPi::checkDAC4shortCTPCable() {

  bool isSet = false;
  u_short read;
  getDACCTPgain(read);
  cout << "getDACCTPgain = " << read << endl;
  cout << "VGain_shortCTP = " <<  VGain_shortCTP << endl;
  if (read == VGain_shortCTP) {
    getDACCTPequ(read);
  cout << "getDACCTPequ = " << read << endl;
  cout << "VCTRL_shortCTP = " << VCTRL_shortCTP  << endl;
      if (read == VCTRL_shortCTP) isSet = true;
  }
  return isSet;
}

//------------------------------------------------------------------------------

bool LTPi::checkDAC4shortLTPCable() {
  bool isSet = false;
  u_short read;
  getDACLTPgain(read);
  if (read == VGain_shortLTP) {
    getDACLTPequ(read);
      if (read == VCTRL_shortLTP) isSet = true;
  }
  return isSet;
}

//------------------------------------------------------------------------------

bool LTPi::isShortCable(int length) {

  return (length < LTPi::maximalShortLength) ? true : false;
}

//------------------------------------------------------------------------------

bool LTPi::isLongCable(int length) {

  return (length > LTPi::minimalLongLength) ? true : false;
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4longCTPCable() {

  return(setDacCtp(VGain_longCTP, VCTRL_longCTP));
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4longLTPCable() {

  return(setDacLtp(VGain_longCTP, VCTRL_longCTP));
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4givenCTPCable(float length) {

  return(setDacCtp(VGain_CTP(length), VCTRL_CTP(length)));
}

//------------------------------------------------------------------------------

bool LTPi::checkDAC4givenCTPCable(float length) {

  bool isSet = false;
  u_short read;
  getDACCTPgain(read);
  if (read == VGain_CTP(length)) {
    getDACCTPequ(read);
      if (read == VCTRL_CTP(length)) isSet = true;
  }
  return isSet;
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4givenLTPCable(float length) {

    return(setDacLtp(VGain_LTP(length), VCTRL_LTP(length)));
}

//------------------------------------------------------------------------------

bool LTPi::checkDAC4givenLTPCable(float length) {

  bool isSet = false;
  u_short read;
  getDACLTPgain(read);
  if (read == VGain_LTP(length)) {
    getDACLTPequ(read);
      if (read == VCTRL_LTP(length)) isSet = true;
  }
  return isSet;
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4shortCTPCable() {

    return(setDacCtp(VGain_shortCTP, VCTRL_shortCTP));
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4shortLTPCable() {

    return(setDacLtp(VGain_shortLTP, VCTRL_shortLTP));
}

//------------------------------------------------------------------------------

int LTPi::setupDAC_LTPCable_toZero() {

    return(setDacLtp(0, 0));
}

//------------------------------------------------------------------------------

int LTPi::setupDAC_CTPCable_toZero() {

    return(setDacCtp(0, 0));
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4longCable() {

  int status = 0;

  // dac addresses for addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  //u_short daccr[4] = {addGain_CTP, addGain_LTP, addCTRL_CTP, addCTRL_LTP};
  // default values: 1.7V, 1.7V, 0.05V, 0.05V
  //u_short dacval[4] = {VGain_longCTP, VGain_longLTP, VCTRL_longCTP, VCTRL_longLTP}; // Correct values
  // loop over addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  //for (int i = 0; i < 4; ++i) {
  //  status |= i2c_writeDAC(daccr[i], dacval[i]);
  //}
  status |= setDacCtp(VGain_longCTP, VCTRL_longCTP);
  status |= setDacLtp(VGain_longLTP, VCTRL_longLTP);

  return status;
}

//------------------------------------------------------------------------------

int LTPi::setupDAC4shortCable() {

  int status = 0;

  // dac addresses for addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  //u_short daccr[4] = {addGain_CTP, addGain_LTP, addCTRL_CTP, addCTRL_LTP};
  // default values: 1.7V, 1.7V, 0.05V, 0.05V
  //u_short dacval[4] = {VGain_shortCTP, VGain_shortLTP, VCTRL_shortCTP, VCTRL_shortLTP}; // Correct values

  // loop over addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  //for (int i = 0; i < 4; ++i) {
  //  status |= i2c_writeDAC(daccr[i], dacval[i]);
  //}
  status |= setDacCtp(VGain_shortCTP, VCTRL_shortCTP);
  status |= setDacLtp(VGain_shortLTP, VCTRL_shortLTP);

  return status;
}

//------------------------------------------------------------------------------

int LTPi::enableCtpInput(bool ena) {

    int ierr = 0;
    u_short gain = (ena) ? m_ctp_gain : 0;
    u_short equ  = (ena) ? m_ctp_equ  : 0;
    ierr |= i2c_writeDAC(addGain_CTP, gain);
    ierr |= i2c_writeDAC(addCTRL_CTP, equ);
    return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::enableLtpInput(bool ena) {
    int ierr = 0;
    u_short gain = (ena) ? m_ltp_gain : 0;
    u_short equ  = (ena) ? m_ltp_equ  : 0;
    ierr |= i2c_writeDAC(addGain_LTP, gain);
    ierr |= i2c_writeDAC(addCTRL_LTP, equ);
    return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::setDacLtp(u_short gain, u_short equ) {

  int ierr = 0;
  m_ltp_gain = gain;
  m_ltp_equ  = equ;
  ierr |= i2c_writeDAC(addGain_LTP, gain);
  ierr |= i2c_writeDAC(addCTRL_LTP, equ );
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::setDacCtp(u_short gain, u_short equ) {

  int ierr = 0;
  m_ctp_gain = gain;
  m_ctp_equ  = equ;
  ierr |= i2c_writeDAC(addGain_CTP, gain);
  ierr |= i2c_writeDAC(addCTRL_CTP, equ );
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::setDACLTPgain(u_short gain) {

  int ierr = 0;
  m_ltp_gain = gain;
  ierr |= i2c_writeDAC(addGain_LTP, gain);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::setDACCTPgain(u_short gain) {

  int ierr = 0;
  m_ctp_gain = gain;
  ierr |= i2c_writeDAC(addGain_CTP, gain);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::setDACLTPequ(u_short equ) {

  int ierr = 0;
  m_ltp_equ = equ;
  ierr |= i2c_writeDAC(addCTRL_LTP, equ);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::setDACCTPequ(u_short equ) {

  int ierr = 0;
  m_ctp_equ = equ;
  ierr |= i2c_writeDAC(addCTRL_CTP, equ);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::getDACLTPgain(u_short& gain) {

  int ierr = 0;
  ierr |= i2c_readDAC(addGain_LTP, gain);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::getDACCTPgain(u_short& gain) {

  int ierr = 0;
  ierr |= i2c_readDAC(addGain_CTP, gain);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::getDACLTPequ(u_short& equ) {

  int ierr = 0;
  ierr |= i2c_readDAC(addCTRL_LTP, equ);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::getDACCTPequ(u_short& equ) {

  int ierr = 0;
  ierr |= i2c_readDAC(addCTRL_CTP, equ);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::i2c_writeDAC(u_short daccr, u_short dacval) {

  int ierr = 0;

    u_short data = 0x98;
    u_short reg = REG_I2C_TRANS_RECEIVE;
    ierr |= m_vmm->WriteSafe(reg,data);

    data = 0x90; // write+start
    reg = REG_I2C_COMMAND_STATUS;
    ierr |= m_vmm->WriteSafe(reg,data);

    // wait for TIP flag to negate
    ierr |= this->waitTransfer();
    // read ack bit from status register, should be zero
    // xxx

    // write data
    data = daccr;
    reg = REG_I2C_TRANS_RECEIVE;
    ierr |= m_vmm->WriteSafe(reg,data);

    data = 0x10; // write
    reg = REG_I2C_COMMAND_STATUS;
    ierr |= m_vmm->WriteSafe(reg,data);

    ierr |= this->waitTransfer();

    // write data
    data = dacval;
    reg = REG_I2C_TRANS_RECEIVE;
    ierr |= m_vmm->WriteSafe(reg,data);

    data = 0x10; // write
    reg = REG_I2C_COMMAND_STATUS;
    ierr |= m_vmm->WriteSafe(reg,data);

    ierr |= this->waitTransfer();

    // write data
    data = 0x00;
    reg = REG_I2C_TRANS_RECEIVE;
    //ierr |= m_vmm->WriteSafe(reg,data);

    data = 0x50; // write+stop
    reg = REG_I2C_COMMAND_STATUS;
    ierr |= m_vmm->WriteSafe(reg,data);

    ierr |= this->waitTransfer();

  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::i2c_readDAC(u_short daccr, u_short &dacval) {

  // this funciton does not work!!!!

  int ierr = 0;
  bool RxACK = 0;

    u_short data = 0x98;
    u_short reg = REG_I2C_TRANS_RECEIVE;
    ierr |= m_vmm->WriteSafe(reg,data);

    data = 0x90; // write+start
    reg = REG_I2C_COMMAND_STATUS;
    ierr |= m_vmm->WriteSafe(reg,data);

    // wait for TIP flag to negate
    ierr |= this->waitTransfer();
    // read ack bit from status register, should be zero
    // xxx
    ierr |= this->testBits(REG_I2C_COMMAND_STATUS, 0x80, &RxACK);
    if (RxACK) {
      printf("<<readDAC>> checking acknowledge bit for DAC");
      std::cout << "<<i2c_write>> RxACK not zero: " << RxACK << std::endl;
  }
    // write data address
    data = daccr;
    reg = REG_I2C_TRANS_RECEIVE;
    ierr |= m_vmm->WriteSafe(reg,data);

    data = 0x10; // write
    reg = REG_I2C_COMMAND_STATUS;
    ierr |= m_vmm->WriteSafe(reg,data);

    ierr |= this->waitTransfer();

    ierr |= this->testBits(REG_I2C_COMMAND_STATUS, 0x80, &RxACK);
    if (RxACK) {
      printf("<<readDAC>> checking acknowledge bit for DAC cr 0x%04x \n", daccr);
      std::cout << "<<i2c_write>> RxACK not zero: " << RxACK << std::endl;
  }

   // write data
//   data = 0x00;
//   reg = REG_I2C_TRANS_RECEIVE;
//   ierr |= m_vmm->WriteSafe(reg,data);
//
//   data = 0x10; // write
//   reg = REG_I2C_COMMAND_STATUS;
//   ierr |= m_vmm->WriteSafe(reg,data);
//
//
//   ierr |= this->waitTransfer();
//

    // write data (chip address + read bit on (LSB=1))
    data = 0x99;
    reg = REG_I2C_TRANS_RECEIVE;
    ierr |= m_vmm->WriteSafe(reg,data);

    data = 0x90; // WRITE + START
    reg = REG_I2C_COMMAND_STATUS;
    ierr |= m_vmm->WriteSafe(reg,data);

    ierr |= this->waitTransfer();
    ierr |= this->testBits(REG_I2C_COMMAND_STATUS, u_short(0x80), &RxACK);
    if (RxACK) {
      printf("<<readDAC>> checking acknowledge bit for DAC cr 0x%04x \n", daccr);
      std::cout << "<<i2c_write>> RxACK not zero: " << RxACK << std::endl;
    }

    ierr |= m_vmm->WriteSafe(REG_I2C_COMMAND_STATUS, u_short(0x20)); // READ|ACK
    ierr |= this->waitTransfer();

    // read data
    ierr |= m_vmm->ReadSafe(REG_I2C_TRANS_RECEIVE, &data);
    dacval = data & 0xff;

    ierr |= m_vmm->WriteSafe(REG_I2C_COMMAND_STATUS, u_short(0x68)); // STOP|READ|NACK
    ierr |= this->waitTransfer();

    ierr |= m_vmm->ReadSafe(REG_I2C_TRANS_RECEIVE, &data);
    dacval |= (data & 0xff) << 8;

    return ierr;
}

//------------------------------------------------------------------------------

int LTPi::setDAC() {

  int ierr = 0;

  //u_short data;
  //u_short reg;

  // dac addresses for addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  u_short daccr[4] = {addGain_CTP, addGain_LTP, addCTRL_CTP, addCTRL_LTP};
  // default values: 1.7V, 1.7V, 0.05V, 0.05V
  u_short dacval[4] = {VGain_shortCTP, VGain_shortLTP, VCTRL_shortCTP, VCTRL_shortLTP}; // Correct values

  // loop over addGain-CTP, addGain-LTP, addCTRL-CTP, addCTRL-LTP
  for (int i = 0; i < 4; ++i) {
    ierr |= i2c_writeDAC(daccr[i], dacval[i]);
  }

  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::resetDelay25Pll() {

  int status = 0;
  u_short chipaddr[5] = {TRT1, TRT2, TTR, BGO, L1AORB};

  for (int i = 0; i < 5; ++i) {
    u_short offset = 5;
    u_short i2cdat = 0x40;
    u_short addr(chipaddr[i]+offset);
    if (debug==3) printf("<<resetDelay25Pll>> (about to call i2c_write) chip address + chanel = 0x%04x; chip add = 0x%04x; offset = %d, delay setting i2cdat = 0x%04x \n", addr, chipaddr[i], offset, i2cdat);

    status |= i2c_write(addr, i2cdat);
    if (status) std::cout << "<<resetDelay25Pll>> status = " << status << std::endl;
  }

  return status;
}

//------------------------------------------------------------------------------

int LTPi::enableCTPout(int delay) {

  int status = 0;
  u_short chipaddr[5] = {TRT1, TRT2, TTR, BGO, L1AORB};

  for (int i = 0; i < 5; ++i)
    {
      for (int j = 5; j >= 0; --j)
        {
          // for test purpose
          // timespec wait_time;
          // wait_time.tv_sec = 0.01;
          // wait_time.tv_nsec = 0;
          // nanosleep(&wait_time, nullptr);
          ////// does not help
          u_short offset = j;
          u_short i2cdat = 0;
          if (offset==5) {
            i2cdat = 0x40;
          }
          else {
            i2cdat = 0x40 | (delay & 0x3f);
          }
          u_short add;
          add = chipaddr[i]+offset;
          if (debug==3) printf("<<enableCTPout>> (about to call i2c_write) chip address + chanel = 0x%04x; chip add = 0x%04x; offset = %d, delay setting i2cdat = 0x%04x \n", add, chipaddr[i], offset, i2cdat);
          status |= i2c_write(add, i2cdat);
          if (status) std::cout << "<<enableCTPout>> status = " << status << std::endl;
        }
    }

  return status;
}

//------------------------------------------------------------------------------

int LTPi::disableCTPout() {

  int status = 0;
  u_short chipaddr[5] = {TRT1, TRT2, TTR, BGO, L1AORB};

  for (int i = 0; i < 5; ++i)
    {
      for (int j = 5; j >= 0; --j)
        {
          // for test purpose
          // timespec wait_time;
          // wait_time.tv_sec = 0.01;
          // wait_time.tv_nsec = 0;
          // nanosleep(&wait_time, nullptr);
          ////// does not help
          u_short offset = j;
          u_short i2cdat = 0;
          i2cdat = 0x0;
          u_short add;
          add = chipaddr[i]+offset;
          if (debug==3) printf("<<disableCTPout>> (about to call i2c_write) chip address + chanel = 0x%04x; chip add = 0x%04x; offset = %d, delay setting i2cdat = 0x%04x \n", add, chipaddr[i], offset, i2cdat);
          status |= i2c_write(add, i2cdat);
          if (status) std::cout << "<<disableCTPout>> status = " << status << std::endl;
        }
    }

  return status;
}

//------------------------------------------------------------------------------

int LTPi::setDelaySingleChannel(u_short delay, u_short channel) {

  int ierr = 0;
  u_short i2cdat = 0;
  ierr = storeChannelDelay(delay, channel);
  i2cdat = delay | 0x40;
  ierr |= i2c_write(channel, i2cdat);
  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::storeChannelDelay(u_short delay, u_short channel) {

  if (channel == DEL25L1A) m_delay_L1A=delay;
  if (channel == DEL25ORB) m_delay_ORB=delay;
  if (channel == DEL25TTR1) m_delay_TTR1=delay;
  if (channel == DEL25TTR2) m_delay_TTR2=delay;
  if (channel == DEL25TTR3) m_delay_TTR3=delay;
  if (channel == DEL25BGO0) m_delay_BGO0=delay;
  if (channel == DEL25BGO1) m_delay_BGO1=delay;
  if (channel == DEL25BGO2) m_delay_BGO2=delay;
  if (channel == DEL25BGO3) m_delay_BGO3=delay;
  if (channel == DEL25TRT0) m_delay_TRT0=delay;
  if (channel == DEL25TRT1) m_delay_TRT1=delay;
  if (channel == DEL25TRT2) m_delay_TRT2=delay;
  if (channel == DEL25TRT3) m_delay_TRT3=delay;
  if (channel == DEL25TRT4) m_delay_TRT4=delay;
  if (channel == DEL25TRT5) m_delay_TRT5=delay;
  if (channel == DEL25TRT6) m_delay_TRT6=delay;
  if (channel == DEL25TRT7) m_delay_TRT7=delay;

  return 0;
}

//------------------------------------------------------------------------------

int LTPi::setDelayAllChannels() {

  int status = 0;

  status |= setDelaySingleChannel(m_delay_L1A, DEL25L1A);
  status |= setDelaySingleChannel(m_delay_ORB, DEL25ORB);
  status |= setDelaySingleChannel(m_delay_BGO1, DEL25BGO1);
  status |= setDelaySingleChannel(m_delay_BGO2, DEL25BGO2);
  status |= setDelaySingleChannel(m_delay_BGO3, DEL25BGO3);
  status |= setDelaySingleChannel(m_delay_BGO0, DEL25BGO0);
  status |= setDelaySingleChannel(m_delay_TTR1, DEL25TTR1);
  status |= setDelaySingleChannel(m_delay_TTR2, DEL25TTR2);
  status |= setDelaySingleChannel(m_delay_TTR3, DEL25TTR3);
  status |= setDelaySingleChannel(m_delay_TRT0, DEL25TRT0);
  status |= setDelaySingleChannel(m_delay_TRT1, DEL25TRT1);
  status |= setDelaySingleChannel(m_delay_TRT2, DEL25TRT2);
  status |= setDelaySingleChannel(m_delay_TRT3, DEL25TRT3);
  status |= setDelaySingleChannel(m_delay_TRT4, DEL25TRT4);
  status |= setDelaySingleChannel(m_delay_TRT5, DEL25TRT5);
  status |= setDelaySingleChannel(m_delay_TRT6, DEL25TRT6);
  status |= setDelaySingleChannel(m_delay_TRT7, DEL25TRT7);

  return status;
}

//------------------------------------------------------------------------------

int LTPi::setDelayAllChannels(u_short delay) {

  int status = 0;

  status |= setDelaySingleChannel(delay, DEL25L1A);
  m_delay_L1A = delay;
  status |= setDelaySingleChannel(delay, DEL25ORB);
  m_delay_ORB = delay;
  status |= setDelaySingleChannel(delay, DEL25TTR1);
  m_delay_TTR1 = delay;
  status |= setDelaySingleChannel(delay, DEL25TTR2);
  m_delay_TTR2 = delay;
  status |= setDelaySingleChannel(delay, DEL25TTR3);
  m_delay_TTR3 = delay;
  status |= setDelaySingleChannel(delay, DEL25BGO0);
  m_delay_BGO0 = delay;
  status |= setDelaySingleChannel(delay, DEL25BGO1);
  m_delay_BGO1 = delay;
  status |= setDelaySingleChannel(delay, DEL25BGO2);
  m_delay_BGO2 = delay;
  status |= setDelaySingleChannel(delay, DEL25BGO3);
  m_delay_BGO3 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT0);
  m_delay_TRT0 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT1);
  m_delay_TRT1 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT2);
  m_delay_TRT2 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT3);
  m_delay_TRT3 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT4);
  m_delay_TRT4 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT5);
  m_delay_TRT5 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT6);
  m_delay_TRT6 = delay;
  status |= setDelaySingleChannel(delay, DEL25TRT7);
  m_delay_TRT7 = delay;
  return status;
}

//------------------------------------------------------------------------------

int LTPi::readDelaySingleChannel(u_short &delay, u_short channel) {

  int ierr = 0;
  delay = 0;
  ierr |= i2c_read(channel, delay);
  delay -= 0x40;
  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::readDelayAllChannels() {

  int status = 0;
  u_short delay = 0;

  status |= readDelaySingleChannel(delay, DEL25L1A);
  printf("Channel L1A, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25ORB);
  printf("Channel ORB, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25BGO1);
  printf("Channel BGO1, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25BGO2);
  printf("Channel BGO2, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25BGO3);
  printf("Channel BGO3, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25BGO0);
  printf("Channel BGO0, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TTR1);
  printf("Channel TTR1, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TTR2);
  printf("Channel TTR2, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TTR3);
  printf("Channel TTR3, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT0);
  printf("Channel TRT0, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT1);
  printf("Channel TRT1, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT2);
  printf("Channel TRT2, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT3);
  printf("Channel TRT3, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT4);
  printf("Channel TRT4, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT5);
  printf("Channel TRT5, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT6);
  printf("Channel TRT6, Delay = %d \n", delay);
  status |= readDelaySingleChannel(delay, DEL25TRT7);
  printf("Channel TRT7, Delay = %d \n", delay);

  return(status);
}

//------------------------------------------------------------------------------

int LTPi::i2c_write(u_short addr, u_short dat) {

  int status = 0;

  u_short data;
  u_short reg;
  bool RxACK;

  data = ((addr&0x7f)<<1)&0xfe;
  reg = REG_I2C_TRANS_RECEIVE;
  status |= m_vmm->WriteSafe(reg,data);
  if (status) {
    std::cout << "<<i2c_write>> address cycle addressing chip" << std::endl;
    printf("<<i2c_write>> chip address + channal offset = 0x%04x \n", addr);
    std::cout << "<<i2c_write>> status1 = " << status << std::endl;
    printf("<<i2c_write>> writing data = 0x%04x to register = 0x%04x \n", data,  reg);
  }

  data = 0x90; // write+start
  reg = REG_I2C_COMMAND_STATUS;
  status |= m_vmm->WriteSafe(reg,data);
  if (status) {
    std::cout << "<<i2c_write>> address cycle write+start" << std::endl;
    printf("<<i2c_write>> chip address + channal offset = 0x%04x \n", addr);
    std::cout << "<<i2c_write>> status2 = " << status << std::endl;
    printf("<<i2c_write>> writing data = 0x%04x to register = 0x%04x \n", data,  reg);
  }

  // wait for TIP flag to negate
  status |= this->waitTransfer();
  if (status) {
    std::cout << "<<i2c_write>> wait " << std::endl;
    std::cout << "<<i2c_write>> status3 = " << status << std::endl;
  }

  // read ack bit from status register, should be zero
  // xxx
  status |= this->testBits(REG_I2C_COMMAND_STATUS, 0x80, &RxACK);
  if (RxACK && debug==3) {
    printf("<<i2c_write>> checking acknowledge bit for chip+channel offset = 0x%04x after address cycle \n", addr);
    std::cout << "<<i2c_write>> RxACK not zero: " << RxACK << std::endl;
  }

  if (status) {
    std::cout << "<<i2c_write>> checking acknowledge bit for chip+channel offset return status "
              << status << std::endl;
  }

  // write data
  data = dat & 0xff;
  reg = REG_I2C_TRANS_RECEIVE;
  status |= m_vmm->WriteSafe(reg,data);
  if (status) {
    std::cout << "<<i2c_write>> write cycle data=" << data << std::endl;
    printf("<<i2c_write>> chip address + channal offset = 0x%04x \n", addr);
    std::cout << "<<i2c_write>> status3 = " << status << std::endl;
    printf("<<i2c_write>> writing data = 0x%04x to register = 0x%04x \n", data,  reg);
  }

  data = 0x50; // write+stop
  reg = REG_I2C_COMMAND_STATUS;
  status |= m_vmm->WriteSafe(reg,data);
  if (status) {
    printf("<<i2c_write>> write + stop reg = 0x%04x, data = 0x%04x \n", reg, data);
    std::cout << "<<i2c_write>> status4 = " << status << std::endl;
    std::cout << "<<i2c_write>> write+stop" <<endl;
  }

  status |= this->waitTransfer();
  if (status) std::cout << "<<i2c_write>> wait transfer status5 = "
                        << status << std::endl;

  // read ack bit from status register, should be zero
  // xxx
  status |= this->testBits(REG_I2C_COMMAND_STATUS, 0x80, &RxACK);
  if (RxACK && debug==3) {
    printf("<<i2c_write>> checking acknowledge bit for chip+channel offset = 0x%04x after writing cycle \n", addr);
    std::cout << "<<i2c_write>> RxACK not zero: " << RxACK << std::endl;
  }
  if (status) {
    printf("<<i2c_write>> acknowledge bit 0x%04x", data);
    printf("<<i2c_write>>chip address + channal offset = 0x%04x", addr);
    std::cout << "<<i2c_write>> status6 = " << status << std::endl;
  }

  return status;
}

//------------------------------------------------------------------------------

int LTPi::i2c_read(u_short addr, u_short& dat) {

  int status = 0;

  u_short data;
  u_short reg;

  data = ((addr&0x7f)<<1) | 0x01;
  reg = REG_I2C_TRANS_RECEIVE;
  status |= m_vmm->WriteSafe(reg,data);

  data = 0x90; // write+start
  reg = REG_I2C_COMMAND_STATUS;
  status |= m_vmm->WriteSafe(reg,data);

  status |= this->waitTransfer();
  // read ack bit from status register, should be zero
  // xxx

  data = 0x68; // STOP|READ|ACK
  reg = REG_I2C_COMMAND_STATUS;
  status |= m_vmm->WriteSafe(reg,data);

  status |= this->waitTransfer();

  // read data
  reg = REG_I2C_TRANS_RECEIVE;
  status |= m_vmm->ReadSafe(reg,&data);

  dat = data;

  return status;
}

//------------------------------------------------------------------------------

int LTPi::waitTransfer() {

  int status = 0;

   int loop = 0;
   while(true) {
     // check for bit 2: 1=transfer in progress, 0=transfer completed
     bool inprogress;
     status |= this->testBits(REG_I2C_COMMAND_STATUS, 0x02, &inprogress);
     // if status completed, then done
     if (!inprogress) {
       break;
     }
     // check for timeout
     if (++loop == 500000) {
       std::cerr << "timeout waiting for transfer to complete" << std::endl;
       return(-1);
       break;
     }
   }

  return status;
}

//------------------------------------------------------------------------------

// temporary write method
int LTPi::write(u_short addr, u_short dat) {

  u_short data = dat;
  u_short reg = addr;

  // write register
  return m_vmm->WriteSafe(reg,data);
}

//  -- Print methods

//------------------------------------------------------------------------------

int LTPi::printBC_status() {

  int ierr = 0;
  u_short bitStatus;
  std::cout << " " << std::endl;
  std::cout << "Bunch Clock Path:" << std::endl;
  ierr = getBC_Path(bitStatus);
  //std::cout << " --------------- " << bitStatus << std::endl;
  if ((bitStatus & MSK_BC) == BC_FROM_CTP_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_BC) == BC_FROM_CTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ctp-Link in" << std::endl;
  if ((bitStatus & MSK_BC) == BC_FROM_LTP)
    std::cout << "***** CTP-Link out from LTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_BC) == BC_FROM_NIM)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from NIM in" << std::endl;
  if ((bitStatus & MSK_BC) == BC_FROM_NIM_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_BC) == 0)
    std::cout << "***** Path not configured" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "BC Status:" << std::endl;
  std::cout << "***** BC signal on CTPout link is:";
  (this->isBC_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BC signal on LTPout link is:";
  (this->isBC_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BC signal on NIM input is:";
  (this->isBC_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return ierr;
}

//------------------------------------------------------------------------------

string LTPi::decodeCodeA(u_short p) {

  string ret("");
  switch (p) {
  case 0:
  case 6:
  case 7:
    ret = "OFF";
    break;
  case 1:
    ret = "CTP_to_CTP + CTP_to_NIM + LTP_to_LTP";
    break;
  case 2:
    ret = "CTP_to_CTP + CTP_to_NIM + CTP_to_LTP";
    break;
  case 3:
    ret = "LTP_to_CTP + LTP_to_NIM + LTP_to_LTP";
    break;
  case 4:
    ret = "NIM_to_CTP + NIM_to_NIM + NIM_to_LTP";
    break;
  case 5:
    ret = "NIM_to_CTP + NIM_to_NIM + LTP_to_LTP";
    break;
  default:
    ret = "Invalid";
    break;
  }
  return ret;
}

//------------------------------------------------------------------------------

string LTPi::decodeCodeB(u_short p) {

  string ret("");

  switch (p) {
  case 0:
  case 4:
  case 5:
  case 6:
  case 7:
    ret = "OFF";
    break;
  case 1:
    ret = "CTP_to_CTP + LTP_to_LTP";
    break;
  case 2:
    ret = "CTP_to_CTP + CTP_to_LTP";
    break;
  case 3:
    ret = "LTP_to_CTP + LTP_to_LTP";
    break;
  default:
    ret = "Invalid";
    break;
  }
  return ret;
}

//------------------------------------------------------------------------------

string LTPi::getBC_Path() {

  u_short path(0);
  this->getBC_Path(path);
  path = path & MSK_BC;
  return this->decodeCodeA(path);
}

//------------------------------------------------------------------------------

string LTPi::getOrbit_Path() {

  u_short path(0);
  this->getOrbit_Path(path);
  path = (path & MSK_ORB) >> 4;
  return this->decodeCodeA(path);
}

//------------------------------------------------------------------------------

string LTPi::getL1A_Path() {

  u_short path(0);
  this->getL1A_Path(path);
  path = (path & MSK_L1A) >> 8;
  return this->decodeCodeA(path);
}

//------------------------------------------------------------------------------

string LTPi::getTTR_Path() {

  u_short path(0);
  this->getTTR_Path(path);
  path = (path & MSK_TTR) >> 12;
  return this->decodeCodeA(path);
}

//------------------------------------------------------------------------------

string LTPi::getBGO_Path() {

  u_short path(0);
  this->getBGO_Path(path);
  path = path & MSK_BGO;
  return this->decodeCodeA(path);
}

//------------------------------------------------------------------------------

string LTPi::getTRT_Path() {

  u_short path(0);
  this->getTRT_Path(path);
  path = (path & MSK_TRT) >> 4;
  return this->decodeCodeA(path);
}

//------------------------------------------------------------------------------

string LTPi::getCAL_Path() {

  u_short path(0);
  this->getCAL_Path(path);
  path = (path & MSK_CAL) >> 8;
  return this->decodeCodeB(path);
}

//------------------------------------------------------------------------------

int LTPi::printOrbit_status() {

  int ierr = 0;
  u_short bitStatus;
  std::cout << " " << std::endl;
  std::cout << "Orbit Path:" << std::endl;
  ierr = getOrbit_Path(bitStatus);
  //std::cout << " --------------- " << bitStatus << std::endl;
  if ((bitStatus & MSK_ORB) == ORB_FROM_CTP_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_ORB) == ORB_FROM_CTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ctp-Link in" << std::endl;
  if ((bitStatus & MSK_ORB) == ORB_FROM_LTP)
    std::cout << "***** CTP-Link out from LTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_ORB) == ORB_FROM_NIM)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from NIM in" << std::endl;
  if ((bitStatus & MSK_ORB) == ORB_FROM_NIM_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_ORB) == 0)
    std::cout << "***** Path not configured" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "Orbit Status:" << std::endl;
  std::cout << "***** Orbit signal on CTPout link is:";
  (this->isORB_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** Orbit signal on LTPout link is:";
  (this->isORB_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** Orbit signal on NIM input is:";
  (this->isORB_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::printL1A_status() {

  int ierr = 0;
  u_short bitStatus;
  std::cout << " " << std::endl;
  std::cout << "L1A Path:" << std::endl;
  ierr = getL1A_Path(bitStatus);
  //std::cout << " --------------- " << bitStatus << std::endl;
  if ((bitStatus & MSK_L1A) == L1A_FROM_CTP_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_L1A) == L1A_FROM_CTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ctp-Link in" << std::endl;
  if ((bitStatus & MSK_L1A) == L1A_FROM_LTP)
    std::cout << "***** CTP-Link out from LTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_L1A) == L1A_FROM_NIM)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from NIM in" << std::endl;
  if ((bitStatus & MSK_L1A) == L1A_FROM_NIM_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_L1A) == 0)
    std::cout << "***** Path not configured" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "L1A Status:" << std::endl;
  std::cout << "***** L1A signal on CTPout link is:";
  (this->isL1A_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** L1A signal on LTPout link is:";
  (this->isL1A_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** L1A signal on NIM input is:";
  (this->isL1A_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::printTTR_status() {

  int ierr = 0;
  u_short bitStatus;
  std::cout << " " << std::endl;
  std::cout << "Test Triggers Path:" << std::endl;
  ierr = getTTR_Path(bitStatus);
  //std::cout << " --------------- " << bitStatus << std::endl;
  if ((bitStatus & MSK_TTR) == TTR_FROM_CTP_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_TTR) == TTR_FROM_CTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ctp-Link in" << std::endl;
  if ((bitStatus & MSK_TTR) == TTR_FROM_LTP)
    std::cout << "***** CTP-Link out from LTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_TTR) == TTR_FROM_NIM)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from NIM in" << std::endl;
  if ((bitStatus & MSK_TTR) == TTR_FROM_NIM_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_TTR) == 0)
    std::cout << "***** Path not configured" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "Test Trigger Status:" << std::endl;
  std::cout << "***** TTR1 signal on CTPout link is:";
  (this->isTTR1_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TTR2 signal on CTPout link is:";
  (this->isTTR2_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TTR3 signal on CTPout link is:";
  (this->isTTR3_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  std::cout << "***** TTR1 signal on LTPout link is:";
  (this->isTTR1_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TTR2 signal on LTPout link is:";
  (this->isTTR2_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TTR3 signal on LTPout link is:";
  (this->isTTR3_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TTR1 signal on NIM input is:";
  (this->isTTR1_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TTR2 signal on NIM input is:";
  (this->isTTR2_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TTR3 signal on NIM input is:";
  (this->isTTR3_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::printBGO_status() {

  int ierr = 0;
  u_short bitStatus;
  std::cout << " " << std::endl;
  std::cout << "BGOs Path:" << std::endl;
  ierr = getBGO_Path(bitStatus);
  //std::cout << " --------------- " << bitStatus << std::endl;
  if ((bitStatus & MSK_BGO) == BGO_FROM_CTP_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_BGO) == BGO_FROM_CTP)
    std::cout << "***** CTP-Link out from CTP-Link in; LTP-Link out from Ctp-Link in" << std::endl;
  if ((bitStatus & MSK_BGO) == BGO_FROM_LTP)
    std::cout << "***** CTP-Link out from LTP-Link in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_BGO) == BGO_FROM_NIM)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from NIM in" << std::endl;
  if ((bitStatus & MSK_BGO) == BGO_FROM_NIM_LTPOUT_FROM_LTP)
    std::cout << "***** CTP-Link out from NIM in; LTP-Link out from Ltp-Link in" << std::endl;
  if ((bitStatus & MSK_BGO) == 0)
    std::cout << "***** Path not configured" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "BGOs Status:" << std::endl;
  std::cout << "***** BGO0 signal on CTPout link is:";
  (this->isBGO0_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO1 signal on CTPout link is:";
  (this->isBGO1_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO2 signal on CTPout link is:";
  (this->isBGO2_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO3 signal on CTPout link is:";
  (this->isBGO3_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  std::cout << "***** BGO0 signal on LTPout link is:";
  (this->isBGO0_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO1 signal on LTPout link is:";
  (this->isBGO1_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO2 signal on LTPout link is:";
  (this->isBGO2_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO3 signal on LTPout link is:";
  (this->isBGO3_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  std::cout << "***** BGO0 signal on NIM input is:";
  (this->isBGO0_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO1 signal on NIM input is:";
  (this->isBGO1_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO2 signal on NIM input is:";
  (this->isBGO2_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BGO3 signal on NIM input is:";
  (this->isBGO3_NIM() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return ierr;
}

//  -- Read methods

//------------------------------------------------------------------------------

int LTPi::getBC_Path(u_short &status) {

  return getBits(REG_CSR1, MSK_BC, &status);
}

//------------------------------------------------------------------------------

int LTPi::getOrbit_Path(u_short &status) {

  return getBits(REG_CSR1, MSK_ORB, &status);
}

//------------------------------------------------------------------------------

int LTPi::getL1A_Path(u_short &status) {

  return getBits(REG_CSR1, MSK_L1A, &status);
}

//------------------------------------------------------------------------------

int LTPi::getTTR_Path(u_short &status) {

  return getBits(REG_CSR1, MSK_TTR, &status);
}

//------------------------------------------------------------------------------

int LTPi::getBGO_Path(u_short &status) {

  return getBits(REG_CSR2, MSK_BGO, &status);
}

//------------------------------------------------------------------------------

int LTPi::getTRT_Path(u_short &status) {

  return getBits(REG_CSR2, MSK_TRT, &status);
}

//------------------------------------------------------------------------------

int LTPi::getBusyCTP_Path(u_short &status) {

  return getBits(REG_CSR3, MSK_BUSY_CTP, &status);
}

//------------------------------------------------------------------------------

int LTPi::getBusyNIM_Path(u_short &status) {

  return getBits(REG_CSR3, MSK_BUSY_NIM, &status);
}

//------------------------------------------------------------------------------

int LTPi::getBusyLTP_Path(u_short &status) {

  return getBits(REG_CSR3, MSK_BUSY_LTP, &status);
}

//------------------------------------------------------------------------------

int LTPi::getCAL_Path(u_short &status) {

  return getBits(REG_CSR2, MSK_CAL, &status);
}

//------------------------------------------------------------------------------

int LTPi::getTRT_SELECTION(u_short &status) {

  return getBits(REG_CSR2, MSK_TRT_SELECTION,  &status);
}

//  -- set methods

//------------------------------------------------------------------------------

int LTPi::setBC_Path(u_short value) {

  return setBits(REG_CSR1, MSK_BC, value);
}

//------------------------------------------------------------------------------

int LTPi::setOrbit_Path(u_short value) {

  return setBits(REG_CSR1, MSK_ORB, value);
}

//------------------------------------------------------------------------------

int LTPi::setL1A_Path(u_short value) {

  return setBits(REG_CSR1, MSK_L1A, value);
}

//------------------------------------------------------------------------------

int LTPi::setTTR_Path(u_short value) {

  return setBits(REG_CSR1, MSK_TTR, value);
}

//------------------------------------------------------------------------------

int LTPi::setBGO_Path(u_short value) {

  return setBits(REG_CSR2, MSK_BGO, value);
}

//------------------------------------------------------------------------------

int LTPi::setTRT_Path(u_short value) {

  return setBits(REG_CSR2, MSK_TRT, value);
}

//------------------------------------------------------------------------------

int LTPi::setBusyCTP_Path(u_short value) {

  return setBits(REG_CSR3, MSK_BUSY_CTP, value);
}

//------------------------------------------------------------------------------

int LTPi::setBusyNIM_Path(u_short value) {

  return setBits(REG_CSR3, MSK_BUSY_NIM, value);
}

//------------------------------------------------------------------------------

int LTPi::setBusyLTP_Path(u_short value) {

  return setBits(REG_CSR3, MSK_BUSY_LTP, value);
}

//------------------------------------------------------------------------------

int LTPi::setCAL_Path(u_short value) {

  return setBits(REG_CSR2, MSK_CAL, value);
}

//------------------------------------------------------------------------------

int LTPi::setTRT_SELECTION(u_short value) {

  return setBits(REG_CSR2, MSK_TRT_SELECTION,  value);
}

//   -- Status Register test bit

//------------------------------------------------------------------------------

bool LTPi::isBC_CTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_BC_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isORB_CTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_ORB_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isL1A_CTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_L1A_CTP, &r);
  //return testBits(REG_STATUS1, MSK_SR_L1A_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBC_LTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_BC_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isORB_LTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_ORB_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isL1A_LTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_L1A_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBC_NIM() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_BC_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isORB_NIM() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_ORB_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isL1A_NIM() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_L1A_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBUSY_CTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_BUSY_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBUSY_LTPout() {

  bool r = false;
  testBits(REG_STATUS1, MSK_SR_BUSY_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR1_CTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR1_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR2_CTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR2_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR3_CTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR3_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR1_LTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR1_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR2_LTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR2_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR3_LTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR3_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR1_NIM() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR1_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR2_NIM() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR2_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTTR3_NIM() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_TTR3_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isCAL0_CTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_CAL0_CTP, &r);
  return !r;
}

//------------------------------------------------------------------------------

bool LTPi::isCAL1_CTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_CAL1_CTP, &r);
  return !r;
}

//------------------------------------------------------------------------------

bool LTPi::isCAL2_CTPout() {

  bool r = false;
  testBits(REG_STATUS2, MSK_SR_CAL2_CTP, &r);
  return !r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO0_CTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO0_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO1_CTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO1_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO2_CTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO2_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO3_CTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO3_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO0_LTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO0_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO1_LTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO1_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO2_LTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO2_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO3_LTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO3_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO0_NIM() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO0_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO1_NIM() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO1_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO2_NIM() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO2_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isBGO3_NIM() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_BGO3_NIM, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isCAL0_LTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_CAL0_LTP, &r);
  return !r;
}

//------------------------------------------------------------------------------

bool LTPi::isCAL1_LTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_CAL1_LTP, &r);
  return !r;
}

//------------------------------------------------------------------------------

bool LTPi::isCAL2_LTPout() {

  bool r = false;
  testBits(REG_STATUS3, MSK_SR_CAL2_LTP, &r);
  return !r;
}

//--------------------------------------------------
bool LTPi::isTRT0_CTPout()
{
  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT0_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT1_CTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT1_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT2_CTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT2_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT3_CTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT3_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT4_CTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT4_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT5_CTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT5_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT6_CTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT6_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT7_CTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT7_CTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT0_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT0_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT1_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT1_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT2_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT2_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT3_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT3_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT4_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT4_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT5_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT5_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT6_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT6_LTP, &r);
  return r;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT7_LTPout() {

  bool r = false;
  testBits(REG_STATUS4, MSK_SR_TRT7_LTP, &r);
  return r;
}

//   -- Set methods

//------------------------------------------------------------------------------

int LTPi::off_mode() {

  //No output going out of the module
  //CTP-link out, LTP-link out, NIM out disalbed
  int ierr = 0;
  ierr |= enableLtpInput(false);
  ierr |= enableCtpInput(false);
  ierr |= this->write(0x80, 0x0000);
  ierr |= this->write(0x82, 0x0000);
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::ctp_mode() {

// The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels
// are routed as:
// CTP-link out from CTP-link in
// LTP-link out from CTP-link in
// The CTP-link out is enabled and
// the delay applied to each channel set to zero
  int ierr = 0;
  // The gain and equalization settings for the CTP input should not be changed here !!!
  //ierr |= this->setupDAC4shortCTPCable();
  // to enable ctp/ltp input i2c needs to be initialised. First check if  i2c frequency is
  // set, if not setup i2c.
  if (this->readI2Cfreq() != 100.0) ierr = this->setupI2C();
  ierr |= enableLtpInput(false);
  ierr |= enableCtpInput(true);
  // The delay values are set to the module default
  ierr |= this->setDelayAllChannels();
  // The delay values should not be changed here
  //ierr |= this->enableCTPout(0);
  ierr |= this->BC_CTPoutFromCTPin_LTPoutFromCTPin();
  ierr |= this->Orbit_CTPoutFromCTPin_LTPoutFromCTPin();
  ierr |= this->L1A_CTPoutFromCTPin_LTPoutFromCTPin();
  ierr |= this->TTR_CTPoutFromCTPin_LTPoutFromCTPin();
  ierr |= this->BGO_CTPoutFromCTPin_LTPoutFromCTPin();
  ierr |= this->TRT_CTPoutFromCTPin_LTPoutFromCTPin();
  ierr |= this->calCTPfromCTP_LTPFromCTP();
  ierr |= this->busyCTP_FROM_CTP_AND_LTP();
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::ltp_mode() {

  // The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels
  // are routed as:
  // CTP-link out from LTP-link in
  // LTP-link out from LTP-link in
  int ierr = 0;
  // The gain and equalization settings for the LTP input should not be changed here !!!
  // ierr |= this->setupDAC4shortLTPCable();
  // to enable ctp/ltp input i2c needs to be initialised. First check if  i2c frequency is
  // set, if not setup i2c.
  if (this->readI2Cfreq() != 100.0) ierr = this->setupI2C();
  ierr |= enableCtpInput(false);
  ierr |= enableLtpInput(true);
  // The delay values are set to the module default
  ierr |= this->setDelayAllChannels();
  //ierr |= this->setupDAC_CTPCable_toZero();
  // The delay values should not be changed here
  //ierr |= this->enableCTPout(0);
  ierr |= this->BC_CTPoutFromLTPin_LTPoutFromLTPin();
  ierr |= this->Orbit_CTPoutFromLTPin_LTPoutFromLTPin();
  ierr |= this->L1A_CTPoutFromLTPin_LTPoutFromLTPin();
  ierr |= this->TTR_CTPoutFromLTPin_LTPoutFromLTPin();
  ierr |= this->BGO_CTPoutFromLTPin_LTPoutFromLTPin();
  ierr |= this->TRT_CTPoutFromLTPin_LTPoutFromLTPin();
  ierr |= this->calCTPfromLTP_LTPFromLTP();
  ierr |= this->busyLTP_FROM_CTP_AND_LTP();
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::nim_mode() {

  // The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels
  // are routed as:
  // CTP-link out from NIM in
  // LTP-link out from NIM in
  int ierr = 0;
  // to enable ctp/ltp input i2c needs to be initialised. First check if  i2c frequency is
  // set, if not setup i2c.
  if (this->readI2Cfreq() != 100.0) ierr = this->setupI2C();
  ierr |= enableCtpInput(false);
  ierr |= enableLtpInput(false);
  // The delay values are set to the module default
  ierr |= this->setDelayAllChannels();
  ierr |= this->BC_CTPoutFromNIMin_LTPoutFromNIMin();
  ierr |= this->Orbit_CTPoutFromNIMin_LTPoutFromNIMin();
  ierr |= this->L1A_CTPoutFromNIMin_LTPoutFromNIMin();
  ierr |= this->TTR_CTPoutFromNIMin_LTPoutFromNIMin();
  ierr |= this->BGO_CTPoutFromNIMin_LTPoutFromNIMin();
  ierr |= this->TRT_CTPoutFromLocal_LTPoutFromLocal();
  ierr |= this->busyNIM_FROM_CTP_AND_LTP();
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::ctpltp_mode() {

  // The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels
  // are routed as:
  // CTP-link out from CTP-link in
  // LTP-link out from LTP-link in
  // The CTP-link out is enabled and
  // the delay applied to each channel set to zero
  int ierr = 0;
  // The gain and equalization settings for the CTP input should not be changed here !!!
  //ierr |= this->setupDAC4shortCTPCable();

  // to enable ctp/ltp input i2c needs to be initialised. First check if  i2c frequency is
  // set, if not setup i2c.
  if (this->readI2Cfreq() != 100.0) ierr = this->setupI2C();
  ierr |= enableCtpInput(true);
  // The gain and equalization settings for the LTP input should not be changed here !!!
  //ierr |= this->setupDAC4shortLTPCable();
  ierr |= enableLtpInput(true);
  // The delay values are set to the module default
  ierr |= this->setDelayAllChannels();
  ierr |= this->BC_CTPoutFromCTPin_LTPoutFromLTPin();
  ierr |= this->Orbit_CTPoutFromCTPin_LTPoutFromLTPin();
  ierr |= this->L1A_CTPoutFromCTPin_LTPoutFromLTPin();
  ierr |= this->TTR_CTPoutFromCTPin_LTPoutFromLTPin();
  ierr |= this->BGO_CTPoutFromCTPin_LTPoutFromLTPin();
  ierr |= this->TRT_CTPoutFromCTPin_LTPoutFromLTPin();
  ierr |= this->calCTPfromCTP_LTPFromLTP();
  ierr |= this->busyCTP_FROM_CTP();
  ierr |= this->busyLTP_FROM_LTP();
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::nimltp_mode() {

  // The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels
  // are routed as:
  // CTP-link out from NIM in
  // LTP-link out from LTP-link in
  int ierr = 0;
  // to enable ctp/ltp input i2c needs to be initialised. First check if  i2c frequency is
  // set, if not setup i2c.
  if (this->readI2Cfreq() != 100.0) ierr = this->setupI2C();
  ierr |= enableCtpInput(false);
  ierr |= enableLtpInput(true);
  // The gain and equalization settings for the LTP input should not be changed here !!!
  //ierr |= this->setupDAC4shortLTPCable();
  // The delay values are set to the module default
  ierr |= this->setDelayAllChannels();
  ierr |= this->BC_CTPoutFromNIMin_LTPoutFromLTPin();
  ierr |= this->Orbit_CTPoutFromNIMin_LTPoutFromLTPin();
  ierr |= this->L1A_CTPoutFromNIMin_LTPoutFromLTPin();
  ierr |= this->TTR_CTPoutFromNIMin_LTPoutFromLTPin();
  ierr |= this->BGO_CTPoutFromNIMin_LTPoutFromLTPin();
  ierr |= this->TRT_CTPoutFromLocal_LTPoutFromLTPin();
  ierr |= this->busyNIM_FROM_CTP();
  ierr |= this->busyLTP_FROM_LTP();
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::BC_Off() {

  return setBits(REG_CSR1, MSK_BC, BC_OFF);
}

//------------------------------------------------------------------------------

int LTPi::Orbit_Off() {

  return setBits(REG_CSR1, MSK_ORB, ORB_OFF);
}

//------------------------------------------------------------------------------

int LTPi::L1A_Off() {

  return setBits(REG_CSR1, MSK_L1A, L1A_OFF);
}

//------------------------------------------------------------------------------

int LTPi::TTR_Off() {

  return setBits(REG_CSR1, MSK_TTR, TTR_OFF);
}

//------------------------------------------------------------------------------

int LTPi::BGO_Off() {

  return setBits(REG_CSR2, MSK_BGO, BGO_OFF);
}

//------------------------------------------------------------------------------

int LTPi::TRT_Off() {

  return setBits(REG_CSR2, MSK_TRT, TRT_OFF);
}

//------------------------------------------------------------------------------

int LTPi::BC_CTPoutFromCTPin_LTPoutFromCTPin() {

  return setBits(REG_CSR1, MSK_BC, BC_FROM_CTP);
}

//------------------------------------------------------------------------------

int LTPi::Orbit_CTPoutFromCTPin_LTPoutFromCTPin() {

  return setBits(REG_CSR1, MSK_ORB, ORB_FROM_CTP);
}

//------------------------------------------------------------------------------

int LTPi::L1A_CTPoutFromCTPin_LTPoutFromCTPin() {

  return setBits(REG_CSR1, MSK_L1A, L1A_FROM_CTP);
}

//------------------------------------------------------------------------------

int LTPi::TTR_CTPoutFromCTPin_LTPoutFromCTPin() {

  return setBits(REG_CSR1, MSK_TTR, TTR_FROM_CTP);
}

//------------------------------------------------------------------------------

int LTPi::BGO_CTPoutFromCTPin_LTPoutFromCTPin() {

  return setBits(REG_CSR2, MSK_BGO, BGO_FROM_CTP);
}

//------------------------------------------------------------------------------

int LTPi::TRT_CTPoutFromCTPin_LTPoutFromCTPin() {

  return setBits(REG_CSR2, MSK_TRT, TRT_FROM_CTP);
}

//------------------------------------------------------------------------------

int LTPi::BC_CTPoutFromLTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_BC, BC_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::Orbit_CTPoutFromLTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_ORB, ORB_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::L1A_CTPoutFromLTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_L1A, L1A_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::TTR_CTPoutFromLTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_TTR, TTR_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::BGO_CTPoutFromLTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR2, MSK_BGO, BGO_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::TRT_CTPoutFromLTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR2, MSK_TRT, TRT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::BC_CTPoutFromNIMin_LTPoutFromNIMin() {

  return setBits(REG_CSR1, MSK_BC, BC_FROM_NIM);
}

//------------------------------------------------------------------------------

int LTPi::Orbit_CTPoutFromNIMin_LTPoutFromNIMin() {

  return setBits(REG_CSR1, MSK_ORB, ORB_FROM_NIM);
}

//------------------------------------------------------------------------------

int LTPi::L1A_CTPoutFromNIMin_LTPoutFromNIMin() {

  return setBits(REG_CSR1, MSK_L1A, L1A_FROM_NIM);
}

//------------------------------------------------------------------------------

int LTPi::TTR_CTPoutFromNIMin_LTPoutFromNIMin() {

  return setBits(REG_CSR1, MSK_TTR, TTR_FROM_NIM);
}

//------------------------------------------------------------------------------

int LTPi::BGO_CTPoutFromNIMin_LTPoutFromNIMin() {

  return setBits(REG_CSR2, MSK_BGO, BGO_FROM_NIM);
}

//------------------------------------------------------------------------------

int LTPi::TRT_CTPoutFromLocal_LTPoutFromLocal() {

  return setBits(REG_CSR2, MSK_TRT, TRT_FROM_LOCAL);
}

//------------------------------------------------------------------------------

int LTPi::BC_CTPoutFromCTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_BC, BC_FROM_CTP_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::Orbit_CTPoutFromCTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_ORB, ORB_FROM_CTP_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::L1A_CTPoutFromCTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_L1A, L1A_FROM_CTP_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::TTR_CTPoutFromCTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_TTR, TTR_FROM_CTP_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::BGO_CTPoutFromCTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR2, MSK_BGO, BGO_FROM_CTP_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::TRT_CTPoutFromCTPin_LTPoutFromLTPin() {

  return setBits(REG_CSR2, MSK_TRT, TRT_FROM_CTP_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::BC_CTPoutFromNIMin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_BC, BC_FROM_NIM_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::Orbit_CTPoutFromNIMin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_ORB, ORB_FROM_NIM_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::L1A_CTPoutFromNIMin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_L1A, L1A_FROM_NIM_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::TTR_CTPoutFromNIMin_LTPoutFromLTPin() {

  return setBits(REG_CSR1, MSK_TTR, TTR_FROM_NIM_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::BGO_CTPoutFromNIMin_LTPoutFromLTPin() {

  return setBits(REG_CSR2, MSK_BGO, BGO_FROM_NIM_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::TRT_CTPoutFromLocal_LTPoutFromLTPin()
{
  return setBits(REG_CSR2, MSK_TRT, TRT_FROM_LOCAL_LTPOUT_FROM_LTP);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPi::TRT_FROM_LOCAL_REGISTER() {

  return setBits(REG_CSR2, MSK_TRT_SELECTION,  SEL_TTR_TEST);
}

//------------------------------------------------------------------------------

int LTPi:: TRT_DERIVED_FROM_CTP_TTR() {

  return setBits(REG_CSR2, MSK_TRT_SELECTION,  SEL_TTR_CTP);
}

//------------------------------------------------------------------------------

int LTPi:: TRT_DERIVED_FROM_LTP_TTR() {

  return setBits(REG_CSR2, MSK_TRT_SELECTION,  SEL_TTR_LTP);
}

//------------------------------------------------------------------------------

int LTPi::TRT_DERIVED_FROM_NIM_TTR() {

  return setBits(REG_CSR2, MSK_TRT_SELECTION,  SEL_TTR_NIM);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int LTPi::SetTRT(int ttrig, u_short value) {

  u_short addr = REG_TTYPE1 + (ttrig & 6);

  if (ttrig & 1) return setBits(addr, MSK_TTYPE1, value << 8);
  return setBits(addr, MSK_TTYPE0, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT000( u_short value) {

  if (MSK_TTYPE0 > 0xff) value = value << 8;
  return setBits(REG_TTYPE1, MSK_TTYPE0, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT001( u_short value) {

  if (MSK_TTYPE1 > 0xff) value = value << 8;
  return setBits(REG_TTYPE1, MSK_TTYPE1, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT010( u_short value) {
  if (MSK_TTYPE2 > 0xff) value = value << 8;
  return setBits(REG_TTYPE2, MSK_TTYPE2, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT011( u_short value) {

  if (MSK_TTYPE3 > 0xff) value = value << 8;
  return setBits(REG_TTYPE2, MSK_TTYPE3, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT100( u_short value) {

  if (MSK_TTYPE4 > 0xff) value = value << 8;
  return setBits(REG_TTYPE3, MSK_TTYPE4, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT101( u_short value) {

  if (MSK_TTYPE5 > 0xff) value = value << 8;
  return setBits(REG_TTYPE3, MSK_TTYPE5, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT110( u_short value) {

  if (MSK_TTYPE6 > 0xff) value = value << 8;
  return setBits(REG_TTYPE4, MSK_TTYPE6, value);
}

//------------------------------------------------------------------------------

int LTPi::SetTRT111( u_short value) {

  if (MSK_TTYPE7 > 0xff) value = value << 8;
  return setBits(REG_TTYPE4, MSK_TTYPE7, value);
}

// -- read methods for TRT

//------------------------------------------------------------------------------

int LTPi::GetTRT(int ttrig, u_short& value) {

  u_short addr = REG_TTYPE1 + (ttrig & 6);
  u_short data;

  int ierr = getBits(addr, 0xffff, &data);
  if (ttrig & 1) data >>= 8;
  value = data & 0xff;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT000(u_short& value) {

  int ierr = getBits(REG_TTYPE1, MSK_TTYPE0, &value);
  if (MSK_TTYPE0 > 0xff) value = value >> 8;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT001(u_short& value) {

  int ierr = getBits(REG_TTYPE1, MSK_TTYPE1, &value);
  if (MSK_TTYPE1 > 0xff) value = value >> 8;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT010(u_short& value) {

  int ierr = getBits(REG_TTYPE2, MSK_TTYPE2, &value);
  if (MSK_TTYPE2 > 0xff) value = value >> 8;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT011(u_short& value) {

  int ierr = getBits(REG_TTYPE2, MSK_TTYPE3, &value);
  if (MSK_TTYPE3 > 0xff) value = value >> 8;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT100(u_short& value) {

  int ierr = getBits(REG_TTYPE3, MSK_TTYPE4, &value);
  if (MSK_TTYPE4 > 0xff) value = value >> 8;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT101(u_short& value) {

  int ierr = getBits(REG_TTYPE3, MSK_TTYPE5, &value);
  if (MSK_TTYPE5 > 0xff) value = value >> 8;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT110(u_short& value) {

  int ierr = getBits(REG_TTYPE4, MSK_TTYPE6, &value);
  if (MSK_TTYPE6 > 0xff) value = value >> 8;
  return ierr;
}

//------------------------------------------------------------------------------

int LTPi::GetTRT111(u_short& value)
{
  int ierr = getBits(REG_TTYPE4, MSK_TTYPE7, &value);
  if (MSK_TTYPE7 > 0xff) value = value >> 8;
  return ierr;
}

// BUSY SETTING

//------------------------------------------------------------------------------

int  LTPi::busyCTP_OFF() {

  return setBits(REG_CSR3, MSK_BUSY_CTP, BUSY_CTP_OFF);
}

//------------------------------------------------------------------------------

int  LTPi::busyCTP_FROM_LTP() {

  return setBits(REG_CSR3, MSK_BUSY_CTP, BUSY_CTP_FROM_LTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyCTP_FROM_CTP() {

  return setBits(REG_CSR3, MSK_BUSY_CTP, BUSY_CTP_FROM_CTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyCTP_FROM_CTP_AND_LTP() {

  return setBits(REG_CSR3, MSK_BUSY_CTP, BUSY_CTP_FROM_CTP_AND_LTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyNIM_OFF() {

  return setBits(REG_CSR3, MSK_BUSY_NIM, BUSY_NIM_OFF);
}

//------------------------------------------------------------------------------

int  LTPi::busyNIM_FROM_LTP() {

  return setBits(REG_CSR3, MSK_BUSY_NIM, BUSY_NIM_FROM_LTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyNIM_FROM_CTP() {

  return setBits(REG_CSR3, MSK_BUSY_NIM, BUSY_NIM_FROM_CTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyNIM_FROM_CTP_AND_LTP() {

  return setBits(REG_CSR3, MSK_BUSY_NIM, BUSY_NIM_FROM_CTP_AND_LTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyLTP_OFF() {

  return setBits(REG_CSR3, MSK_BUSY_LTP, BUSY_LTP_OFF);
}

//------------------------------------------------------------------------------

int  LTPi::busyLTP_FROM_LTP() {

  return setBits(REG_CSR3, MSK_BUSY_LTP, BUSY_LTP_FROM_LTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyLTP_FROM_CTP() {

  return setBits(REG_CSR3, MSK_BUSY_LTP, BUSY_LTP_FROM_CTP);
}

//------------------------------------------------------------------------------

int  LTPi::busyLTP_FROM_CTP_AND_LTP() {

  return setBits(REG_CSR3, MSK_BUSY_LTP, BUSY_LTP_FROM_CTP_AND_LTP);
}

// --calibration request selection

//------------------------------------------------------------------------------

int LTPi::calCTPfromCTP_LTPFromLTP() {

  return setBits(REG_CSR2, MSK_CAL, CAL_CTP_FROM_CTP_LTP_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::calCTPfromLTP_LTPFromLTP() {

  return setBits(REG_CSR2, MSK_CAL, CAL_FROM_LTP);
}

//------------------------------------------------------------------------------

int LTPi::calCTPfromCTP_LTPFromCTP() {

  return setBits(REG_CSR2, MSK_CAL, CAL_FROM_CTP);
}

//------------------------------------------------------------------------------

int LTPi::dumpCalibrationStatus() {

  int ierr = 0;
  u_short bits = 0;
  ierr = getCAL_Path(bits);
  std::printf("\n");
  std::printf("Calibration Request Path: \n");

  if ((bits & MSK_CAL) == 0)
    std::printf("***** Disabled \n");
  if ((bits & MSK_CAL) == CAL_CTP_FROM_CTP_LTP_FROM_LTP)
    std::printf("***** CTP-Link in from CTP-link out; LTP-Link in from LTP-link out \n");
  if ((bits & MSK_CAL) == CAL_FROM_CTP)
    std::printf("***** CTP-Link in from CTP-link out; LTP-Link in from CTP-link out \n");
  if ((bits & MSK_CAL) == CAL_FROM_LTP)
    std::printf("***** CTP-Link in from LTP-link out; LTP-Link in from LTP-link out \n");
  if ((bits & MSK_CAL) == 0)
    std::printf("***** Path not configured \n");

  std::cout << "" << std::endl;
  std::cout << "Calibration Request Status:" << std::endl;
  std::cout << "***** CAL0 signal on CTPout link is:";
  (this->isCAL0_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** CAL1 signal on CTPout link is:";
  (this->isCAL1_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** CAL2 signal on CTPout link is:";
  (this->isCAL2_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  std::cout << "***** CAL0 signal on LTPout link is:";
  (this->isCAL0_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** CAL1 signal on LTPout link is:";
  (this->isCAL1_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** CAL2 signal on LTPout link is:";
  (this->isCAL2_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return(ierr);
}

//------------------------------------------------------------------------------

string LTPi::CTPout_BUSY() {

  u_short bits_ctp = 0;
  string busy_ctp;

  getBusyCTP_Path(bits_ctp);

  if ((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_OFF)
    busy_ctp="OFF";
  if((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_FROM_LTP)
    busy_ctp="LTP_OUT";
  if((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_FROM_CTP)
    busy_ctp="CTP_OUT";
  if((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_FROM_CTP_AND_LTP)
    busy_ctp="CTP_OUT & LTP_OUT";

  return busy_ctp;
}

//------------------------------------------------------------------------------

string LTPi::LTPout_BUSY() {

  u_short bits_ltp = 0;
  string busy_ltp;
  getBusyLTP_Path(bits_ltp);

  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_OFF)
    busy_ltp="OFF";
  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_FROM_LTP)
    busy_ltp="LTP_OUT";
  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_FROM_CTP)
    busy_ltp="CTP_OUT";
  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_FROM_CTP_AND_LTP)
    busy_ltp="CTP_OUT & LTP_OUT";

  return busy_ltp;
}

//------------------------------------------------------------------------------

string LTPi::NIM_BUSY() {

  u_short bits_nim = 0;

  string busy_nim;

  getBusyNIM_Path(bits_nim);
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_OFF)
    busy_nim="OFF";
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_FROM_LTP)
    busy_nim="LTP_OUT";
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_FROM_CTP)
    busy_nim="CTP_OUT";
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_FROM_CTP_AND_LTP)
    busy_nim="CTP_OUT & LTP_OUT";

  return busy_nim;
}

//------------------------------------------------------------------------------

int LTPi::dumpBusyStatus() {

  int ierr = 0;
  u_short bits_ctp = 0;
  u_short bits_nim = 0;
  u_short bits_ltp = 0;
  ierr |= getBusyCTP_Path(bits_ctp);
  ierr |= getBusyNIM_Path(bits_nim);
  ierr |= getBusyLTP_Path(bits_ltp);

  std::printf("\n");
  std::printf("BUSY Path: \n");

  std::printf("\n");
  std::printf("BUSY signal CTP-link in from: \n");
  if((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_OFF)
    std::printf("***** Disabled \n");
  if((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_FROM_LTP)
    std::printf("***** LTP-link out \n");
  if((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_FROM_CTP)
    std::printf("***** CTP-link out \n");
  if((bits_ctp & MSK_BUSY_CTP) == BUSY_CTP_FROM_CTP_AND_LTP)
    std::printf("***** CTP-link out and LTP-link out \n");

  std::printf("\n");
  std::printf("BUSY signal LTP-link in from: \n");
  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_OFF)
    std::printf("***** Disabled \n");
  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_FROM_LTP)
    std::printf("***** LTP-link out \n");
  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_FROM_CTP)
    std::printf("***** CTP-link out \n");
  if((bits_ltp & MSK_BUSY_LTP) == BUSY_LTP_FROM_CTP_AND_LTP){
    std::printf("***** CTP-link out and LTP-link out \n");
  }

    std::printf("\n");
  std::printf("BUSY signal NIM in from: \n");
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_OFF)
    std::printf("***** Disabled \n");
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_FROM_LTP)
    std::printf("***** LTP-link out \n");
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_FROM_CTP)
    std::printf("***** CTP-link out \n");
  if((bits_nim & MSK_BUSY_NIM) == BUSY_NIM_FROM_CTP_AND_LTP)
    std::printf("***** CTP-link out and LTP-link out \n");

  std::cout << "" << std::endl;
  std::cout << "BUSY Status:" << std::endl;
  std::cout << "***** BUSY signal on CTPout link is:";
  (this->isBUSY_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** BUSY signal on LTPout link is:";
  (this->isBUSY_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::dumpModuleRegisters() {

  int ierr = 0;
  u_short bits = 0;
  u_short mask = 0xffff;

  std::printf("\n");
  std::cout << "CONTROL REGISTERS >>>>>>" << std::endl;
  ierr = getBits(REG_CSR1, mask, &bits);
  string s = printBinary16(bits);
  std::cout << "CONTROL REGISTER 1 " << s << std::endl;

  ierr = getBits(REG_CSR2, mask, &bits);
  s = printBinary16(bits);
  std::cout << "CONTROL REGISTER 2 " << s << std::endl;

  ierr = getBits(REG_CSR3, mask, &bits);
  s = printBinary16(bits);
  std::cout << "CONTROL REGISTER 3 " << s << std::endl;

  std::printf("\n");
  std::cout << "STATUS REGISTERS >>>>>>" << std::endl;
  ierr = getBits(REG_STATUS1, mask, &bits);
  s = printBinary16(bits);
  std::cout << "STATUS REGISTER 1 " << s << std::endl;

  ierr = getBits(REG_STATUS2, mask, &bits);
  s = printBinary16(bits);
  std::cout << "STATUS REGISTER 2 " << s << std::endl;

  ierr = getBits(REG_STATUS3, mask, &bits);
  s = printBinary16(bits);
  std::cout << "STATUS REGISTER 3 " << s << std::endl;

  ierr = getBits(REG_STATUS4, mask, &bits);
  s = printBinary16(bits);
  std::cout << "STATUS REGISTER 4 " << s << std::endl;

  std::printf("\n");
  std::cout << "TRIGGER TYPE REGISTERS >>>>>>" << std::endl;
  ierr = getBits(REG_TTYPE1, mask, &bits);
  s = printBinary16(bits);
  std::cout << "TRIGGER TYPE REGISTER 1 " << s << std::endl;

  ierr = getBits(REG_TTYPE2, mask, &bits);
  s = printBinary16(bits);
  std::cout << "TRIGGER TYPE REGISTER 2 " << s << std::endl;

  ierr = getBits(REG_TTYPE3, mask, &bits);
  s = printBinary16(bits);
  std::cout << "TRIGGER TYPE REGISTER 3 " << s << std::endl;

  ierr = getBits(REG_TTYPE4, mask, &bits);
  s = printBinary16(bits);
  std::cout << "TRIGGER TYPE REGISTER 4 " << s << std::endl;

  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::dumpTRTStatus() {

  int ierr = 0;
  u_short bits = 0;
  ierr = getTRT_Path(bits);
  std::printf("\n");
  std::printf("Trigger Type path: \n");
  //string s = printBinary16(bits);

  //std::cout << "bits = " << s << std::endl;

  if ((bits & MSK_TRT) == TRT_FROM_CTP_LTPOUT_FROM_LTP)
    std::printf("***** CTP-link out from CTP-link in; LTP-link out from LTP-link in \n");
  if ((bits &  MSK_TRT) ==  TRT_FROM_CTP)
    std::printf("***** CTP-link out from CTP-link in; LTP-link out from CTP-link in \n");
  if ((bits &  MSK_TRT) ==  TRT_FROM_LTP)
    std::printf("***** CTP-link out from LTP-link in; LTP-link out from LTP-link in \n");
  if ((bits &  MSK_TRT) ==  TRT_FROM_LOCAL)
    std::printf("***** CTP-link out from local; LTP-link out from Local \n");
  if ((bits &  MSK_TRT) ==  TRT_FROM_LOCAL_LTPOUT_FROM_LTP)
    std::printf("***** CTP-link out from local; LTP-link out from LTP-link in \n");
  if ((bits &  MSK_TRT) ==  0)
    std::printf("***** Path not configured \n");

  std::cout << "" << std::endl;
  std::cout << "Trigger Type Status:" << std::endl;
  std::cout << "***** TRT0 signal on CTPout link is:";
  (this->isTRT0_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT1 signal on CTPout link is:";
  (this->isTRT1_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT2 signal on CTPout link is:";
  (this->isTRT2_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT3 signal on CTPout link is:";
  (this->isTRT3_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT4 signal on CTPout link is:";
  (this->isTRT4_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT5 signal on CTPout link is:";
  (this->isTRT5_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT6 signal on CTPout link is:";
  (this->isTRT6_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT7 signal on CTPout link is:";
  (this->isTRT7_CTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  std::cout << "***** TRT0 signal on LTPout link is:";
  (this->isTRT0_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT1 signal on LTPout link is:";
  (this->isTRT1_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT2 signal on LTPout link is:";
  (this->isTRT2_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT3 signal on LTPout link is:";
  (this->isTRT3_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT4 signal on LTPout link is:";
  (this->isTRT4_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT5 signal on LTPout link is:";
  (this->isTRT5_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT6 signal on LTPout link is:";
  (this->isTRT6_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;
  std::cout << "***** TRT7 signal on LTPout link is:";
  (this->isTRT7_LTPout() == 1) ?
    std::cout << " Active" << std::endl : std::cout << " NOT Active" << std::endl;

  return(ierr);
}

//------------------------------------------------------------------------------

bool LTPi::isTRT_FROM_CTP_LTPOUT_FROM_LTP() {

  u_short bits=0;
  getTRT_Path(bits);
  bool isTrue=0;
  isTrue = ((bits & MSK_TRT) == TRT_FROM_CTP_LTPOUT_FROM_LTP);
  return isTrue;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT_FROM_CTP() {

  u_short bits=0;
  getTRT_Path(bits);
  bool isTrue=0;
  isTrue = ((bits & MSK_TRT) == TRT_FROM_CTP);
  return isTrue;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT_FROM_LTP() {

  u_short bits=0;
  getTRT_Path(bits);
  bool isTrue=0;
  isTrue = ((bits & MSK_TRT) == TRT_FROM_LTP);
  return isTrue;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT_FROM_LOCAL() {

  u_short bits=0;
  getTRT_Path(bits);
  bool isTrue=0;
  isTrue = ((bits & MSK_TRT) == TRT_FROM_LOCAL);
  return isTrue;
}

//------------------------------------------------------------------------------

bool LTPi::isTRT_FROM_LOCAL_LTPOUT_FROM_LTP() {

  u_short bits=0;
  getTRT_Path(bits);
  bool isTrue=0;
  isTrue = ((bits & MSK_TRT) == TRT_FROM_LOCAL_LTPOUT_FROM_LTP);
  return isTrue;
}

//------------------------------------------------------------------------------

int LTPi::dumpTRTinput() {

  int ierr = 0;
  u_short bits = 0;
  ierr = getTRT_SELECTION(bits);
  string s = printBinary16(bits);
  std::printf("\n");
  std::printf("Trigger Type Input:\n");
  //std::cout << "bits = " << s << std::endl;

  if ((bits & MSK_TRT_SELECTION) == SEL_TTR_TEST)
    std::printf("***** No Test Triggers considered; Trigger type assigned from register \n");
  if ((bits &  MSK_TRT_SELECTION) ==  SEL_TTR_CTP)
    std::printf("***** Test Triggers from CTP-link in \n");
  if ((bits &  MSK_TRT_SELECTION) ==  SEL_TTR_LTP)
    std::printf("***** Test Triggers from LTPin \n");
  if ((bits &  MSK_TRT_SELECTION) ==  SEL_TTR_NIM)
    std::printf("***** Test Triggers from NIMin \n");

  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::dumpTRTTable() {

  int ierr  = 0;
  u_short value = 0;
  std::printf("\n");
  std::printf("Trigger Type Table: \n");
  ierr |= GetTRT000(value);
  std::printf("TRT000 = 0x%02x \n", value);
  ierr |= GetTRT001(value);
  std::printf("TRT001 = 0x%02x \n", value);
  ierr |= GetTRT010(value);
  std::printf("TRT010 = 0x%02x \n", value);
  ierr |= GetTRT011(value);
  std::printf("TRT011 = 0x%02x \n", value);
  ierr |= GetTRT100(value);
  std::printf("TRT100 = 0x%02x \n", value);
  ierr |= GetTRT101(value);
  std::printf("TRT101 = 0x%02x \n", value);
  ierr |= GetTRT110(value);
  std::printf("TRT110 = 0x%02x \n", value);
  ierr |= GetTRT111(value);
  std::printf("TRT111 = 0x%02x \n", value);
  return(ierr);
}

//------------------------------------------------------------------------------

int LTPi::writeprom4(u_int offset, u_int concat) {

  int loop;
  u_short data[4];
  int rtnv = 0;

  data[0] = (concat >> 24) & 0xff;
  data[1] = (concat >> 16) & 0xff;
  data[2] = (concat >> 8) & 0xff;
  data[3] = concat & 0xff;

  for(loop = 0; loop < 4; loop++)
    {
      //printf("writing to the address: 0x%08x, the data: 0x%08x \n",(offset + 2 + loop * 4), data[loop]);
      rtnv |= m_vmm->WriteSafe((offset + 2 + loop * 4), data[loop]);
      //ts_delay(5000);
      // for test purpose
      timespec wait_time;
      wait_time.tv_sec = 0;
      wait_time.tv_nsec = 5000000;
      nanosleep(&wait_time, nullptr);
    }
  return rtnv;
}

//------------------------------------------------------------------------------

int LTPi::readprom4(u_int offset, u_int *concat) {

  u_int loop;
  u_short data[4];
  int rtnv = 0;

  for(loop = 0; loop < 4; loop++)
    {
      rtnv |= m_vmm->ReadSafe((offset + 2 + loop * 4), &data[loop]);
      data[loop] &= 0xff;
    }

  *concat = ( u_int(data[0]) << 24) || ( u_int(data[1]) << 16) || ( u_int(data[2]) << 8) || u_int(data[3]);
  return rtnv;
}

//------------------------------------------------------------------------------

int LTPi::flash_read(u_short addr, u_short &data) {

  int rtnv = 0;
  u_short busy = SFL_BUSY;

  rtnv |= m_vmm->WriteSafe(SFL_ADDR_REG, addr);
  rtnv |= m_vmm->WriteSafe(SFL_CSR_REG, SFL_READ);
  while (!rtnv && (busy & SFL_BUSY)) rtnv |= m_vmm->ReadSafe(SFL_CSR_REG, &busy);
  rtnv |= m_vmm->ReadSafe(SFL_RDAT_REG, &data);
  data &= 0xff;
  return rtnv;
}

//------------------------------------------------------------------------------

int LTPi::flash_write(u_short addr, u_short data) {

  int rtnv = 0;
  u_short busy = SFL_BUSY;

  rtnv |= m_vmm->WriteSafe(SFL_ADDR_REG, addr);
  rtnv |= m_vmm->WriteSafe(SFL_WDAT_REG, data);
  rtnv |= m_vmm->WriteSafe(SFL_CSR_REG, SFL_WRITE);
  while (!rtnv && (busy & SFL_BUSY)) rtnv |= m_vmm->ReadSafe(SFL_CSR_REG, &busy);
  return rtnv;
}

//------------------------------------------------------------------------------

int LTPi::read_base_addr(u_short &baseaddr) {

  int rtnv = 0;
  u_short data;

  this->flash_read(BASE_ADDR_OFFSET, data);
  baseaddr = data & 0xff;
  this->flash_read(BASE_ADDR_OFFSET+1, data);
  baseaddr |= (data & 0xff) << 8;
  return rtnv;
}

//------------------------------------------------------------------------------

int LTPi::write_base_addr(u_short baseaddr) {

  int rtnv = 0;
  u_short busy = SFL_BUSY;
  u_short addr;

  rtnv |= this->read_base_addr(addr);
  if (addr == baseaddr) return rtnv;
  rtnv |= m_vmm->WriteSafe(SFL_CSR_REG, SFL_ERASE);
  while (!rtnv && (busy & SFL_BUSY)) rtnv |= m_vmm->ReadSafe(SFL_CSR_REG, &busy);
  this->flash_write(BASE_ADDR_OFFSET, baseaddr);
  this->flash_write(BASE_ADDR_OFFSET+1, baseaddr >> 8);
  return rtnv;
}
