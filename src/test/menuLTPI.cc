//******************************************************************************
// file: menuLTPI.cc
// desc: menu LTPI module
// auth: 08-JUL-2013, R. Spiwoks (rewrite from menuRCDLtpi.cc)
//******************************************************************************

#include "RCDLtpi/LTPI.h"
#include "RCDUtilities/RCDUtilities.h"
#include "RCDMenu/RCDMenu.h"
#include <unistd.h>    /* for getopt */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <sstream>

using namespace RCD;

// VME singleton
VME* vme;

// LTPI module and configuration
LTPI* ltpi;
LTPI::Configuration cfg;

// menu program configuration
u_int base_addr(0xee1000);
bool base_addr_flag(false);
bool expert_mode(false);

//------------------------------------------------------------------------------

class LTPIOpen : public MenuItem {
  public:
    LTPIOpen() { setName("Open LTPI"); }
    int action() {

        int rtnv = 0;

        // open VME driver/library
        vme = VME::Open();

        // get module offset
        if(!base_addr_flag) {
            base_addr = (u_int)enterHex("<base>",0,0x00ffffff);
        }
        if(base_addr < 0x100) base_addr = base_addr << 24;

        // create LTPI module
        ltpi = new LTPI(vme,base_addr);
        if((*ltpi)() != LTPI::SUCCESS) {
            CERR("opening module \"LTPI\" at vme = 0x%08x, size = 0x%08x, type = \"%s\"",base_addr,LTPI::size(),LTPI::code().c_str());
            std::exit(-1);
        }
        COUT("opened module \"LTPI\" at vme = 0x%08x, size = 0x%08x, type = \"%s\"",base_addr,LTPI::size(),LTPI::code().c_str());

        bool ready;
        if((rtnv = ltpi->ready(ready)) != LTPI::SUCCESS) {
            CERR("reading ready status of LTPI","");
            return(rtnv);
        }
        if(!ready) {
            if((rtnv = ltpi->init()) != LTPI::SUCCESS) {
                CERR("initializing LTPI","");
                return(rtnv);
            }
        }

        unsigned int manuf,board, revision;
        if((rtnv = ltpi->readEEPROM(manuf,board,revision)) != LTPI::SUCCESS) {
            CERR("reading EEPROM","");
            return(rtnv);
        }
        std::cout << std::endl;
        std::cout << "****************************" << std::endl;
        std::printf("Manufacturer ID =   0x%06x\n",manuf);
        std::printf("Board ID        = 0x%08x\n",board);
        std::printf("Revision number = 0x%08x\n",revision);
        std::cout << "****************************" << std::endl;

        if(expert_mode) {
            std::cout << std::endl;
            std::cout << "****************************" << std::endl;
            std::cout << "EXPERT MODE !!!!" << std::endl;
            std::cout << "****************************" << std::endl;
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class LTPIClose : public MenuItem {
  public:
    LTPIClose() { setName("Close LTPI"); }
    int action() {

        // delete LTPI module
        delete ltpi;

        // close VME driver/library
        VME::Close();
        COUT("closed module \"LTPI\"","");

        return(0);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class OpenConfiguration : public MenuItem {
  public:
    OpenConfiguration() { setName("open configuration object"); }
    int action() {

        int rtnv(0);

        if((rtnv = ltpi->read(cfg)) != LTPI::SUCCESS) {
             CERR("reading configuration object from LTPI","");
             return(rtnv);
        }
        std::printf("read configuration object from LTPI\n");

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class PrintConfiguration : public MenuItem {
  public:
    PrintConfiguration() { setName("print configuration object"); }
    int action() {

        cfg.print();

        return(0);
    }
};

//------------------------------------------------------------------------------

class ReadConfigurationFromLTPI : public MenuItem {
  public:
    ReadConfigurationFromLTPI() { setName("read  configuration object from LTPI"); }
    int action() {

        int rtnv(0);

        if((rtnv = ltpi->read(cfg)) != LTPI::SUCCESS) {
             CERR("reading configuration object from LTPI","");
             return(rtnv);
        }
        cfg.print();

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ReadConfigurationFromFile : public MenuItem {
  public:
    ReadConfigurationFromFile() { setName("read  configuration object from file"); }
    int action() {

        std::string fn;
        int rtnv(0);

        fn = enterString("File name");
        if((rtnv = cfg.read(fn)) != LTPI::SUCCESS) {
             CERR("reading configuration object from file \"%s\"",fn.c_str());
             return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteConfigurationToLTPI : public MenuItem {
  public:
    WriteConfigurationToLTPI() { setName("write configuration object to   LTPI"); }
    int action() {

        int rtnv(0);

        if((rtnv = ltpi->write(cfg)) != LTPI::SUCCESS) {
             CERR("writing configuration object to LTPI","");
             return(rtnv);
        }
        cfg.print();

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteConfigurationToFile : public MenuItem {
  public:
    WriteConfigurationToFile() { setName("write configuration object to   file"); }
    int action() {

        std::string fn;
        int rtnv(0);

        fn = enterString("File name");
        if((rtnv = cfg.write(fn)) != LTPI::SUCCESS) {
             CERR("writing configuration object to file \"%s\"",fn.c_str());
             return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ResetConfiguration : public MenuItem {
  public:
    ResetConfiguration() { setName("reset configuration object"); }
    int action() {

        cfg.reset();

        return(0);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class ReadAllSignalSettings : public MenuItem {
  public:
    ReadAllSignalSettings() { setName("read  all signal settings\n        ---------------------------------------------------------"); }
    int action() {

        int rtnv(0);

        if((rtnv = ltpi->printLinkEqualization()) != LTPI::SUCCESS) {
             CERR("printing link equalization","");
             return(rtnv);
        }

        if((rtnv = ltpi->printSignalSelection()) != LTPI::SUCCESS) {
             CERR("printing signal selection","");
             return(rtnv);
        }

        if((rtnv = ltpi->printSignalDelay()) != LTPI::SUCCESS) {
             CERR("printing signal delay","");
             return(rtnv);
        }
        std::cout << "----------------------------------------------------------------------------" << std::endl;


        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ReadAllLinkEqualization : public MenuItem {
  public:
    ReadAllLinkEqualization() { setName("read  input link equalization [CTPin,LTPin]"); }
    int action() {

        int rtnv(0);

        if((rtnv = ltpi->printLinkEqualization()) != LTPI::SUCCESS) {
             CERR("printing link equalization","");
             return(rtnv);
        }
        std::cout << "----------------------------------------------------------------------------" << std::endl;

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteLinkEqualization : public MenuItem {
  public:
    WriteLinkEqualization() { setName("write input link equalization [CTPin,LTPin: parameters]"); }
    int action() {

        unsigned int lnk;
        std::ostringstream buf;
        int rtnv(0);

        for(lnk=0; lnk<LTPI::EQUAL_LINK_NUMBER; lnk++) buf << (buf.str().empty()?"":", ") << lnk << "=" << LTPI::EQUAL_LINK_NAME[lnk];
        buf << ", " << LTPI::EQUAL_LINK_NUMBER << "=ALL";
        lnk = enterInt(buf.str().c_str(),0,LTPI::EQUAL_LINK_NUMBER);

        u_short gain, ctrl;
        unsigned int nlo, nhi;

        gain = enterHex("Gain",0,LTPI::EQUAL_MAXIMUM);
        ctrl = enterHex("CTRL",0,LTPI::EQUAL_MAXIMUM);

        nlo = (lnk == LTPI::EQUAL_LINK_NUMBER) ? 0 : lnk;
        nhi = (lnk == LTPI::EQUAL_LINK_NUMBER) ? LTPI::EQUAL_LINK_NUMBER : lnk+1;
        for(lnk=nlo; lnk<nhi; lnk++) {
            if((rtnv = ltpi->writeLinkEqualization(static_cast<LTPI::EQUAL_LINK>(lnk),gain,ctrl)) != LTPI::SUCCESS) {
                CERR("writing equalization of link \"%s\"",LTPI::EQUAL_LINK_NAME[lnk].c_str());
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteLinkEqualizationSEL : public MenuItem {
  public:
    WriteLinkEqualizationSEL() { setName("write input link equalization [CTPin,LTPin: selection]"); }
    int action() {

        unsigned int lnk;
        std::ostringstream buf;
        int rtnv(0);

        for(lnk=0; lnk<LTPI::EQUAL_LINK_NUMBER; lnk++) buf << (buf.str().empty()?"":", ") << lnk << "=" << LTPI::EQUAL_LINK_NAME[lnk];
        buf << ", " << LTPI::EQUAL_LINK_NUMBER << "=ALL";
        lnk = enterInt(buf.str().c_str(),0,LTPI::EQUAL_LINK_NUMBER);

        unsigned int nlo, nhi;
        bool ena;
	bool sho(false);

        ena = static_cast<bool>(enterInt("Enable link      (0=NO,1=YES)",0,1));
        if(ena) {
            sho = !static_cast<bool>(enterInt("Cable length (0=SHORT,1=LONG)",0,1));
        }
        nlo = (lnk == LTPI::EQUAL_LINK_NUMBER) ? 0 : lnk;
        nhi = (lnk == LTPI::EQUAL_LINK_NUMBER) ? LTPI::EQUAL_LINK_NUMBER : lnk+1;
        for(lnk=nlo; lnk<nhi; lnk++) {
            if((rtnv = ltpi->writeLinkEqualization(static_cast<LTPI::EQUAL_LINK>(lnk),ena,sho)) != LTPI::SUCCESS) {
                CERR("writing equalization of link \"%s\"",LTPI::EQUAL_LINK_NAME[lnk].c_str());
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteLinkEqualizationLGT : public MenuItem {
  public:
    WriteLinkEqualizationLGT() { setName("write input link equalization [CTPin,LTPin: cable length]\n        ---------------------------------------------------------"); }
    int action() {

        unsigned int lnk;
        std::ostringstream buf;
        int rtnv(0);

        for(lnk=0; lnk<LTPI::EQUAL_LINK_NUMBER; lnk++) buf << (buf.str().empty()?"":", ") << lnk << "=" << LTPI::EQUAL_LINK_NAME[lnk];
        buf << ", " << LTPI::EQUAL_LINK_NUMBER << "=ALL";
        lnk = enterInt(buf.str().c_str(),0,LTPI::EQUAL_LINK_NUMBER);

        unsigned int nlo, nhi;
        bool ena;
        double lgt(0);

        ena = static_cast<bool>(enterInt("Enable link (0=NO,1=YES)",0,1));
        if(ena) {
            lgt = static_cast<double>(enterInt("Cable length in m",0,100));
        }
        nlo = (lnk == LTPI::EQUAL_LINK_NUMBER) ? 0 : lnk;
        nhi = (lnk == LTPI::EQUAL_LINK_NUMBER) ? LTPI::EQUAL_LINK_NUMBER : lnk+1;
        for(lnk=nlo; lnk<nhi; lnk++) {
            if((rtnv = ltpi->writeLinkEqualization(static_cast<LTPI::EQUAL_LINK>(lnk),ena,lgt)) != LTPI::SUCCESS) {
                CERR("writing equalization of link \"%s\"",LTPI::EQUAL_LINK_NAME[lnk].c_str());
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ReadAllSignalSelection : public MenuItem {
  public:
    ReadAllSignalSelection() { setName("read  signal selection"); }
    int action() {

        int rtnv(0);

        if((rtnv = ltpi->printSignalSelection()) != LTPI::SUCCESS) {
             CERR("printing signal selection","");
             return(rtnv);
        }
        std::cout << "----------------------------------------------------------------------------" << std::endl;

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteSignalSelection : public MenuItem {
  public:
    WriteSignalSelection() { setName("write signal selection [BC,ORB,L1A,TTRG,BGO]"); }
    int action() {

        unsigned int sig, sel;
        std::ostringstream buf;
        int rtnv(0);

        for(sig=0; sig<LTPI::SELECT_SIGNAL_NUMBER; sig++) buf << (buf.str().empty()?"":", ") << sig << "=" << LTPI::SELECT_SIGNAL_NAME[sig];
        buf << ", " << LTPI::SELECT_SIGNAL_NUMBER << "=ALL";
        sig = enterInt(buf.str().c_str(),0,LTPI::SELECT_SIGNAL_NUMBER);

        // print instructions
        LTPI::printSignalSelectionLegend(LTPI::PRINT_SIG);

        buf.str("");
        for(sel=0; sel<LTPI::SIGNAL_SELECTION_NUMBER; sel++) buf << (buf.str().empty()?"":", ") << sel << "=" << LTPI::SIGNAL_SELECTION_NAME[sel];
        sel = enterInt(buf.str().c_str(),0,LTPI::SIGNAL_SELECTION_NUMBER-1);

        if(sig < LTPI::SELECT_SIGNAL_NUMBER) {
            if((rtnv = ltpi->writeSignalSelection(static_cast<LTPI::SELECT_SIGNAL>(sig),static_cast<LTPI::SIGNAL_SELECTION>(sel))) != LTPI::SUCCESS) {
                CERR("writing selection of signal \"%s\"",LTPI::SELECT_SIGNAL_NAME[sig].c_str());
                return(rtnv);
            }
        }
        else {
            if((rtnv = ltpi->writeSignalSelection(static_cast<LTPI::SIGNAL_SELECTION>(sel))) != LTPI::SUCCESS) {
                CERR("writing selection of all signals","");
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteTriggerTypeSelection : public MenuItem {
  public:
    WriteTriggerTypeSelection() { setName("write signal selection [TTYP]"); }
    int action() {

        unsigned int typ, loc(0);
        std::ostringstream buf;
        int rtnv(0);

        // print instructions
        LTPI::printSignalSelectionLegend(LTPI::PRINT_TTYP);

        for(typ=0; typ<LTPI::TTYP_SELECTION_NUMBER; typ++) buf << (buf.str().empty()?"":", ") << typ << "=" << LTPI::TTYP_SELECTION_NAME[typ];
        typ = enterInt(buf.str().c_str(),0,LTPI::TTYP_SELECTION_NUMBER-1);

        if((typ == LTPI::TTYP_FROM_LOCAL) || (typ == LTPI::TTYP_PARALLEL_FROM_LOCAL)) {
            buf.str("");
            for(loc=0; loc<LTPI::LOCTTYP_SELECTION_NUMBER; loc++) buf << (buf.str().empty()?"":", ") << loc << "=" << LTPI::LOCTTYP_SELECTION_NAME[loc];
            loc = enterInt(buf.str().c_str(),0,LTPI::LOCTTYP_SELECTION_NUMBER-1);
        }

        if((rtnv = ltpi->writeTriggerTypeSelection(static_cast<LTPI::TTYP_SELECTION>(typ),static_cast<LTPI::LOCTTYP_SELECTION>(loc))) != LTPI::SUCCESS) {
            CERR("writing selection of signal \"TTYP\"","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteLocalTriggerTypeWord : public MenuItem {
  public:
    WriteLocalTriggerTypeWord() { setName("write signal selection [LOCAL TTYP WORD]"); }
    int action() {

        unsigned int idx, nlo, nhi;
        std::ostringstream buf;
        u_short word;
        int rtnv(0);

        buf << "local TTYP index      [ALL=" << LTPI::LOCTTYP_NUMBER << "]";
        idx = enterInt(buf.str().c_str(),0,LTPI::LOCTTYP_NUMBER);
        word = enterHex("local TTYP word",0,LTPI::LOCTTYP_MASK);

        nlo = (idx == LTPI::LOCTTYP_NUMBER) ? 0 : idx;
        nhi = (idx == LTPI::LOCTTYP_NUMBER) ? LTPI::LOCTTYP_NUMBER : idx+1;
        for(idx=nlo; idx<nhi; idx++) {
            if((rtnv = ltpi->writeLocalTriggerTypeWord(idx,word)) != LTPI::SUCCESS) {
                CERR("writing local TTYP word index %d",idx);
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteCalibrationRequestSelection : public MenuItem {
  public:
    WriteCalibrationRequestSelection() { setName("write signal selection [CALREQ]"); }
    int action() {

        unsigned int req;
        std::ostringstream buf;
        int rtnv(0);

        // print instructions
        LTPI::printSignalSelectionLegend(LTPI::PRINT_CALREQ);

        for(req=0; req<LTPI::CALREQ_SELECTION_NUMBER; req++) buf << (buf.str().empty()?"":", ") << req << "=" << LTPI::CALREQ_SELECTION_NAME[req];
        req = enterInt(buf.str().c_str(),0,LTPI::CALREQ_SELECTION_NUMBER-1);

        if((rtnv = ltpi->writeCalibrationRequestSelection(static_cast<LTPI::CALREQ_SELECTION>(req))) != LTPI::SUCCESS) {
            CERR("writing selection of signal \"CALREQ\"","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteBusySelection : public MenuItem {
  public:
    WriteBusySelection() { setName("write signal selection [BUSY]\n        ---------------------------------------------------------"); }
    int action() {

        unsigned int lnk, bsy, nlo, nhi;
        std::ostringstream buf;
        int rtnv(0);

        for(lnk=0; lnk<LTPI::LINK_NUMBER; lnk++) buf << (buf.str().empty()?"":", ") << lnk << "=" << LTPI::LINK_NAME[lnk];
        buf << ", " << LTPI::LINK_NUMBER << "=ALL";
        std::cout << "Output link selection:" << std::endl;
        lnk = enterInt(buf.str().c_str(),0,LTPI::LINK_NUMBER);

        // print instructions
        LTPI::printSignalSelectionLegend(LTPI::PRINT_BUSY);

        buf.str("");
        for(bsy=0; bsy<LTPI::BUSY_SELECTION_NUMBER; bsy++) buf << (buf.str().empty()?"":", ") << bsy << "=" << LTPI::BUSY_SELECTION_NAME[bsy];
        bsy = enterInt(buf.str().c_str(),0,LTPI::BUSY_SELECTION_NUMBER-1);

        nlo = (lnk == LTPI::LINK_NUMBER) ? 0 : lnk;
        nhi = (lnk == LTPI::LINK_NUMBER) ? LTPI::LINK_NUMBER : lnk+1;
        for(lnk=nlo; lnk<nhi; lnk++) {
            if((rtnv = ltpi->writeBusySelection(static_cast<LTPI::LINK>(lnk),static_cast<LTPI::BUSY_SELECTION>(bsy))) != LTPI::SUCCESS) {
                CERR("writing selection of signal \"BUSY\" of link \"%s\"",LTPI::LINK_NAME[lnk].c_str());
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ReadAllSignalDelay : public MenuItem {
  public:
    ReadAllSignalDelay() { setName("read  output signal delay  [CTPout]"); }
    int action() {

        int rtnv(0);

        if((rtnv = ltpi->printSignalDelay()) != LTPI::SUCCESS) {
             CERR("printing signal delay","");
             return(rtnv);
        }
        std::cout << "----------------------------------------------------------------------------" << std::endl;

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteSignalDelay : public MenuItem {
  public:
    WriteSignalDelay() { setName("write output signal delay  [CTPout]"); }
    int action() {

        unsigned int sig;
        u_short del(0);
        bool grp, ena;
        std::ostringstream buf;
        int rtnv(0);

        grp = static_cast<bool>(enterInt("Single signal or group of signals (0=SINGLE,1=GROUP)",0,1));

        if(!grp) {
            for(sig=0; sig<LTPI::DELAY_SIGNAL_NUMBER; sig++) buf << (buf.str().empty()?"":", ") << sig << "=" << LTPI::DELAY_SIGNAL_NAME[sig];
            buf << ", " << LTPI::DELAY_SIGNAL_NUMBER << "=ALL";
            sig = enterInt(buf.str().c_str(),0,LTPI::DELAY_SIGNAL_NUMBER);
        }
        else {
            for(sig=0; sig<LTPI::DELAY_GROUP_NUMBER; sig++) buf << (buf.str().empty()?"":", ") << sig << "=" << LTPI::DELAY_GROUP_NAME[sig];
            buf << ", " << LTPI::DELAY_GROUP_NUMBER << "=ALL          ";
            sig = enterInt(buf.str().c_str(),0,LTPI::DELAY_GROUP_NUMBER);
        }

        ena = static_cast<bool>(enterInt("Enable channel                          (0=NO,1=YES)",0,1));
        if(ena) {
            buf.str("");
            buf << "Delay in steps of " << std::fixed << std::setprecision(1) << LTPI::DELAY_STEP << " ns                           ";
            del = enterInt(buf.str().c_str(),0,LTPI::DELAY_MAXIMUM);
        }

        if(!grp) {
            if(sig != LTPI::DELAY_SIGNAL_NUMBER) {
                if((rtnv = ltpi->writeSignalDelay(static_cast<LTPI::DELAY_SIGNAL>(sig),del,ena)) != LTPI::SUCCESS) {
                    CERR("writing delay of signal \"%s\"",LTPI::DELAY_SIGNAL_NAME[sig].c_str());
                    return(rtnv);
                }
            }
            else {
                if((rtnv = ltpi->writeSignalDelay(del,ena)) != LTPI::SUCCESS) {
                    CERR("writing delay of all signals","");
                    return(rtnv);
                }
            }
        }
        else {
            if(sig != LTPI::DELAY_GROUP_NUMBER) {
                if((rtnv = ltpi->writeSignalDelay(static_cast<LTPI::DELAY_GROUP>(sig),del,ena)) != LTPI::SUCCESS) {
                    CERR("writing delay of signal group \"%s\"",LTPI::DELAY_GROUP_NAME[sig].c_str());
                    return(rtnv);
                }
            }
            else {
                if((rtnv = ltpi->writeSignalDelay(del,ena)) != LTPI::SUCCESS) {
                    CERR("writing delay of all signals","");
                    return(rtnv);
                }
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteSignalEnable : public MenuItem {
  public:
    WriteSignalEnable() { setName("write output signal enable [CTPout]\n        ---------------------------------------------------------"); }
    int action() {

        unsigned int sig;
        bool grp, ena;
        std::ostringstream buf;
        int rtnv(0);

        grp = static_cast<bool>(enterInt("Single signal or group of signals (0=SINGLE,1=GROUP)",0,1));

        if(!grp) {
            for(sig=0; sig<LTPI::DELAY_SIGNAL_NUMBER; sig++) buf << (buf.str().empty()?"":", ") << sig << "=" << LTPI::DELAY_SIGNAL_NAME[sig];
            buf << ", " << LTPI::DELAY_SIGNAL_NUMBER << "=ALL";
            sig = enterInt(buf.str().c_str(),0,LTPI::DELAY_SIGNAL_NUMBER);
        }
        else {
            for(sig=0; sig<LTPI::DELAY_GROUP_NUMBER; sig++) buf << (buf.str().empty()?"":", ") << sig << "=" << LTPI::DELAY_GROUP_NAME[sig];
            buf << ", " << LTPI::DELAY_GROUP_NUMBER << "=ALL          ";
            sig = enterInt(buf.str().c_str(),0,LTPI::DELAY_GROUP_NUMBER);
        }

        ena = static_cast<bool>(enterInt("Enable channel                          (0=NO,1=YES)",0,1));

        if(!grp) {
            if(sig != LTPI::DELAY_SIGNAL_NUMBER) {
                if((rtnv = ltpi->writeSignalEnable(static_cast<LTPI::DELAY_SIGNAL>(sig),ena)) != LTPI::SUCCESS) {
                    CERR("writing enable of signal \"%s\"",LTPI::DELAY_SIGNAL_NAME[sig].c_str());
                    return(rtnv);
                }
            }
            else {
                if((rtnv = ltpi->writeSignalEnable(ena)) != LTPI::SUCCESS) {
                    CERR("writing enable of all signals","");
                    return(rtnv);
                }
            }
        }
        else {
            if(sig != LTPI::DELAY_GROUP_NUMBER) {
                if((rtnv = ltpi->writeSignalEnable(static_cast<LTPI::DELAY_GROUP>(sig),ena)) != LTPI::SUCCESS) {
                    CERR("writing enable of signal group \"%s\"",LTPI::DELAY_GROUP_NAME[sig].c_str());
                    return(rtnv);
                }
            }
            else {
                if((rtnv = ltpi->writeSignalEnable(ena)) != LTPI::SUCCESS) {
                    CERR("writing enable of all signals","");
                    return(rtnv);
                }
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteLtpiMode : public MenuItem {
  public:
    WriteLtpiMode() { setName("write LTPI mode\n        ---------------------------------------------------------"); }
    int action() {

        unsigned int mod;
        std::ostringstream buf;
        int rtnv(0);

        // print instructions
        LTPI::printSignalSelectionLegend(LTPI::PRINT_SIG);
        std::cout << "WARNING:" << std::endl;
        std::cout << "  LTPI modes are not complete settings:" << std::endl;
        std::cout << "    CABLE LENGTH (CTPin,LTPin), DELAY VALUE (CTPout), and BUSY and CALREQ settings" << std::endl;
        std::cout << "    will have to be checked and overwritten if required!" << std::endl;
        std::cout << std::endl;
        for(mod=0; mod<LTPI::MODE_SELECTION_NUMBER; mod++) buf << (buf.str().empty()?"":", ") << mod << "=" << LTPI::MODE_SELECTION_NAME[mod];
        mod = enterInt(buf.str().c_str(),0,LTPI::MODE_SELECTION_NUMBER-1);

        // read configuration from LTPI
        if((rtnv = ltpi->read(cfg)) != LTPI::SUCCESS) {
             CERR("reading configuration object from LTPI","");
             return(rtnv);
        }

        // set LTPI mode in configuration and write to LTPI
        cfg.setMode(static_cast<LTPI::MODE_SELECTION>(mod));
        if((rtnv = ltpi->write(cfg)) != LTPI::SUCCESS) {
            CERR("writing mode \"%s\"",LTPI::MODE_SELECTION_NAME[mod].c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ReadAllSignalStatus : public MenuItem {
  public:
    ReadAllSignalStatus() { setName("read  signal status"); }
    int action() {

        unsigned int sig, lnk;
        bool act;
        std::string nam;
        int rtnv(0);

        std::printf("                      LINK:");
        for(lnk=0; lnk<LTPI::LINK_NUMBER; lnk++) std::printf("    %s   ",LTPI::LINK_NAME[lnk].c_str());
        std::cout << std::endl;
        std::printf("--------------------------------------------------------\n");
        for(sig=0; sig<LTPI::STATUS_SIGNAL_NUMBER; sig++) {
            nam = "\"" + LTPI::STATUS_SIGNAL_NAME[sig] + "\"";
            std::printf("Status of signal %-9s:",nam.c_str());
            for(lnk=0; lnk<LTPI::LINK_NUMBER; lnk++) {
                if((rtnv = ltpi->readSignalStatus(static_cast<LTPI::STATUS_SIGNAL>(sig),static_cast<LTPI::LINK>(lnk),act)) != LTPI::SUCCESS) {
                     if(rtnv == LTPI::ERROR_NOTEXIST) {
                         std::printf("     -  ");
                         rtnv = LTPI::SUCCESS;
                         continue;
                     }
                     CERR("reading status of signal \"%s\" of link \"%s\"",LTPI::STATUS_SIGNAL_NAME[sig].c_str(),LTPI::LINK_NAME[lnk].c_str());
                     return(rtnv);
                }
                std::printf("  %s  ",act?"ACTIVE":" idle ");
            }
            std::cout << std::endl;
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class ReadI2CFrequency : public MenuItem {
  public:
    ReadI2CFrequency() { setName("read  I2C frequency"); }
    int action() {

        double frq;
        int rtnv(0);

        if((rtnv = ltpi->readI2CFrequency(frq)) != LTPI::SUCCESS) {
            CERR("reading I2C frequency","");
            return(rtnv);
        }
        std::printf("I2C frequency = %5.2f kHz\n",frq);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteI2CFrequency : public MenuItem {
  public:
    WriteI2CFrequency() { setName("write I2C frequency"); }
    int action() {

        double frq;
        int rtnv(0);

        frq = static_cast<double>(enterInt("I2C frequency in kHz",0,10000));
        if((rtnv = ltpi->writeI2CFrequency(frq)) != LTPI::SUCCESS) {
            CERR("writing I2C frequency","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class InitI2C : public MenuItem {
  public:
    InitI2C() { setName("init  I2C"); }
    int action() {

        bool cnt;
        int rtnv(0);

        cnt = static_cast<bool>(enterInt("Are you sure",0,1));
        if(!cnt) return(rtnv);

        if((rtnv = ltpi->initI2C()) != LTPI::SUCCESS) {
            CERR("initializing I2C","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ResetI2C : public MenuItem {
  public:
    ResetI2C() { setName("reset I2C"); }
    int action() {

        bool cnt;
        int rtnv(0);

        cnt = static_cast<bool>(enterInt("Are you sure",0,1));
        if(!cnt) return(rtnv);

        if((rtnv = ltpi->resetI2C()) != LTPI::SUCCESS) {
            CERR("resetting I2C","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class ReadEEPROM : public MenuItem {
  public:
    ReadEEPROM() { setName("read  EEPROM"); }
    int action() {

        unsigned int manuf, board, revision;
        int rtnv(0);

        if((rtnv = ltpi->readEEPROM(manuf,board,revision)) != LTPI::SUCCESS) {
            CERR("reading EEPROM","");
            return(rtnv);
        }
        std::cout << std::endl;
        std::cout << "****************************" << std::endl;
        std::printf("Manufacturer ID =   0x%06x\n",manuf);
        std::printf("Board ID        = 0x%08x\n",board);
        std::printf("Revision number = 0x%08x\n",revision);
        std::cout << "****************************" << std::endl;

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteEEPROM : public MenuItem {
  public:
    WriteEEPROM() { setName("write EEPROM"); }
    int action() {

        unsigned int manuf, board, revision;
        int rtnv(0);

        manuf = enterHex("Manufacturer ID",0,LTPI::EEPROM_MANUF_MASK);
        board = enterHex("Board ID       ",0,LTPI::EEPROM_BOARD_MASK);
        revision = enterHex("Revision ID    ",0,LTPI::EEPROM_REVISION_MASK);
        if((rtnv = ltpi->writeEEPROM(manuf,board,revision)) != LTPI::SUCCESS) {
            CERR("reading EEPROM","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class ReadBaseAddress : public MenuItem {
  public:
    ReadBaseAddress() { setName("read  base address from serial flash"); }
    int action() {

        u_short base;
        int rtnv(0);

        if((rtnv = ltpi->readBaseAddress(base)) != LTPI::SUCCESS) {
            CERR("reading base address","");
            return(rtnv);
        }
        std::cout << std::endl;
        std::printf("Base address = 0x%04x00\n",base);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class WriteBaseAddress : public MenuItem {
  public:
    WriteBaseAddress() { setName("write base address to   serial flash"); }
    int action() {

        bool cnt;
        u_short base;
        int rtnv(0);

        cnt = static_cast<bool>(enterInt("Are you sure",0,1));
        if(!cnt) return(rtnv);

        base = enterHex("Base address (upper four bytes)",0,LTPI::SFL_BASE_MASK);
        if((rtnv = ltpi->writeBaseAddress(base)) != LTPI::SUCCESS) {
            CERR("writing base address","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class ResetModule : public MenuItem {
  public:
    ResetModule() { setName("reset LTPI"); }
    int action() {

        bool cnt;
        int rtnv(0);

        cnt = static_cast<bool>(enterInt("Are you sure",0,1));
        if(!cnt) return(rtnv);

        if((rtnv = ltpi->reset()) != LTPI::SUCCESS) {
            CERR("resetting LTPI","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void usage() {

    char os[1024];

    std::printf("--------------------------------------------------------------------------------\n");
    std::printf("menuLTPI <OPTIONS>:\n");
    std::printf("--------------------------------------------------------------------------------\n");
    std::sprintf(os,"0x%06x",base_addr); 
    std::printf(" -a <hex>  => LTPI base address         (def = %s)\n",base_addr_flag?os:"<NONE>");
    std::printf(" -e        => LTPI expert mode          (def = \"%s\")\n",expert_mode?"YES":"NO");
}

//------------------------------------------------------------------------------

int main(int argc, char* argv[]) {

    int c;

    // read command-line options
    while((c = getopt(argc,argv,":a:eh")) != -1) {
        switch(c) {
        case 'a':
            if(std::sscanf(optarg,"%x",&base_addr) != 1) {
                CERR("error reading base address of LTPI","");
                usage();
                std::exit(-1);
            }
            base_addr_flag = true;
            break;

        case 'e':
            expert_mode = true;
            break;

        case 'h':
            usage();
            std::exit(0);
            break;

        case '?':
            CERR("invalid option \"%c\"",optopt);
            usage();
            std::exit(-1);
            break;

        case ':':
            CERR("missing argument for option \"%c\"",optopt);
            usage();
            std::exit(-1);
            break;
        }
    }

    // create main menu
    Menu menu("LTPI main menu");

    // open LTPI
    menu.init(new LTPIOpen);

    // menu for configurations
    Menu menuCFG("CFG - read/write configuration from/to LTPI and file");
    menuCFG.init(new OpenConfiguration);
    menuCFG.add(new PrintConfiguration);
    menuCFG.add(new ReadConfigurationFromLTPI);
    menuCFG.add(new ReadConfigurationFromFile);
    menuCFG.add(new WriteConfigurationToLTPI);
    menuCFG.add(new WriteConfigurationToFile);
    menuCFG.add(new ResetConfiguration);
    menu.add(&menuCFG);

    // menu for signals
    Menu menuSIG("SIG - read/write signal selection/status and LTPI modes");
    menuSIG.add(new ReadAllSignalSettings);
    menuSIG.add(new ReadAllLinkEqualization);
    if(expert_mode) menuSIG.add(new WriteLinkEqualization);
    menuSIG.add(new WriteLinkEqualizationSEL);
    menuSIG.add(new WriteLinkEqualizationLGT);
    menuSIG.add(new ReadAllSignalSelection);
    menuSIG.add(new WriteSignalSelection);
    menuSIG.add(new WriteTriggerTypeSelection);
    menuSIG.add(new WriteLocalTriggerTypeWord);
    menuSIG.add(new WriteCalibrationRequestSelection);
    menuSIG.add(new WriteBusySelection);
    menuSIG.add(new ReadAllSignalDelay);
    menuSIG.add(new WriteSignalDelay);
    menuSIG.add(new WriteSignalEnable);
    menuSIG.add(new WriteLtpiMode);
    menuSIG.add(new ReadAllSignalStatus);
    menu.add(&menuSIG);

    // menu for I2C
    Menu menuI2C("I2C - read/write low-level I2C parameters");
    if(expert_mode) {
        menuI2C.add(new ReadI2CFrequency);
        menuI2C.add(new WriteI2CFrequency);
        menuI2C.add(new InitI2C);
        menuI2C.add(new ResetI2C);
        menu.add(&menuI2C);
    }

    // menu for EEPROM
    Menu menuPRM("PRM - read/write low-level EEPROM data");
    menuPRM.add(new ReadEEPROM);
    if(expert_mode) menuPRM.add(new WriteEEPROM);
    menu.add(&menuPRM);

    // menu for BASE
    Menu menuBAS("BAS - read/write LTPI base address from/to serial flash");
    menuBAS.add(new ReadBaseAddress);
    if(expert_mode) menuBAS.add(new WriteBaseAddress);
    menu.add(&menuBAS);

    // menu for RESET
    Menu menuRST("RST - reset LTPI using hardware register");
    menuRST.add(new ResetModule);
    menu.add(&menuRST);

    // close LTPI
    menu.exit(new LTPIClose);

    // start menu
    menu.execute();

    return 0;
}
