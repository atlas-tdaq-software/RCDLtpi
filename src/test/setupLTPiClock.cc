#include <string>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>
#include <cstdint>

#include "cmdl/cmdargs.h"

#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"
#include <signal.h>

#include "RCDLtpi/RCDLtpi.h"

using namespace RCD;

std::string prog(">> setupLTPiClock: ");

void sigint_handler(int) {
  std::cout << prog << "Received signal SIGINT" << std::endl;
  exit(0);
}

// global variable
LTPi* ltpi;


int main(int argc, char ** argv)
{
  signal(SIGINT,  sigint_handler);

  // Declare arguments
  CmdArgStr base_cmd('a', "vme-address", "vme-address", "VME base address in hex, e.g. 0xff1000", CmdArg::isREQ);
  CmdArgBool verbose_cmd('v', "verbose", "verbose", CmdArg::isOPT);

  CmdArgStr mode_cmd('m', "mode", "mode", "Mode to set up the module: \"CTP\", \"LTPSlave\", \"LTPMaster\"", CmdArg::isOPT);
  CmdArgStr cablelength_cmd('c', "cablelength", "cablelength", "Cable length (\"short\" < 20m, \"long\" > 30m) of the used input link, i.e. CTP link for CTP mode and LTP for LTPSlave mode", CmdArg::isOPT);
  CmdArgStr gain_cmd('g', "gain", "gain", "Gain used for input cable", CmdArg::isOPT);
  CmdArgStr eq_cmd('e', "equalisation", "equalisation", "Equalisation used for the input cable", CmdArg::isOPT);

  CmdArgInt delay_cmd('d', "delay", "delay", "Delay (0..63) for specific signal (or all if not specified)", CmdArg::isOPT);
  CmdArgStr signal_cmd('s', "signal", "signal", "Signal to be delayed (L1A, TT1, TT2, TT3, ORB, BGO, BG1, BG2, BG3, ALL)", CmdArg::isOPT);

  // Declare command object and its argument-iterator
  CmdLine cmd(*argv, &base_cmd, &verbose_cmd, &mode_cmd, &cablelength_cmd, &gain_cmd, &eq_cmd, &delay_cmd, &signal_cmd, nullptr);
  CmdArgvIter arg_iter(--argc, ++argv);
 
  cmd.description("This program sets up the LTPi into a standard configuration.");

  // default values
  base_cmd = "0xee1000";
  verbose_cmd = false;
  mode_cmd = "";
  cablelength_cmd = "";
  gain_cmd = "";
  eq_cmd = "";
  delay_cmd = -1;
  signal_cmd = "NONE";

  // Parse arguments
  cmd.parse(arg_iter);
  std::string base_s(base_cmd);
  bool verbose(verbose_cmd);
  std::string mode(mode_cmd);
  std::string cablelength(cablelength_cmd);  
  std::string gain_s(gain_cmd);
  std::string eq_s(eq_cmd);
  int delay(delay_cmd);
  std::string signal(signal_cmd);

  // check consistency:
  if ((delay < -1) || (delay > 63) ) {
    std::cerr << prog << "ERROR: delay " << delay << " out of range" << std::endl;
    exit(-1);
  }
  if ( (signal != "L1A") 
       && (signal != "TT1") 
       && (signal != "TT2") 
       && (signal != "TT3") 
       && (signal != "ORB") 
       && (signal != "BG0") 
       && (signal != "BG1") 
       && (signal != "BG2") 
       && (signal != "BG3") 
       && (signal != "ALL") 
       && (signal != "NONE")
       ) {
    std::cerr << prog << "ERROR: unknown signal \"" << signal << "\" requested" << std::endl;
    exit(-1);
  }
  if ((signal == "NONE") && (delay != -1)) {
    std::cerr << prog << "ERROR: Delay " << delay << " specified without valid signal" << std::endl;
    exit(-1);
  }

  // Setting the equaliser only makes sense when 
  bool setEqualiser(false);
  uint32_t gain(0xffffffff);
  uint32_t eq(0xffffffff);
  if ( (cablelength != "") || (gain_s != "") || (eq_s != "") ) {
    if ( (mode != "CTP") && (mode != "LTPSlave") ) {
      std::cerr << prog << "ERROR: setting equalizer without proper mode (CTP or LTPSlave)" << std::endl;
      exit(-1);
    }
  }
  // check exclusiveness of cablelength and (gain/eq)
  if ( (cablelength != "") && ((gain_s !="")||(eq_s !=""))  ) {
    std::cerr << prog << "ERROR: Mixing Cable length and Gain/Eq" << std::endl;
    exit(-1);
  }
  if ( (gain_s != "") && (eq_s == "") ) {
    std::cerr << prog << "ERROR: gain specified without equalisation. Use -e option." << std::endl;
    exit(-1);
  }
  if ( (gain_s == "") && (eq_s != "") ) {
    std::cerr << prog << "ERROR: equalisation specified without gain. Use -g option." << std::endl;
    exit(-1);
  }

  if (cablelength == "long") {
    setEqualiser = true;
    gain = 0xa0;
    eq   = 0x10;
  } else if (cablelength == "short") {
    setEqualiser = true;
    gain = 0x80;
    eq   = 0x00;
  } else if (cablelength != "") {
    std::cerr << prog << "ERROR: wrong cablelength specified" << std::endl;
    exit(-1);
  }

  if (gain_s != "") {
    sscanf(gain_s.c_str(), "%x", &gain);
    if (gain>255) {
      std::cerr << prog << "ERROR: value of gain is not 8-bit" << std::endl;
      exit(-1);
    }
    setEqualiser = true;
  }
  if (eq_s != "") {
    sscanf(eq_s.c_str(), "%x", &eq);
    if (eq>255) {
      std::cerr << prog << "ERROR: value of eq is not 8-bit" << std::endl;
      exit(-1);
    }
    setEqualiser = true;
  }

  if ( (mode != "") && (mode != "CTP") && (mode != "LTPMaster") && (mode != "LTPSlave") ) {
    std::cerr << prog << "ERROR: unknown mode \"" << mode << "\"" << std::endl;
    exit(-1);
  }


  if (verbose) {
    std::cout << prog << "Start" << std::endl;
  }

  uint32_t base(0);
  sscanf(base_s.c_str(), "%x", &base);

  if (verbose) {
    std::cout << prog;
    printf("VME base address = 0x%08x\n", base);
    std::cout << prog << "mode = " << mode << std::endl;
    if (cablelength != "") {
      std::cout << prog << "cablelength = \"" << cablelength << "\"" << std::endl;
    }
    if (setEqualiser) {
      std::cout << prog << "gain = 0x" << std::hex << gain << std::dec << std::endl;
      std::cout << prog << "eq = 0x" << std::hex << eq << std::dec << std::endl;
    }

    std::cout << prog << "delay = " << delay << std::endl;
    std::cout << prog << "signal = " << signal << std::endl;
  }

  u_int rtnv(0);

  ltpi = new LTPi();
  if (ltpi==nullptr) {
    std::cerr << prog << "ERROR: Could not create LTPi" << std::endl;
    exit(-1);
  }

  rtnv = ltpi->Open(base);
  if (rtnv != 0) {
    std::cerr << prog << "ERROR: Could not open LTPi at address 0x" << std::hex << base << std::dec << std::endl;
    exit(-1);
  }

  if (mode == "CTP") {
    rtnv |= ltpi->setupI2C();
    if (ltpi->readI2Cfreq() != 100.0) {
      std::cerr << prog << "ERROR: I2C frequency is not 100.0 kHz" << std::endl;
      exit(-1);
    }

    // configure DAC
    if (setEqualiser) {
      rtnv |= ltpi->setDACCTPgain(static_cast<u_short>(gain));
      rtnv |= ltpi->setDACCTPequ(static_cast<u_short>(eq));
    }

    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25L1A);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25ORB);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO0);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT0);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT4);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT5);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT6);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT7);

    // Switching:
    rtnv |= ltpi->BC_CTPoutFromCTPin_LTPoutFromCTPin();
    rtnv |= ltpi->Orbit_CTPoutFromCTPin_LTPoutFromCTPin();
    rtnv |= ltpi->L1A_CTPoutFromCTPin_LTPoutFromCTPin();
    rtnv |= ltpi->TTR_CTPoutFromCTPin_LTPoutFromCTPin();
    rtnv |= ltpi->BGO_CTPoutFromCTPin_LTPoutFromCTPin();
    rtnv |= ltpi->TRT_CTPoutFromCTPin_LTPoutFromCTPin();
    rtnv |= ltpi->calCTPfromCTP_LTPFromCTP();

    rtnv |= ltpi->busyCTP_FROM_CTP();


  } else if (mode == "LTPSlave") {
    // reset the module
    rtnv |= ltpi->Reset();
    // set I2C
    rtnv |= ltpi->setupI2C();
    if (ltpi->readI2Cfreq() != 100.0) { 
      std::cerr << prog << "ERROR: I2C frequency is not 100kHz" << std::endl;
      exit(-1);
    }

    // configure DAC
    if (setEqualiser) {
      rtnv |= ltpi->setDACLTPgain(static_cast<u_short>(gain));
      rtnv |= ltpi->setDACLTPequ(static_cast<u_short>(eq));
    }

    // cofigure DELAYs
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25L1A);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25ORB);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO0);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT0);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT4);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT5);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT6);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT7);

    // Switching:
    rtnv |= ltpi->BC_CTPoutFromLTPin_LTPoutFromLTPin();
    rtnv |= ltpi->Orbit_CTPoutFromLTPin_LTPoutFromLTPin();
    rtnv |= ltpi->L1A_CTPoutFromLTPin_LTPoutFromLTPin();
    rtnv |= ltpi->TTR_CTPoutFromLTPin_LTPoutFromLTPin();
    rtnv |= ltpi->BGO_CTPoutFromLTPin_LTPoutFromLTPin();
    rtnv |= ltpi->TRT_CTPoutFromLTPin_LTPoutFromLTPin();
    rtnv |= ltpi->calCTPfromLTP_LTPFromLTP();

    rtnv |= ltpi->busyLTP_FROM_CTP_AND_LTP();

  } else if (mode == "LTPMaster") {
    rtnv |= ltpi->Reset();
    // set I2C
    rtnv |= ltpi->setupI2C();

    // cofigure DELAYs
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25L1A);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25ORB);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TTR3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25BGO0);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT0);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT1);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT2);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT3);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT4);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT5);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT6);
    rtnv |= ltpi->setDelaySingleChannel(0, LTPi::DEL25TRT7);

    // Switching:
    rtnv |= ltpi->BC_CTPoutFromNIMin_LTPoutFromNIMin();
    rtnv |= ltpi->Orbit_CTPoutFromNIMin_LTPoutFromNIMin();
    rtnv |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromNIMin();
    rtnv |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromNIMin();
    rtnv |= ltpi->BGO_CTPoutFromNIMin_LTPoutFromNIMin();
    rtnv |= ltpi->TRT_CTPoutFromLocal_LTPoutFromLocal();

    rtnv |= ltpi->busyNIM_FROM_LTP();
  }


  if (delay != -1) { // set delay
    if ((signal == "L1A") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25L1A);
    }
    if ((signal == "ORB") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25ORB);
    }
    if ((signal == "TT1") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25TTR1);
    }
    if ((signal == "TT2") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25TTR2);
    }
    if ((signal == "TT3") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25TTR3);
    }
    if ((signal == "BG0") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25BGO0);
    }
    if ((signal == "BG1") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25BGO1);
    }
    if ((signal == "BG2") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25BGO2);
    }
    if ((signal == "BG3") || (signal == "ALL") ) {
      rtnv |= ltpi->setDelaySingleChannel(delay, LTPi::DEL25BGO3);
    }
  }

  if (rtnv != 0) {
    std::cerr << prog << "ERROR: VME error = :" << rtnv << " (0x" << hex << rtnv << dec << ")" << std::endl;
    exit(-1);
  }
  
  if (verbose) {
    std::cout << prog << "Done" << std::endl;
  }

  return 0;
}
