//******************************************************************************
// file: menuRCDLtpi.cc
// desc: menu for test of library for LTPi
//******************************************************************************

#include <fstream>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <list>
#include <errno.h>
#include <stdint.h>

#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDVme.h"
#include "DFDebug/DFDebug.h"

#include "RCDLtpi/RCDLtpi.h"


using namespace RCD;


// global variable
LTPi* ltpi;
int selchannel;

class LTPiOpen : public MenuItem {
public:
  LTPiOpen() { setName("Open LPTi"); }
  int action() {
    int status = 0;
    std::string		in;
    u_int base;

    base = enterHex("base address for the LTPi <base>?", 0, 0xffff00);
    printf("Module VME base address: 0x%06x\n", base);
    
    ltpi = new LTPi();
    status = ltpi->Open(base);

    u_int board_man = 0;
    u_int board_revision = 0;
    u_int board_id = 0;
    status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);

    printf("\n");
    printf(" Manufacturer: 0x%08x (CERN = 0x00080030)\n", board_man);
    printf(" Revision    : 0x%08x \n", board_revision);
    printf(" Id          : 0x%08x \n", board_id);

    int ierr = 0;
   
    // reset I2C
    ierr |= ltpi->resetI2C();
    ierr |= ltpi->setupI2C();
    

    return ierr;
  }
};

//------------------------------------------------------------------------------
class LTPClose : public MenuItem {
public:
  LTPClose() { setName("Close LTP"); }
  int action() {
    int status = ltpi->Close();
    delete ltpi;

    return(status);
  }
};


//-----------------------------------------------------------------------------
class runTestDelay : public MenuItem {
public:
  runTestDelay() { setName("Run Delay25 Test"); }
  int action() {
    int ierr = 0;
    for (int i=0; i<1000; ++i) 
      {
	std::cout << i << std::endl;
	ierr |= ltpi->enableCTPout();
	for (int j=0; j<63; ++j) {ierr |= ltpi->setDelayAllChannels(j);}
	ierr |= ltpi->disableCTPout();
      }
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class runTestDac : public MenuItem {
public:
  runTestDac() { setName("Run Dac Test"); }
  int action() {
    int ierr = 0;
    u_short data;
    for (int i=0; !ierr && i<100; ++i) 
      {
	std::cout << i << std::endl;
	ierr |= ltpi->setupDAC4longCable();

	ierr |= ltpi->getDACCTPgain(data);
	ierr |= ltpi->getDACCTPequ(data);
	ierr |= ltpi->getDACLTPgain(data);
	ierr |= ltpi->getDACLTPequ(data);

	ierr |= ltpi->setupDAC4shortCable();

	ierr |= ltpi->getDACCTPgain(data);
	ierr |= ltpi->getDACCTPequ(data);
	ierr |= ltpi->getDACLTPgain(data);
	ierr |= ltpi->getDACLTPequ(data);

	ierr |= ltpi->setDACCTPgain(0);
	ierr |= ltpi->setDACCTPequ(0);
	ierr |= ltpi->setDACLTPgain(0);
	ierr |= ltpi->setDACLTPequ(0);
      }
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class reset : public MenuItem {
public:
  reset() { setName("Software reset"); }
  int action() {
    int ierr = 0;

    printf("\n");
    std::cout << ">>>>>>>>>>>>>>>>> software reset the module" << std::endl;
    printf("\n");
    ierr |= ltpi->Reset();
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class initialize : public MenuItem {
public:
  initialize() { setName("Initialize"); }
  int action() {
    int ierr = 0;

    printf("\n");
    std::cout << ">>>>>>>>>>>>>>>>> set up i2c" << std::endl;
    std::cout << ">>>>>>>>>>>>>>>>> set global configuration to CTP" << std::endl;
    printf("\n");
    ierr |= ltpi->Reset();
    ierr |= ltpi->setupI2C();
    ierr |= ltpi->ctp_mode();
    return(ierr);
  }
};
int main() {
  
  // set defaults:
  
  Menu	menu("LTPi Menu");
  
  // generate menu
  menu.init(new LTPiOpen);

  menu.add(new reset);
  menu.add(new initialize);

  Menu menuTest("start");
  menuTest.add(new runTestDelay);
  menuTest.add(new runTestDac);
  menu.add(&menuTest);

  menu.exit(new LTPClose);
  // start menu
  menu.execute();

  return 0;
}
