// Author: Catalin Hanga, 2013 Summer Student Program
// e-mail: catalin.hanga@cern.ch

#include "RCDLtpi/testLTPI.h" 

// global variables

int number_pg_loop; // outer loop in testConnection() 
// represents the number of time you want to send the PG signals
// TODO: make it a member of MainProgram with default value 1, 
// and give the user the posibility to change its value with the Read/Write configuration 

int print; // TODO: print level

ofstream m_file;
bool save = true;

//----------------------------------------------------------------------------------------------------

int Test::readAndCompareCounters(LTPextension* sender, LTPextension* receiver, LTPI::SIGNAL sig, int &n)
{
  u_int cnt_src, cnt_dst;
  float rel_err;

  if( ( m_status = sender->COUNTER_read_32(&cnt_src) ) != SUCCESS ) {
    CERR( "Error at sender->COUNTER_read_32 for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status;
  }
  if( ( m_status = receiver->COUNTER_read_32(&cnt_dst) ) != SUCCESS ) {
    CERR( "Error at receiver->COUNTER_read_32 for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status;
  }

  if( cnt_src == 0 ) {
    cout<<" FAILED ! Zero "<<LTPI::SIGNAL_NAME[sig]<<" pulses sent"<<endl; 
    m_status = ERROR; 
    return m_status;
  }
  else m_status = SUCCESS;
    
  if( cnt_dst != cnt_src ) {
    rel_err = (static_cast<float>(cnt_src - cnt_dst))/cnt_src*100;
    cout<<"FAILED: received "<<-rel_err<<" %"<<endl;
    m_status = ERROR;
    return m_status;
  }
  else m_status = SUCCESS;
    
  n = cnt_dst;

  return m_status;
}

int Test::testSignal(LTPextension* sender, LTPextension* receiver, LTPI::SIGNAL sig)
{
  if((m_status = sender->LTPCounterSource(sig)) != SUCCESS ) {
    CERR("Error at LtpCounterSource for sender & signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status;
  }
  if( ( m_status = sender->COUNTER_reset() ) != SUCCESS ) {
    CERR("Error at sender->COUNTER_reset() for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status;
  }
   
  if((m_status = receiver->LTPCounterSource(sig)) != SUCCESS) {
    CERR("Error at LtpCounterSource for reciever & signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status;
  }
  if( ( m_status = receiver->COUNTER_reset() ) != SUCCESS ) {
    CERR("Error at receiver->COUNTER_reset() for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status;
  }
   
  if( ( m_status = sender->PG_runmode_cont() ) != SUCCESS ) {
    CERR("Error at sender->PG_runmode_cont() for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status; 
  }
  if( ( m_status = sender->PG_start() ) != SUCCESS ) { 
    CERR( "Error at sender->PG_start() for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status; 
  } 
   
  sleep( m_prog->getSleepPeriod() );
   
  if( ( m_status = sender->PG_runmode_load() ) != SUCCESS ) { // PG stop
    CERR( "Error at sender->PG_runmode_load() for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
    return m_status;
  }
   
  // wait for the pattern generator to finish
  u_int  loop = 0;
  while(true)
    {
      if( sender->PG_is_idle() )
	break;
      if( ++loop >= TIMEOUT ) {
	CERR( "Timeout error for signal %s ", LTPI::SIGNAL_NAME[sig].c_str() );
	return ERROR_TIMEOUT;
      }
    }
   
  int q; // never used in this function
  m_status = readAndCompareCounters(sender, receiver, sig, q);
   
  return m_status;
}

//-----------------------------------------------------------------------------------------------------

int printConfiguration::action() 
{ 
  string s[2]={"LONG CABLE", "SHORT CABLE"};
  cout<<endl;
  for(unsigned int i=0; i<LTPI::EQUAL_LINK_NUMBER; i++)
    cout<<" "<<LTPI::EQUAL_LINK_NAME[i]<<" = "
	<<s[ static_cast<int>(m_prog->getLTPIConfig()->equal_short_cable[i]) ]<<endl;
  cout<<endl;
   
  for(unsigned int i=0; i<LTPI::DELAY_SIGNAL_NUMBER; i++)
    cout<<" delay[ "<<LTPI::DELAY_SIGNAL_NAME[i]<<
      setw(7-LTPI::DELAY_SIGNAL_NAME[i].size())<<"]"<<" = "<<m_prog->getLTPIConfig()->ctpout_delay[i]
	<<" ("<<m_prog->getLTPIConfig()->ctpout_delay[i]/2.<<" nsec)"<<endl;
    
  cout<<endl<<" sleep period = "<<m_prog->getSleepPeriod()<<" sec"<<endl;
  cout<<endl<<" TTYP iteration number = "<<m_prog->getTTYPiter()<<endl;
    
  return 0; 
} 

int LongShortCable::action()
{
  for(unsigned int i=0; i<LTPI::EQUAL_LINK_NUMBER; i++)
    cout<<i<<"="<<LTPI::EQUAL_LINK_NAME[i]<<", ";
  cout<<LTPI::EQUAL_LINK_NUMBER<<"=ALL";
  unsigned int lnk = enterInt("", 0, LTPI::EQUAL_LINK_NUMBER);
   
  bool sho = !static_cast<bool>(enterInt("Cable length (0=SHORT, 1=LONG)",0,1));
    
  unsigned int nlo, nhi;
  nlo = (lnk == LTPI::EQUAL_LINK_NUMBER) ? 0 : lnk;
  nhi = (lnk == LTPI::EQUAL_LINK_NUMBER) ? LTPI::EQUAL_LINK_NUMBER : lnk+1;
    
  for(lnk=nlo; lnk<nhi; lnk++)
    m_prog->getLTPIConfig()->equal_short_cable[static_cast<LTPI::EQUAL_LINK>(lnk)] = sho;
  return 0;
}

int SetDelayTime::action()
{
  unsigned int delay = enterInt("Enter delay value",0,50);
  m_prog->getLTPIConfig()->setSignalDelay(delay);
  return 0; 
} 

int SetSleepPeriod::action()
{
  unsigned int s = enterInt("Enter sleep period (in seconds) ",0,100);
  m_prog->setSleepPeriod(s);
  return 0;
}

int SetTTYPiteration::action()
{
  unsigned int s = enterInt("Enter TTYP iteration number ",0,1000);
  m_prog->setTTYPiter(s);
  return 0;
}

//------------------------------------------------------------------------------------------------------

void ConnectionTest::printTableHeader( ostream &print ) const
{
  print<<endl<<endl<<"Summary of results for signal connection test:"<<endl
       <<endl<<"+------------+-----+-----+-------+-------+-------+------+------+------+------+" 
       <<endl<<"|            | "; 
  for(unsigned int i=0; i<LTPI::SIGNAL_NUMBER; i++)
    print<<LTPI::SIGNAL_NAME[i]<<" | "; 
  print<<endl<<"+------------+-----+-----+-------+-------+-------+------+------+------+------+"<<endl;
} 


void ConnectionTest::printTableLine( LTPI::SIGNAL_SELECTION s , ostream &print ) const
{
  for(int i=0; i<3; i++)
    {
      print<<"| "<<desc[s-1][i]<<" | ";
      for(unsigned int j=0; j<LTPI::SIGNAL_NUMBER; j++)
	if(m_result[s-1][i][j]) print<<setw(LTPI::SIGNAL_NAME[j].size()-1)<<"*"<<"    ";
	else                    print<<setw(LTPI::SIGNAL_NAME[j].size()-1)<<"X"<<"    ";
      print<<endl;
    }
  print<<"+------------+-----+-----+-------+-------+-------+------+------+------+------+"<<endl;
}


int ConnectionTest::action()
{
  if( (m_status = m_prog->checkForFile( m_prog->getPGFile() )) != SUCCESS )
    { 
      cout<<endl<<">>  PATTERN GENERATOR FILE \""<<m_prog->getPGFile()<<"\" COULD NOT BE FOUND !!!"
	  <<endl<<">>  An example of an pattern generator file can be found in RCDLtpi/data/pg_rand.dat"
	  <<endl<<">>  Signal Connection Test and Delay Scan Test will NOT work !"<<endl<<endl;
      return m_status;
    }
    
  if( m_sig != LTPI::SIGNAL_SELECTION_NUMBER )
    {
      subTest( static_cast<LTPI::SIGNAL_SELECTION>(m_sig) );
       
      printTableHeader();
      printTableLine(static_cast<LTPI::SIGNAL_SELECTION>(m_sig));
      printTableHeader( m_file );
      printTableLine( static_cast<LTPI::SIGNAL_SELECTION>(m_sig), m_file);
    }
  else
    {
      for( unsigned int i=1; i<LTPI::SIGNAL_SELECTION_NUMBER; i++ )
	subTest( static_cast<LTPI::SIGNAL_SELECTION>(i) );
       
      printTableHeader();
      for( unsigned int i=1; i<LTPI::SIGNAL_SELECTION_NUMBER; i++ )
	printTableLine(static_cast<LTPI::SIGNAL_SELECTION>(i));
      printTableHeader( m_file );
      for( unsigned int i=1; i<LTPI::SIGNAL_SELECTION_NUMBER; i++ )
	printTableLine( static_cast<LTPI::SIGNAL_SELECTION>(i), m_file );
    }
  return m_status;
}

const string ConnectionTest::desc[LTPI::SIGNAL_SELECTION_NUMBER][3]  = 
  { {"CTP -> CTP", "CTP -> NIM", "LTP -> LTP"}, 
    {"CTP -> CTP", "CTP -> NIM", "CTP -> LTP"}, 
    {"LTP -> CTP", "LTP -> NIM", "LTP -> LTP"},
    {"NIM -> CTP", "NIM -> NIM", "NIM -> LTP"},
    {"NIM -> CTP", "NIM -> NIM", "LTP -> LTP"} };
 

void ConnectionTest::subTest( LTPI::SIGNAL_SELECTION sig )
{
  cout<<endl<<"Start testing signal "<<LTPI::SIGNAL_SELECTION_NAME[sig]<<endl<<endl;

  // establish the mode
  LTPI::MODE_SELECTION m;
  if( sig == LTPI::SIGNAL_PARALLEL ) 
    m = LTPI::MODE_PARALLEL;
  else if ( sig == LTPI::SIGNAL_PARALLEL_FROM_NIM )
    m = LTPI::MODE_PARALLEL_FROM_NIM;
  else 
    m = static_cast<LTPI::MODE_SELECTION>(sig-1);
    
  // LTPI setup
  m_prog->getLTPIConfig()->setMode(m);
  if( (m_status = m_prog->getLTPI()->write(*(m_prog->getLTPIConfig()))) != LTPI::SUCCESS )
    CERR("Error in setting the LTPI mode %s", LTPI::MODE_SELECTION_NAME[m].c_str());
     
  LTP_TYPE master[3];
  LTP_TYPE  slave[3] = {SLAVE_CTP, SLAVE_CTP, SLAVE_LTP};
    
  // establish the masters
  if( sig == LTPI::SIGNAL_FROM_CTP || sig == LTPI::SIGNAL_FROM_NIM) for(int k=0; k<3; k++) master[k] = MASTER_CTP;
  else if( sig == LTPI::SIGNAL_FROM_LTP )                           for(int k=0; k<3; k++) master[k] = MASTER_LTP;
  else { master[0]=MASTER_CTP; master[1]=MASTER_CTP; master[2]=MASTER_LTP; }
    
  m_prog->getLTP(master[0])->SetupSender( m_prog->getPGFile() );
    
  for(int i=0; i<3; i++)   
    {
      if( (i == 2) && (sig == LTPI::SIGNAL_PARALLEL || sig == LTPI::SIGNAL_PARALLEL_FROM_NIM) )
	{
	  cout<<endl;
	  m_prog->getLTP(master[i])->SetupSender( m_prog->getPGFile() );
	}
      cout<<endl<<"Testing signal connections between "<<desc[sig-1][i]<<endl;
      m_prog->getLTP( slave[i] )->SetupReceiver(i);
      for( unsigned int j=0; j<LTPI::SIGNAL_NUMBER; j++ )
	{
	  cout<<" For signal "<<setw(5)<<LTPI::SIGNAL_NAME[j]<<" . . . ";
	  if( (m_status = testSignal(m_prog->getLTP(master[i]), 
				     m_prog->getLTP(slave[i]), static_cast<LTPI::SIGNAL>(j))) == SUCCESS)
	    cout<<"ok!"<<endl;
	  m_result[sig-1][i][j] = !static_cast<bool>(m_status);
	}
    }
}

//----------------------------------------------------------------------------------------------------

void BusyTest::printTableHeader( ostream &print ) const
{
  print<<endl<<endl<<"Summary of results for busy test:"<<endl
       <<endl<<"+--------------------------------+-----+-----+-----+" 
       <<endl<<"|                                | "; 
  for(unsigned int i=0; i<LTPI::LINK_NUMBER; i++)  
    print<<LTPI::LINK_NAME[i]<<" | "; 
  print<<endl<<"+-----------------+--------------+-----+-----+-----+"; 
} 

void BusyTest::printTableLine( LTPI::BUSY_SELECTION s, ostream &print ) const
{ 
  if(s == LTPI::BUSY_FROM_CTP || s == LTPI::BUSY_FROM_CTP_OR_LTP)  
    {
      print<<endl<<"|                 | CTP busy on  |";
      for( unsigned int i=0; i<LTPI::LINK_NUMBER; i++ )
        if(m_result[s-1][i][0]) print<<"  *   ";
        else                    print<<"  X   ";
    }
    
  if(s == LTPI::BUSY_FROM_LTP || s == LTPI::BUSY_FROM_CTP_OR_LTP)  
    {  
      print<<endl<<"|                 | LTP busy on  |";
      for( unsigned int i=0; i<LTPI::LINK_NUMBER; i++ )
        if(m_result[s-1][i][1]) print<<"  *   ";
        else                    print<<"  X   ";
    }
    
  print<<endl<<"| "<<LTPI::BUSY_SELECTION_NAME[s]<<setw(17-LTPI::BUSY_SELECTION_NAME[s].size())<<"|"<<"     busy off |"; 
  for( unsigned int i=0; i<LTPI::LINK_NUMBER; i++ )
    if(m_result[s-1][i][2]) print<<"  *   ";
    else                    print<<"  X   ";
  print<<endl<<"+-----------------+--------------+-----+-----+-----+"; 
}


int BusyTest::subTest( LTPI::BUSY_SELECTION m )  
{
  if( (m == LTPI::BUSY_FROM_CTP) || (m == LTPI::BUSY_FROM_CTP_OR_LTP) )  m_status = enableBusy(SLAVE_CTP, true );
  if( (m == LTPI::BUSY_FROM_LTP) || (m == LTPI::BUSY_FROM_CTP_OR_LTP) )  m_status = enableBusy(SLAVE_LTP, true ); 
    
  // switching off busy output on LTPI 
  m_status = setLTPIBusySelection(LTPI::BUSY_DISABLED, LTPI::BUSY_DISABLED, LTPI::BUSY_DISABLED);
    
  // check that busy is off
  if( (m_status = checkBusyStatus( false, false, false )) != SUCCESS ) 
    { 
      CERR(" the LTPI is receiveing busy while it is in OFF mode ", ""); 
      return m_status; 
    } 
   
  cout<<endl<<"Testing BUSY "<<LTPI::BUSY_SELECTION_NAME[m]<<endl<<endl;
    
  m_prog->getLTPIConfig()->equal_enable[ LTPI::EQUAL_CTP] = true; 
  m_prog->getLTPIConfig()->equal_enable[ LTPI::EQUAL_LTP] = true;
  m_prog->getLTPIConfig()->signal_select[LTPI::SELECT_BC] = LTPI::SIGNAL_PARALLEL;
     
  for( unsigned int i=0; i<LTPI::LINK_NUMBER; i++ )
    {
      LTPI::BUSY_SELECTION w[LTPI::LINK_NUMBER]={LTPI::BUSY_DISABLED, LTPI::BUSY_DISABLED, LTPI::BUSY_DISABLED};
      w[i] = m;
      bool z[LTPI::LINK_NUMBER] = { false, false, false };
      z[i] = true;
        
      if( (m_status = setLTPIBusySelection( w[LTPI::CTP], w[LTPI::LTP], w[LTPI::NIM] )) != SUCCESS) 
	{
	  CERR("Error while enabling the LTPI %s output for busy %s", 
	       LTPI::LINK_NAME[i].c_str(), LTPI::BUSY_SELECTION_NAME[m].c_str());
	  return m_status;
	}
      
      cout<<"Checking busy status for "<<LTPI::LINK_NAME[i]<<" <- "<<LTPI::BUSY_SELECTION_NAME[m]<<endl;
      
      if( (m == LTPI::BUSY_FROM_CTP) || (m == LTPI::BUSY_FROM_CTP_OR_LTP) ) 
	{
	  cout<<" CTP busy on / LTP busy off ... ";
	  m_status = enableBusy(SLAVE_CTP, true ); 
	  m_status = enableBusy(SLAVE_LTP, false);
	  if( (m_status = checkBusyStatus( z[LTPI::CTP], z[LTPI::LTP], z[LTPI::NIM] )) == SUCCESS)
	    cout<<"ok !"<<endl;
	  m_result[m-1][i][0] = !( static_cast<bool>(m_status));
	}
      
        
      if( (m == LTPI::BUSY_FROM_LTP) || (m == LTPI::BUSY_FROM_CTP_OR_LTP) ) 
	{
	  cout<<" LTP busy on / CTP busy off ... ";
	  m_status = enableBusy(SLAVE_CTP, false); 
	  m_status = enableBusy(SLAVE_LTP, true );
	  if( (m_status = checkBusyStatus( z[LTPI::CTP], z[LTPI::LTP], z[LTPI::NIM] )) == SUCCESS)
	    cout<<"ok !"<<endl;
	  m_result[m-1][i][1] = !( static_cast<bool>(m_status));
	}
      
      
      if ( m == LTPI::BUSY_FROM_CTP ) 
	{
	  cout<<" CTP busy off / LTP busy on ... ";
	  m_status = enableBusy(SLAVE_CTP, false);
	  m_status = enableBusy(SLAVE_LTP, true );
	}
      else if ( m == LTPI::BUSY_FROM_LTP )
	{
	  cout<<" LTP busy off / CTP busy on ... ";
	  m_status = enableBusy(SLAVE_CTP, true );
	  m_status = enableBusy(SLAVE_LTP, false);
	}
      else
	{
	  cout<<" CTP busy off / LTP busy off .. ";
	  m_status = enableBusy(SLAVE_CTP, false);
	  m_status = enableBusy(SLAVE_LTP, false);
	}
      if( (m_status = checkBusyStatus(false, false, false)) == SUCCESS )
	cout<<"ok !"<<endl;
      m_result[m-1][i][2] = !( static_cast<bool>(m_status));
      
      cout<<endl;
    }

  return m_status;
}
 
 
int BusyTest::action()
{
  // setup the LTPs
  if( (m_status = m_prog->getLTP(MASTER_CTP)->setupLTPForBusy(false)) != SUCCESS){
    CERR("Error setting up the Master CTP", "");
    return m_status;
  }
  if( (m_status = m_prog->getLTP(MASTER_LTP)->setupLTPForBusy(false)) != SUCCESS){
    CERR("Error setting up the Master LTP", "");
    return m_status;
  }
  if( (m_status = m_prog->getLTP(SLAVE_CTP )->setupLTPForBusy(true )) != SUCCESS){
    CERR("Error setting up the Slave CTP", "");
    return m_status;
  }
  if( (m_status = m_prog->getLTP(SLAVE_LTP )->setupLTPForBusy(true )) != SUCCESS){
    CERR("Error setting up the Slave LTP", "");
    return m_status;
  }
     
    
  if( m_busy != LTPI::BUSY_SELECTION_NUMBER ) 
    {     
      subTest( static_cast<LTPI::BUSY_SELECTION>(m_busy) );

      printTableHeader();
      printTableLine( static_cast<LTPI::BUSY_SELECTION>(m_busy) );
      printTableHeader( m_file );
      printTableLine( static_cast<LTPI::BUSY_SELECTION>(m_busy), m_file );
    }
  else 
    {
      for( unsigned int i=1; i<LTPI::BUSY_SELECTION_NUMBER; i++ )
	subTest(static_cast<LTPI::BUSY_SELECTION>(i));

      printTableHeader( );
      for( unsigned int i=1; i<LTPI::BUSY_SELECTION_NUMBER; i++ )
	printTableLine( static_cast<LTPI::BUSY_SELECTION>(i) );
      printTableHeader( m_file );
      for( unsigned int i=1; i<LTPI::BUSY_SELECTION_NUMBER; i++ )
	printTableLine( static_cast<LTPI::BUSY_SELECTION>(i), m_file );
    }

  if( (m_status = setLTPIBusySelection( LTPI::BUSY_DISABLED, LTPI::BUSY_DISABLED, LTPI::BUSY_DISABLED )) != SUCCESS ) {
    CERR(" Error switching off busy output on LTPI (after subtest) ", ""); 
    return m_status; 
  } 
   
  return m_status; 
}
  
 
int BusyTest::checkBusyStatus( bool ctp, bool ltp, bool nim ) 
{
  bool busy_ctp  = m_prog->getLTP(MASTER_CTP)->BUSY_linkin_is_busy();
  bool busy_ltp  = m_prog->getLTP(MASTER_LTP)->BUSY_linkin_is_busy();
  bool busy_nim  = m_prog->getLTP(MASTER_CTP)->BUSY_nimin_is_busy() ;
  bool busy_nim2 = m_prog->getLTP(MASTER_LTP)->BUSY_nimin_is_busy();
   
  if( (busy_ctp != ctp) || (busy_ltp != ltp) || (busy_nim != nim) || (busy_nim2 != nim) ) 
    {
      cout<<"FAILED !"<<endl
	  <<"expected CTP = "<<ctp<<      ", LTP = "<<ltp<<     ", NIM = "<<nim<<endl
	  <<"received CTP = "<<busy_ctp <<", LTP = "<<busy_ltp<<", NIM local op = "<<busy_nim<<", NIM ltp op = "<<busy_nim2<<endl<<endl; 
          
      m_status = ERROR;
      return m_status;
    }
  else m_status = SUCCESS;
  return m_status;
}
  

int BusyTest::enableBusy( LTP_TYPE slave, bool ena) 
{
  bool ltpi_isBusy;
    
  if (ena) m_prog->getLTP(slave)->BUSY_enable_constant_level();
  else     m_prog->getLTP(slave)->BUSY_disable_constant_level();

  if( m_prog->getLTP(slave)->BUSY_linkout_is_busy() != ena ) 
    {
      CERR("%s is not sending busy when enabled", LTP_TYPE_NAME[slave].c_str() );
      m_status = ERROR;
      return m_status;
    }
  else m_status = SUCCESS;

  LTPI::LINK lnk = static_cast<LTPI::LINK>(slave-2);
  m_prog->getLTPI()->readSignalStatus( LTPI::STATUS_BUSY, lnk, ltpi_isBusy );
  if ( ltpi_isBusy != ena ) 
    {
      CERR("busy received on LTPI %s does not match busy from %s", LTPI::LINK_NAME[lnk].c_str(), LTP_TYPE_NAME[slave].c_str() );
      m_status = ERROR;
      return m_status;
    }
  else m_status = SUCCESS;

  return m_status;
}


int BusyTest::setLTPIBusySelection(LTPI::BUSY_SELECTION ctp, LTPI::BUSY_SELECTION ltp, LTPI::BUSY_SELECTION nim) 
{ 
  LTPI::BUSY_SELECTION v[LTPI::LINK_NUMBER]={ ctp, ltp, nim };
  for( unsigned int i=0; i<LTPI::LINK_NUMBER; i++ )  
    m_prog->getLTPIConfig()->busy_select[static_cast<LTPI::LINK>(i)] = static_cast<LTPI::BUSY_SELECTION>(v[i]);
  m_prog->getLTPI()->write(*(m_prog->getLTPIConfig()));
   
  return m_status;
}

//-----------------------------------------------------------------------------------------------

const string CalibrationTest::desc[LTPI::CALREQ_SELECTION_NUMBER][2] = 
  { { "CTP <- CTP", "LTP <- LTP" } , { "CTP <- CTP", "LTP <- CTP" } , { "CTP <- LTP", "LTP <- LTP" } };

void CalibrationTest::printTableHeader( ostream &print ) const 
{
  print<<endl<<endl<<endl<<"Summary of results for calibration test :"<<endl
       <<endl<<"+------------+-----+-----+-----+-----+-----+-----+-----+-----+"
       <<endl<<"|            | 000 | 001 | 010 | 011 | 100 | 101 | 110 | 111 |"
       <<endl<<"+------------+-----+-----+-----+-----+-----+-----+-----+-----+"<<endl;
}

 
void CalibrationTest::printTableLine( LTPI::CALREQ_SELECTION m, ostream &print ) const
{
  print<<"| "<<desc[m-1][0]<<" |"; 
  for(int i=0; i<pow(2, LTPI::CALREQ_NUMBER); i++) 
    if(!m_result[m-1][i][0]) print<<"  *   ";
    else                     print<<"  X   ";
  print<<endl<<"| "<<desc[m-1][1]<<" |";
  for(int i=0; i<pow(2, LTPI::CALREQ_NUMBER); i++)
    if(!m_result[m-1][i][1]) print<<"  *   ";
    else                     print<<"  X   ";
  print<<endl<<"+------------+-----+-----+-----+-----+-----+-----+-----+-----+"<<endl;
}


void CalibrationTest::subTest( LTPI::CALREQ_SELECTION m )
{
  cout<<endl<<"Testing the calibration request "<<LTPI::CALREQ_SELECTION_NAME[m]<<" (sent ^ received = XOR)"<<endl<<endl;
   
  // LTPI setup
  m_prog->getLTPIConfig()->calreq_select = m;
  m_prog->getLTPIConfig()->signal_select[LTPI::SELECT_BC] = LTPI::SIGNAL_PARALLEL;
  m_prog->getLTPIConfig()->equal_enable[ LTPI::EQUAL_CTP] = true;
  m_prog->getLTPIConfig()->equal_enable[ LTPI::EQUAL_LTP] = true;
  m_prog->getLTPI()->write(*(m_prog->getLTPIConfig()));
   
  LTP_TYPE slave[2];
  // establish first slave
  if( m == LTPI::CALREQ_FROM_LTP ) slave[0] = SLAVE_LTP;
  else                             slave[0] = SLAVE_CTP;
  // establish second slave
  if( m == LTPI::CALREQ_FROM_CTP ) slave[1] = SLAVE_CTP;
  else                             slave[1] = SLAVE_LTP;
   
  int r[LTP_TYPE_NUMBER] = {0};
  int j0, j1;
   
  for(int i=0; i<pow(2, LTPI::CALREQ_NUMBER); i++) 
    {
      if( m != LTPI::CALREQ_FROM_LTP )
	m_prog->getLTP(SLAVE_CTP)->CAL_set_preset_value(u_short(7-i)); 
      if( m != LTPI::CALREQ_FROM_CTP )
	m_prog->getLTP(SLAVE_LTP)->CAL_set_preset_value(u_short(i));

      if      ( m == LTPI::CALREQ_FROM_CTP ) { j0 = 7-i; j1 = 7-i; }
      else if ( m == LTPI::CALREQ_FROM_LTP ) { j0 = i;   j1 = i;   }
      else                                   { j0 = 7-i; j1 = i;   }
     
      m_prog->getLTP(MASTER_CTP)->CAL_get_linkstatus(&r[0]);
      m_prog->getLTP(MASTER_LTP)->CAL_get_linkstatus(&r[1]);
      m_prog->getLTP( SLAVE_CTP)->CAL_get_linkstatus(&r[2]);
      m_prog->getLTP( SLAVE_LTP)->CAL_get_linkstatus(&r[3]);
     
      cout<<" "<<desc[m-1][0]<<" ... ";
      if( (m_result[m-1][j0][0] = (r[MASTER_CTP] != r[slave[0]])) ) cout<<"FAILED: "; 
      else                                                          cout<<"ok      "; 
      cout<<bitset<3>(r[slave[0]])<<" ^ "<<bitset<3>(r[MASTER_CTP])<<" = "<<bitset<3>(r[slave[0]]^r[MASTER_CTP])<<endl;
     
      cout<<" "<<desc[m-1][1]<<" ... ";
      if( (m_result[m-1][j1][1] = (r[MASTER_LTP] != r[slave[1]])) ) cout<<"FAILED: ";
      else                                                          cout<<"ok      ";
      cout<<bitset<3>(r[slave[1]])<<" ^ "<<bitset<3>(r[MASTER_LTP])<<" = "<<bitset<3>(r[slave[1]]^r[MASTER_LTP])<<endl<<endl;
    } 
}


int CalibrationTest::action()
{
  // setup the LTPs
  if( (m_status = m_prog->getLTP(MASTER_CTP)->setupMasterForCalreq()) != SUCCESS) {
    CERR("Error setting up the CTP Master", "");
    return m_status;
  }
  if( (m_status = m_prog->getLTP(MASTER_LTP)->setupMasterForCalreq()) != SUCCESS) {
    CERR("Error setting up the LTP Master", "");
    return m_status;
  }
  if( (m_status = m_prog->getLTP(SLAVE_CTP)->setupSlaveForCalreq())   != SUCCESS ) {
    CERR("Error setting up the CTP Slave", "");
    return m_status;
  }
  if( (m_status = m_prog->getLTP(SLAVE_LTP)->setupSlaveForCalreq())   != SUCCESS ) {
    CERR("Error setting up the LTP Slave", "");
    return m_status;
  }
        
  if( m_cal != LTPI::CALREQ_SELECTION_NUMBER )
    {
      subTest(static_cast<LTPI::CALREQ_SELECTION>(m_cal));

      printTableHeader();
      printTableLine( static_cast<LTPI::CALREQ_SELECTION>(m_cal) );
      printTableHeader( m_file );
      printTableLine( static_cast<LTPI::CALREQ_SELECTION>(m_cal), m_file );
     
    }
  else
    {
      for( unsigned int i=1; i<LTPI::CALREQ_SELECTION_NUMBER; i++ ) 
	subTest(static_cast<LTPI::CALREQ_SELECTION>(i));
     
      printTableHeader();
      for( unsigned int i=1; i<LTPI::CALREQ_SELECTION_NUMBER; i++ )
	printTableLine( static_cast<LTPI::CALREQ_SELECTION>(i) );
      printTableHeader( m_file );
      for( unsigned int i=1; i<LTPI::CALREQ_SELECTION_NUMBER; i++ )
	printTableLine( static_cast<LTPI::CALREQ_SELECTION>(i), m_file );
    }
  return m_status;
}


int LTPextension::setupMasterForCalreq()
{
  if( (m_status = this->Reset() ) != Test::SUCCESS) {
    CERR("Error at master->Reset()", "");
    return m_status;
  }
    
  // BC
  if( (m_status = this->BC_local_intern()) != Test::SUCCESS) {
    CERR("Error at master->BC_local_intern()", "");
    return m_status;
  }
  if( (m_status = this->BC_linkout_from_local())!= Test::SUCCESS) {
    CERR("Error at master->BC_linkout_from_local()", "");
    return m_status;
  }
  if( (m_status = this->BC_lemoout_from_local()) != Test::SUCCESS) {
    CERR("Error at master->BC_lemoout_from_local()", "");
    return m_status;
  }
    
  // enable test pad
  if( (m_status = this->CAL_enable_testpath()) != Test::SUCCESS) {
    CERR("Error at master->CAL_enable_testpath()", "");
    return m_status;
  }
  // read calibration from link
  if( (m_status = this->CAL_source_link()) != Test::SUCCESS) {
    CERR("Error at master->CAL_source_link()", "");
    return m_status;
  }
    
  return m_status;
}

int LTPextension::setupSlaveForCalreq()
{
  if( (m_status = this->Reset()) != Test::SUCCESS) {
    CERR("Error at slave->Reset()", "");
    return m_status;
  }
    
  // BC
  if( (m_status = this->BC_linkout_from_linkin()) != Test::SUCCESS) {
    CERR("Error at slave->BC_linkout_from_linkin()", "");
    return m_status;
  }
  if( (m_status = this->BC_lemoout_from_linkin()) != Test::SUCCESS) {
    CERR("Error at slave->BC_lemoout_from_linkin()", "");
    return m_status;
  }
        
  // set calibration request from preset
  if( (m_status = this->CAL_source_preset()) != Test::SUCCESS) {
    CERR("Error at slave->CAL_source_preset()", "");
    return m_status;
  }
  if( (m_status = this->CAL_set_preset_value(0)) != Test::SUCCESS) {
    CERR("Error at slave->CAL_set_preset_value(0)", "");
    return m_status;
  }
   
  // enable test pad
  if( (m_status = this->CAL_enable_testpath()) != Test::SUCCESS) {
    CERR("Error at slave->CAL_enable_testpath()", "");
    return m_status;
  }
   
  return m_status;
}

//------------------------------------------------------------------------------------------------

int TriggerTypeTest::action()
{
  // LTPI::printSignalSelectionLegend(LTPI::PRINT_TTYP);
  if(!m_all)
    subTest( m_ttyp_ext, m_ttyp_loc );
  // TODO: print results ?
  else
    {
      subTest( LTPI::TTYP_PARALLEL, -1 );
      subTest( LTPI::TTYP_FROM_CTP, -1 );
      subTest( LTPI::TTYP_FROM_LTP, -1 );
      subTest( LTPI::TTYP_FROM_LOCAL, LTPI::LOCTTYP_FROM_TTYP_WORD0 );
      subTest( LTPI::TTYP_FROM_LOCAL, LTPI::LOCTTYP_FROM_CTP_TTRG   );
      subTest( LTPI::TTYP_FROM_LOCAL, LTPI::LOCTTYP_FROM_LTP_TTRG   );
      subTest( LTPI::TTYP_FROM_LOCAL, LTPI::LOCTTYP_FROM_NIM_TTRG   );
    }
  return 0;
}


int TriggerTypeTest::subTest( LTPI::TTYP_SELECTION lev0, int lev1 )
{
  if( (m_status = m_prog->checkForFile( m_prog->getPGForTTYP(lev1) )) != SUCCESS )
    {
      cout<<"FILE "<<m_prog->getPGForTTYP(lev1)<<" NOT FOUND \nTriggerType Test will NOT work"<<endl;
      return m_status;
    }
   
  cout<<"Testing trigger type connections "<<LTPI::TTYP_SELECTION_DESC[lev0];
  if(lev1>=0) cout<<" (LOC = "<<LTPI::LOCTTYP_SELECTION_NAME[lev1]<<")";
  cout<<endl;
    
  // set the LTPI
  if( lev0 == LTPI::TTYP_PARALLEL )  
    m_prog->getLTPIConfig()->setMode( LTPI::MODE_PARALLEL );
  if( lev0 == LTPI::TTYP_FROM_CTP || lev1 == LTPI::LOCTTYP_FROM_CTP_TTRG || lev1 ==LTPI::LOCTTYP_FROM_TTYP_WORD0 )
    m_prog->getLTPIConfig()->setMode( LTPI::MODE_FROM_CTP );
  if( lev0 == LTPI::TTYP_FROM_LTP || lev1 == LTPI::LOCTTYP_FROM_LTP_TTRG )  
    m_prog->getLTPIConfig()->setMode( LTPI::MODE_FROM_LTP );
  if( lev1 == LTPI::LOCTTYP_FROM_NIM_TTRG )
    m_prog->getLTPIConfig()->setMode( LTPI::MODE_FROM_NIM );
  m_prog->getLTPIConfig()->ttyp_select = lev0;
  if(lev0 == LTPI::TTYP_FROM_LOCAL )
    m_prog->getLTPIConfig()->locttyp_select = static_cast<LTPI::LOCTTYP_SELECTION>(lev1);
  m_prog->getLTPI()->write(*(m_prog->getLTPIConfig()));
    

  // establish the masters
  LTP_TYPE master[2];
    
  // establish first master
  if( lev0 == LTPI::TTYP_PARALLEL || lev0 == LTPI::TTYP_FROM_CTP 
      || lev1 == LTPI::LOCTTYP_FROM_CTP_TTRG || lev1 == LTPI::LOCTTYP_FROM_NIM_TTRG )
    master[0] = MASTER_CTP;
  if( lev0 == LTPI::TTYP_FROM_LTP || lev1 == LTPI::LOCTTYP_FROM_LTP_TTRG )  
    master[0] = MASTER_LTP;
       
  // establish second master
  if( lev0 == LTPI::TTYP_FROM_CTP || lev1 == LTPI::LOCTTYP_FROM_CTP_TTRG || lev1 == LTPI::LOCTTYP_FROM_NIM_TTRG )
    master[1] = MASTER_CTP;
  if( lev0 == LTPI::TTYP_FROM_LTP || lev0 == LTPI::TTYP_PARALLEL || lev1 == LTPI::LOCTTYP_FROM_LTP_TTRG  )
    master[1] = MASTER_LTP;
    
  if( lev0 == LTPI::TTYP_FROM_LOCAL && lev1 == LTPI::LOCTTYP_FROM_TTYP_WORD0 )
    {
      // TTYP[0] -> slave CTP
      cout<<endl<<"Testing trigger type from local register to "<<LTP_TYPE_NAME[SLAVE_CTP]<<" ... ";
      if( (m_status = testTTYPFromLocalRegister(m_prog->getLTP(SLAVE_CTP))) == SUCCESS )
        cout<<" ok"<<endl;
      
      // TTYP[0] -> slave LTP
      cout<<endl<<"Testing trigger type from local register to "<<LTP_TYPE_NAME[SLAVE_LTP]<<" ... ";
      if( (m_status = testTTYPFromLocalRegister(m_prog->getLTP(SLAVE_LTP))) == SUCCESS )
        cout<<" ok"<<endl;
    }
    
  if( lev0 == LTPI::TTYP_PARALLEL || lev0 == LTPI::TTYP_FROM_CTP || lev0 == LTPI::TTYP_FROM_LTP ||
      (lev0 == LTPI::TTYP_FROM_LOCAL && lev1 != LTPI::LOCTTYP_FROM_TTYP_WORD0) )
    {
      string pg_trt = m_prog->getPGForTTYP(lev1);
       
      // master1 -> slave CTP
      cout<<" First case (to slave CTP)"<<endl;
      m_prog->getLTP(master[0])->setupSenderForTTYP(pg_trt);
      m_prog->getLTP(SLAVE_CTP)->setupReceiverForTTYP(lev1);
      testTTYPsignal( m_prog->getLTP(master[0]), m_prog->getLTP(SLAVE_CTP), lev1 );
     
      // master2 -> slave LTP 
      cout<<" Second case (to slave LTP)"<<endl;
      m_prog->getLTP(master[1])->setupSenderForTTYP( pg_trt);
      m_prog->getLTP(SLAVE_LTP)->setupReceiverForTTYP(lev1);
      testTTYPsignal(m_prog->getLTP(master[1]), m_prog->getLTP(SLAVE_LTP), lev1 );
    }

  return 0;
}


int Test::readAndCompareFIFO( int counted, int lev1, LTPextension* sender, LTPextension* receiver)
{
  int k = 0;
  int b=0;
    
  u_short trt_slave, trt_master;
  while(counted--)
    {
      // check the receiver trigger type FIFOs
      if( lev1 < 0 ) // external
        {
	  if ( (m_status=(sender->TTYPE_fifo_is_empty())) != SUCCESS ) { 
	    cout<<endl<<"ERROR: master trigger type FIFO empty at L1A number "<<k<<endl;
	    return m_status;
          }
	  sender->TTYPE_read_fifo(&trt_master);
        }
      else // local ( Get the expected value from the LTPI register file )
	m_prog->getLTPI()->readLocalTriggerTypeWord( k, trt_master );
       
      // Check  slave trigger type FIFO
      if ( (m_status=(receiver->TTYPE_fifo_is_empty())) != SUCCESS ) {
	cout<<endl<<"ERROR slave trigger type FIFO empty at L1A number "<<k<<endl;
	return m_status; 
      }
      receiver->TTYPE_read_fifo(&trt_slave);
       
      // check the values
      if(trt_slave != trt_master) {
	cout<<endl<<"FIFO "<<setw(3)<<k<<" ERROR: "<<setbase(16)<<"0x"<<setw(2)<<trt_master
	    <<" ^ 0x"<<setw(2)<<trt_slave<<" = "<<(bitset<8>(trt_master^trt_slave))<<setbase(10); 
	b++;
      }
       
      if(lev1<0) k++;
      else       k = (k + 1) & 7;
    } // end while
     
  // check if there is still something in the FIFO after end of while loop
  if(!receiver->TTYPE_fifo_is_empty()) {
    CERR( "ERROR: FIFO still not empty!", "" );
    b++;
  }
    
  if(b!=0) m_status = ERROR;
    
  return m_status;
}


int Test::testTTYPsignal( LTPextension* sender, LTPextension* receiver , int lev1 )
{
  
  for( unsigned int i=0; i<m_prog->getTTYPiter(); i++ )
    {
      cout<<endl<<"Iteration "<<i+1<<"/"<<m_prog->getTTYPiter()<<" : ";
      
      setTable(lev1, sender);
       
      sender->TTYPE_reset_fifo();
      receiver->TTYPE_reset_fifo();
      sender->COUNTER_reset();
      receiver->COUNTER_reset();
      
      // send the test triggers
      sender->PG_start();
      
      // wait for the pattern generator to finish
      u_int  loop = 0;
      while(true)
	{
	  if( sender->PG_is_idle() )
	    break;
	  if( ++loop >= TIMEOUT ) {
	    CERR( "Timeout error for %s", LTPI::SIGNAL_NAME[LTPI::L1A].c_str() );
	    return ERROR_TIMEOUT;
	  }
	}
       
      int counted;
      m_status = readAndCompareCounters(sender, receiver, LTPI::L1A, counted);
      
      cout<<endl<<" counted "<<counted<<"; reading and comparing FIFO couters ... ";
      if( (m_status = readAndCompareFIFO( counted, lev1 , sender, receiver )) == SUCCESS)
        cout<<"ok"<<endl;
    }
  cout<<endl;
    
  return m_status;
} 
 
 
int LTPextension::setupSenderForTTYP( string pg_trt )
{
  m_status = this->OM_master_patgen( pg_trt );
  m_status = this->PG_runmode_singleshot_vme();
  m_status = this->TTYPE_local_from_pg();
  m_status = this->TTYPE_linkout_from_local();
  m_status = this->TTYPE_fifotrigger_L1A();
  m_status = this->LTPFifoDelay(2);
  m_status = this->LTPCounterSource(LTPI::L1A);
  return m_status;
}
 
int LTPextension::setupReceiverForTTYP( int lev1 )
{
  // setup the receiver
  m_status = this->OM_slave();
  m_status = this->BUSY_local_without_nimin();
  m_status = this->TTYPE_linkout_from_linkin();
  m_status = this->TTYPE_fifotrigger_L1A();
  // TODO: give the user the option of choosing the FIFO delay value between 2-5
  if( lev1 < 0 ) m_status = this->LTPFifoDelay(2); // external
  else           m_status = this->LTPFifoDelay(3); // local
  m_status = this->LTPCounterSource(LTPI::L1A);
  return m_status;
}
 
     
int Test::setTable( int lev1, LTPextension* sender )
{
  if( lev1 >= 0 ) // local case
    {
      u_short value(0);
      u_short check(0);
      for( unsigned int num=0; num<LTPI::LOCTTYP_NUMBER; num++ ) 
	{
	  if( num%2 == 0 )
	    { 
	      value = rand()%256;
	      m_prog->getLTPI()->writeLocalTriggerTypeWord(num, value);
	      m_prog->getLTPI()->readLocalTriggerTypeWord(num, check);
	      if (check != value) 
		{
		  cout<<setbase(16)<<"setTable: <<reading back a different value ("<<
		    check<<") that what was tried to be written ("<<value<< ") on 000>>"<<endl<<setbase(10);
		  m_status=ERROR;
		}
	    }
	  else
	    {
	      m_prog->getLTPI()->writeLocalTriggerTypeWord(num, ~value & 0xff);
	      m_prog->getLTPI()->readLocalTriggerTypeWord(num, check);
	      if (check != (~value & 0xff)) 
		{
		  cout<<setbase(16)<<"setTable: <<reading back a different value ("<< 
		    check<<") that what was tried to be written ("<<(~value & 0xff)<<") on 000>>"<<endl<<setbase(10);
		  m_status=ERROR;
		}
	    }
	}
      /*   
	   for (unsigned int i = 0; i < LTPI::LOCTTYP_NUMBER; i++)
	   {m_prog->getLTPI()->readLocalTriggerTypeWord(i, value);
	   cout<<"TRT"<<bitset<3>(i)<<" = "<<setbase(16)<<value<<endl;
	   cout<<setbase(10)<<endl; }*/
    }
    
  else // external case
    { 
      int trt;
      trt = rand()%256;
      sender->TTYPE_write_regfile0( trt );
      sender->TTYPE_write_regfile1( ~trt & 0xff ); 
      trt = rand()%256;
      sender->TTYPE_write_regfile2( trt );
      sender->TTYPE_write_regfile3( ~trt & 0xff );
      
      u_short regfr;
      cout<<setbase(16); 
      sender->TTYPE_read_regfile0(&regfr); cout<< " 0x"<<regfr; 
      sender->TTYPE_read_regfile1(&regfr); cout<<", 0x"<<regfr; 
      sender->TTYPE_read_regfile2(&regfr); cout<<", 0x"<<regfr; 
      sender->TTYPE_read_regfile3(&regfr); cout<<", 0x"<<regfr;
      cout<<setbase(10);
      
    }
  return m_status;
}

 
int TriggerTypeTest::testTTYPFromLocalRegister( LTPextension* receiver ) 
{
  int b=0;
    
  // receiner setup
  m_status = receiver->OM_slave();
  m_status = receiver->BUSY_local_without_nimin();
  m_status = receiver->TTYPE_linkout_from_linkin();
  m_status = receiver->TTYPE_eclout_from_linkin();
  m_status = receiver->TTYPE_reset_fifo();
    
  for (u_short trt = 0; trt < pow(2, LTPI::TTYP_NUMBER); trt++) 
    {
      m_status = m_prog->getLTPI()->writeLocalTriggerTypeWord(0, trt);
      u_short ltp_trt; // write pulse for the trigger type fifo
      m_status = receiver->TTYPE_write_fifo();
      m_status = receiver->TTYPE_read_fifo(&ltp_trt);
      if (ltp_trt != trt) 
	{
	  cout<<endl<<"FAILED: trigger type mismatch, "<<setbase(16)<<trt<<" ^ "<<ltp_trt<<" = "<<(trt^ltp_trt)<<setbase(10);
	  b++;
	}
    }

  if(b!=0) 
    {m_status = ERROR; cout<<endl;}
   
  return m_status;
}

//---------------------------------------------------------------------------------------------------

void DelayScan::printResults( unsigned int sig1, unsigned int sig2, LTP_TYPE master, LTP_TYPE slave, ostream &print ) const
{
  print<<endl<<endl<<"Summary of results for delay scan test between "
       <<LTP_TYPE_NAME[master]<<" -> "<<LTP_TYPE_NAME[slave]<<":"<<endl;
    
  print<<endl<<"+------------+";
  for(u_short i=0; i<=DELAY_MAX; i++)
    print<<"--";
  print<<endl<<"|            |";
  for(u_short i=0; i<=19; i++)
    print<<"  ";
  for(u_short i=0; i<=19; i++)
    print<<"1 ";
  for(u_short i=0; i<=DELAY_MAX-40; i++)
    print<<"2 ";
  print<<endl<<"|            |";
  for(u_short i=0; i<=DELAY_MAX; i++)
    print<<(static_cast<int>(floor(i*0.5)))%10<<" ";
  print<<endl<<"|            |";
  for(u_short i=0; i<=DELAY_MAX; i++)
    print<<". ";
  print<<endl<<"|            |";
  for(u_short i=0; i<=DELAY_MAX; i++)
    print<<10*(i*0.5-i/2)<<" ";
  print<<endl<<"+------------+";
  for(u_short i=0; i<=DELAY_MAX; i++)
    print<<"--";
  
  for(unsigned int sig=sig1; sig<sig2; sig++)
    {
      if(sig!=9)
        print<<endl<<"| "<<setw(10)<<LTPI::DELAY_SIGNAL_NAME[sig]<<" |";
      else
        print<<endl<<"| TTYP (L1A) |";
      for(u_short i=0; i<=DELAY_MAX; i++)
        if(m_result[sig][i]) print<<"* ";
        else                 print<<"X ";
    }
  print<<endl<<"+------------+";
  for(u_short i=0; i<=DELAY_MAX; i++)
    print<<"--";
  print<<endl;
}


void DelayScan::subTest( LTP_TYPE master, LTP_TYPE slave, unsigned int sig , bool ttyp)
{
  cout<<endl<<"Start testing delay scan for signal "<<LTPI::DELAY_SIGNAL_NAME[sig]<<
    " between "<<LTP_TYPE_NAME[master]<<" -> "<<LTP_TYPE_NAME[slave]<<endl<<endl;
   
  for(u_short delay=0; delay<=DELAY_MAX; delay++) 
    { 
      if( slave == SLAVE_CTP ) 
        m_prog->getLTPI()->writeSignalDelay(       static_cast<LTPI::DELAY_SIGNAL>(sig), delay, true ); 
      else   
        m_prog->getLTPIhelper()->writeSignalDelay( static_cast<LTPI::DELAY_SIGNAL>(sig), delay, true );
      
      cout<<"For delay[ "<<LTPI::DELAY_SIGNAL_NAME[sig]<<" ] = "<<delay*0.5;
      if(delay%2==0) cout<<".0 ns => ";
      else           cout<<  " ns => ";
      
      if(!ttyp) 
	{
	  m_status = testSignal(m_prog->getLTP(master), m_prog->getLTP(slave), static_cast<LTPI::SIGNAL>(sig));
	  m_result[sig][delay] = !(static_cast<bool>(m_status));
	  if(m_status!=ERROR) cout<<"ok"<<endl;
	}
      else
	{
	  m_status = testTTYPsignal(m_prog->getLTP(master), m_prog->getLTP(slave), -1);
	  m_result[9][delay] = !(static_cast<bool>(m_status));
	  //if(m_status!=ERROR) cout<<"ok"<<endl;
	}
    }
}


int DelayScan::action()
{ 
  if( (m_status = m_prog->checkForFile(m_prog->getPGFile())) != SUCCESS )
    {
      cout<<endl<<">>  PATTERN GENERATOR FILE \""<<m_prog->getPGFile()<<"\" COULD NOT BE FOUND !!!"
	  <<endl<<">>  An example of an pattern generator file can be found in RCDLtpi/data/pg_rand.dat"
	  <<endl<<">>  Signal Connection Test and Delay Scan Test will NOT work !"<<endl<<endl;
      return m_status;
    }
  if( (m_status = m_prog->checkForFile( m_prog->getPGForTTYP(-1) )) != SUCCESS )
    {
      cout<<"FILE "<<m_prog->getPGForTTYP(-1)<<" NOT FOUND \nTriggerType Test will NOT work"<<endl;
      return m_status;
    }
        
        
  if( !all_combinations )
    {
      LTP_TYPE master = static_cast<LTP_TYPE>(enterInt(  "Select the sender   (0=CTP master, 1=LTP master)",0,1));
      LTP_TYPE slave  = static_cast<LTP_TYPE>(2+enterInt("Select the receiver (0=CTP slave,  1=LTP slave )",0,1));
      cout<<endl;
      singleCombination(master, slave);
    }
  else
    {
      singleCombination(MASTER_CTP, SLAVE_CTP);
      singleCombination(MASTER_LTP, SLAVE_CTP);
      singleCombination(MASTER_CTP, SLAVE_LTP);
      singleCombination(MASTER_LTP, SLAVE_LTP);
    }
  return 0;
}

 
void DelayScan::singleCombination( LTP_TYPE master, LTP_TYPE slave)
{
  if(static_cast<bool>(master)) m_prog->getLTPIConfig()->setMode(LTPI::MODE_FROM_LTP);
  else                          m_prog->getLTPIConfig()->setMode(LTPI::MODE_FROM_CTP);
  m_prog->getLTPI()->write(*(m_prog->getLTPIConfig()));
   
  if( sig != 0 )  
    {
      m_prog->getLTP(master)->SetupSender( m_prog->getPGFile() );
      m_prog->getLTP(slave)->SetupReceiver(0);
      subTest( master, slave, sig, false );

      printResults(sig, sig+1, master, slave);
      printResults(sig, sig+1, master, slave, m_file);
    }
   
  else if( grp != 0 )
    {
      cout<<endl<<"Start testing delay scan for group "<<LTPI::DELAY_GROUP_NAME[grp]<<endl<<endl;
     
      if( grp != LTPI::DELAY_GROUP_TTYP ) 
	{
	  m_prog->getLTP(master)->SetupSender( m_prog->getPGFile() );
	  m_prog->getLTP(slave )->SetupReceiver(0);
	  for( unsigned int signal = LTPI::DELAY_GROUP_LO[grp]; signal < LTPI::DELAY_GROUP_HI[grp]; signal++ )
	    subTest(master, slave, signal, false );
 
	  printResults(LTPI::DELAY_GROUP_LO[grp], LTPI::DELAY_GROUP_HI[grp], master, slave);
	  printResults(LTPI::DELAY_GROUP_LO[grp], LTPI::DELAY_GROUP_HI[grp], master, slave, m_file);
	}
      else // ttyp group
	{
	  string pg_trt = m_prog->getPGForTTYP(-1);
	  m_prog->getLTP(master)->setupSenderForTTYP(pg_trt);
	  m_prog->getLTP(slave )->setupReceiverForTTYP(-1);
	  subTest(master, slave, LTPI::DELAY_L1A, true);

	  printResults(9,10, master, slave);
	  printResults(9,10, master, slave, m_file);
	}
    }
   
  else // all signals
    {
      m_prog->getLTP(master)->SetupSender( m_prog->getPGFile() );
      m_prog->getLTP(slave )->SetupReceiver(0);
      string pg_trt = m_prog->getPGForTTYP(-1);
      for( unsigned int signal = LTPI::DELAY_ORB; signal <= LTPI::DELAY_BGO3; signal++ )
        subTest(master, slave, signal, false );
      m_prog->getLTP(master)->setupSenderForTTYP(pg_trt);
      m_prog->getLTP(slave )->setupReceiverForTTYP(-1);
      subTest(master, slave, LTPI::DELAY_L1A, true);

      printResults(0,10, master, slave);
      printResults(0,10, master, slave, m_file);
    }
}

//--------------------------------------------------------------------------------------

int SuperTest::action()
{
  bool delay = static_cast<bool>( enterInt("Include delay scan test ? (0=NO, 1=YES)",0,1) );
  
  ConnectionTest con_t( m_prog, "Connection Test", LTPI::SIGNAL_SELECTION_NUMBER );
  con_t.action();       
                           
  BusyTest busy_t(      m_prog, "Busy Test", LTPI::CALREQ_SELECTION_NUMBER);
  busy_t.action();      
                          
  CalibrationTest cal_t(m_prog, "Calibration Test", LTPI::CALREQ_SELECTION_NUMBER );
  cal_t.action();       
                         
  if(delay)             
    {                    
      DelayScan del_t(    m_prog, "Delay Scan", 0, 0, true);
      del_t.action();
    }
   
  TriggerTypeTest ttyp_t(m_prog, "Trigger Types Test", static_cast<LTPI::TTYP_SELECTION>(0), 0, true);
  ttyp_t.action();  
   
  return 0;
}

//------------------------------------------------------------------------------------------------

//----------------------- used in Trigger Type Test ---------------------------------
int LTPextension::LTPFifoDelay( int  delay)
{
  switch(delay) 
    {
    case 2:  return this->TTYPE_fifodelay_2bc();
    case 3:  return this->TTYPE_fifodelay_3bc();
    case 4:  return this->TTYPE_fifodelay_4bc();
    case 5:  return this->TTYPE_fifodelay_5bc();
    default: return(-1);
    }
}
 
//----------------------- used in Connection Test ---------------------------------

int LTPextension::LoadPatGenFile( const string& fn )
{
  if( fn == m_last_pg_file )
    {
      COUT("pattern generator file \"%s\" already loaded",fn.c_str());
      return Test::SUCCESS;
    }
    
  // running in master mode with pattern generator
  if( (m_status = this->OM_master_patgen(const_cast<string&>(fn))) != 0 ) 
    {
      CERR("loading pattern generator file \"%s\"", fn.c_str());
      return m_status;
    }
  m_last_pg_file = fn;
  return m_status;
}


int LTPextension::SetupSender( string pgfilename )
{
  //if( this->LoadPatGenFile(pgfilename) != 0 )
  //  return m_status;
  this->OM_master_patgen( pgfilename );
  this->ORB_local_pg();
  // this->IO_disable_busy_gating(L1A);
  // this->IO_disable_busy_gating(TR1);
  // this->IO_disable_busy_gating(TR2);
  // this->IO_disable_busy_gating(TR3);
  return m_status;
}


int LTPextension::SetupReceiver( int n )
{
  if( n%2 == 1 ) // lemo mode  ( makes it a slave with input from NIM )
    {
      this->OM_master_lemo();
      this->BC_local_lemo();
      this->ORB_local_lemo();
    }
  else // link mode
    {
      this->OM_slave();                 // running in slave mode
      this->BUSY_local_without_nimin(); // disable busy input
    }
  return m_status;
}


int LTPextension::LTPCounterSource(LTPI::SIGNAL sig)
{ 
  switch(sig)
    {
    case LTPI::ORB:   return this->COUNTER_orb();
    case LTPI::L1A:   return this->COUNTER_L1A();
    case LTPI::TTRG1: return this->COUNTER_TR1();
    case LTPI::TTRG2: return this->COUNTER_TR2();
    case LTPI::TTRG3: return this->COUNTER_TR3();
    case LTPI::BGO0:  return this->COUNTER_BG0();
    case LTPI::BGO1:  return this->COUNTER_BG1();
    case LTPI::BGO2:  return this->COUNTER_BG2();
    case LTPI::BGO3:  return this->COUNTER_BG3();
    default:          return(-1);
    }
}

// --------------------------- used in Busy Test -------------------------------

int LTPextension::setupLTPForBusy( bool slave )
{
  if( ( m_status = this->Reset() ) != Test::SUCCESS) {
    CERR("Error at LTP->Reset()", "");
    return m_status;
  }
    
  if(slave)
    {
      if( ( m_status = this->BUSY_local_without_linkin() ) != Test::SUCCESS ) {
	CERR( "Error at slave->BUSY_local_without_linkin()", "" );
	return m_status;
      }
      
      // BC for slave
      if( ( m_status = this->BC_linkout_from_linkin() ) != Test::SUCCESS ) {
	CERR( "Error at slave->BC_linkout_from_linkin()", "" );
	return m_status;
      }
      if( ( m_status = this->BC_lemoout_from_linkin() ) != Test::SUCCESS ) {
	CERR( "Error at slave->BC_lemoout_from_linkin()", "" );
	return m_status;
      }
    }
  else
    {
      if( ( m_status =  this->BUSY_local_with_linkin() ) != Test::SUCCESS ) {
	CERR( "Error at master->BUSY_local_with_linkin()", "" );
	return m_status;
      }
      
      // BC for master
      if( ( m_status = this->BC_local_intern() ) != Test::SUCCESS ) {
	CERR( "Error at master->BC_local_intern()", "" );
	return m_status;
      }
      if( ( m_status = this->BC_linkout_from_local() ) != Test::SUCCESS ) {
	CERR( "Error at master->BC_linkout_from_local()", "" );
	return m_status;
      }
      if( ( m_status = this->BC_lemoout_from_local()) != Test::SUCCESS ) {
	CERR( "Error at master->BC_lemoout_from_local()()", "" );
	return m_status;
      }
    }
    
  if( ( m_status = this->BUSY_local_without_nimin() ) != Test::SUCCESS ) {
    CERR( "Error at BUSY_local_without_nimin()", "" );
    return m_status;
  }
  if( ( m_status = this->BUSY_local_without_ttlin() ) != Test::SUCCESS ) {
    CERR( "Error at BUSY_local_without_ttlin()", "" );
    return m_status;
  }
  if( ( m_status = this->BUSY_local_without_deadtime() ) != Test::SUCCESS ) {
    CERR( "Error at BUSY_local_without_deadtime()", "" );
    return m_status;
  }
  // send busy to link
  if( ( m_status = this->BUSY_linkout_from_local() ) != Test::SUCCESS ) {
    CERR( "Error at BUSY_linkout_from_local()", "" );
    return m_status;
  }
  // send busy to NIM
  if( ( m_status =  this->BUSY_lemoout_busy_only() ) != Test::SUCCESS ) {
    CERR( "Error at this->BUSY_lemoout_busy_only()", "" );
    return m_status;
  }

  return m_status;
}

//--------------------------------------------------------------------------------------------

int MainProgram::action() 
{
  // creating LTPI configuration
  m_ltpi_cfg = new LTPI::Configuration();
  m_ltpi_cfg->setSignalDelay(DEFAULT_DELAY);
    
    
  m_status = readInputFile();
    

  // creating LTPI module 
  m_ltpi = new LTPI( m_vme, m_base_ltpi );
  if( ( m_status = (*m_ltpi)() ) != LTPI::SUCCESS ) {
    CERR("opening module \"LTPI\" at VME address = 0x%06x ", m_base_ltpi );
    return m_status;
  }
   
  bool ready;
  if((m_status = m_ltpi->ready(ready)) != LTPI::SUCCESS) {
    CERR("reading ready status of LTPI","");
    return m_status;
  }
   
  if(!ready) {
    if( ( m_status = m_ltpi->init()) != LTPI::SUCCESS ) {
      CERR("initalizing LTPI", "");
      return m_status;
    }
  }
   
  unsigned int manuf, board, revision;
  if((m_status = m_ltpi->readEEPROM(manuf,board,revision)) != LTPI::SUCCESS) {
    CERR("reading EEPROM","");
    return m_status; 
  }  
   
  // structure for time and user name
  time_t ntm;
  struct tm* now;
  struct passwd* pwd;
   
  // get date and user name
  ntm = time((time_t*)0);
  now = localtime(&ntm);
  pwd = getpwuid(getuid());
   
  int year = now->tm_year+1900;
  int mon  = now->tm_mon;
  int day  = now->tm_mday;
  int hour = now->tm_hour;
  int min  = now->tm_min;
   
  // set the output file name 
  ostringstream ss;
  ss << hex << board;
  string board_string = ss.str();
  string year_string  = std::to_string( year );
  string mon_string   = std::to_string( mon );
  if (mon<10)  mon_string = "0" + mon_string;
  string day_string   = std::to_string( day  );
  if (day<10)  day_string = "0" + day_string;
  string hour_string  = std::to_string( hour );
  if (hour<10)  hour_string = "0" + hour_string;
  string min_string   = std::to_string( min  );
  if (min<10)  min_string = "0" + min_string;
  m_output_filename = "LTPI" + board_string.substr(board_string.size()-2, 2) + "_" 
    + year_string + mon_string + day_string + hour_string + min_string + ".out";
  cout<<"output file name = "<<m_output_filename<<endl;
   
  if(save)
    m_file.open( m_output_filename.c_str() ); // correct ?

  if(!m_file) 
    CERR("cannot open output file \"%s\"", m_output_filename.c_str());

  // print the user name and the date
  cout<<endl<<"**********************************"<<endl 
      <<"User Name : "<<pwd->pw_name<<" ("<<pwd->pw_gecos<<")"<<endl 
      <<"Date      : "<<setfill('0')<<setw(2)<<day<<"-"<<setw(2)<<MON[mon]<<"-"<<setw(4)<<year
      <<" "<<setw(2)<<hour<<":"<<setw(2)<<min<<":"<<setw(2)<<(now->tm_sec)<<setfill(' ')<<endl 
      <<"**********************************"<<endl<<endl;

  m_file<<endl<<"**********************************"<<endl
	<<"User Name : "<<pwd->pw_name<<" ("<<pwd->pw_gecos<<")"<<endl
	<<"Date      : "<<setfill('0')<<setw(2)<<day<<"-"<<setw(2)<<MON[mon]<<"-"<<setw(4)<<year
	<<" "<<setw(2)<<hour<<":"<<setw(2)<<min<<":"<<setw(2)<<(now->tm_sec)<<setfill(' ')<<endl
	<<"**********************************"<<endl<<endl;
   
  // creating LTPI helper module
  m_ltpi_helper = new LTPI( m_vme, m_base_ltpi_helper );
  if( ( m_status = (*m_ltpi_helper)() ) != LTPI::SUCCESS ) {
    CERR("opening module \"LTPI helper\" at VME address = 0x%06x ", m_base_ltpi_helper );
    return m_status;
  }
  if((m_status = m_ltpi_helper->ready(ready)) != LTPI::SUCCESS) {
    CERR("reading ready status of LTPI helper","");
    return m_status;
  }
  if(!ready)
    {
      if( ( m_status = m_ltpi_helper->init()) != LTPI::SUCCESS ) {
	CERR("initalizing LTPI helper ", "");
	return m_status;
      }
    }

  // createing LTPI helper configuration
  m_ltpi_helper_cfg = new LTPI::Configuration();
  m_ltpi_helper_cfg->setMode(LTPI::MODE_FROM_LTP);
  m_ltpi_helper_cfg->busy_select[LTPI::LTP]=LTPI::BUSY_FROM_CTP;
  m_ltpi_helper_cfg->calreq_select = LTPI::CALREQ_FROM_CTP;
  m_ltpi_helper_cfg->setSignalDelay(DEFAULT_DELAY);
  if( (m_status = m_ltpi_helper->write(*(m_ltpi_helper_cfg))) != LTPI::SUCCESS  ) {
    CERR("configuring \"LTP helper\" ", "");
    return m_status;
  }

  // creating LTPs
  for(unsigned int i=0; i<LTP_TYPE_NUMBER; i++)
    {
      m_ltp[i] = new LTPextension();
      if( ( m_status =  m_ltp[i]->Open(m_base_ltp[i]) ) != LTPI::SUCCESS )
	{
	  CERR("opening module \"%s\" at VME address = 0x%06x ", LTP_TYPE_NAME[i].c_str(), m_base_ltp[i] );
	  return m_status;
	}
    }
  
  
  //m_status = readInputFile();
  m_status = readInputFile( m_file );

  printInfo();
  printInfo(m_file);
  
  return m_status;
}


void MainProgram::printInfo( ostream &print ) const
{
  unsigned int manuf, board, revision;
   
  m_ltpi->readEEPROM(manuf,board,revision);
   
  print<<endl<<"Opened module \"LTPI\" at VME address = 0x"<<setbase(16)
       <<m_base_ltpi<<", size = "<<LTPI::size()<<", type = "<<LTPI::code()<<endl
       <<"****************************"<<endl
       <<"Manufacturer ID = 0x"<<setfill('0')<<setw(8)<<manuf   <<" (CERN = 0x00080030)"<<endl
       <<"Board ID        = 0x"<<setfill('0')<<setw(8)<<board   <<" (0xmmyyT0NN; T=0 for successful test, 1 otherwise; NN=ID)"<<endl
       <<"Revision number = 0x"<<setfill('0')<<setw(8)<<revision<<" (Firmware Date: 0xddmmyyyy)"<<endl
       <<"****************************"<<endl<<endl; 
    
  m_ltpi_helper->readEEPROM(manuf,board,revision);
  print<<"Opened module \"LTPI helper\", "<<"VME address = 0x"
       <<m_base_ltpi_helper<<", board ID = 0x"<<setw(8)<<board<<setfill(' ')<<endl;
   
  for( unsigned int i=0; i<LTP_TYPE_NUMBER; i++ ) 
    {
      m_ltp[i]->ReadConfigurationEEPROM(manuf,revision,board); 
      print<<"Opened module \""<<LTP_TYPE_NAME[i]<<"\""<<setw(13-LTP_TYPE_NAME[i].size())<<", "
	   <<"VME address = 0x"<<m_base_ltp[i]<<", board ID = 0x"<<board<<setfill(' ')<<endl;
    }
  cout<<setbase(10);
}

int MainProgram::checkForFile( string filename )
{
  ifstream fin(filename.c_str(), ios::in);
  if (!fin)
    return LTPI::ERROR;
  else 
    return LTPI::SUCCESS;
}

int MainProgram::readInputFile( ostream &dump )
{
  string CTPin_cable, LTPin_cable;
  int delay25_setting(DEFAULT_DELAY);
  
  static const int BUF_SIZE = 256;
  ifstream fin(m_input_filename.c_str(), ios::in);
  if (!fin) 
    {
      dump<<endl<<">>  INPUT FILE \""<<m_input_filename<<"\" COULD NOT BE FOUND !!!"
	  <<endl<<">>  An example of an input-test file can be found in RCDLtpi/data/test_ltpi.in"<<endl<<endl;
      m_status = LTPI::ERROR;
      return m_status;
    }
  else 
    m_status = LTPI::SUCCESS;
  
  vector<string> tokens;
  char buf[BUF_SIZE];
  dump<<"Opened input file \""<<m_input_filename<<"\" with the input values (LTP and LTPI address, ...): "
      <<endl<<"+---------------------------------------------+"
      <<endl<<"|                 INPUT VALUES                |"
      <<endl<<"+---------------------------------------------+"<<endl;
  while (fin.getline(buf, BUF_SIZE, '\n')) 
    {
      // pops off the newline character
      string line(buf);
      if (line.empty() || line == "START") continue;
      // enable '#' and '//' style comments
      if (line.substr(0,1) == "#" || line.substr(0,2) == "//") continue;
      if (line == "END") break;
      tokenize(line, tokens);
    
      //unsigned int size = tokens.size();
      dump << "| " << setw(22) << tokens[0] << "  = " << setw(15) << tokens[1] << "   |"<< endl;
      if (tokens[0] ==  "LTPI_address")           m_base_ltpi            = strtol(tokens[1].c_str(), nullptr, 16);
      if (tokens[0] ==  "LTPI_helper_address")    m_base_ltpi_helper     = strtol(tokens[1].c_str(), nullptr, 16);
      if (tokens[0] ==  "CTPin_cable")            CTPin_cable            = tokens[1];
      if (tokens[0] ==  "LTPin_cable")            LTPin_cable            = tokens[1];
      if (tokens[0] ==  "LTP_masterCTP_address")  m_base_ltp[MASTER_CTP] = strtol(tokens[1].c_str(), nullptr, 16);
      if (tokens[0] ==  "LTP_masterLTP_address")  m_base_ltp[MASTER_LTP] = strtol(tokens[1].c_str(), nullptr, 16);
      if (tokens[0] ==  "LTP_slaveCTP_address")   m_base_ltp[SLAVE_CTP]  = strtol(tokens[1].c_str(), nullptr, 16);
      if (tokens[0] ==  "LTP_slaveLTP_address")   m_base_ltp[SLAVE_LTP]  = strtol(tokens[1].c_str(), nullptr, 16);
      if (tokens[0] ==  "pg_filename")            m_pg_filename          = tokens[1];
      if (tokens[0] ==  "pg_ttyp_ext")            m_pg_ttyp_ext          = tokens[1];
      if (tokens[0] ==  "pg_ttyp_loc")            m_pg_ttyp_loc          = tokens[1];
      if (tokens[0] ==  "print")                  print                  = atoi(tokens[1].c_str());
      if (tokens[0] ==  "delay25_setting")        delay25_setting        = atoi(tokens[1].c_str());
    
      tokens.clear();
    }
  dump<<"+---------------------------------------------+"<<endl;
  
  // set LTPI Configuration 
  m_ltpi_cfg->equal_short_cable[LTPI::EQUAL_CTP] = (CTPin_cable == "short");
  m_ltpi_cfg->equal_short_cable[LTPI::EQUAL_LTP] = (LTPin_cable == "short");
  m_ltpi_cfg->setSignalDelay(delay25_setting);
  
  return m_status;
}

void MainProgram::tokenize(const string& str, vector<string>& tokens, const string& delimiters)
{
  string::size_type lastPos = str.find_first_not_of(delimiters, 0); // Skip delimiters at beginning.
  string::size_type pos = str.find_first_of(delimiters, lastPos);   // Find first "non-delimiter".
  while (string::npos != pos || string::npos != lastPos) 
    {                                                       // Found a token, add it to the vector.
      tokens.push_back(str.substr(lastPos, pos - lastPos));  // Skip delimiters.  Note the "not_of"
      lastPos = str.find_first_not_of(delimiters, pos);      // Find next "non-delimiter"
      pos = str.find_first_of(delimiters, lastPos);
    }
}

//-------------------------------------------------------------------------------------------------------

int main( int argc, char* argv[] )
{
  int c;
    
  // read command-line options
  while((c = getopt(argc,argv,":n")) != -1) 
    {
      switch(c) 
	{
	case 'n':
	  save = false;
	  break;
	}
    }
      

  srand(time(nullptr));

  // create main menu
  Menu menu("LTPI Test Main Menu");
   
  MainProgram *prog =  new MainProgram();
   
  menu.init(prog);
   
  // Read/Write Configuration
  Menu menuConfig("Read/Write configuration");
  menuConfig.add(new printConfiguration(prog));
  menuConfig.add(new LongShortCable(prog));
  menuConfig.add(new SetDelayTime(prog));
  menuConfig.add(new SetSleepPeriod(prog));
  menuConfig.add(new SetTTYPiteration(prog));
  menu.add(&menuConfig);
   
  // menu for Signal Connection Test
  Menu menuSIGNAL("Test signal connection");
  menuSIGNAL.add(new ConnectionTest( prog, "ALL", LTPI::SIGNAL_SELECTION_NUMBER ));
  for( unsigned int i=1; i<LTPI::SIGNAL_SELECTION_NUMBER; i++ )
    menuSIGNAL.add(new ConnectionTest( prog, (LTPI::SIGNAL_SELECTION_NAME[i] 
					      + "  (" + LTPI::SIGNAL_SELECTION_DESC[i] + ")").c_str(), i) );
  menu.add(&menuSIGNAL);
   
  // menu for Calibration Request Test
  Menu menuCALREQ("Test calibration request");
  menuCALREQ.add(new CalibrationTest(prog, "ALL", LTPI::CALREQ_SELECTION_NUMBER));
  for( unsigned int i=1; i<LTPI::CALREQ_SELECTION_NUMBER; i++ )
    menuCALREQ.add(new CalibrationTest( prog, (LTPI::CALREQ_SELECTION_NAME[i] 
					       + "  (" + LTPI::CALREQ_SELECTION_DESC[i] + ")").c_str(), i ));
  menu.add(&menuCALREQ);
   
  // menu for Busy Test
  Menu menuBUSY("Test busy");
  menuBUSY.add(new BusyTest(prog, "ALL", LTPI::BUSY_SELECTION_NUMBER ));
  for( unsigned int i=1; i<LTPI::BUSY_SELECTION_NUMBER; i++ )
    menuBUSY.add(new BusyTest( prog, (LTPI::BUSY_SELECTION_NAME[i] 
				      + "  (" + LTPI::BUSY_SELECTION_DESC[i] + ")").c_str(), i ));
  menu.add(&menuBUSY);
   
  // menu for Trigger Type Test
  Menu menuTTYP("Test trigger types");

  menuTTYP.add( new TriggerTypeTest(prog, "ALL", static_cast<LTPI::TTYP_SELECTION>(0), 0, true) );
   
  for( unsigned int i=LTPI::TTYP_PARALLEL; i<=LTPI::TTYP_FROM_LTP; i++ )
    menuTTYP.add(new TriggerTypeTest(prog, ( LTPI::TTYP_SELECTION_NAME[i] + "    (" +
					     LTPI::TTYP_SELECTION_DESC[i] + ")" ).c_str(), static_cast<LTPI::TTYP_SELECTION>(i), -1, false ));

  Menu menuLOCTTYP( ( LTPI::TTYP_SELECTION_NAME[LTPI::TTYP_FROM_LOCAL] + "  (" +
		      LTPI::TTYP_SELECTION_DESC[LTPI::TTYP_FROM_LOCAL] + ")").c_str() ); // (just a label)
  for( unsigned int j=0; j<LTPI::LOCTTYP_SELECTION_NUMBER; j++ )
    menuLOCTTYP.add(new TriggerTypeTest(prog, LTPI::LOCTTYP_SELECTION_NAME[j].c_str(), LTPI::TTYP_FROM_LOCAL, j, false));
  // TODO: PARALLEL FROM LOCAL !!!
  menuTTYP.add(&menuLOCTTYP);
  menu.add(&menuTTYP);
     
  // menu for Signal Delay
  Menu menuDELAY("Test delay scan");
  menuDELAY.add(new DelayScan(prog, "All signals", 0, 0, false));
  Menu menuIND("Individual signal");
  for( unsigned int i = LTPI::DELAY_ORB; i <= LTPI::DELAY_BGO3; i++ ) 
    menuIND.add( new DelayScan(prog, LTPI::DELAY_SIGNAL_NAME[i].c_str(), i, 0 , false));
  Menu menuGROUP("Group of signals");
  menuGROUP.add( new DelayScan(prog, (LTPI::DELAY_GROUP_NAME[LTPI::DELAY_GROUP_TTRG]
				      +" [1..3]").c_str(), 0, LTPI::DELAY_GROUP_TTRG , false));
  menuGROUP.add( new DelayScan(prog, (LTPI::DELAY_GROUP_NAME[LTPI::DELAY_GROUP_BGO]
				      +"  [0..3]").c_str(), 0, LTPI::DELAY_GROUP_BGO , false));
  menuGROUP.add( new DelayScan(prog, (LTPI::DELAY_GROUP_NAME[LTPI::DELAY_GROUP_TTYP]
				      +" [0..7]").c_str(), 0, LTPI::DELAY_GROUP_TTYP , false));
  menuDELAY.add(&menuIND); 
  menuDELAY.add(&menuGROUP);  
  menu.add(&menuDELAY);  
   
  menu.add(new SuperTest(prog));
   
  // start program
  menu.execute();
  if(save)
    m_file.close(); // close output file ( opened in MainProgram::action() )
   
  return 0;
}
