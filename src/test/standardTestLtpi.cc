//******************************************************************************
// file: standardTestLtpi.cc
// desc: standard test program for the LTPi module
// First version: Andrea Messina
// Last modifications: Carolina Gabaldon, carolina.gabaldon.ruiz@cern.ch
//                     Stefan Haas, stefan.haas@cern.ch
//******************************************************************************

#include <cstdlib>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <list>
#include <cerrno>

#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDVme.h"
#include "DFDebug/DFDebug.h"

#include "RCDLtpi/RCDLtpi.h"
#include "RCDLtp/RCDLtp.h"

#define DEFAULT_DELAY 0 
//#define DEFAULT_DELAY 40 
//#define DEFAULT_DELAY 20 // changed because of problem with LTPI #16

using namespace std;
using namespace RCD;

//--- standalone part:
//--- check the serial number
//--- check VME access
//--- read status registers
//--- write and read cofiguration registers
//--- i2c access
//--- setup up i2c
//--- write & read delays & gain & equalization settings
//--- initialize the module & check values are written correctely
//--- DAC
//--- Delays 

//--- This part of the test requires the LTPs
//--- ltpMasterCTP will emulate the "CTP" and be connected to the LTPi CTP-link-in 
//--- ltpMasterLTP will emulate a "master LTP in the local TTC partition" and be 
//    connected to the LTPi LTP-link-in and via the NIM don't forget to connect the 
//    busy too.
//--- ltpSlaveCTP will emulate the "LTP" slave of the CTP in the TTC partition and be 
//    connected to the LTPi via the CTP-link-out and via the NIM 
//--- ltpSlaveLTP will emulate the "LTP" slave of a LTP in another TTC partition and be
//    connected to the LTPi via the LTP-link-out

//--- Start test
//--- Check connection from ltpMasterCTP to ltpSlaveCTP & ltpSlaveLTP (via links)
//--- Check connection from ltpMasterLTP to ltpSlaveCTP & ltpSlaveLTP (via links)
//--- Check connection from ltpMasterLTP to ltpSlaveCTP & ltpSlaveLTP (via nim)
//--- Check trigger type
//--- Check calibration requests
//--- Check busy

enum ltp_signal_channel  { L1A = 1, TR1, TR2, TR3, ORB, BG0, BG1, BG2, BG3 }; 
enum ltpi_busy_selection { BUSY_OFF, BUSY_CTP, BUSY_LTP, BUSY_CTP_LTP };

// globals
LTPi* ltpi = 0;
LTP*  ltp_masterLTP = 0;
LTP*  ltp_masterCTP = 0;
LTP*  ltp_slaveCTP  = 0;
LTP*  ltp_slaveLTP  = 0;

vector<int> channelResultCTP;
vector<int> channelResultLTP;
vector<int> channelResult;

u_int base_ltpMasterCTP = 0xff6000;
u_int base_ltpMasterLTP = 0xff5000;
u_int base_ltpSlaveCTP  = 0xff1000;
u_int base_ltpSlaveLTP  = 0xff2000;
u_int base_ltpi         = 0xee1000;

u_short ltp_gain = 0x0;
u_short ltp_equ  = 0x0;
u_short ctp_gain = 0x0;
u_short ctp_equ  = 0x0;

string pgfilename, log_file_name, err_file_name, out_file_name;
string pgfilename_trt = "pg1.dat"; 
string pgfilename_status = "pg_st.dat";

static ofstream logg, err, out;
u_int test = 1; // global variable to monitor the status of the test.
u_int firmware_v = 0;
u_int module_id  = 0;

string CTPin_cable = "short";
string LTPin_cable = "short";

int sel;
int ltpCTP_status = 0; // 0 is not in pg &/|| has not the random pattern 
int ltpLTP_status = 0; // 0 is not in pg &/|| has not the random pattern 
int print = 10;
int delay25_setting = DEFAULT_DELAY;

int scanAddress();
// void printChannelResultSingle(ostream& os);
// void printChannelResult(ostream& os, vector<int>, vector<int>);
// void printChannelResult(ostream& os) { printChannelResult(os, channelResultCTP, channelResultLTP); }
int number_pg_loop;
int trt_itr;
int open_ltpi();
int check_id();
int printDAC(ostream& os);
int check_eeprom_values();
void open_output_files(int mode=0);
void close_output_files();
int read_test(const string& job_file);
void tokenize(const string& str, vector<string>& tokens, const string& delimiters=" ");
void print_results(ostream& os);
int testVMEaccess();
int write_and_read_CR();
int read_status_register();
int test_status_register();
int set_i2c();
int reset();
int check_delay_chips();
int check_dac_chips();
int initializeLTPi(int delay);
int initializeLTPs();
int initializeLTPsContinuous();
int testCTP2CTPLTP();
int testCTP2CTP_LTP2LTP();
int testLTP2CTPLTP();
int testLTP2LTP(LTP* sender, LTP *receiver);
int testLTP2NIM(LTP* sender, LTP *receiver);
int testCTP2CTP(LTP* sender, LTP *receiver);
int testCTP2NIM(LTP* sender, LTP *receiver);
int testNIM2CTP();
int testNIM2LTP();
int testNIM2CTPLTP();
int testCTP2NIMCTP();
int testLTP2NIMCTP();
int printCounters(LTP* ltpSender, LTP* ltpReceiver, ostream& os);
int printCounters(LTP* ltpSender, LTP* ltpReceiving1, LTP* ltpReceiving2, ostream& os);
int printCounters(LTP* ltpSenderCTP, LTP* ltpSenderLTP, LTP* ltpReceiving1, LTP* ltpReceiving2, ostream& os);
void wait(double sec, long nsec);
int testConnection(LTP* sender, LTP* receiver, LTP* ltp_receiver); // = nullptr);
int testConnection(LTP* sender) { return testConnection(sender, ltp_slaveCTP, ltp_slaveLTP); }
int testConnection(LTP* sender, LTP* receiver) { return testConnection(sender, receiver, nullptr); }
int testConnection();
int testCalibrationsRequest();

int testTriggerType();
int testTriggerType(LTP* sender);
int test_TRT_testTrigLTPNim();
int test_TRT_testTrigLTPLink();
int test_TRT_testTrigCTPLink();
int testTriggerTypeFromLocal();
int test_TRT_local_register(LTP* receiver);
int test_TRT_connection(LTP* sender, LTP* receiver);
int test_TRT_CTP2CTP();
int test_TRT_CTP2LTP();
int test_TRT_LTP2CTP();
int test_TRT_LTP2LTP();
int TRT_set_table(int i);

int standardTest(int mode);
int setDAC(string cable);
int testLongCable();
int changeInput();
int writeeprom();
int printheader(ostream& os);
int programDAC();
int menu();
int menu_triggertype();
int LtpCounterSource(LTP* ltp, ltp_signal_channel chan);
void printErrorMessage(string message);
int setBusy(bool ctp, bool ltp);
int checkBusyStatus(bool ctp, bool ltp, bool nim);
int setLtpiBusySelection(ltpi_busy_selection ctp, ltpi_busy_selection ltp, ltpi_busy_selection nim);
int setupLtpBusy(LTP* ltp, bool sender);
int testBusyToCTP();
int testBusyToLTP();
int testBusyToNIM();
int testBusy();

//----------------------------------------------------------------------------------------------------------

int main()
{
  int status = 0;
  int sel = 0;
  int stat = 1;

  // read test 
  status |= read_test("test_ltpi.in");
  if (!status) open_output_files();
  while(stat!=0) {
    status |= open_ltpi();
    stat    = check_id();
    status |= stat;

    if (stat != 0) {
      cout << "Bus error! most probably the base address of the module is wrong" << endl;
      cout << "Press 0 to end the test, 1 to enter a new base address:" << endl;
      cin >> sel;
      if (!sel) return status;
      else {
	string tmp;
	cout << "Enter the new base address" << endl;
	cin >> tmp;
	base_ltpi = strtol(tmp.c_str(), nullptr, 16);
      }

    }
  }

  while((sel=menu())!=0) 
    {
      // stand alone part of the test
      if (sel==1)   standardTest(0);
      if (sel==2)   standardTest(1);
      if (sel==3)   status |= open_ltpi();
      if (sel==4)   status |= testVMEaccess();
      if (sel==5)   status |= check_eeprom_values();
      if (sel==6)   status |= reset();
      if (sel==7)   status |= read_status_register();
      if (sel==8)   status |= write_and_read_CR();
      if (sel==9)   status |= set_i2c();
      if (sel==10)  status |= check_delay_chips();
      if (sel==11)  status |= check_dac_chips();

      // connections and signals routing test, requires three LTP modules
      if (sel==12) status |= initializeLTPi(delay25_setting);
      if (sel==13) status |= initializeLTPs();
      if (sel==14) status |= testCTP2CTPLTP();
      if (sel==15) status |= testLTP2CTPLTP();
      if (sel==16) status |= testCTP2CTP_LTP2LTP();
      if (sel==17) status |= testNIM2CTPLTP();
      if (sel==18) status |= testCTP2NIMCTP();
      if (sel==19) status |= testLTP2NIMCTP();

      // check calibration requests
      if (sel==20) status |= testCalibrationsRequest();

      // check trigger type
      if (sel==21) status |= testTriggerType();

      // check busy
      if (sel==22) status |= testBusy();
      if (sel==23) status |= test_status_register();
      if (sel==24) status |= testLongCable();
      if (sel==25) status |= changeInput();
      if (sel==26) standardTest(2);
      if (sel==27) status |= initializeLTPsContinuous();
      if (sel==28) status |= writeeprom();
      if (sel==29) status |= programDAC();

      // connection test, uses only two LTP modules
      if (sel==30) status |= testCTP2CTP(ltp_masterLTP, ltp_slaveLTP);
      if (sel==31) status |= testCTP2NIM(ltp_masterLTP, ltp_slaveLTP);
      if (sel==32) status |= testLTP2LTP(ltp_masterLTP, ltp_slaveLTP);
      if (sel==33) status |= testLTP2NIM(ltp_masterLTP, ltp_slaveLTP);
      if (sel==34) status |= testNIM2CTP();
      if (sel==35) status |= testNIM2LTP();

      // trigger type test menu
      if (sel == 36) menu_triggertype();
      
      if (sel == 37) {
	// set the default delay for the DELAY25
	cout << "Enter DELAY25 channel delay value (0..50): ";
	cin >> delay25_setting; 
	status |= ltpi->setDelayAllChannels(delay25_setting);
      }
    }  

  cout << "If the test was successful and complete, remember to lower the flag 0x1000 in the ID."<<endl; 
  cout << "Enter 1 to write the eprom, 0 otherwise:"<< endl;
  cin >> sel;
  if (sel==1) writeeprom();

  return status;
}

//********************************************************************************

void printStatusMessage(string message, int status, int level = 0)
{
  if (print >= level || status != 0)
    logg << message << " return " << setbase(10) << status << endl;
  if (status != 0) {
    out << message << " returns " << setbase(10) << status << endl;
    cout << message << " returns " << setbase(10) << status << endl;
  }
}

//********************************************************************************

void printResultMessage(string message, int status = 0)
{
  if (status == 0) {
    cout << message << ": OK" << endl;
    out  << message << ": OK" << endl;
  } else {
    cout << message << ": FAIL" << endl;
    out  << message << ": FAIL" << endl;
  }
}

//********************************************************************************

void printLogMessage(string message, int level = 0)
{
  if (print >= level) logg  << message << endl;
}

//********************************************************************************

void printErrorMessage(string message)
{
  cout << "ERROR - " << message << endl;
  out  << "ERROR - " << message << endl;
  logg  << "ERROR - " << message << endl;
}

//********************************************************************************


int open_ltpi()
{
  int status = 0;

  //   if (ltpi) {
  //     delete ltpi;
  //     ltpi = nullptr;
  //   }
  //logg << "Open LTPi: <<about to create the the LTPi object>>" << endl;
  if (!ltpi) ltpi = new LTPi();

  status |= ltpi->Open(base_ltpi);
  if (!status) logg << "open_ltpi(): opened LTPI module at VME address = " << setbase(16) << base_ltpi << setbase(10) << endl;
  printResultMessage("Open LTPI", status);
  printStatusMessage("open_ltpi()", status);
  return status;
}


//-------------------------------------------
int check_id()
{
  int status = 0;
  u_int board_man = 0;
  u_int board_revision = 0;
  u_int board_id = 0;
  status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);
  if (status != 0) return status;

  int sel = 0;
  int mid = -1;
  cout << "Check LTPi id: Input file module ID:  " << setbase(16) << module_id << setbase(10) << endl;
  cout << "Check LTPi id: Module ID written on eeprom: "<< setbase(16) << board_id << setbase(10) << endl;
  while (mid<0 || mid>40) {
    cout << "Check LTPi id: Enter module ID [1:40]: " << endl;
    cin >> mid;
  }
  module_id = (((module_id>>8)<<8)|mid); 
  cout << setbase(16) << "0x" << module_id << setbase(10) << endl;
  if ((board_id & 0x0000f000) == 0x1000) {
    cout << "Check LTPi id: This module has not completed the test, it has the flag 0x1000 on" << endl;
  }
  if ((board_id & 0x0000f000) == 0x0)    {
    cout << "Check LTPi id: This module has already completed the test, it has the flag 0x1000 off" << endl;
    test=0;
  }

  cout << "Check LTPi id: " << "Board ID entered is: " << setbase(16) << module_id << ", the ID on the eprom is: "  << setbase(16) << board_id << endl; 
  cout << "Check LTPi id: If you want to associate the ID written on the eprom (" << board_id << setbase(10) << ") "<<endl; 
  cout << "to the test output enter 1; 0 otherwise" << endl;
  cin >> sel;
  if (sel == 1)  {
    module_id = board_id; 
    open_output_files();
  }
  cout << "" << endl; 
  return status;
}

//-------------------------------------------
int menu()
{
  cout << "" << endl;
  cout << "Exit the test ...........................................[0]"<< endl;
  cout << "Standard test - it will execute the whole test ..........[1]"<< endl;
  cout << "Standard test - it will stop if it fails ................[2]"<< endl;
  cout << "Open LTPi ...............................................[3]"<< endl;
  cout << "Test VME access .........................................[4]"<< endl;
  cout << "Check LTPi prom values ..................................[5]"<< endl;
  cout << "Reset the LTPi ..........................................[6]"<< endl;
  cout << "Read LTPi status registers ..............................[7]"<< endl;
  cout << "Write and read back the control registers ...............[8]"<< endl;
  cout << "Setup LTPi I2C bus ......................................[9]"<< endl;
  cout << "Check LTPi delay chips: write & read ...................[10]"<< endl;
  cout << "Check LTPi DAC chips: write & read .....................[11]"<< endl;
  cout << "Initialize LTPi ........................................[12]"<< endl;
  cout << "Initialize LTPs ........................................[13]"<< endl;
  cout << "Test connection: CTP to CTP & LTP ......................[14]"<< endl;
  cout << "Test connection: LTP to CTP & LTP ......................[15]"<< endl;
  cout << "Test connection: CTP to CTP & LTP to LTP ...............[16]"<< endl;
  cout << "Test connection: NIM to CTP & LTP ......................[17]"<< endl;
  cout << "Test connection: CTP to CTP & NIM ......................[18]"<< endl;
  cout << "Test connection: LTP to CTP & NIM ......................[19]"<< endl;
  cout << "Test calibration request ...............................[20]"<< endl;
  cout << "Test trigger type ......................................[21]"<< endl;
  cout << "Test busy distribution .................................[22]"<< endl;
  cout << "Test status registers...................................[23]"<< endl;
  cout << "Test long cables .......................................[24]"<< endl;
  cout << "Change input values ....................................[25]"<< endl;
  cout << "Standard test - Standalone .............................[26]"<< endl;
  cout << "Initialize LTPs masters in continous mode ..............[27]"<< endl;
  cout << "Write EPROM ............................................[28]"<< endl;
  cout << "Program DAC ............................................[29]"<< endl;
  cout << "Test connection: CTP to CTP or LTP .....................[30]"<< endl;
  cout << "Test connection: CTP to NIM ............................[31]"<< endl;
  cout << "Test connection: LTP to LTP or CTP......................[32]"<< endl;
  cout << "Test connection: LTP to NIM ............................[33]"<< endl;
  cout << "Test connection: NIM to CTP ............................[34]"<< endl;
  cout << "Test connection: NIM to LTP ............................[35]"<< endl;
  cout << "Triggertype menu .......................................[36]"<< endl;
  cout << "Set the DELAY25 channel delay...........................[37]" << endl;
  cout << "Input selection [0..36]: ";
  
  int input = 0;
  cin >> input;
  return input;
}

int menu_triggertype()
{
  int status = 0;
  int sel;
  
  while (true) {
    cout << "Main menu                                                [0]" << endl;
    cout << "Test trigger type connection from CTP to CTP             [1]" << endl;
    cout << "Test trigger type connection from CTP to LTP             [2]" << endl;
    cout << "Test trigger type connection from LTP to CTP             [3]" << endl;
    cout << "Test trigger type connection from LTP to LTP             [4]" << endl;
    cout << "Test trigger type generated from CTP input test triggers [5]" << endl;
    cout << "Test trigger type generated from LTP input test triggers [6]" << endl;
    cout << "Test trigger type generated from NIM test triggers       [7]" << endl;
    cout << "Test trigger type generated from registers               [8]" << endl;
    cout << "Set the DELAY25 channel delay                            [9]" << endl;
    cout << "Input selection [0..9]: ";
    cin >> sel;
    switch(sel) {
    case 1: 
      status |= test_TRT_CTP2CTP();
      break;
    case 2: 
      status |= test_TRT_CTP2LTP();
      break;
    case 3: 
      status |= test_TRT_LTP2CTP();
      break;
    case 4: 
      status |= test_TRT_LTP2LTP();
      break;
    case 5: 
      status |= test_TRT_testTrigCTPLink();
      break;
    case 6: 
      status |= test_TRT_testTrigLTPLink();
      break;
    case 7: 
      status |= test_TRT_testTrigLTPNim();
      break;
    case 8: 
      status |= testTriggerTypeFromLocal();
      break;
    case 9:
      // set the default delay for the DELAY25
      cout << "Enter DELAY25 channel delay value (0..50): ";
      cin >> delay25_setting; 
      status |= ltpi->setDelayAllChannels(delay25_setting);
      break;
    case 0: 
      return(0);
    }
  }
}

//-------------------------------------------
int standardTest(int mode) 
{
  int status = 0;
  // read test 
  if (status == 0 || mode == 0) status |= open_ltpi();
  if (status == 0 || mode == 0) status |= testVMEaccess();
  if (status == 0 || mode == 0) status |= check_eeprom_values();
  if (status == 0 || mode == 0) status |= reset();
  if (status == 0 || mode == 0) status |= read_status_register();
  if (status == 0 || mode == 0) status |= write_and_read_CR();
  if (status == 0 || mode == 0) status |= set_i2c();
  if (status == 0 || mode == 0) status |= check_delay_chips();
  if (status == 0 || mode == 0) status |= check_dac_chips();
  if (status == 0 || mode == 0) status |= initializeLTPi(delay25_setting);
  if (mode == 2) return status;
  if (status == 0 || mode == 0) status |= initializeLTPs();
  if (status == 0 || mode == 0) status |= testCTP2CTPLTP();
  if (status == 0 || mode == 0) status |= testLTP2CTPLTP();
  if (status == 0 || mode == 0) status |= testCTP2CTP_LTP2LTP();
  if (status == 0 || mode == 0) status |= testNIM2CTPLTP();
  if (status == 0 || mode == 0) status |= testCTP2NIMCTP();
  if (status == 0 || mode == 0) status |= testLTP2NIMCTP();
  if (status == 0 || mode == 0) status |= testCalibrationsRequest();
  if (status == 0 || mode == 0) status |= testTriggerType();
  if (status == 0 || mode == 0) status |= testBusy();
  // finishing the test
  if (status != 0 && mode != 0) return status;
  int inp;
  cout << "Enter 1 to test long cables, 0 otherwise" << endl;
  cin >> inp;
  if (inp==1) {
    status |= testLongCable();
  }
  print_results(logg);
  close_output_files();
  return status;
}

//-------------------------------------------
int testVMEaccess()
{
  int status = 0;
  
  //logg << "Test VME access: <<about to start>>" << endl;
  u_short rbits;

  status |= ltpi->getBits(0x90, 0xffff, &rbits);
  printResultMessage("Test VME access", status);
  printStatusMessage("testVMEaccess()", status);
  return status;
}

//-------------------------------------------
int check_eeprom_values()
{
  int status = 0;
  u_int board_man = 0;
  u_int board_revision = 0;
  u_int board_id = 0;

  status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);

  if (board_man != 0x00080030) {
    err  << "Check EEPROM: Manufacturer is not set! " << setbase(16) << board_man << setbase(10) << endl; 
    logg  << "Check EEPROM: Manufacturer is not set! " << setbase(16) << board_man << setbase(10) << endl; 
    cout << "Check EEPROM: Manufacturer is not set! " << setbase(16) << board_man << setbase(10) << endl; 
    cout << "Check EEPROM................................................[FAILED]" << endl;
    out <<  "Check EEPROM................................................[FAILED]" << endl;   
    test |= (1 << 0);
  }
  else if (board_revision != firmware_v) {
    logg << setbase(16) << "Check EEPROM: Firmware version does not mach input file! " << board_revision << "; EEprom value = " << firmware_v << endl; 
    cout << setbase(16) << "Check EEPROM: Firmware version does not mach input file! " << board_revision << "; EEprom value = " << firmware_v << endl;
    cout << "Check EEPROM................................................[FAILED]" << endl;
    out <<  "Check EEPROM................................................[FAILED]" << endl; 
    test |= (1 << 0);
  }
  else if (board_id != module_id) {
    logg << setbase(16) << "Check EEPROM: Board ID does not mach input file! " << module_id  << "; EEprom value = " << board_id << endl;
    cout << setbase(16) << "Check EEPROM: Board ID does not mach input file! " << module_id  << "; EEprom value = " << board_id << endl; 
    cout << "Check EEPROM................................................[FAILED]" << endl;
    out <<  "Check EEPROM................................................[FAILED]" << endl; 
    test |= (1 << 0);
  }
  else {
    cout << "Check EEPROM................................................[OK]" << endl;
    out <<  "Check EEPROM................................................[OK]" << endl;    
  }

  logg <<  "Check EEPROM: MODULE DETAILS" << endl;
  logg <<  "Check EEPROM: Manufacturer: 0x" << setbase(16) << board_man << " (CERN = 0x00080030)" << endl;
  logg <<  "Check EEPROM: Revision    : 0x"                << board_revision << endl;
  logg <<  "Check EEPROM: Id          : 0x"                << board_id << setbase(10) << endl;
  logg <<  "Check EEPROM: <<exit code = " << status << ">>" <<endl;

  cout <<  "Check EEPROM: MODULE DETAILS" << endl;
  cout <<  "Check EEPROM: Manufacturer: 0x" << setbase(16) << board_man << " (CERN = 0x00080030)" << endl;
  cout <<  "Check EEPROM: Revision    : 0x"                << board_revision << endl;
  cout <<  "Check EEPROM: Id          : 0x"                << board_id << setbase(10) << endl;
  //   cout << "Check EEPROM: <<exit code = " << status << ">>" <<endl;
  return status;
}

int reset()
{
  int status = 0;
  logg << "Reset LTPi: <<issuing ltpi software reset command>>" << endl;
  status |= ltpi->Reset();
  logg << "Reset LTPi: <<exit code = " << status << ">>" <<endl;
  //   cout << "Reset LTPi: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int read_status_register()
{
  int status = 0;
  u_short rbits = 0;

  //   logg  << "Read LTPi status register: <<about to start>>" << endl;
  //   cout << "Read LTPi status register: <<about to start>>" << endl;

  status |= ltpi->getBits(0x90, 0xffff, &rbits);
  logg  << "Read LTPi status register: <<STATUS 1 = " << setbase(16) << rbits << ">>" << endl;
  //   cout << "Read LTPi status register: <<STATUS 1 = " << setbase(16) << rbits << ">>" << endl;
  status |= ltpi->getBits(0x92, 0xffff, &rbits);
  logg  << "Read LTPi status register: <<STATUS 2 = " << setbase(16) << rbits << ">>" << endl;
  //   cout << "Read LTPi status register: <<STATUS 2 = " << setbase(16) << rbits << ">>" << endl;
  status |= ltpi->getBits(0x94, 0xffff, &rbits);
  logg  << "Read LTPi status register: <<STATUS 3 = " << setbase(16) << rbits << ">>" << endl;
  //   cout << "Read LTPi status register: <<STATUS 2 = " << setbase(16) << rbits << ">>" << endl;
  status |= ltpi->getBits(0x96, 0xffff, &rbits);
  logg  << "Read LTPi status register: <<STATUS 4 = " << setbase(16) << rbits << ">>" << endl;
  //   cout << "Read LTPi status register: <<STATUS 4 = " << setbase(16) << rbits << ">>" << endl;

  printResultMessage("Read LTPI status register", status);
  printStatusMessage("read_status_register()", status);
  return status;
}

//-------------------------------------------
int write_and_read_CR() 
{
  int status = 0;
  int testt =0;
  u_short wbits = 0;
  u_short rbits = 0;
  
  //   logg  << "Write and read back the control registers: <<about to start>>" << endl;
  //   cout << "Write and read back the control registers: <<about to start>>" << endl;

  for (int i = 0; i<16; ++i) {
    wbits = (1 << i);
    status |= ltpi->setBits(0x80, 0xffff, wbits); // cr1
    status |= ltpi->getBits(0x80, 0xffff, &rbits);
    if (rbits != wbits) {
      logg << "Write and read back the control registers: <<CSR 1 readback does not read what was written>>" << endl;
      testt = 1;
    }
    logg << "Write and read back the control registers: <<CSR 1 = " << setbase(16) << rbits << ">>" << endl;
    status |= ltpi->setBits(0x82, 0xffff, wbits); // cr2
    status |= ltpi->getBits(0x82, 0xffff, &rbits);
    if (rbits != wbits) {
      logg << "Write and read back the control registers: <<CSR 2 readback does not read what was written>>" << endl;
      testt = 2;
    }
    logg << "Write and read back the control registers: <<CSR 2 = " << setbase(16) << rbits << ">>" << endl;
    status |= ltpi->setBits(0x84, 0xffff, wbits); // c3
    status |= ltpi->getBits(0x84, 0xffff, &rbits);
    if (rbits != wbits) {
      logg << "Write and read back the control registers: <<CSR 3 readback does not read what was written>>" << endl;
      testt = 3;
    }
    logg << "Write and read back the control registers: <<CSR 3 = " << setbase(16) << rbits << ">>" << endl;
  }
  logg << setbase(10) << endl;
  if (testt==0) {
    cout << "Write and read back the control registers ..................[OK]" << endl;
    out  << "Write and read back the control registers ..................[OK]" << endl;
  }  else {
    cout << "Write and read back the control registers ..................[FAILED]" << endl;
    out  << "Write and read back the control registers ..................[FAILED]" << endl;
  }
  logg  << "Write and read back the control registers: <<exit code = " << status << ">>" <<endl;
  //   cout << "Write and read back the control registers: <<exit code = " << status << ">>" <<endl;
  cout << "" <<endl;
  return status;
}

int printDAC(ostream& os)
{
  int status = 0;
  u_short rgain;
  status |= ltpi->getDACCTPgain(rgain);
  os << "+--------------------------------------+" << endl;
  os << "| CTP gain:                     0x" << setbase(16) << rgain << "   |"  << endl;;
  status |= ltpi->getDACLTPgain(rgain);
  os << "+--------------------------------------+" << endl;
  os << "| LTP gain:                     0x" << setbase(16) << rgain  << "   |" << endl;;
  status |= ltpi->getDACCTPequ(rgain);
  os << "+--------------------------------------+" << endl;
  os << "| CTP equalizer:                0x" << setbase(16) << rgain  << "   |" << endl;;
  status |= ltpi->getDACLTPequ(rgain);
  os << "+--------------------------------------+" << endl;
  os << "| LTP equalizer:                0x" << setbase(16) << rgain  << "   |" << endl;;
  os << "+--------------------------------------+" << endl;
  os << setbase(10) << endl;
  return status;
}

//-------------------------------------------
int programDAC()
{
  int sel = 0;
  int cab = 0;
  int status = 0;

  cout << "Input: " << endl;
  cout << " 0 for the CTP-link input,\n 1 for the LTP-link input." << endl;
  cin >> sel;
  cout << "Input 0 for short cable, 1 for long cable" << endl;
  cin >> cab;
  if (sel==0 && cab==0) {
    //    status |= ltpi->setupDAC4shortCTPCable();
    status |= ltpi->setDacCtp(LTPi::VGain_shortCTP, LTPi::VCTRL_shortCTP);
    CTPin_cable = "short";
  } else if (sel==0 && cab==1) {
    //    status |= ltpi->setupDAC4longCTPCable();
    status |= ltpi->setDacCtp(LTPi::VGain_longCTP, LTPi::VCTRL_longCTP);
    CTPin_cable = "long";
  } else if (sel==1 && cab==0) {
    //    status |= ltpi->setupDAC4shortLTPCable();
    status |= ltpi->setDacLtp(LTPi::VGain_shortLTP, LTPi::VCTRL_shortLTP);
    LTPin_cable = "short";
  } else if (sel==1 && cab==1) {
    //    status |= ltpi->setupDAC4longLTPCable();
    status |= ltpi->setDacLtp(LTPi::VGain_longLTP, LTPi::VCTRL_longLTP);
    LTPin_cable = "long";
  }
  status |= printDAC(cout);
  return status;
}

//-------------------------------------------
int writeeprom()
{
  int status = 0;
  u_int ma, bo, re;
  u_int board_man, board_revision, board_id;
  //u_short data;
  //char mode[9];
  int sel=0;
  printf("           WRITE IDPROM\n");
  
  printf("The following values are programmed. [0] to keep value, [1] otherwise :\n");
  status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);
  cout << setbase(16) << "Manufacturer ID (CERN: 0x00080030): .......................................... 0x" << board_man << setbase(10) << endl;
  cout << setbase(16) << "Revision (Firmware Date: 0xddmmyyyy): ........................................ 0x" << board_revision << setbase(10) << endl;
  cout << setbase(16) << "Serial Number (0xmmyyT0NN; T=0 for successful test, 1 otherwise; NN=ID): ..... 0x" << board_id << setbase(10) << endl;
  cin >> sel;
  if (sel==0) return status;

  string tmp;
  cout << "Manufacturer ID (CERN: 0x00080030): 0xffffffff:" << endl;
  cin >> tmp;
  ma = strtol(tmp.c_str(), nullptr, 16);
  cout << "Revision (Firmware Date: 0xddmmyyyy): 0xffffffff:" << endl;
  cin >> tmp;
  re = strtol(tmp.c_str(), nullptr, 16);
  cout << "Serial Number (0xmmyyT00N; T=0 for successful test, 1 otherwise; N=ID): 0xffffffff:" << endl;
  cin >> tmp;
  bo = strtol(tmp.c_str(), nullptr, 16);
  
  status |= ltpi->writeprom4(ltpi->MANUFID, ma);
  status |= ltpi->writeprom4(ltpi->BOARDID, bo);
  status |= ltpi->writeprom4(ltpi->REVISION, re);
  
  status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);
  
  printf("\n");
  printf(" Manufacturer: 0x%08x (CERN = 0x00080030)\n", board_man);
  printf(" Revision    : 0x%08x \n", board_revision);
  printf(" Id          : 0x%08x \n", board_id);
  

  return status;
}

//-------------------------------------------
int changeInput()
{
  int status = 0;
  vector<string> InputValueNames;
  InputValueNames.push_back("quit ......................");
  InputValueNames.push_back("loggile ...................");
  InputValueNames.push_back("errFile ...................");
  InputValueNames.push_back("outFile ...................");
  InputValueNames.push_back("firmwareVersion ...........");
  InputValueNames.push_back("module_id .................");
  InputValueNames.push_back("LTPi_adress ...............");
  InputValueNames.push_back("CTPin_cable ...............");
  InputValueNames.push_back("LTPin_cable ...............");
  InputValueNames.push_back("TEST ......................");
  InputValueNames.push_back("LTP_masterCTP_address .....");
  InputValueNames.push_back("LTP_masterLTP_address .....");
  InputValueNames.push_back("LTP_slaveCTP_address ......");
  InputValueNames.push_back("LTP_slaveLTP_address ......");
  InputValueNames.push_back("number_pg_loop ............");
  InputValueNames.push_back("pgfilename ................");
  InputValueNames.push_back("print .....................");
  for(u_int i=0; i<InputValueNames.size(); ++i) cout << InputValueNames[i] << "["<< i<<"]" << endl;
  int sel;
  cin >> sel;
  string tmp;
  int fil=0;
  switch (sel)
    {
    case 0:
      return status;
      break;
    case 1:
      cout << "Input name of the log file (it will add the ID of the board in front):" << endl;
      cin >> log_file_name;
      open_output_files(1);
      break;
    case 2:
      cout << "Input name of the error file (it will add the ID of the board in front):" << endl;
      cin >> err_file_name;
      open_output_files(2);
      break;
    case 3:
      cout << "Input name of the output file (it will add the ID of the board in front):" << endl;
      cin >> out_file_name;
      open_output_files(3);
      break;
    case 4:
      cout << "Input revision number (firmware version 0xddmmyyyy) 0xffffffff:" << endl;
      cin >> tmp;
      firmware_v = strtol(tmp.c_str(), nullptr, 16);
      break;
    case 5:
      cout << "Input module ID number (0xyyyyffff) 0xffffffff:" << endl;
      cin >> tmp;
      module_id = strtol(tmp.c_str(), nullptr, 16);
      cout << "The module ID has been changed to: " << module_id << "Enter 1 if you want to change the test output files accordling; 0 otherwise" << endl;
      cin >> fil; 
      if (fil==1) open_output_files();
      break;
    case 6:
      cout << "LTPi base address  0xffffffff:" << endl;
      cin >> tmp;
      base_ltpi = strtol(tmp.c_str(), nullptr, 16);
      break;
    case 7:
      cout << "Input TEST" << endl;
      cin >> test;
      break;
    case 8:
      cout << "Input CTPin cable (short/long):" << endl;
      cin >> tmp;
      CTPin_cable = tmp;
      break;
    case 9:
      cout << "Input LTPin cable (short/long):" << endl;
      cin >> tmp;
      LTPin_cable = tmp;
      break;
    case 10:
      cout << "LTP master CTP base address  0xffffffff:" << endl;
      cin >> tmp;
      base_ltpMasterCTP = strtol(tmp.c_str(), nullptr, 16);
      break;
    case 11:
      cout << "LTP master LTP base address  0xffffffff:" << endl;
      cin >> tmp;
      base_ltpMasterLTP = strtol(tmp.c_str(), nullptr, 16);
      break;
    case 12:
      cout << "LTP slave CTP base address  0xffffffff:" << endl;
      cin >> tmp;
      base_ltpSlaveCTP = strtol(tmp.c_str(), nullptr, 16);
      break;
    case 13:
      cout << "LTP slave LTP base address  0xffffffff:" << endl;
      cin >> tmp;
      base_ltpSlaveLTP = strtol(tmp.c_str(), nullptr, 16);
      break;
    case 14:
      cout << "Input number of time you want to send the PG signals" << endl;
      cin >> number_pg_loop;
      break;
    case 15:
      cout << "Input the name of the pattern generator file (from the test dir):" << endl;
      cin >> pgfilename;
      break;
    case 16:
      cout << "Input 0 if you don't want to print the outcome of the sending tests, 1 otherwise:" << endl;
      cin >> fil;
      if(fil==0) print=0;
      if(fil==1) print=1;
      if(fil==2) print=2;
      cout << fil << " print " << print << endl; 
      break;
    }
  return status;
}


//-------------------------------------------
int scanAddress()
{
  int status = 0;
  
  return status;
}

//-------------------------------------------
int testLongCable()
{
  int status = 0;
  int inp, opt;
  
  cout << "Enter 0 to skip the test, 1 to set the DAC to the vaules in the input file for both the CTP and the LTP input," << endl;
  cout << "2 to set first the CTP and then the LTP" << endl;
  cin >> opt;
  if (opt == 0) return status;
  if (opt == 1) {
    status |= ltpi->setDACCTPgain(ctp_gain);
    status |= ltpi->setDACCTPequ(ctp_equ);
    status |= ltpi->setDACLTPgain(ltp_gain);
    status |= ltpi->setDACLTPequ(ltp_equ);
    status |= ltpi->printDACsettings();
    cout << "Enter 1 once the cables are connected, 0 to skip the test" << endl;
    cin >> inp;
    if (inp == 0) return status;    
    status |= testCTP2CTPLTP();
    status |= testCTP2CTP_LTP2LTP();
    status |= testLTP2CTPLTP();
    status |= testCTP2NIMCTP();
    status |= testLTP2NIMCTP();
    status |= testCalibrationsRequest();
    status |= testTriggerType();
    status |= testBusy();
    // finishing the test
    return status;
  }
  if (opt == 2) {
    status |= ltpi->setupDAC4shortCable();
    status |= ltpi->setDACCTPgain(ctp_gain);
    status |= ltpi->setDACCTPequ(ctp_equ);
    status |= ltpi->printDACsettings();
    cout << "Enter 1 once the CTP cable is connected, 0 to skip the test" << endl;
    cin >> inp;  
    if (inp == 0) return status;      
    status |= testCTP2CTPLTP();
    status |= testCTP2CTP_LTP2LTP();
    status |= testCTP2NIMCTP();
    status |= testCalibrationsRequest();
    status |= testTriggerType();
    status |= testBusy();

    status |= ltpi->setupDAC4shortCable();
    status |= ltpi->setDACLTPgain(ltp_gain);
    status |= ltpi->setDACLTPequ(ltp_equ);
    status |= ltpi->printDACsettings();
    cout << "Enter 1 once the LTP cable is connected, 0 to skip the test" << endl;
    cin >> inp;    
    if (inp == 0) return status;    
    status |= testCTP2CTP_LTP2LTP();
    status |= testLTP2CTPLTP();
    status |= testLTP2NIMCTP();
    status |= testCalibrationsRequest();
    status |= testTriggerType();
    status |= testBusy();
    // finishing the test
  } 
 
  if (status==0) { 
    cout << "Test long cables on " << setw(4) << number_pg_loop << " iterations ........................[OK]" << endl;
    out  << "Test long cables on " << setw(4) << number_pg_loop << " iterations ........................[OK]" << endl;
  }  else {
    cout << "Test long cables on " << setw(4) << number_pg_loop << " iterations ....................[FAILED]" << endl;
    out << "Test long cables on " << setw(4) << number_pg_loop << " iterations ....................[FAILED]" << endl;
  }

  return status;
}

//-------------------------------------------
int testCalibrationsRequest()
{
  // This will test the possible configuration of the ltpi in routing calibration requests
  //   logg << "testCalibrationRequest: <<about to start>>" << endl;  
  int status = 0;
  status |= ltp_slaveCTP->CAL_source_preset(); // set calibration request from preset
  status |= ltp_slaveCTP->CAL_set_preset_value(0);

  status |= ltp_slaveLTP->CAL_source_preset(); // set calibration request from preset
  status |= ltp_slaveLTP->CAL_set_preset_value(0);
  
  status |=  ltp_slaveCTP->CAL_enable_testpath(); // enable test pad
  status |=  ltp_slaveLTP->CAL_enable_testpath(); // enable test pad
  status |= ltp_masterLTP->CAL_enable_testpath();
  status |= ltp_masterCTP->CAL_enable_testpath();

  status |= ltp_masterCTP->CAL_source_link(); // read calibration from link
  status |= ltp_masterLTP->CAL_source_link();

  //u_short w;
  int r1 = 0; int r2 = 0; int r3 = 0; int r4 = 0;
  int ierr = 0;

  /// PARALEL
  logg << "Starting testing the calibration request path:" << endl;
  logg << "1 ---- checking CTP-link-to-CTP-link & LTP-link-to-LTP-link path" << endl;
  for(int i =0; i<8; ++i) {
    status |= ltp_slaveLTP->CAL_set_preset_value(u_short(i));
    status |= ltp_slaveCTP->CAL_set_preset_value(u_short(7-i));
    status |= ltpi->calCTPfromCTP_LTPFromLTP(); // then check the ctp & ltp connection

    status |= ltp_slaveCTP->CAL_get_linkstatus(&r1);
    status |= ltp_slaveLTP->CAL_get_linkstatus(&r3);
    status |= ltp_masterCTP->CAL_get_linkstatus(&r2);
    status |= ltp_masterLTP->CAL_get_linkstatus(&r4);

    if (r1 != r2) ierr |= 906;
    if (r3 != r4) ierr |= 909;
    logg << " sender(CTP)       receiver(CTP)" << endl;
    logg << "  r1 = " << r1 << ";        r2 = " << r2 << endl;
    logg << " sender(LTP)       receiver(LTP)" << endl;
    logg << "  r3 = " << r3 << ";        r4 = " << r4 << endl;
  }
  
  // FROM CTP
  logg << "Starting testing the calibration request path:" << endl;
  logg << "1 ---- checking LTP-link-to-CTP-link & LTP-link-to-LTP-link path" << endl;
  for(int i =0; i<8; ++i) {
    status |= ltp_slaveLTP->CAL_set_preset_value(u_short(i));
    status |= ltp_slaveCTP->CAL_set_preset_value(u_short(7-i));
    status |= ltpi->calCTPfromCTP_LTPFromCTP(); // then check the ltp connection
    status |= ltp_slaveCTP->CAL_get_linkstatus(&r1);
    status |= ltp_slaveLTP->CAL_get_linkstatus(&r3);
    status |= ltp_masterCTP->CAL_get_linkstatus(&r2);
    status |= ltp_masterLTP->CAL_get_linkstatus(&r4);
    if (r1 != r2) ierr |= 906;
    if (r1 != r4) ierr |= 909;
    logg << " sender(CTP)       receiver(CTP)" << endl; 
    logg << "  r1 = " << r1 << ";        r2 = " << r2 << endl;
    logg << " sender(LTP)       receiver(LTP)" << endl; 
    logg << "  r3 = " << r3 << ";        r4 = " << r4 << endl;
  }

  // FROM LTP
  logg << "1 ---- checking CTP-link-to-CTP-link & CTP-link-to-LTP-link path" << endl;
  for(int i =0; i<8; ++i) {
    status |= ltp_slaveLTP->CAL_set_preset_value(u_short(i));
    status |= ltp_slaveCTP->CAL_set_preset_value(u_short(7-i));
    status |= ltpi->calCTPfromLTP_LTPFromLTP(); 
    // first check the ctp connection: the CTPslave is sending to both CTP and LTP
    status |= ltp_slaveCTP->CAL_get_linkstatus(&r1);
    status |= ltp_slaveLTP->CAL_get_linkstatus(&r3);
    status |= ltp_masterCTP->CAL_get_linkstatus(&r2);
    status |= ltp_masterLTP->CAL_get_linkstatus(&r4);
    if (r3 != r2) ierr |= 606; 
    if (r3 != r4) ierr |= 609; 
    logg << " sender(CTP)       receiver(CTP)" << endl;
    logg << "  r1 = " << r1 << ";        r2 = " << r2 << endl;
    logg << " sender(LTP)       receiver(LTP)" << endl;
    logg << "  r3 = " << r3 << ";        r4 = " << r4 << endl;
  }
  
  if ((ierr & 606)==606) logg << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi CTP-link-out and the LTP acting as a CTP must be connected to the LTPi CTP-link-in" << endl;
  else if ((ierr & 609)==609) logg << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi CTP-link-out and the LTP acting as a LTP-master must be connected to the LTPi LTP-link-in" << endl;
  else if ((ierr & 906)==906) logg << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi LTP-link-out and the LTP acting as a CTP must be connected to the LTPi CTP-link-in" << endl;
  else if ((ierr & 909)==909) logg << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi LTP-link-out and the LTP acting as a LTP-master must be connected to the LTPi LTP-link-in" << endl;
  else if (ierr == 0) logg <<         "    <<TEST PASSED>>" << endl; 
  
  if (status==0 && ierr==0) {  
    cout << "Test calibration request ...................................[OK]" << endl;
    out  << "Test calibration request ...................................[OK]" << endl;
  }  else {
    cout << "Test calibration request ...............................[FAILED]" << endl;
    out << "Test calibration request ...............................[FAILED]" << endl;
  }
  if (print > 1) logg << "testCalibrationRequest: <<exit code = " << status << ">>" <<endl;
  //if (print > 1) cout << "testCalibrationRequest: <<exit code = " << status << ">>" <<endl;
  return status;
}
//-------------------------------------------
int testTriggerTypeFromCTP()
{
  int status = 0;

  status |= test_TRT_CTP2CTP();
  status |= test_TRT_CTP2LTP();
  if (status==0) {  
    cout << "Test trigger type from CTP .................................[OK]" << endl;
    out  << "Test trigger type from CTP .................................[OK]" << endl;
  }  else {
    cout << "Test trigger type from CTP .............................[FAILED]" << endl;
    out << "Test trigger type from CTP .............................[FAILED]" << endl;
  }
  if (print > 1) logg << "testTriggerTypeFromCTP: <<exit code = " << status << ">>" <<endl;
  //if (print > 1) cout << "testTriggerTypeFromCTP: <<exit code = " << status << ">>" <<endl;

  return status;
}

//-------------------------------------------
int testTriggerTypeFromLTP()
{
  int status = 0;

  status |= test_TRT_LTP2CTP();
  status |= test_TRT_LTP2LTP();
  if (status==0) {  
    cout << "Test trigger type from LTP .................................[OK]" << endl;
    out  << "Test trigger type from LTP .................................[OK]" << endl;
  }  else {
    cout << "Test trigger type from LTP .............................[FAILED]" << endl;
    out << "Test trigger type from LTP .............................[FAILED]" << endl;
  }
  if (print > 1) logg << "testTriggerTypeFromLTP: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testTriggerTypeFromLTP: <<exit code = " << status << ">>" <<endl;

  return status;
}

//-------------------------------------------
int testTriggerTypeFromTTRFromNIM()
{
  int status = 0;

  logg << "test trigger type, test triggers from NIM" << endl;
  status |= test_TRT_testTrigLTPNim();
  if (print > 1) logg << "testTriggerTypeFromTTRFromNIM: <<exit code = " << status << ">>" <<endl;
  //if (print > 1) cout << "testTriggerTypeFromTTRFromNIM: <<exit code = " << status << ">>" <<endl;
  if (status != 0) {
    logg  << "Error: Test trigger type problems with deriving the Trigger type from the Nim inputs" << endl;
    out  << "Test trigger type from test triggers from NIM ..........[FAILED]" << endl;
    cout << "Test trigger type from test triggers from NIM ..........[FAILED]" << endl;
  } else {
    cout << "Test trigger type from test triggers from NIM ..............[OK]" << endl;
    out  << "Test trigger type from test triggers from NIM ..............[OK]" << endl;
  }
  return status;
}

//-------------------------------------------
int testTriggerTypeFromTTRFromLTP()
{
  int status = 0;

  logg << "test trigger type, test triggers from LTP Link-in" << endl;
  status |= test_TRT_testTrigLTPLink(); 
  if (print > 1) logg << "testTriggerTypeFromTTRFromLTP: <<exit code = " << status << ">>" <<endl;
  //if (print > 1) cout << "testTriggerTypeFromTTRFromLTP: <<exit code = " << status << ">>" <<endl;
  if (status != 0) {
    logg  << "Error: Test trigger type problems with deriving the Trigger type from the LTP Link-in" << endl;
    cout << "Test trigger type from test triggers from LTP ..........[FAILED]" << endl;
    out <<  "Test trigger type from test triggers from LTP ..........[FAILED]" << endl;
  } else {
    cout << "Test trigger type from test triggers from LTP ..............[OK]" << endl;
    out  << "Test trigger type from test triggers from LTP ..............[OK]" << endl;
  }
  return status;
}

//-------------------------------------------
int testTriggerTypeFromTTRFromCTP()
{
  int status = 0;

  logg << "test trigger type, test triggers from CTP" << endl;
  status = test_TRT_testTrigCTPLink();
  if (print > 1) logg << "testTriggerTypeFromTTRFromCTP: <<exit code = " << status << ">>" <<endl;
  //if (print > 1) cout << "testTriggerTypeFromTTRFromCTP: <<exit code = " << status << ">>" <<endl;
  if (status == 0) {  
    cout << "Test trigger type from test triggers from CTP ..............[OK]" << endl;
    out  << "Test trigger type from test triggers from CTP ..............[OK]" << endl;
  } else {
    cout << "Test trigger type from test triggers from CTP ..........[FAILED]" << endl;
    out <<  "Test trigger type from test triggers from CTP ..........[FAILED]" << endl;
    logg  << "Error: Test trigger type problems with deriving the Trigger type from the CTP Link-in" << endl;
  }
  return status;
}

//-------------------------------------------
int testTriggerTypeFromLocal()
{
  int status = 0;

  logg << "test trigger type, trigger type assigned locally" << endl;
  status |= test_TRT_local_register(ltp_slaveCTP);
  status |= test_TRT_local_register(ltp_slaveLTP);
  if (status == 0) {  
    cout << "Test trigger type from local register ......................[OK]" << endl;
    out  << "Test trigger type from local register ......................[OK]" << endl;
  }  else {
    cout << "Test trigger type from local register ..................[FAILED]" << endl;
    out  << "Test trigger type from local register ..................[FAILED]" << endl;
    logg  << "Error: Test trigger type problems with assigning the Trigger type locally" << endl;
  }
  if (print > 1) logg << "testTriggerTypeFromLocal: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int testTriggerType()
{
  int status = 0;
  status |= testTriggerTypeFromCTP();
  status |= testTriggerTypeFromLTP();
  status |= testTriggerTypeFromTTRFromNIM();
  status |= testTriggerTypeFromTTRFromLTP();
  status |= testTriggerTypeFromTTRFromCTP();
  status |= testTriggerTypeFromLocal();
  //   if (status==0) {  
  //     cout << "Test trigger type ..........................................[OK]" << endl;
  //     out  << "Test trigger type ..........................................[OK]" << endl;
  //   }  else {
  //     cout << "Test trigger type ......................................[FAILED]" << endl;
  //     out << "Test trigger type ......................................[FAILED]" << endl;
  //   }
  //   if (print > 1) logg << "testTriggerType: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testTriggerType: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int test_TRT_CTP2CTP()
{
  int status = 0;

  logg << "Test trigger type connection, CTP input to CTP output" << endl;
  status |= ltpi->ctp_mode();
  //  status |= ltpi->setDelayAllChannels(delay25_setting);
  status |= test_TRT_connection(ltp_masterCTP, ltp_slaveCTP);
  if (status == 0) {  
    cout << "Test trigger type connection CTP input to CTP output .......[OK]" << endl;
  } else {
    cout << "Test trigger type connection CTP input to CTP output ...[FAILED]" << endl;
  }
  return(status);
}

int test_TRT_CTP2LTP()
{
  int status = 0;

  logg << "Test trigger type connection, CTP input to LTP output" << endl;
  status |= ltpi->ctp_mode();
  //  status |= ltpi->setDelayAllChannels(delay25_setting);
  //   if (CTPin_cable == "long") {
  //     status |= ltpi->setupDAC4longCTPCable();
  //   } else {
  //     status |= ltpi->setupDAC4shortCTPCable();
  //   }
  status |= test_TRT_connection(ltp_masterCTP, ltp_slaveLTP);
  if (status == 0) {  
    cout << "Test trigger type connection CTP input to LTP output .......[OK]" << endl;
  } else {
    cout << "Test trigger type connection CTP input to LTP output ...[FAILED]" << endl;
  }
  return(status);
}

int test_TRT_LTP2CTP()
{
  int status = 0;

  logg << "Test trigger type connection, LTP input to CTP output" << endl;
  status |= ltpi->ltp_mode();
  status |= test_TRT_connection(ltp_masterLTP, ltp_slaveCTP);
  if (status == 0) {  
    cout << "Test trigger type connection LTP input to CTP output .......[OK]" << endl;
  } else {
    cout << "Test trigger type connection LTP input to CTP output ...[FAILED]" << endl;
  }
  return(status);
}

int test_TRT_LTP2LTP()
{
  int status = 0;

  logg << "Test trigger type connection, LTP input to LTP output" << endl;
  status |= ltpi->ctpltp_mode(); // WHY ??? see in source file what's the difference
  status |= test_TRT_connection(ltp_masterLTP, ltp_slaveLTP);
  cout << "Test trigger type connection LTP input to LTP output ";
  if (status == 0) {
    cout << ".......[OK]";
  } else {
    cout << "...[FAILED]" << endl;
  }
  cout << endl;
  return(status);
}

int test_TRT_connection(LTP* sender, LTP* receiver)
{
  int status = 0;

  // setup the sender ltp
  string pg_trt = "pg_trt.dat";
  if (sender->OM_master_patgen(pg_trt)) {
    cout << "Pattern generator file: <<" << pg_trt << ">> NOT FOUND, EXITING !!!" << endl;
    return -1;
  }
  if (sender == ltp_masterCTP) ltpCTP_status = 0; //
  if (sender == ltp_masterLTP) ltpLTP_status = 0; //
  status |= sender->PG_runmode_singleshot_vme();
  status |= sender->TTYPE_local_from_pg();
  status |= sender->TTYPE_linkout_from_local();
  status |= sender->TTYPE_fifotrigger_L1A();
  status |= sender->TTYPE_fifodelay_2bc();
  status |= sender->COUNTER_L1A();

  // setup the receiver ltp
  status |= receiver->OM_slave();
  status |= receiver->BUSY_local_without_nimin(); // disable busy input
  status |= receiver->TTYPE_linkout_from_linkin();
  status |= receiver->TTYPE_fifotrigger_L1A();
  status |= receiver->TTYPE_fifodelay_2bc();
  status |= receiver->COUNTER_L1A();

  for (int i = 0; i < trt_itr; ++i) 
    {
      //for (u_short j = 0; j < 0x80;) {
      u_short trt = 1;
      for (int j = 0; j < 4; j++) 
	{
	  status |= sender->TTYPE_write_regfile0(trt);
	  status |= sender->TTYPE_write_regfile1(~trt & 0xff);
	  trt <<= 1;
	  status |= sender->TTYPE_write_regfile2(trt);
	  status |= sender->TTYPE_write_regfile3(~trt & 0xff);
	  trt <<= 1;
	  if (i == 0 && print > 1) 
	    {
	      u_short regfr;
	      sender->TTYPE_read_regfile0(&regfr);
	      logg << setbase(16);
	      logg << "regfile0 = " << regfr << endl;
	      sender->TTYPE_read_regfile1(&regfr);
	      logg << "regfile1 = " << regfr << endl;
	      sender->TTYPE_read_regfile2(&regfr);
	      logg << "regfile2 = " << regfr << endl;
	      sender->TTYPE_read_regfile3(&regfr);
	      logg << "regfile3 = " << regfr << endl;
	      logg << setbase(10);
	    } 
    
	  //logg << "Iteration ----------> " << i << endl;
	  status |= sender->TTYPE_reset_fifo();    //
	  status |= receiver->TTYPE_reset_fifo();  //
	  status |= sender->COUNTER_reset();       // 
	  status |= receiver->COUNTER_reset();     // 
	  // send the test triggers
	  status |= sender->PG_start();            // 

	  // wait for the pattern generator to finish
	  while (!sender->PG_is_idle()) {};        // 

	  u_int l1a_master, l1a_slave;
	  status |= sender->COUNTER_read_32(&l1a_master);
	  status |= receiver->COUNTER_read_32(&l1a_slave);
	  //logg << "test_TRT_connections(): sent " << setbase(10) << l1a_master << " triggers " << endl;
	  if (l1a_master == 0) {
	    logg << "ERROR: zero L1As sent from master LTP" << endl;
	    status |= 1;
	    return(status);
	  }
	  if (l1a_slave != l1a_master) {
	    logg << "ERROR: L1A counter mismatch, sent " << l1a_master << 
	      " triggers, received " << l1a_slave << " triggers" << endl;
	    status |= 1;
	    return(status);
	  }

	  u_short trt_slave, trt_master;
	  int n_l1a = 1;
	  // reading output form fifo
	  //logg << "Fifo entry number before while: " << c1 << endl;
	  while (l1a_master--) {
	    if (sender->TTYPE_fifo_is_empty()) {
	      status |= 1;
	      logg << "Error LTP master fifo empty at L1A " << n_l1a << endl; 
	      return(status);
	    }
	    status |= sender->TTYPE_read_fifo(&trt_master);
	    if (receiver->TTYPE_fifo_is_empty()) {
	      status |= 3;
	      logg << "Error LTP slave fifo empty at L1A " << n_l1a << endl; 
	      return(status);
	    }
	    status |= receiver->TTYPE_read_fifo(&trt_slave);
	    if (print > 1) logg << "Fifo entry number: " << n_l1a << ": sender TRT FIFO = " 
				<< setbase(16) << trt_master << "; receiver TRT FIFO = " << trt_slave << setbase(10) << endl;
	    if (trt_slave != trt_master) {
	      status |= 3;
	      logg << "test_TRT_connection: <<Error: slaveCTP received a different trigger type, iteration "<< setbase(10) << i << ">>" << endl;
	      logg << setbase(16);
	      logg << "sender value = " << trt_master
		   << "; slaveCTP = " << trt_slave
		   << endl;
	    }
	    //if (status != 0) break;
	    n_l1a++;
	  } // end while
      
	  //logg << "Fifo entry number after while: " << int(c1) << endl;
	  if (status != 0) break;
	} // end inner loop 
    } //  end outer loop
  logg << "<<test_TRT_connection: exit code = " << setbase(10) << status  << ">>" << endl;
  return status;
}

//-------------------------------------------
int test_TRT_local_register(LTP* receiver)
{
  int status = 0;

  status |= ltpi->TRT_FROM_LOCAL_REGISTER();  // ???
 
  // setup the receiver LTP
  status |= receiver->OM_slave();
  status |= receiver->BUSY_local_without_nimin(); // disable busy input
  status |= receiver->TTYPE_linkout_from_linkin();
  status |= receiver->TTYPE_eclout_from_linkin();
  status |= receiver->TTYPE_reset_fifo();

  for (u_short trt = 0; (status == 0) && (trt < 256); ++trt) {
    status |= ltpi->SetTRT(0, trt); 
    //     status |= ltpi->GetTRT(0, trt);
    //     if (print > 1) logg << "TRT000 (init value) = " << setbase(16) <<  trt << setbase(10) << endl;
    u_short ltp_trt;
    // write pulse for the trigger type fifo
    status |= receiver->TTYPE_write_fifo();
    status |= receiver->TTYPE_read_fifo(&ltp_trt);

    if (ltp_trt != trt) {
      cout << "ERROR in test_TRT_local_register(): trigger type mismatch, sent " 
	   << setbase(16) << trt << ", received " << ltp_trt << setbase(10) << endl;
      status |= 0x1000;
    }
  }

  if (print > 1) logg << "test_TRT_local_register: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int test_TRT_testTrigCTPLink()
{
  int status = 0;

  if ((status |= ltp_masterCTP->OM_master_patgen(pgfilename_trt))) {
    cout << "Patter generator file: <<" << pgfilename_trt << ">> NOT FOUND, EXITING !!!" << endl;
    return status;
  }
  status |= ltp_masterCTP->PG_runmode_singleshot_vme();

  status |= ltpi->ctp_mode();                            // done
  status |= ltpi->TRT_CTPoutFromLocal_LTPoutFromLocal(); // done
  status |= ltpi->TRT_DERIVED_FROM_CTP_TTR();            // done

  for (int i = 0; i < 128; i += 4) {
    status |= TRT_set_table(i);
    status |= testTriggerType(ltp_masterCTP);
    if (status != 0) break;
  }
  if (print > 1) logg << "test_TRT_testTrigCTPLink: <<exit code = " << status << ">>" <<endl;
  cout << "Test trigger type form CTP test triggers ot CTP and LTP outputs ";
  if (status == 0) {
    cout << "[OK]";
  } else {
    cout << "[FAILED]" << endl;
  }
  cout << endl;
  return status;
}

//-------------------------------------------
int test_TRT_testTrigLTPLink()
{
  int status = 0;

  if ((status |= ltp_masterLTP->OM_master_patgen(pgfilename_trt))) {
    cout << "Patter generator file: <<" << pgfilename_trt << ">> NOT FOUND, EXITING !!!" << endl;
    return status;
  }
  status |= ltp_masterLTP->PG_runmode_singleshot_vme();

  status |= ltpi->ltp_mode();
  status |= ltpi->TRT_CTPoutFromLocal_LTPoutFromLocal();
  status |= ltpi->TRT_DERIVED_FROM_LTP_TTR();

  for (int i=0; i<128; i += 4) {
    status |= TRT_set_table(i);
    status |= testTriggerType(ltp_masterLTP);
  }
  if (print > 0) logg << "test_TRT_testTrigLTPLink: <<exit code = " << status << ">>" <<endl;
  cout << "Test trigger type form LTP test triggers to CTP and LTP outputs ";
  if (status == 0) {
    cout << "[OK]";
  } else {
    cout << "[FAILED]" << endl;
  }
  cout << endl;
  return status;
}

//-------------------------------------------
int test_TRT_testTrigLTPNim()
{
  int status = 0;

  if ((status |= ltp_masterLTP->OM_master_patgen(pgfilename_trt))) {
    cout << "Patter generator file: <<" << pgfilename_trt << ">> NOT FOUND, EXITING !!!" << endl;
    return status;
  }
  status |= ltp_masterLTP->PG_runmode_singleshot_vme();

  status |= ltpi->nim_mode();
  status |= ltpi->TRT_CTPoutFromLocal_LTPoutFromLocal();
  status |= ltpi->TRT_DERIVED_FROM_NIM_TTR();

  for (int i=0; i<128; i += 4) {
    status |= TRT_set_table(i);
    status |= testTriggerType(ltp_masterLTP);
  }
  if (print > 0) logg << "test_TRT_testTrigLTPNim: <<exit code = " << status << ">>" <<endl;
  cout << "Test trigger type form NIM test triggers to CTP and LTP outputs ";
  if (status == 0) {
    cout << "[OK]";
  } else {
    cout << "[FAILED]" << endl;
  }
  cout << endl;

  return status;
}

//------------------------------------------------------------------------------------------------

int TRT_set_table(int i)
{  
  int ierr = 0;
  u_short value = i & 0xff;
  u_short check;
  for (int num = 0; num < 8;) {
    ierr |= ltpi->SetTRT(num, value);
    ierr |= ltpi->GetTRT(num, check);
    if (check != value) {
      ierr |= 1;
      logg << setbase(16);
      logg << "TRT_set_table: <<reading back a different value (" 
	   << check << ") that what was tried to be written ("
	   << value << ") on 000>>" << endl;
      logg << setbase(10);
    }
    num++;
    ierr |= ltpi->SetTRT(num, ~value & 0xff);
    ierr |= ltpi->GetTRT(num, check);
    if (check != (~value & 0xff)) {
      ierr |= 1;
      logg << setbase(16);
      logg << "TRT_set_table: <<reading back a different value (" 
	   << check << ") that what was tried to be written ("
	   << (~value & 0xff) << ") on 000>>" << endl;
      logg << setbase(10);
    }
    num++;
    value = (value + 1) & 0xff;
  }
  // print trigger type table
  if (print > 2) ierr |= ltpi->dumpTRTTable();
  if (print > 2) logg << "TRT_set_table: <<exit code = " << ierr << ">>" <<endl;
  return(ierr);
}

//-------------------------------------------
int testTriggerType(LTP* sender)
{
  //  LTP* ltp[3] = { sender, ltp_slaveCTP, ltp_slaveCTP );
  //   if (print > 1) logg << "testTriggerType: <<about to start>>" << endl;  
  int status = 0;

  //   if (sender == ltp_masterCTP && ltpCTP_status != 5) 
  //     if (sender->OM_master_patgen(pgfilename_trt)) {
  //       cout << "Patter generator file: <<" << pgfilename_trt << ">> NOT FOUND, EXITING !!!" << endl;
  //       return -1;
  //     }
  //   if (sender == ltp_masterLTP && ltpLTP_status != 5)
  //     if (sender->OM_master_patgen(pgfilename_trt)) {
  //       cout << "Patter generator file: <<" << pgfilename_trt << ">> NOT FOUND, EXITING !!!" << endl;
  //       return -1;
  //     }
  //   if (sender == ltp_masterCTP) ltpCTP_status = 5;
  //   if (sender == ltp_masterLTP) ltpLTP_status = 5;
  //status |= sender->PG_runmode_cont();
  //status |= sender->ORB_local_pg();
  status |= sender->COUNTER_L1A();
  status |= sender->COUNTER_reset();

  // CTP
  status |= ltp_slaveCTP->COUNTER_L1A();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveCTP->TTYPE_fifotrigger_L1A();
  status |= ltp_slaveCTP->TTYPE_fifodelay_3bc();

  // LTP
  status |= ltp_slaveLTP->COUNTER_L1A();
  status |= ltp_slaveLTP->COUNTER_reset();
  status |= ltp_slaveLTP->TTYPE_fifotrigger_L1A();
  status |= ltp_slaveLTP->TTYPE_fifodelay_3bc();

  status |= sender->TTYPE_reset_fifo(); 
  //if (!sender->TTYPE_fifo_is_empty()) logg << "Error master fifo not empty!" << endl; 
  
  status |= ltp_slaveCTP->TTYPE_reset_fifo(); // CTP
  //if (!ltp_slaveCTP->TTYPE_fifo_is_empty()) logg << "Error slave CTP fifo not empty!" << endl; 
  status |= ltp_slaveLTP->TTYPE_reset_fifo(); // LTP
  //if (!ltp_slaveLTP->TTYPE_fifo_is_empty()) logg << "Error slave LTP fifo not empty!" << endl; 

  //if (print > 1) logg << "FiFo triggered by L1A" << endl;
  //if (print > 1) logg << " ******* start testing channels" << endl;

  // send the test triggers
  status |= sender->PG_start();
  // wait for the pattern generator to finish
  while (!sender->PG_is_idle()) {};
  //sleep(1);


  // check the number of L1As sent and received
  u_int l1a_master, l1a_ctp_slave, l1a_ltp_slave;
  status |= sender->COUNTER_read_32(&l1a_master);
  status |= ltp_slaveCTP->COUNTER_read_32(&l1a_ctp_slave); // CTP
  status |= ltp_slaveLTP->COUNTER_read_32(&l1a_ltp_slave); // LTP
 
  if (print > 1) logg << "sent " << l1a_master << " triggers, received " 
		      << l1a_ctp_slave << " triggers from LTP output, received "  // CTP
		      << l1a_ltp_slave << " triggers from LTP output" << endl;    // LTP

  // CTP
  if (l1a_ctp_slave != l1a_master) {
    logg << "ERROR: L1A counter mismatch, sent " << l1a_master << 
      " triggers, CTP output received " << l1a_ctp_slave << " triggers" << endl;
    status |= 1;
    return(status);
  }

  // LTP
  if (l1a_ltp_slave != l1a_master) {
    logg << "ERROR: L1A counter mismatch, sent " << l1a_master << 
      " triggers, CTP output received " << l1a_ltp_slave << " triggers" << endl;
    status |= 1;
    return(status);
  }

  if (status != 0) 
    return(status);

  // check the receiver trigger type FIFOs
  int i = 0;
  u_short ctp_trt, ltp_trt, ltpi_trt;
  if (print > 0) logg << "TR(3:1) LTPI  CTP  LTP" << endl;
  while (l1a_master-- > 0) 
    {
      // Get the expected value from the LTPI register file
      status |= ltpi->GetTRT(i, ltpi_trt); // <-------

      // Check CTP slave           LTP
      if (ltp_slaveCTP->TTYPE_fifo_is_empty()) {
	status |= 999;
	logg << "ERROR: CTP slave LTP trigger type FIFO empty" << endl; 
      }
      status |= ltp_slaveCTP->TTYPE_read_fifo(&ctp_trt);

      // Check LTP slave            trigger type FIFO
      if (ltp_slaveLTP->TTYPE_fifo_is_empty()) {
	status |= 999;
	logg << "ERROR: LTP slave LTP trigger type FIFO empty" << endl; 
      }
      status |= ltp_slaveLTP->TTYPE_read_fifo(&ltp_trt);

      // check the values // CTP
      if (ctp_trt != ltpi_trt) {
	logg << "ERROR: CTP output received trigger type " << setbase(16) << ctp_trt 
	     << " differs from the expected LTPI value " << ltpi_trt << setbase(10) 
	     << " for test trigger = " << i <<endl;
	status |= 999;
      }

      // LTP
      if (ltp_trt != ltpi_trt) {
	logg << "ERROR: LTP output received trigger type " << setbase(16) << ctp_trt 
	     << " differs from the expected LTPI value " << ltpi_trt << setbase(10) 
	     << " for test trigger = " << i <<endl;
	status |= 999;
      }
      if(print>1) logg<<i<<setbase(16)<<"       0x"<<ltpi_trt<<"   0x"<<ctp_trt<<"   0x"<<ltp_trt<<setbase(10)<<endl;
      // increment test trigger value
      i = (i + 1) & 7;
    } // end while
    
  if (print > 1) logg << "testTriggerType: <<exit code = " << status << ">>" <<endl;
  return status;
}

//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------

void checkErrorCondition(bool condition, string message)
{
  if (condition) printErrorMessage(message);
}

//********************************************************************************

int setBusy(bool ctp, bool ltp)
{
  int status = 0;

  if (ctp) status |= ltp_slaveCTP->BUSY_enable_constant_level();
  else status |= ltp_slaveCTP->BUSY_disable_constant_level();
  if (ltp_slaveCTP->BUSY_linkout_is_busy() != ctp) 
    printErrorMessage("setBusy(): CTP output LTP is not sending busy when enabled");
  if (ltpi->isBUSY_CTPout() != ctp) // ???
    printErrorMessage("setBusy(): busy received on LTPI CTP output link does match busy from connected LTP");

  if (ltp) status |= ltp_slaveLTP->BUSY_enable_constant_level();
  else status |= ltp_slaveLTP->BUSY_disable_constant_level();
  if (ltp_slaveLTP->BUSY_linkout_is_busy() != ltp) 
    printErrorMessage("setBusy(): LTP output LTP is not sending busy when enabled");
  if (ltpi->isBUSY_LTPout() != ltp) // ???
    printErrorMessage("setBusy(): busy received on LTPI LTP output link does match busy from connected LTP");

  if (print > 0) logg << "setBusy() returns " << status << endl;
  return status;
}

//********************************************************************************

int checkBusyStatus(bool ctp, bool ltp, bool nim)
{
  bool busy_ctp, busy_ltp, busy_nim;

  busy_ctp = ltp_masterCTP->BUSY_linkin_is_busy();
  busy_ltp = ltp_masterLTP->BUSY_linkin_is_busy();
  busy_nim = ltp_masterLTP->BUSY_nimin_is_busy();
  if (busy_ctp != ctp || busy_ltp != ltp || busy_nim != nim) {
    logg << "ERROR - checkBusyStatus():" << endl
	 << "expected CTP = " << ctp << ", LTP = " << ltp << ", NIM = " << nim << endl
	 << "received CTP = " << busy_ctp << ", LTP = " << busy_ltp << ", NIM = " << busy_nim << endl;
    return 3;
  }
  return 0;
}

//********************************************************************************

int setLtpiBusySelection(ltpi_busy_selection ctp, ltpi_busy_selection ltp, ltpi_busy_selection nim)
{
  int status = 0;

  switch (ctp) {
  case BUSY_OFF:     status |= ltpi->busyCTP_OFF(); 
    break;
  case BUSY_CTP:     status |= ltpi->busyCTP_FROM_CTP();
    break;
  case BUSY_LTP:     status |= ltpi->busyCTP_FROM_LTP();
    break;
  case BUSY_CTP_LTP: status |= ltpi->busyCTP_FROM_CTP_AND_LTP();
    break;
  }

  switch (ltp) {
  case BUSY_OFF:     status |= ltpi->busyLTP_OFF();
    break;
  case BUSY_CTP:     status |= ltpi->busyLTP_FROM_CTP();
    break;
  case BUSY_LTP:     status |= ltpi->busyLTP_FROM_LTP();
    break;
  case BUSY_CTP_LTP: status |= ltpi->busyLTP_FROM_CTP_AND_LTP();
    break;
  }

  switch (nim) {
  case BUSY_OFF:     status |= ltpi->busyNIM_OFF();
    break;
  case BUSY_CTP:     status |= ltpi->busyNIM_FROM_CTP();
    break;
  case BUSY_LTP:     status |= ltpi->busyNIM_FROM_LTP();
    break;
  case BUSY_CTP_LTP: status |= ltpi->busyNIM_FROM_CTP_AND_LTP();
    break;
  }

  if (print > 0) logg << "setLtpiBusySelection() returns " << status << endl;
  return(status);
}

//********************************************************************************

int setupLtpBusy(LTP* ltp, bool sender)
{
  int status = 0;
  
  if (sender) {
    status |= ltp->BUSY_local_without_linkin();
  } else {
    status |= ltp->BUSY_local_with_linkin();
  }
  status |= ltp->BUSY_local_without_nimin();
  status |= ltp->BUSY_local_without_ttlin();
  status |= ltp->BUSY_local_without_deadtime();
  status |= ltp->BUSY_linkout_from_local();     // send busy to link
  status |= ltp->BUSY_lemoout_busy_only();      // send busy to NIM
  if (print > 0) logg << "setupLtpBusy() returns " << status << endl;
  return(status);
}

//********************************************************************************

int testBusyToLTP()
{
  int status = 0;

  // check LTPI CTP -> LTP busy connection
  // LTPI sends busy from CTP output to LTP input
  status |= setLtpiBusySelection(BUSY_OFF, BUSY_CTP, BUSY_OFF);
  status |= setBusy(true, true); // enable CTP busy
  status |= checkBusyStatus(false, true, false);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP -> LTP, busy on");
  status |= setBusy(false, true);// disable CTP busy
  status |= checkBusyStatus(false, false, false);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP -> LTP, busy off");

  // check the LTPI LTP -> LTP busy connection
  // LTPI sends busy from LTP output to LTP input
  status |= setLtpiBusySelection(BUSY_OFF, BUSY_LTP, BUSY_OFF); 
  status |= setBusy(true, true); // enable busy
  status |= checkBusyStatus(false, true, false); // check busy
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy LTP -> LTP, busy on");
  status |= setBusy(true, false); // disable LTP busy
  status |= checkBusyStatus(false, false, false);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy LTP -> LTP, busy off");

  // check LTPI CTP or LTP -> LTP busy connection
  // LTPI sends busy from CTP or LTP output to LTP input
  status |= setLtpiBusySelection(BUSY_OFF, BUSY_CTP_LTP, BUSY_OFF); 
  status |= setBusy(false, true); // enable LTP busy
  status |= checkBusyStatus(false, true, false);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> LTP, LTP busy on");
  status |= setBusy(true, false); // enable CTP busy
  status |= checkBusyStatus(false, true, false);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> LTP, CTP busy on");
  status |= setBusy(false, false); // disable busy
  status |= checkBusyStatus(false, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> LTP, busy off");

  if (print > 0) logg << "testBusyToLTP() returns " << status << endl;
  return status;
}

//********************************************************************************

int testBusyToNIM()
{
  int status = 0;

  // check LTPI CTP -> NIM connection
  // LTPI sends busy from CTP input to NIM output
  status |= setLtpiBusySelection(BUSY_OFF, BUSY_OFF, BUSY_CTP);
  status |= setBusy(true, true); // enable CTP busy
  status |= checkBusyStatus(false, false, true);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP -> NIM, busy on");
  status |= setBusy(false, true);
  // disable CTP busy
  status |= checkBusyStatus(false, false, false);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP -> NIM, busy off");
         
  // check LTPI LTP -> NIM connection
  // LTPI sends busy from LTP input to NIM output
  status |= setLtpiBusySelection(BUSY_OFF, BUSY_OFF, BUSY_LTP);
  status |= setBusy(true, true); // enable LTP busy
  status |= checkBusyStatus(false, false, true);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy LTP -> NIM, busy on");
  status |= setBusy(true, false); // disable LTP busy
  status |= checkBusyStatus(false, false, false);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy LTP -> NIM, busy off");
        
  // check LTPI CTP or LTP -> NIM busy connection
  // LTPI sends busy from CTP or LTP output to NIM output
  status |= setLtpiBusySelection(BUSY_OFF, BUSY_OFF, BUSY_CTP_LTP);  
  status |= setBusy(false, true); // enable LTP busy
  status |= checkBusyStatus(false, false, true);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> NIM, LTP busy on");
  status |= setBusy(true, false); // enable CTP busy
  status |= checkBusyStatus(false, false, true);
  //if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> NIM, CTP busy on");
  status |= setBusy(false, false); // disable busy
  status |= checkBusyStatus(false, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> NIM, busy off");

  if (print > 0) logg << "testBusyToNIM() returns " << status << endl;
  return status;
}

//-------------------------------------------
int testBusyToCTP()
{
  int status = 0;

  // check LTPI CTP -> CTP busy connection
  // LTPI sends busy from CTP output to CTP input
  status |= setLtpiBusySelection(BUSY_CTP, BUSY_OFF, BUSY_OFF);
  status |= setBusy(true, true);// enable CTP busy
  status |= checkBusyStatus(true, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP -> CTP, busy on");
  status |= setBusy(false, true);// disable CTP busy
  status |= checkBusyStatus(false, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP -> CTP, busy off");

  // check the LTPI LTP -> CTP busy connection
  // LTPI sends busy from LTP output to CTP input
  status |= setLtpiBusySelection(BUSY_LTP, BUSY_OFF, BUSY_OFF); 
  status |= setBusy(true, true); // enable busy
  status |= checkBusyStatus(true, false, false); // check busy
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy LTP -> CTP, busy on");
  status |= setBusy(true, false); // disable LTP busy
  status |= checkBusyStatus(false, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy LTP -> CTP, busy off");

  // check LTPI CTP or LTP -> CTP busy connection
  // LTPI sends busy from CTP or LTP output to CTP input
  status |= setLtpiBusySelection(BUSY_CTP_LTP, BUSY_OFF, BUSY_OFF);
  status |= setBusy(false, true); // enable LTP busy
  status |= checkBusyStatus(true, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> CTP, LTP busy on");
  status |= setBusy(true, false); // enable CTP busy
  status |= checkBusyStatus(true, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> CTP, CTP busy on");
  status |= setBusy(false, false); // disable busy
  status |= checkBusyStatus(false, false, false);
  // if (status != 0) printErrorMessage("testBusy(): LTPI busy CTP or LTP -> CTP, busy off");

  if (print > 0) logg << "testBusyToCTP() returns " << status << endl;
  return status;
}

//-------------------------------------------

int testBusy()
{
  int status = 0; //
  LTP* receiver[2] = { ltp_masterCTP, ltp_masterLTP }; //
  LTP* sender[2]   = { ltp_slaveCTP,  ltp_slaveLTP };  //
  //string ltp_name = {"CTP", "LTP");

  if (print > 1) logg << "testBusy()" << endl;   //
  // setup the LTPs
  for (int i = 0; i < 2; i++) {//
    status |= setupLtpBusy(sender[i], true); //
    status |= setupLtpBusy(receiver[i], false); //
  }

  // check the busy off mode
  status |= setBusy(true, true); // enable busy
  // switching off busy output on ltpi
  status |= setLtpiBusySelection(BUSY_OFF, BUSY_OFF, BUSY_OFF);//
  // check that busy is off
  status |= checkBusyStatus(false, false, false); //
  if (status != 0) printErrorMessage("testBusy(): received busy while LTPI is in off mode");

  status |= testBusyToCTP(); // check that the LTPI sends busy to the CTP input link
  status |= testBusyToLTP(); // check that the LTPI sends busy to the LTP input link
  status |= testBusyToNIM(); // check that the LTPI sends busy to the local NIM output

  status |= setLtpiBusySelection(BUSY_OFF, BUSY_OFF, BUSY_OFF);

  printStatusMessage("testBusy() returns", status);
  printResultMessage("Test busy distribution", status);
  return status;
}

//-------------------------------------------

int testCTP2CTPLTP()
{
  int status = 0;
  //   logg  << "Test connection CTP to CTP & LTP: <<about to start>>" << endl;
  //   cout << "Test connection CTP to CTP & LTP: <<about to start>>" << endl;  
  status |= ltpi->ctp_mode();
  //   if (CTPin_cable == "long") {
  //     status |= ltpi->setupDAC4longCTPCable();
  //   } else {
  //     status |= ltpi->setupDAC4shortCTPCable();
  //   }
  printLogMessage("Testing connection from CTP input to CTP & LTP output ...");
  status |= testConnection(ltp_masterCTP);

  printResultMessage("Test connection CTP to CTP & LTP", status);
  printStatusMessage("testCTP2CTPLTP()", status, 2);
  //  logg  << "Test connection CTP to CTP & LTP: <<exit code = " << status << ">>" <<endl;
  //   cout << "Test connection CTP to CTP & LTP: <<exit code = " << status << ">>" <<endl;
  //   cout << "" <<endl;
  return status;
}

//-------------------------------------------

int testCTP2CTP_LTP2LTP()
{
  int status = 0;
  //   logg << "testCTP2CTP_LTP2LTP: <<about to start>>" << endl;  
  printLogMessage("Testing connection from CTP input to CTP output & LTP input to LTP output ...");
  status |= ltpi->ctpltp_mode();
  //   if (CTPin_cable == "long") {
  //     status |= ltpi->setupDAC4longCTPCable();
  //   } else {
  //     status |= ltpi->setupDAC4shortCTPCable();
  //   }
  //  status |= testConnection();
  status |= testConnection(ltp_masterCTP, ltp_slaveCTP);
  status |= testConnection(ltp_masterLTP, ltp_slaveLTP);
  printResultMessage("Test connection CTP to CTP & LTP to LTP", status);
  printStatusMessage("testCTP2CTP_LTP2LTP()", status, 2);
  return status;
}

//-------------------------------------------
int testLTP2CTPLTP()
{
  int status = 0;
  //   if (print > 1) logg << "testLTP2CTPLTP: <<about to start>>" << endl;  
  printLogMessage("Testing connection from LTP input to CTP & LTP outputs ...");
  status |= ltpi->ltp_mode();
  //   if (LTPin_cable == "long") {
  //     status |= ltpi->setupDAC4longLTPCable();
  //   } else {
  //     status |= ltpi->setupDAC4shortLTPCable();
  //   }
  status |= testConnection(ltp_masterLTP);
  printResultMessage("Test connection: LTP to CTP & LTP", status);
  printStatusMessage("testLTP2CTPLTP()", status, 2);
  return status;
}

//-------------------------------------------

// called only once ( in main() ) !!
int testCTP2CTP(LTP* sender, LTP *receiver)
{
  int status = 0;

  //   if (print > 1) logg << "testCTP2CTP: <<about to start>>" << endl;  
  printLogMessage("Testing connection from CTP input to CTP or LTP output ...");
  status |= ltpi->ctp_mode();
  status |= receiver->OM_slave();
  status |= receiver->BUSY_local_without_nimin(); // disable busy input
  status |= testConnection(sender, receiver);
  printResultMessage("Test connection CTP to CTP", status);
  printStatusMessage("testCTP2CTP()", status, 2);
  return status;
}

//-------------------------------------------

int testCTP2NIM(LTP* sender, LTP *receiver)
{
  int status = 0;

  //   if (print > 1) logg << "testCTP2NIM: <<about to start>>" << endl;  
  status |= ltpi->ctp_mode();
  status |= receiver->OM_master_lemo(); // make it a slave with input from NIM
  status |= receiver->BC_local_lemo();
  status |= receiver->ORB_local_lemo();
  printLogMessage("Testing connection from NIM input to CTP or LTP output ...");
  status |= testConnection(sender, receiver);
  if (status==0) {  
    cout << "Test connection CTP to NIM ... " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection CTP to NIM ... " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection CTP to NIM ... " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection CTP to NIM ... " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testCTP2NIM: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testCTP2NIM: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------

int testLTP2LTP(LTP* sender, LTP *receiver)
{
  int status = 0;
  //   if (print > 1) logg << "testLTP2LTP: <<about to start>>" << endl;  
  status |= ltpi->ltp_mode();
  status |= receiver->OM_slave();
  status |= receiver->BUSY_local_without_nimin(); // disable busy input
  printLogMessage("Testing connection from LTP input to CTP or LTP output ...");
  status |= testConnection(sender, receiver);
  if (status==0) {
    cout << "Test connection: LTP to CTP/LTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection: LTP to CTP/LTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection: LTP to CTP/LTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection: LTP to CTP/LTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testLTP2LTP: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testLTP2LTP: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------

int testLTP2NIM(LTP* sender, LTP *receiver)
{
  int status = 0;
  //   if (print > 1) logg << "testLTP2NIM: <<about to start>>" << endl;  
  status |= receiver->OM_master_lemo(); // make it a slave with input from NIM
  status |= receiver->BC_local_lemo();
  status |= receiver->ORB_local_lemo();
  status |= ltpi->ltp_mode();
  printLogMessage("Testing connection from LTP input to NIM output ...");
  status |= testConnection(sender, receiver);
  if (status==0) {  
    cout << "Test connection: LTP to NIM on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection: LTP to NIM on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection: LTP to NIM on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection: LTP to NIM on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testLTP2NIM: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testLTP2NIM: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------

int testNIM2CTPLTP()
{
  int status = 0;
  //   if (print > 1) logg << "testNIM2CTPLTP: <<about to start>>" << endl;  
  status |= ltpi->nim_mode();
  printLogMessage("Testing connection from NIM inputs to CTP & LTP outputs ...");
  status |= testConnection(ltp_masterLTP);
  if (status==0) {  
    cout << "Test connection: NIM to CTP & LTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection: NIM to CTP & LTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection: NIM to CTP & LTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection: NIM to CTP & LTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testNIM2CTPLTP: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testNIM2CTPLTP: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------

int testNIM2CTP()
{
  int status = 0;
  //   if (print > 1) logg << "testNIM2CTP: <<about to start>>" << endl;  
  status |= ltpi->nim_mode();
  printLogMessage("Testing connection from NIM inputs to CTP output ...");
  status |= testConnection(ltp_masterLTP, ltp_slaveCTP);
  if (status==0) {  
    cout << "Test connection: NIM to CTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection: NIM to CTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection: NIM to CTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection: NIM to CTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testNIM2CTP: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testNIM2CTP: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int testNIM2LTP()
{
  int status = 0;
  //   if (print > 1) logg << "testNIM2LTP: <<about to start>>" << endl;  
  status |= ltpi->nim_mode();
  printLogMessage("Testing connection from NIM inputs to LTP output ...");
  status |= testConnection(ltp_masterLTP, ltp_slaveLTP);
  if (status==0) {  
    cout << "Test connection: NIM to LTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection: NIM to LTP on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection: NIM to LTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection: NIM to LTP on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testNIM2LTP: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testNIM2LTP: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int testCTP2NIMCTP()
{
  int status = 0;
  //   if (print > 1) logg << "testCTP2NIMCTP: <<about to start>>" << endl;  
  status |= ltpi->ctp_mode();
  status |= ltp_slaveLTP->OM_master_lemo(); // make it a slave with input from NIM
  status |= ltp_slaveLTP->BC_local_lemo();
  status |= ltp_slaveLTP->ORB_local_lemo();
  printLogMessage("Testing connection from CTP input to CTP & NIM outputs ...");
  status |= testConnection(ltp_masterCTP);
  status |= ltp_slaveLTP->OM_slave(); // set it back to slave with input from Linkin
  status |= ltp_slaveLTP->BUSY_local_without_nimin(); // disable busy input
  if (status==0) {  
    cout << "Test connection: CTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection: CTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection: CTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection: CTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testCTP2NIMCTP: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testCTP2NIMCTP: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int testLTP2NIMCTP()
{
  int status = 0;
  //   if (print > 1) logg << "testLTP2NIMCTP: <<about to start>>" << endl;  
  status |= ltpi->ltp_mode();
  status |= ltp_slaveLTP->OM_master_lemo(); // make it a slave with input from NIM
  status |= ltp_slaveLTP->BC_local_lemo();
  status |= ltp_slaveLTP->ORB_local_lemo();
  printLogMessage("Testing connection from LTP input to CTP & NIM outputs ...");
  status |= testConnection(ltp_masterLTP);
  status |= ltp_slaveLTP->OM_slave(); // set it back to slave with input from Linkin
  status |= ltp_slaveLTP->BUSY_local_without_nimin(); // disable busy input
  logg << "testLTP2NIMCTP: <<exit code = " << status << ">>" <<endl;
  if (status==0) {  
    cout << "Test connection: LTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
    out  << "Test connection: LTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations .......[OK]" << endl;
  }  else {
    cout << "Test connection: LTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
    out << "Test connection: LTP to CTP & NIM on " << setw(4) << number_pg_loop << " iterations ...[FAILED]" << endl;
  }
  if (print > 1) logg << "testLTP2NIMCTP: <<exit code = " << status << ">>" <<endl;
  //   if (print > 1) cout << "testLTP2NIMCTP: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------

int testConnection(LTP* sender, LTP* receiver, LTP* /* ltp_receiver */)
{
  int status = 0;
  vector<int> result[2];
  ltp_signal_channel channel[] = { L1A, TR1, TR2, TR3, ORB, BG0, BG1, BG2, BG3 };
  string channel_name[] = { "L1A", "TTR1", "TTR2", "TTR3", "ORB", "BGO0", "BGO1", "BGO2", "BGO3" };

  if (print > 1) logg << "******* start testing channels" << endl;
  if ((sender == ltp_masterCTP) && (ltpCTP_status != 1)) {
    if (sender->OM_master_patgen(pgfilename)) {
      cout << "Patter generator file: <<" << pgfilename << ">> NOT FOUND, EXITING !!!" << endl;
      return -1;
    } 
    ltpCTP_status = 1;
  }
  if ((sender == ltp_masterLTP) && (ltpLTP_status != 1)) {
    if (sender->OM_master_patgen(pgfilename))  {
      cout << "Patter generator file: <<" << pgfilename << ">> NOT FOUND, EXITING !!!" << endl;
      return -1;
    } 
    ltpLTP_status = 1;
  }
  status |= sender->ORB_local_pg();

  status |= receiver->OM_slave();
  status |= receiver->BUSY_local_without_nimin(); // disable busy input
  
  for (int i = 0; i < number_pg_loop; ++i) { 
    result[0].clear();
    result[1].clear();
    for (i = 0; i < 9; i++) {
      if (print) logg <<"Testing " << channel_name[i] << endl;
      
      status |= LtpCounterSource(sender, channel[i]);
      status |= sender->COUNTER_reset();

      status |= LtpCounterSource(receiver, channel[i]);
      status |= receiver->COUNTER_reset();
           
      status |= sender->PG_runmode_cont();
      status |= sender->PG_start();
      sleep(1);
      status |= sender->PG_runmode_load();
      while (!sender->PG_is_idle()) {};
      
      u_int cnt_src, cnt_dst[2]; // must not be a vector !
      status |= sender->COUNTER_read_32(&cnt_src);
      status |= receiver->COUNTER_read_32(&cnt_dst[0]);
      
      //----------------------------------------------------------------
      
      if (cnt_src == 0) {
        logg  << "ERROR - testConnection(): zero pulses sent" << endl;
        cout << "ERROR - testConnection(): zero pulses sent" << endl;
        result[0].push_back(2);
        status |= 0x1000;
      } 
      else if ( cnt_dst[0] != cnt_src ) {
        logg << "ERROR - testConnection(): counter mismatch" << endl;
        result[0].push_back(1);
        status |= 0x2000;
      } 
      else
        result[0].push_back(0);
              
    }  // end inner loop  (j)
    
    /*  
	if (status != 0) {
	// printChannelResult(cout, result[0], result[1]);
	// printChannelResult(out,  result[0], result[1]);
	os << "" << endl;
	os << "+-----+--------------------------------------------------------------+" << endl;
	os << "| CH  | ORBT | LV1A | TTR1 | TTR2 | TTR3 | BGO0 | BGO1 | BGO2 | BGO3 |" << endl;

	os << "+-----+--------------------------------------------------------------+" << endl;
	os << "| CTP |";
	for (u_int i = 0; i < ctp.size(); ++i)
	os << setbase(10) << setw(5) << ctp[i] << " |";
	os << endl;

	os << "+-----+--------------------------------------------------------------+" << endl;
	os << "| LTP |";
	for (u_int i = 0; i < ltp.size(); ++i) // ltp.size() is always 0 !!!
	os << setbase(10) << setw(5) << ltp[i] << " |";
	os << endl;

	os << "+-----+--------------------------------------------------------------+" << endl;
      
	//string CHname[9] = { "ORB", "L1A", "TTR1", "TTR2", "TTR3", "BGO0", "BGO1", "BGO2", "BGO3" };
	// not necessary any more
      
	os << "" << endl;
	os << "CTP receiver - channels with problems: ";
      
	for (u_int i = 0; i < ctp.size(); ++i )
        if (ctp[i] != 0) os << CHname[i] << " - ";
	os << endl;
      
	os << "LTP receiver - channels with problems: ";
	for (u_int i = 0; i < ltp.size(); ++i )
        if (ltp[i] != 0) os << CHname[i] << " - ";
	os << endl;
      
	break;
	}*/
    
  }  // end outer loop (i)

  //------------------------------------------------------------------------

  logg << "testConnection: <<exit code = " << status << ">>" <<endl;
  return status;
}


//-------------------------------------- never used -----------------------------------
/*
  int testConnection()
  {
  int status = 0;
  int pmax   = 0;
  if (print) logg << "******* start testing channels" << endl;
  
  if (ltpCTP_status != 1) {
  if (ltp_masterCTP->OM_master_patgen(pgfilename))  {
  cout << "Patter generator file: <<" << pgfilename << ">> NOT FOUND, EXITING !!!" << endl;
  return -1;
  } 
  ltpCTP_status=1;
  }
  if (ltpLTP_status != 1) {
  if (ltp_masterLTP->OM_master_patgen(pgfilename)) {
  cout << "Patter generator file: <<" << pgfilename << ">> NOT FOUND, EXITING !!!" << endl;
  return -1;
  }
  ltpLTP_status=1;
  }
  status |= ltp_masterCTP->ORB_local_pg();
  status |= ltp_masterLTP->ORB_local_pg();
  status |= ltp_masterCTP->PG_runmode_singleshot_vme();
  status |= ltp_masterLTP->PG_runmode_singleshot_vme();

  pmax = number_pg_loop; 
  for (int i=0; i<pmax; ++i) { 
  channelResultCTP.clear();
  channelResultLTP.clear();

  if (print) logg <<" ....... L1A" << endl;

  status |= ltp_masterCTP->COUNTER_L1A();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_L1A();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_L1A();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_L1A();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);

  if (print) logg <<" ....... Test Trigger 1" << endl;

  status |= ltp_masterCTP->COUNTER_TR1();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_TR1();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_TR1();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_TR1();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);


  if (print) logg <<" ....... Test Trigger 2" << endl;

  status |= ltp_masterCTP->COUNTER_TR2();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_TR2();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_TR2();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_TR2();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);

  if (print) logg <<" ....... Test Trigger 3" << endl;

  status |= ltp_masterCTP->COUNTER_TR3();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_TR3();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_TR3();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_TR3();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);


  if (print) logg <<" ....... ORBIT" << endl;

  status |= ltp_masterCTP->COUNTER_orb();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_orb();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_orb();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_orb();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();                    
  status |= ltp_masterLTP->PG_start();               
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};       

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);


  if (print) logg <<" ....... BGO0" << endl;      

  status |= ltp_masterCTP->COUNTER_BG0();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_BG0();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_BG0();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_BG0();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);



  if (print) logg <<" ....... BGO1" << endl;

  status |= ltp_masterCTP->COUNTER_BG1();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_BG1();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_BG1();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_BG1();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);


  if (print) logg <<" ....... BGO2" << endl;

  status |= ltp_masterCTP->COUNTER_BG2();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_BG2();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_BG2();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_BG2();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);


  if (print) logg <<" ....... BGO3" << endl;

  status |= ltp_masterCTP->COUNTER_BG3();
  status |= ltp_masterCTP->COUNTER_reset();
  status |= ltp_masterLTP->COUNTER_BG3();
  status |= ltp_masterLTP->COUNTER_reset();

  status |= ltp_slaveCTP->COUNTER_BG3();
  status |= ltp_slaveCTP->COUNTER_reset();
  status |= ltp_slaveLTP->COUNTER_BG3();
  status |= ltp_slaveLTP->COUNTER_reset();

  status |= ltp_masterCTP->PG_start();
  status |= ltp_masterLTP->PG_start();
  while (!ltp_masterCTP->PG_is_idle() || !ltp_masterLTP->PG_is_idle()) {};

  status |= printCounters(ltp_masterCTP, ltp_masterLTP, ltp_slaveCTP, ltp_slaveLTP, logg);

  //cout << "Iteration -----------> " << i << endl; 
  //logg << "Iteration -----------> " << i << endl;
  printChannelResult(logg);
  if (status!=0) printChannelResult(cout);
  if (status!=0) printChannelResult(out);

  if (status != 0) break;
  }
  logg << "testConnection: <<exit code = " << status << ">>" <<endl;
  return status;
  }



  //-------------------------------------------
  void printChannelResult(ostream& os, vector<int> ctp, vector<int> ltp)
  {
  os << "" << endl;
  os << "+-----+--------------------------------------------------------------+" << endl;
  os << "| CH  | LV1A | TTR1 | TTR2 | TTR3 | ORBT | BGO0 | BGO1 | BGO2 | BGO3 |" << endl;
  os << "+-----+--------------------------------------------------------------+" << endl;
  os << "| CTP |"; 
  for (u_int i = 0; i < ctp.size(); ++i)
  os << setbase(10) << setw(5) << ctp[i] << " |";
  os << endl;
  os << "+-----+--------------------------------------------------------------+" << endl;
  os << "| LTP |"; 
  for (u_int i = 0; i < ltp.size(); ++i)
  os << setbase(10) << setw(5) << ltp[i] << " |";
  os << endl;
  os << "+-----+--------------------------------------------------------------+" << endl;
  
  string CHname[9] = {"LV1A", "TTR1", "TTR2", "TTR3", "ORBT", "BGO0", "BGO1", "BGO2", "BGO3"};

  os << "" << endl;
  os << "CTP receiver - channels with problems: ";  
  for (u_int i = 0; i < ctp.size(); ++i )
  if (ctp[i] != 0) os << CHname[i] << " - ";
  os << endl;
  os << "LTP receiver - channels with problems: ";
  for (u_int i = 0; i < ltp.size(); ++i )
  if (ltp[i] != 0) os << CHname[i] << " - ";
  os << endl;
  }

  //---------------------------------- never used -----------------------------------------------

  void printChannelResultSingle(ostream& os)
  {
  os << "" << endl;
  os << "+-----+--------------------------------------------------------------+" << endl;
  os << "| CH  | LV1A | TTR1 | TTR2 | TTR3 | ORBT | BGO0 | BGO1 | BGO2 | BGO3 |" << endl;
  os << "+-----+--------------------------------------------------------------+" << endl;
  os << "| OUT |"; for (u_int i=0; i<channelResult.size(); ++i) {os << setw(5)  << channelResult[i] << " |";}  os << endl;
  os << "+-----+--------------------------------------------------------------+" << endl;
  
  string CHname[9] = {"LV1A", "TTR1", "TTR2", "TTR3", "ORBT", "BGO0", "BGO1", "BGO2", "BGO3"};

  os << "" << endl;
  os << "receiver - channels with problems: ";  for (u_int i=0; i<channelResultCTP.size(); ++i ) {if(channelResultCTP[i]!=0) os << CHname[i] << " - ";} os<< endl;
  os << "" << endl;
  }
*/
//-------------------------------------------

//-------------------------------------------
int initializeLTPs()
{
  int status = 0;
  //   logg << "initializeLTPs: <<about to start>>" << endl;  

  // check  that the addresses are different and valid
  // implement some write and read back to make sure that 
  // the module can be accessed

  if (!ltp_masterCTP) ltp_masterCTP = new LTP();
  status |= ltp_masterCTP->Open(base_ltpMasterCTP);
  status |= ltp_masterCTP->Reset();
  if (ltpCTP_status != 0) {
    if (ltp_masterCTP->OM_master_patgen(pgfilename_trt)) {
      cout << "Patter generator file: <<" << pgfilename_trt << ">> NOT FOUND, EXITING !!!" << endl;
      return -1;
    } 
    ltpCTP_status = 0;
  }
  status |= ltp_masterCTP->PG_runmode_cont();
  status |= ltp_masterCTP->ORB_local_pg();
  status |= ltp_masterCTP->PG_start();

  if (base_ltpMasterLTP != 0) {
    if (!ltp_masterLTP) ltp_masterLTP = new LTP();
    status |= ltp_masterLTP->Open(base_ltpMasterLTP);
    status |= ltp_masterLTP->Reset();
    if (ltpLTP_status != 0) {
      if (ltp_masterLTP->OM_master_patgen(pgfilename_trt)) {
	cout << "Patter generator file: <<" << pgfilename_trt << ">> NOT FOUND, EXITING !!!" << endl;
	return -1;
      }
      ltpLTP_status = 0;
    }
    status |= ltp_masterLTP->PG_runmode_cont();
    status |= ltp_masterLTP->ORB_local_pg();
    status |= ltp_masterLTP->PG_start();
  }

  if (base_ltpSlaveCTP != 0) {
    if (!ltp_slaveCTP) ltp_slaveCTP = new LTP();
    status |= ltp_slaveCTP->Open(base_ltpSlaveCTP);
    status |= ltp_slaveCTP->Reset();
    status |= ltp_slaveCTP->OM_slave(); // make it a slave
  }

  if (!ltp_slaveLTP) ltp_slaveLTP = new LTP();
  status |= ltp_slaveLTP->Open(base_ltpSlaveLTP);
  status |= ltp_slaveLTP->Reset();
  status |= ltp_slaveLTP->OM_slave(); // make it a slave
  logg << "initializeLTPs: <<exit code = " << status << ">>" <<endl;  
  return status;
}

//-------------------------------------------
int initializeLTPsContinuous()
{
  int status = 0;
  //   logg << "initializeLTPsContinuous: <<about to start>>" << endl;  
  //   logg << "initializeLTPsContinuous: <<this function needs to be completed!!!>>" << endl;  

  // check  that the addresses are different and valid
  if (!ltp_masterCTP) ltp_masterCTP = new LTP();
  status |= ltp_masterCTP->Open(base_ltpMasterCTP);
  if (ltpCTP_status != 1) { 
    if (ltp_masterCTP->OM_master_patgen(pgfilename)) {
      cout << "Patter generator file: <<" << pgfilename << ">> NOT FOUND, EXITING !!!" << endl;
      return -1;
    } 
    ltpCTP_status = 1;
  }
  status |= ltp_masterCTP->PG_runmode_cont();
  status |= ltp_masterCTP->ORB_local_pg();
  status |= ltp_masterCTP->PG_start();


  // implement some write and read back to make sure that 
  // the module cam be accessed
  //status |= 
  if (!ltp_masterLTP) ltp_masterLTP = new LTP();
  status |= ltp_masterLTP->Open(base_ltpMasterLTP);
  if (ltpLTP_status != 1) {
    if (ltp_masterLTP->OM_master_patgen(pgfilename)) {
      cout << "Patter generator file: <<" << pgfilename << ">> NOT FOUND, EXITING !!!" << endl;
      return -1;
    } 
    ltpLTP_status = 1;
  }

  status |= ltp_masterLTP->PG_runmode_cont();
  status |= ltp_masterLTP->ORB_local_pg();
  status |= ltp_masterLTP->PG_start();

  logg << "initializeLTPsContinuous: <<exit code = " << status << ">>" <<endl;  
  return status;
}

int setDAC(string cable)
{
  int status = 0;
  if (cable=="short") {
    status |= ltpi->setupDAC4shortCable();
  }
  else if (cable=="long") {
    status |= ltpi->setupDAC4longCable();
  }
  else {
    logg << "initializeLTPi: <<make sure that in the test file you are setting the cable for both CTPin and LTPin>>" << endl;
    logg << "initializeLTPi: <<connectors to either short or long>>" << endl;
    status = 1;
  }
  return status;
}

int initializeLTPi(int delay)
{
  int status = 0;
  //   logg << "initializeLTPi: <<about to start>>" << endl;  
  //   logg << "initializeLTPi: <<note that now it anly supports the same cable for both connectors>>"<< endl;
  status |= setDAC(CTPin_cable);
  u_short read;
  status |= ltpi->getDACCTPgain(read); logg << "initializeLTPi: <<CTP gain set to: " << setbase(16) << read <<">>"<< endl;
  status |= ltpi->getDACLTPgain(read); logg << "initializeLTPi: <<LTP gain set to: " << setbase(16) << read <<">>"<< endl;
  status |= ltpi->getDACCTPequ(read); logg << "initializeLTPi: <<CTP equalized set to: " << setbase(16) << read <<">>"<< endl;
  status |= ltpi->getDACLTPequ(read); logg << "initializeLTPi: <<LTP equalizer set to: " << setbase(16) << read <<">>"<< endl;

  
  status |= ltpi->enableCTPout();
  status |= ltpi->setDelayAllChannels(delay);
  if (!status) logg << "initializeLTPi: <<Delay chips initialized; delay set to " << delay * 0.5 << " ns>>" << endl;
  else logg << "initializeLTPi: <<Delay chips cannot set delay>>"<< endl;
  logg << setbase(10) << endl;
  logg << "initializeLTPi: <<exit code = " << status << ">>" <<endl;
  if (status==0) {  
    cout << "Initialize LTPi ............................................[OK]" << endl;
    out  << "Initialize LTPi ............................................[OK]" << endl;
  }  else {
    cout << "Initialize LTPi ........................................[FAILED]" << endl;
    out << "Initialize LTPi ........................................[FAILED]" << endl;
  }
  logg << "initializeLTPi: <<exit code = " << status << ">>" <<endl;
  //   cout << "initializeLTPi: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int check_dac_chips()
{
  //   logg << "check_dac_chips: <<about to start>>" << endl;  
  int status = 0;
  int testt=0;
  u_short wgain, wequ, rgain, requ;

  for (int i=0; !status && i<8; ++i) {  
    wgain = (1 << i);
    //wequ  = (1 << i);
    wequ  = 2 * i; //(0xff >> i);
    
    status |= ltpi->setDACCTPgain(wgain);
    logg << "check_dac_chips: <<CTP gain: set to: " << setbase(16) << wgain << " >>" << endl;
    status |= ltpi->setDACCTPequ(wequ);
    logg << "check_dac_chips: <<CTP equ: set to: " << setbase(16) << wequ << " >>" << endl;
    status |= ltpi->getDACCTPgain(rgain);
    logg << "check_dac_chips: <<CTP gain: readback: " << setbase(16) << rgain << " >>" << endl;
    if (wgain != rgain) {logg << "check_dac_chips: <<CTP gain: readback does not mach what has been written>>" << endl; testt=1;}
    status |= ltpi->getDACCTPequ(requ);
    logg << "check_dac_chips: <<CTP equ: readback: " << setbase(16) << requ << " >>" << endl;
    if (wequ != requ) {logg << "check_dac_chips: <<CTP equ: readback does not mach what has been written>>" << endl; testt=1;}

    status |= ltpi->setDACLTPgain(wgain);
    logg << "check_dac_chips: <<LTP gain: set to: " << setbase(16) << wgain << " >>" << endl;
    status |= ltpi->setDACLTPequ(wequ);
    logg << "check_dac_chips: <<LTP equ: set to: " << setbase(16) << wequ << " >>" << endl;

    status |= ltpi->getDACLTPgain(rgain);
    logg << "check_dac_chips: <<LTP gain: readback: " << setbase(16) << rgain << " >>" << endl;
    if (wgain != rgain) {logg << "check_dac_chips: <<LTP gain: readback does not mach what has been written>>" << endl; testt=3;}
    status |= ltpi->getDACLTPequ(requ); 
    logg << "check_dac_chips: <<LTP equ: readback: " << setbase(16) << requ << " >>" << endl;
    if (wequ != requ) {logg << "check_dac_chips: <<LTP equ: readback does not mach what has been written>>" << endl; testt=4;}
  }
  logg<< setbase(10) << endl;  
  logg << "check_dac_chips: <<exit code = " << status << ">>" <<endl;
  //   cout << "check_dac_chips: <<exit code = " << status << ">>" <<endl;

  if (testt==0) {
    cout << "Check LTPi DAC chips: wirte & read .........................[OK]" << endl;
    out  << "Check LTPi DAC chips: wirte & read .........................[OK]" << endl;
  }  else {
    cout << "Check LTPi DAC chips: wirte & read .....................[FAILED]" << endl;
    out << "Check LTPi DAC chips: wirte & read .....................[FAILED]" << endl;
  }                                               
  status |= ltpi->setupDAC4shortCable();
  return status;
}

//-------------------------------------------
int check_delay_chips()
{
  int status = 0;
  const u_short channel[] = { ltpi->DEL25L1A,  ltpi->DEL25ORB,  
			      ltpi->DEL25BGO0, ltpi->DEL25BGO2, ltpi->DEL25BGO3, ltpi->DEL25BGO3,
			      ltpi->DEL25TTR1, ltpi->DEL25TTR2, ltpi->DEL25TTR3,
			      ltpi->DEL25TRT0, ltpi->DEL25TRT1, ltpi->DEL25TRT2, ltpi->DEL25TRT3, 
			      ltpi->DEL25TRT4, ltpi->DEL25TRT5, ltpi->DEL25TRT6, ltpi->DEL25TRT7 };

  //   logg << "checka_delay_chips: <<about to start>>" << endl;  
  for (int j=0; j<17; ++j)// set each channel to a different default delay
    status |= ltpi->setDelaySingleChannel(2*j+1, channel[j]);
  
  for (int j=0; j<17; ++j) {
    //    logg << "check_delay_chips: <<set channel to: " << setbase(16) << channel[j] << ">>" << setbase(10) << endl;
    for (int i=0; i<63; ++i) {
      u_short rdelay, wdelay(i);
      status |= ltpi->setDelaySingleChannel(wdelay, channel[j]);
      status |= ltpi->readDelaySingleChannel(rdelay, channel[j]);
      //      logg << "check_delay_chips: <<Channel: " << setbase(16) << channel[j] << setbase(10) << " setting delay to: "<< wdelay 
      //	  << "; reading delay: " << rdelay  << ">>" << endl;
      if (wdelay != rdelay) {
	logg << "check_delay_chips: <<readback delay does not mach what has been written>>" << endl; 
	status |= 0x1000;
      }
    }
    status |= ltpi->setDelaySingleChannel(j, channel[j]); // set it back to is default delay
  }
  printResultMessage("Check LTPI delay chips write & read", status);
  printStatusMessage("check_delay_chips()", status);
  return status;
}

//-------------------------------------------
int set_i2c()
{
  int status = 0;
  //   logg << "set_i2c: <<about to start>>" << endl;  
  status |= ltpi->setupI2C();
  if (status==0) {  
    cout << "Setup LTPi I2C bus .........................................[OK]" << endl;
    out  << "Setup LTPi I2C bus .........................................[OK]" << endl;
  }  else {
    cout << "Setup LTPi I2C bus .....................................[FAILED]" << endl;
    out << "Setup LTPi I2C bus .....................................[FAILED]" << endl;
  }
  logg << "set_i2c: <<exit code = " << status << ">>" <<endl;
  //   cout << "set_i2c: <<exit code = " << status << ">>" <<endl;
  return status;
}

//-------------------------------------------
int test_status_register()
{

  int status = 0;
  int test = 0;

  // BUSY
  status |= ltp_slaveLTP->BUSY_disable_constant_level();
  if (ltp_slaveLTP->BUSY_linkout_is_busy()) {test |= 1; logg << "<<test_status_register>>: slaveLTP is BUSY on the linkin" << endl;}
  else { if (ltpi->isBUSY_LTPout()) 
      {test |= 1; logg <<"<<test_status_register>>: LTP is not sending busy but the LTPi status sees a busy on LTPout" << endl;}
  }

  status |= ltp_slaveCTP->BUSY_disable_constant_level();
  if (ltp_slaveCTP->BUSY_linkout_is_busy()) {test |= 1; logg << "<<test_status_register>>: slaveCTP is BUSY on the linkin" << endl;}
  else { if (ltpi->isBUSY_CTPout()) 
      {test |= 1; logg <<"<<test_status_register>>: LTP is not sending busy but the LTPi status sees a busy on CTPout" << endl;}
  }

  status |= ltp_slaveLTP->BUSY_enable_constant_level();
  if (!ltpi->isBUSY_LTPout()) {test |= 1; logg << "<<test_status_register>>: LTP slave busy but LTPi status sees no busy on the LTPout" << endl;}
  status |= ltp_slaveLTP->BUSY_disable_constant_level();

  status |= ltp_slaveCTP->BUSY_enable_constant_level();
  if (!ltpi->isBUSY_CTPout()) {test |= 1; logg << "<<test_status_register>>: CTP slave busy but LTPi status sees no busy on the CTPout" << endl;}
  status |= ltp_slaveCTP->BUSY_disable_constant_level();

  if (test != 1) {
    cout << "Test status register for BUSY signals ..................[OK]"<< endl;
    out << "Test status register for BUSY signals ..................[OK]"<< endl;
  } else {
    cout << "Test status register for BUSY signals ..............[FAILED]"<< endl;
    out << "Test status register for BUSY signals ..............[FAILED]"<< endl;
  }
  
  // LV1, ORBIT, BC
  ltp_masterLTP->Reset();
  ltp_masterCTP->Reset();
  if (ltpLTP_status != 0) {
    if (ltp_masterLTP->OM_master_patgen(pgfilename_status)) {
      cout << "Patter generator file NOT FOUND, EXITING !!!" << endl;
      return -1;
    }
    ltpLTP_status = 0;
  }
  status |= ltp_masterLTP->PG_runmode_cont();
  status |= ltp_masterLTP->ORB_local_pg();

  if (ltpCTP_status != 0) {
    if (ltp_masterCTP->OM_master_patgen(pgfilename_status)) {
      cout << "Patter generator file NOT FOUND, EXITING !!!" << endl;
      return -1;
    }
    ltpCTP_status = 0;
  }
  status |= ltp_masterCTP->PG_runmode_cont();
  status |= ltp_masterCTP->ORB_local_pg();

  status |= ltpi->nim_mode();
  status |= ltp_masterLTP->BC_lemoout_from_linkin();
  logg << "Start testing the status registers" << endl;
  logg << "First all signals off including the BC" << endl;
  wait(1,1);
  if (ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>: BC on NIMin seen by status register" << endl;}
  if (ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>: BC on CTPout seen by status register" << endl;}
  if (ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>: BC on LTPout seen by status register" << endl;}
  
  status |= ltp_masterLTP->BC_lemoout_from_local();

  status |= ltp_masterLTP->PG_start();

  logg << "" << endl;
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode" << endl;
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORB on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}

  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off BGO3" << endl;
  status |= ltp_masterLTP->IO_local_no_source(LTP::BG3);
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:   BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:   BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:   BGO3 on LTPout seen by status register" << endl;}
  status |= ltp_masterLTP->IO_local_pg(LTP::BG3);
  status |= ltp_masterLTP->IO_local_no_source(LTP::BG2);
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off BGO2" << endl;
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:   BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:   BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:   no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:   BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off BGO1" << endl;
  status |= ltp_masterLTP->IO_local_pg(LTP::BG2);
  status |= ltp_masterLTP->IO_local_no_source(LTP::BG1);
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:   BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:   BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:   BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off BGO0" << endl;
  status |= ltp_masterLTP->IO_local_pg(LTP::BG1);
  status |= ltp_masterLTP->IO_local_no_source(LTP::BG0);
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:   BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:   BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:   BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off TR3" << endl;
  status |= ltp_masterLTP->IO_local_pg(LTP::BG0);
  status |= ltp_masterLTP->IO_local_no_source(LTP::TR3);
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:   TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:   TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:   TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  status |= ltp_masterLTP->IO_local_pg(LTP::TR3);
  status |= ltp_masterLTP->IO_local_no_source(LTP::TR2);
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off TR2" << endl;
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:   TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:   TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:   TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  status |= ltp_masterLTP->IO_local_pg(LTP::TR2);
  status |= ltp_masterLTP->IO_local_no_source(LTP::TR1);
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off TR1" << endl;
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:  no L1A on NIMin seen by status register" << endl;}
  if (ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:   TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on CTPout seen by status register" << endl;}
  if (ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:  no L1A on LTPout seen by status register" << endl;}
  if (ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:   TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  status |= ltp_masterLTP->IO_local_pg(LTP::TR1);
  status |= ltp_masterLTP->IO_local_no_source(LTP::L1A);
  wait(1, 1); // for the status to change
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off L1A" << endl;
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (!ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:  no ORBIT on NIMin seen by status register" << endl;}
  if (ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:   L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (!ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on CTPout seen by status register" << endl;}
  if (ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:   L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (!ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:  no ORB on LTPout seen by status register" << endl;}
  if (ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:   L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  status |= ltp_masterLTP->IO_local_pg(LTP::L1A);
  status |= ltp_masterLTP->ORB_local_no();
  wait(1, 1); // for the status to change
  logg << "Sending all signals from the ltp_masterLTP, with the ltpi in NIM mode, turning off Orbit" << endl;
  // NIM input status register
  if (!ltpi->isBC_NIM()) {test |=2; logg << "<<test_status_register>>:  no BC on NIMin seen by status register" << endl;}
  if (ltpi->isORB_NIM()) {test |=2; logg << "<<test_status_register>>:   ORB on NIMin seen by status register" << endl;}
  if (!ltpi->isL1A_NIM()) {test |=2; logg << "<<test_status_register>>:   no L1A on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR1_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR2_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on NIMin seen by status register" << endl;}
  if (!ltpi->isTTR3_NIM()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO0_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO1_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO2_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on NIMin seen by status register" << endl;}
  if (!ltpi->isBGO3_NIM()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on NIMin seen by status register" << endl;}
  // Link CTP-out status register
  if (!ltpi->isBC_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on CTPout seen by status register" << endl;}
  if (ltpi->isORB_CTPout()) {test |=2; logg << "<<test_status_register>>:   ORB on CTPout seen by status register" << endl;}
  if (!ltpi->isL1A_CTPout()) {test |=2; logg << "<<test_status_register>>:   no L1A on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on CTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_CTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isBC_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BC on LTPout seen by status register" << endl;}
  if (ltpi->isORB_LTPout()) {test |=2; logg << "<<test_status_register>>:   ORB on LTPout seen by status register" << endl;}
  if (!ltpi->isL1A_LTPout()) {test |=2; logg << "<<test_status_register>>:   no L1A on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTTR3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no TTR3 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO0_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO0 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO1_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO1 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO2_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO2 on LTPout seen by status register" << endl;}
  if (!ltpi->isBGO3_LTPout()) {test |=2; logg << "<<test_status_register>>:  no BGO3 on LTPout seen by status register" << endl;}
  status |= ltp_masterLTP->ORB_local_pg();


  cout << "test = " << test << endl;
  if (test != 2) {
    cout << "Test status register for TTC signals ...................[OK]"<< endl;
    out << "Test status register for TTC signals ...................[OK]"<< endl;
  } else {
    cout << "Test status register for TTC signals ...............[FAILED]"<< endl;
    out << "Test status register for TTC signals ...............[FAILED]"<< endl;
  }

  logg << "" << endl;
  logg << "Checking calibration request status register" << endl;

  status |= ltpi->calCTPfromCTP_LTPFromLTP(); // then check the ctp & ltp connection
  status |= ltp_slaveCTP->CAL_source_preset(); // set calibration request from preset
  status |= ltp_slaveCTP->CAL_enable_testpath(); // enable test pad
  status |= ltp_slaveLTP->CAL_source_preset(); // set calibration request from preset
  status |= ltp_slaveLTP->CAL_enable_testpath(); // enable test pad

  status |= ltp_slaveCTP->CAL_set_preset_value(0);

  // Link CTP-out status register
  if (ltpi->isCAL0_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL0 on CTPout seen by status register" << endl;}
  if (ltpi->isCAL1_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL1 on CTPout seen by status register" << endl;}
  if (ltpi->isCAL2_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL2 on CTPout seen by status register" << endl;}

  status |= ltp_slaveLTP->CAL_set_preset_value(0);

  // Link LTP-out status register 
  if (ltpi->isCAL0_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL0 on LTPout seen by status register" << endl;}
  if (ltpi->isCAL1_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL1 on LTPout seen by status register" << endl;}
  if (ltpi->isCAL2_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL2 on LTPout seen by status register" << endl;}

  status |= ltp_slaveCTP->CAL_set_preset_value(1);

  // Link CTP-out status register
  if (!ltpi->isCAL0_CTPout()) {test |=3; logg << "<<test_status_register>>:   CAL0 on CTPout seen by status register" << endl;}
  if (ltpi->isCAL1_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL1 on CTPout seen by status register" << endl;}
  if (ltpi->isCAL2_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL2 on CTPout seen by status register" << endl;}

  status |= ltp_slaveLTP->CAL_set_preset_value(1);

  // Link LTP-out status register 
  if (!ltpi->isCAL0_LTPout()) {test |=3; logg << "<<test_status_register>>:  CAL0 on LTPout seen by status register" << endl;}
  if (ltpi->isCAL1_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL1 on LTPout seen by status register" << endl;}
  if (ltpi->isCAL2_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL2 on LTPout seen by status register" << endl;}

  status |= ltp_slaveCTP->CAL_set_preset_value(2);

  // Link CTP-out status register
  if (ltpi->isCAL0_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL0 on CTPout seen by status register" << endl;}
  if (!ltpi->isCAL1_CTPout()) {test |=3; logg << "<<test_status_register>>:   CAL1 on CTPout seen by status register" << endl;}
  if (ltpi->isCAL2_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL2 on CTPout seen by status register" << endl;}

  status |= ltp_slaveLTP->CAL_set_preset_value(2);

  // Link LTP-out status register 
  if (ltpi->isCAL0_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL0 on LTPout seen by status register" << endl;}
  if (!ltpi->isCAL1_LTPout()) {test |=3; logg << "<<test_status_register>>:   CAL1 on LTPout seen by status register" << endl;}
  if (ltpi->isCAL2_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL2 on LTPout seen by status register" << endl;}

  status |= ltp_slaveCTP->CAL_set_preset_value(4);

  // Link CTP-out status register
  if (ltpi->isCAL0_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL0 on CTPout seen by status register" << endl;}
  if (ltpi->isCAL1_CTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL1 on CTPout seen by status register" << endl;}
  if (!ltpi->isCAL2_CTPout()) {test |=3; logg << "<<test_status_register>>:   CAL2 on CTPout seen by status register" << endl;}

  status |= ltp_slaveLTP->CAL_set_preset_value(4);

  // Link LTP-out status register 
  if (ltpi->isCAL0_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL0 on LTPout seen by status register" << endl;}
  if (ltpi->isCAL1_LTPout()) {test |=3; logg << "<<test_status_register>>:  no CAL1 on LTPout seen by status register" << endl;}
  if (!ltpi->isCAL2_LTPout()) {test |=3; logg << "<<test_status_register>>:   CAL2 on LTPout seen by status register" << endl;}

  if (test != 3) {
    cout << "Test status register for CAL REQ signals ...............[OK]"<< endl;
    out << "Test status register for CAL REQ signals ...............[OK]"<< endl;
  } else {
    cout << "Test status register for CAL REQ signals ...........[FAILED]"<< endl;
    out << "Test status register for CAL REQ signals ...........[FAILED]"<< endl;
  }

  int r1, r3;
  status |= ltp_slaveCTP->CAL_get_linkstatus(&r1);
  status |= ltp_slaveLTP->CAL_get_linkstatus(&r3);
  logg << "CTP slave -> " << r1 << "; LTP slave ->" << r3 << endl;

  status |= ltp_masterLTP->CAL_enable_testpath();
  status |= ltp_masterCTP->CAL_enable_testpath();

  status |= ltp_masterCTP->CAL_source_link(); // read calibration from link
  status |= ltp_masterLTP->CAL_source_link();

  status |= ltp_masterCTP->CAL_get_linkstatus(&r1);
  status |= ltp_masterLTP->CAL_get_linkstatus(&r3);
  logg << "CTP master -> " << r1 << "; LTP master ->" << r3 << endl;

  //ltpi->dumpModuleRegisters();

  
  logg << "" << endl;
  logg << "Checking trigger type status register" << endl;

  status |= ltpi->TRT_FROM_LOCAL_REGISTER();   
  u_short value = 0xff;
  u_short rvalue = 0;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  logg << "All bits on" << endl;
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}
  // switching of 7
  logg << "All bits on, but #7" << endl;
  value = 0x7f;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  TRT7 on LTPout seen by status register" << endl;}
  // switching of 6
  logg << "All bits on, but #6" << endl;
  value = 0xbf;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:   TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}
  // switching of the 5
  logg << "All bits on, but #5" << endl;
  value = 0xdf;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on LTPout seen by status register" << endl;}
  if (ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}
  // switching of the 4
  logg << "All bits on, but #4" << endl;
  value = 0xef;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}
  // switching of the 3
  logg << "All bits on, but #3" << endl;
  value = 0xf7;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}
  // switching of the 2
  logg << "All bits on, but #2" << endl;
  value = 0xfb;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:   TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:   TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}
  // switching of 1
  logg << "All bits on, but #1" << endl;
  value = 0xfd;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (!ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on CTPout seen by status register" << endl;}
  if (ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:   TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (!ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT0 on LTPout seen by status register" << endl;}
  if (ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:   TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:   no TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}
  // switching of 0
  logg << "All bits on, but #0" << endl;
  value = 0xfe;
  status = ltpi->SetTRT(0, value); 
  status = ltpi->GetTRT(0, rvalue);
  if (rvalue != value) {status |= 4; logg << "Error not able to read/write to the LTPi trigger type local register" << endl;}
  printf("TRT000 (init value) = 0x%02x\n", value);
  // Link CTP-out status register
  if (ltpi->isTRT0_CTPout()) {test |=4; logg << "<<test_status_register>>:   TRT0 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on CTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_CTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on CTPout seen by status register" << endl;}
  // Link LTP-out status register 
  if (ltpi->isTRT0_LTPout()) {test |=4; logg << "<<test_status_register>>:   TRT0 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT1_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT1 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT2_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT2 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT3_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT3 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT4_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT4 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT5_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT5 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT6_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT6 on LTPout seen by status register" << endl;}
  if (!ltpi->isTRT7_LTPout()) {test |=4; logg << "<<test_status_register>>:  no TRT7 on LTPout seen by status register" << endl;}

  if (test != 4) {
    cout << "Test status register for TRIGGER TYPE signals ..........[OK]"<< endl;
    out << "Test status register for TRIGGER TYPE signals ..........[OK]"<< endl;
  } else {
    cout << "Test status register for TRIGGER TYPE signals ......[FAILED]"<< endl;
    out << "Test status register for TRIGGER TYPE signals ......[FAILED]"<< endl;
  }


  if (test == 0) {
    cout << "Test status registers ..................................[OK]"<< endl;
    out << "Test status registers ..................................[OK]"<< endl;
  } else {
    cout << "Test status registers ..............................[FAILED]"<< endl;
    out << "Test status registers ..............................[FAILED]"<< endl;
  }



  return status;
}



//-------------------------------------------
int address_scan()
{
  int status = 0;  
  return status;
}

//-------------------------------------------
void open_output_files(int mode)
{
  char log_tmp[100];
  char err_tmp[100];
  char out_tmp[100];

  sprintf(log_tmp, "%08x_%s", module_id, log_file_name.c_str());
  sprintf(err_tmp, "%08x_%s", module_id, err_file_name.c_str());
  sprintf(out_tmp, "%08x_%s", module_id, out_file_name.c_str());

  if (mode==0 || mode==1) {
    if(logg.is_open()) logg.close();
    logg.open(log_tmp, ios::out);
  }

  if (mode==0 || mode==2) {
    if(err.is_open()) err.close();
    err.open(err_tmp, ios::out);
  }

  if (mode==0 || mode==3) {
    if(out.is_open()) out.close();
    out.open(out_tmp, ios::out);
  }
}

//-------------------------------------------
int printheader(ostream& os)
{
  int status = 0;
  
  os << "" << endl;
  os << "" << endl;
  
  os << "      *****************************************************************"<< endl;
  os << "      *"<< endl;
  os << "      *"<< endl;
  os << "      *"<< endl;
  os << "      *"<< endl;
  os << "      *****************************************************************"<< endl;

  os << "" << endl;
  os << "" << endl;
  
  return status;
}
//-------------------------------------------
void close_output_files()
{
  logg.close();
  err.close();
  out.close();
}

//-------------------------------------------
int read_test(const string& job_file)
{
  int status = 0;
  static const int BUF_SIZE = 256;                                                                                    
  cout << "Open input file with the input values (LTP and LTPi address, ...): " << job_file << endl; 
                   
  ifstream fin(job_file.c_str(), ios::in);                                                                             
  if (!fin) {                                                                                                         
    cout << "read_test: <<Input File: " << job_file << " could not be opened!>>" << endl; 
    cout << "read_test: <<Input File: an example of an input-test file could be found in RCDLtpi/src/test/test_ltpi.in>>" << endl; 
    status = 1;
    return status;
  } 

  vector<string> tokens;
  char buf[BUF_SIZE];
  cout << "" << endl;
  cout << "+---------------------------------------------+" << endl;
  cout << "|                 INPUT VALUES                |" << endl;
  cout << "+---------------------------------------------+" << endl;
  while (fin.getline(buf, BUF_SIZE, '\n')) {  // pops off the newline character
    string line(buf);
    if (line.empty() || line == "START") continue; 
    // enable '#' and '//' style comments
    if (line.substr(0,1) == "#" || line.substr(0,2) == "//") continue; 
    if (line == "END") break;   
    tokenize(line, tokens);
    //unsigned int size = tokens.size();    
    cout << "| " << setw(22) << tokens[0] << "  = " << setw(15) << tokens[1] << "   |"<< endl; 
    if (tokens[0] ==  "loggile")                 log_file_name       = tokens[1];
    if (tokens[0] ==  "outFile")                 out_file_name       = tokens[1];
    if (tokens[0] ==  "errFile")                 err_file_name       = tokens[1];
    if (tokens[0] ==  "firmwareVersion")         firmware_v          = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "module_id")               module_id           = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "LTPi_address")            base_ltpi           = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "CTPin_cable")             CTPin_cable         = tokens[1];
    if (tokens[0] ==  "LTPin_cable")             LTPin_cable         = tokens[1];
    if (tokens[0] ==  "TEST")                    test                = atoi(tokens[1].c_str());
    if (tokens[0] ==  "LTP_masterCTP_address")   base_ltpMasterCTP   = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "LTP_masterLTP_address")   base_ltpMasterLTP   = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "LTP_slaveCTP_address")    base_ltpSlaveCTP    = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "LTP_slaveLTP_address")    base_ltpSlaveLTP    = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "ctp_longcable_gain")      ctp_gain            = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "ctp_longcable_equ")       ctp_equ             = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "ltp_longcable_gain")      ltp_gain            = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "ltp_longcable_equ")       ltp_equ             = strtol(tokens[1].c_str(), nullptr, 16);
    if (tokens[0] ==  "number_pg_loop")          number_pg_loop      = atoi(tokens[1].c_str());
    if (tokens[0] ==  "trt_iteration")           trt_itr             = atoi(tokens[1].c_str());
    if (tokens[0] ==  "pgfilename")              pgfilename          = tokens[1];
    if (tokens[0] ==  "print")                   print               = atoi(tokens[1].c_str());
    if (tokens[0] ==  "delay25_setting")         delay25_setting     = atoi(tokens[1].c_str());
    tokens.clear();      
  }
  cout << "+---------------------------------------------+" << endl;
  cout << "" << endl;
  return status;
}

//-------------------------------------------
void tokenize(const string& str, vector<string>& tokens, const string& delimiters) 
{
  // Skip delimiters at beginning. 
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  string::size_type pos = str.find_first_of(delimiters, lastPos);
  while (string::npos != pos || string::npos != lastPos) { 
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);  
  }   
}  

//-------------------------------------------
void print_results(ostream& os)
{
  if ((test & (0x1)) == 0x1)
    os << "It was not possible to start the test. Check err & log file " << endl;
  if (((test & (1<<1)) == (1<<1)))
    os <<  "" << endl;
}

//-------------------------------------------
int printCounters(LTP* ltpSender, LTP* ltpCTP, LTP* ltpLTP, ostream& os) {
  int status = 0;
  u_int f, c1, c2;

  if (ltpCTP) {
    status |= ltpCTP->COUNTER_read_32(&c1);
    if (print) os << "  LTP slave CTP counter value:......................" << c1 << endl;
  }

  if (ltpLTP) {
    status |= ltpLTP->COUNTER_read_32(&c2);
    if (print) os << "  LTP slave LTP counter value:......................" << c2 << endl;
  }

  status |= ltpSender->COUNTER_read_32(&f);
  if (print) os << "  LTP master counter value:........................." << f << endl;
  
  if (f == 0) {
    status = 666;
    os << "!!! ERROR !!! <<Zero signal sent>>" << endl;
    cout << "!!! ERROR !!! <<Zero signals sent>>" << endl;
  }

  if (((c1 != f) || (c2 != f)) && (f != 0)) {
    status = 999;
    os << "!!! ERROR !!! <<Counters do not match>>" << endl;; 
  }

  if (c1 != f && f != 0)      channelResultCTP.push_back(1);
  else if (c1 == f && f != 0) channelResultCTP.push_back(0);
  else if (c1 == f && f == 0) channelResultCTP.push_back(2);

  if (c2 != f && f != 0)       channelResultLTP.push_back(1);
  else if (c2 == f && f != 0)  channelResultLTP.push_back(0);
  else if (c2 == f && f == 0)  channelResultLTP.push_back(2);
  
  //logg << "print_counters: <<exit code = " << status << ">>" <<endl;
  return(status);
}

//-------------------------------------------
int printCounters(LTP* ltpSender, LTP* ltpReceiver, ostream& os) {
  int status = 0;
  u_short m(0), l(0), m1(0),l1(0);
  u_int c1 = 0;
  u_int f(0);

  if (ltpReceiver) {
    status |= ltpReceiver->COUNTER_read_lsw(&l1);
    status |= ltpReceiver->COUNTER_read_msw(&m1);
    c1 = (m1 << 16) | l1;
    if (print) os << "  LTP slave counter value:......................" << c1 << endl;
  }

  status |= ltpSender->COUNTER_read_lsw(&l);
  status |= ltpSender->COUNTER_read_msw(&m);
  f = (m << 16) | l;
  if (print) os << "  LTP master counter value:........................." << f << endl;
  
  if ((c1 != f) && f != 0) {
    status = 999;
    os << "!!! ERROR !!! <<Counters do not match>>" << endl;; 
  }

  if (c1 != f && f != 0)      channelResult.push_back(1);
  else if (c1 == f && f != 0) channelResult.push_back(0);
  else if (c1 == f && f == 0) channelResult.push_back(2);
  
  if (f == 0) {
    status = 666;
    os << "!!! ERROR !!! <<Zero signal sent>>" << endl;
    cout << "!!! ERROR !!! <<Zero signal sent>>" << endl;
  }
  logg << "print_counters: <<exit code = " << status << ">>" <<endl;
  return(status);
}

//-------------------------------------------
int printCounters(LTP* ltpSenderCTP, LTP* ltpSenderLTP, LTP* ltpCTP, LTP* ltpLTP, ostream& os) {
  int status = 0;
  u_short m, l, m1,l1, m2, l2;
  u_int c1 = 0, c2 = 0;
  u_int f1 = 0, f2 = 0;
  //wait(0.01,0);

  if (ltpCTP) {
    status |= ltpCTP->COUNTER_read_lsw(&l1);
    status |= ltpCTP->COUNTER_read_msw(&m1);
    c1 = m1*0x10000 + l1;
    if (print) os << "  LTP slave CTP counter value:......................" << c1 << endl;
  }

  if (ltpLTP) {
    status |= ltpLTP->COUNTER_read_lsw(&l2);
    status |= ltpLTP->COUNTER_read_msw(&m2);
    c2 = m2*0x10000 + l2;
    if (print) os << "  LTP slave LTP counter value:......................" << c2 << endl;
  }

  if (ltpSenderCTP) {
    status |= ltpSenderCTP->COUNTER_read_lsw(&l);
    status |= ltpSenderCTP->COUNTER_read_msw(&m);
    f1 = m*0x10000 + l;
    if (print) os << "  LTP master counter value:........................." << f1 << endl;
  }

  if (ltpSenderLTP) {
    status |= ltpSenderLTP->COUNTER_read_lsw(&l);
    status |= ltpSenderLTP->COUNTER_read_msw(&m);
    f2 = m*0x10000 + l;
    if (print) os << "  LTP master counter value:........................." << f2 << endl;
  }

  if (((c1 != f1) || (c2 != f2)) && (f1 != 0 && f2 != 0)) {
    status = 999;
    os << "!!! ERROR !!! <<Counters do not match>>" << endl;; 
  }

  if (c1 != f1 && f1 != 0)      channelResultCTP.push_back(1);
  else if (c1 == f1 && f1 != 0) channelResultCTP.push_back(0);
  else if (c1 == f1 && f1 == 0) channelResultCTP.push_back(2);

  if (c2 != f2 && f2 != 0)       channelResultLTP.push_back(1);
  else if (c2 == f2 && f2 != 0)  channelResultLTP.push_back(0);
  else if (c2 == f2 && f2 == 0)  channelResultLTP.push_back(2);
  
  if (f1 == 0 || f2 == 0) {
    status = 666;
    os << "!!! ERROR !!! <<Zero signal sent>>" << endl;
    cout << "!!! ERROR !!! <<Zero signal sent>>" << endl;
  }
  logg << "print_counters: <<exit code = " << status << ">>" <<endl;
  return(status);
}

//-------------------------------------------
void wait(double sec=0.1, long nsec=0) {
  timespec wait_time;
  wait_time.tv_sec = static_cast<time_t>(sec);
  wait_time.tv_nsec = nsec;
  nanosleep(&wait_time, nullptr);
}

int LtpCounterSource(LTP* ltp, ltp_signal_channel chan)
{
  switch(chan) {
  case ORB: return ltp->COUNTER_orb();
  case L1A: return ltp->COUNTER_L1A();
  case TR1: return ltp->COUNTER_TR1();
  case TR2: return ltp->COUNTER_TR2();
  case TR3: return ltp->COUNTER_TR3();
  case BG0: return ltp->COUNTER_BG0();
  case BG1: return ltp->COUNTER_BG1();
  case BG2: return ltp->COUNTER_BG2();
  case BG3: return ltp->COUNTER_BG3();
  default: return(-1);
  }
}
