//******************************************************************************
// file: testLtpi.cc
// desc: test program for LTPi
//******************************************************************************

#include <fstream>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <list>
#include <errno.h>
#include <stdint.h>

#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDVme.h"
#include "DFDebug/DFDebug.h"

#include "RCDLtpi/RCDLtpi.h"
#include "RCDLtp/RCDLtp.h"

using namespace RCD;

// global variable
LTPi* ltpi;
LTP*  ltp_masterLTP;
LTP*  ltp_masterCTP;
LTP*  ltp_slave;
u_int base_ltpMasterCTP = 0xff1000;
u_int base_ltpMasterLTP = 0xff2000;
u_int base_ltpSlave     = 0xff5000;
u_int base_ltpi         = 0xee3000;

std::string pgfilename;

class LTPOpen : public MenuItem {
public:
  LTPOpen() { setName("Open Boards"); }
  int action() {
    int status = 0;
    pgfilename = "pg11.dat";
    std::string in;

    int chadd = 1;
    while (chadd != 0) {
      printf("base address for the ltp acting as master CTP: 0x%06x\n", base_ltpMasterCTP);
      printf("base address for the ltp acting as master LTP: 0x%06x\n", base_ltpMasterLTP);
      printf("base address for the ltp acting as slave:      0x%06x\n", base_ltpSlave);
      printf("base address for the LTPi:                     0x%06x\n", base_ltpi);
      chadd = enterInt("If you need to change base address of a module input: 0-Continue, 1-LTPi, 2-master CTP, 3-master LTP, 4-slave", 0, 4);
      switch(chadd) {
        case 1:
          base_ltpi = enterHex("base address for the LTPi <base>?", 0, 0xffff00);
          break;
        case 2:
          base_ltpMasterCTP = enterHex("base address for the ltp acting as master CTP <base>?", 0, 0xffff00);
          break;
        case 3:
          base_ltpMasterLTP = enterHex("base address for the ltp acting as master LTP <base>?", 0, 0xffff00);
          break;
        case 4:
          base_ltpSlave = enterHex("base address for the ltp acting as slave LTP <base>?", 0, 0xffff00);
          break;
      }
    }

    ltpi = new LTPi();
    status |= ltpi->Open(base_ltpi);
    if (status != 0) printf("Error opening the board. \n Make sure that the address of the LTPi is: 0x%06x\n", base_ltpi);

    ltp_masterCTP = new LTP();
    status |= ltp_masterCTP->Open(base_ltpMasterCTP);
    if (status != 0) printf("Error opening the board. \n Make sure that the address of the LTP connected to the CTP-link-in is: 0x%06x\n", base_ltpMasterCTP);

    ltp_masterLTP = new LTP();
    status |= ltp_masterLTP->Open(base_ltpMasterLTP);
    if (status != 0) printf("Error opening the board. \n Make sure that the address of the LTP connected to the LTP-link-in is:  0x%06x\n", base_ltpMasterLTP);

    ltp_slave = new LTP();
    status |= ltp_slave->Open(base_ltpSlave);
    if (status != 0) printf("Error opening the board. \n Make sure that the address of the slave LTP is: 0x%06x\n", base_ltpSlave);

    printf("\n");
    printf("\n");
    printf("********************************************** \n");
    printf("************ LTPi TEST PROGRAM  ************** \n");
    printf("********************************************** \n");
    printf("\n");
    printf("\n");
    printf("3 LTP and 1 LTPi are used. \n");
    printf("LTPs: \n");
    printf("- LTP (address: 0x%06x) acting as a master connected to the CTP-link-in of the LTPi \n", base_ltpMasterCTP);
    printf("  and the NIM-in front pannel of the LTPi \n");
    printf("- LTP (address: 0x%06x) acting as a master connected to the LTP-link-in of the LTPi \n", base_ltpMasterLTP);
    printf("- LTP (address: 0x%06x) acting as a slave connected either: \n", base_ltpSlave);
    printf("........................................................ to the LTP-link-out LTPi (to test LTP-link-out) \n");
    printf("........................................................ or to the CTP-link-out  LTPi (to test CTP-link-out) \n");
    printf("\n");
    printf("\n");
    printf("\n");

    return(status);
  }
};

//-----------------------------------------------------------------------------
class connectLTP2CTPlinout : public MenuItem {
public:
  connectLTP2CTPlinout() { setName("Connection to test CTP-link-out"); }
  int action()
  {
    printf("Connect the slave LTP (0x%06x) to the LTPi (0x%06x) via a link cable. \n", base_ltpSlave, base_ltpi);
    printf("LTPi CTP-link-out to LTP CTP-link-in \n");
    int dummy = enterInt("Once done enter 0", 0, 0);
    return(dummy);
  }
};

//-----------------------------------------------------------------------------
class connectLTP2LTPlinout : public MenuItem {
public:
  connectLTP2LTPlinout() { setName("Connection to test LTP-link-out"); }
  int action() {
    printf("Connect the slave LTP (0x%06x) to the LTPi (0x%06x) via a link cable. \n", base_ltpSlave, base_ltpi);
    printf("LTPi LTP-link-out to LTP CTP-link-in \n");
    int dummy = enterInt("Once done enter 0", 0, 0);
    return(dummy);

  }
};

//-----------------------------------------------------------------------------
class confLTPs : public MenuItem {
public:
  confLTPs() { setName("Configure LTPs"); }
  int action() {
    int status = 0;

    // LTP masterCTP ///////////////////////////////
    // set pg to start with vme command ////////////
    //status |= ltp_masterCTP->PG_load_memory_from_file(&pgfilename);
    status |= ltp_masterCTP->OM_master_patgen(pgfilename);
    status |= ltp_masterCTP->PG_runmode_cont();
    status |= ltp_masterCTP->ORB_local_pg();
    status |= ltp_masterCTP->PG_start();
    ///////////////////////////////////////////////

    // LTP master LTP //////////////////////////////
    //status |= ltp_masterLTP->PG_load_memory_from_file(&pgfilename);
    status |= ltp_masterLTP->OM_master_patgen(pgfilename);
    status |= ltp_masterLTP->PG_runmode_cont();
    status |= ltp_masterLTP->ORB_local_pg();
    status |= ltp_masterLTP->PG_start();
    ///////////////////////////////////////////////

    // LTP slave //////////////////////////////////
    status |= ltp_slave->OM_slave(); // make it a slave
    ///////////////////////////////////////////////

    return(status);
  }
};

//-----------------------------------------------------------------------------
class confLTPs4NIMout : public MenuItem {
public:
  confLTPs4NIMout() { setName("Configure LTPs"); }
  int action() {
    int status = 0;

    // LTP masterCTP ///////////////////////////////
    // set pg to start with vme command ////////////
    //status |= ltp_masterCTP->PG_load_memory_from_file(&pgfilename);
    status |= ltp_masterCTP->OM_master_patgen(pgfilename);
    status |= ltp_masterCTP->PG_runmode_cont();
    status |= ltp_masterCTP->ORB_local_pg();
    status |= ltp_masterCTP->PG_start();
    ///////////////////////////////////////////////

    // LTP master LTP //////////////////////////////
    //status |= ltp_masterLTP->PG_load_memory_from_file(&pgfilename);
    status |= ltp_masterLTP->OM_master_patgen(pgfilename);
    status |= ltp_masterLTP->PG_runmode_cont();
    status |= ltp_masterLTP->ORB_local_pg();
    status |= ltp_masterLTP->PG_start();
    ///////////////////////////////////////////////

    // LTP slave //////////////////////////////////
    status |= ltp_slave->OM_master_lemo(); // make it a slave
    status |= ltp_slave->BC_local_lemo();
    status |= ltp_slave->ORB_local_lemo();
    ///////////////////////////////////////////////

    return(status);
  }
};

//-----------------------------------------------------------------------------
class confLTPsTRT : public MenuItem {
public:
  confLTPsTRT() { setName("Configure LTPs for Trigger types"); }
  int action() {
    int status = 0;
    pgfilename = "pg1.dat";
    // LTP masterCTP ///////////////////////////////
    // set pg to start with vme command ////////////
    status |= ltp_masterCTP->OM_master_patgen(pgfilename);
    status |= ltp_masterCTP->PG_runmode_cont();
    status |= ltp_masterCTP->ORB_local_pg();
    status |= ltp_masterCTP->PG_start();
    ///////////////////////////////////////////////
    //pgfilename = "pg3.dat";
    pgfilename = "pg1.dat";
    // LTP master LTP //////////////////////////////
    status |= ltp_masterLTP->OM_master_patgen(pgfilename);
    status |= ltp_masterLTP->PG_runmode_cont();
    status |= ltp_masterLTP->ORB_local_pg();
    status |= ltp_masterLTP->PG_start();
    ///////////////////////////////////////////////

    // LTP slave //////////////////////////////////
    status |= ltp_slave->OM_slave(); // make it a slave
    status |= ltp_slave->BUSY_local_with_linkin(); // enable busy from link in
    ///////////////////////////////////////////////

    printf("\n");
    printf("\n");
    printf("**** Make sure the front pannel leds are on for the LTP master (0x%06x) and \n", base_ltpMasterLTP);
    printf("**** for the LTP slave (0x%06x) \n", base_ltpSlave);

    return(status);
  }
};

//-----------------------------------------------------------------------------
class confLTPiCTP : public MenuItem {
public:
  confLTPiCTP() {setName("Configure LTPi such that CTP-link-in ==> CTP-link-out & LTP-link-out"); }
  int action() {
    int status = 0;

    status |= ltpi->BC_CTPoutFromCTPin_LTPoutFromCTPin();
    status |= ltpi->Orbit_CTPoutFromCTPin_LTPoutFromCTPin();
    status |= ltpi->L1A_CTPoutFromCTPin_LTPoutFromCTPin();
    status |= ltpi->TTR_CTPoutFromCTPin_LTPoutFromCTPin();
    status |= ltpi->BGO_CTPoutFromCTPin_LTPoutFromCTPin();
    status |= ltpi->TRT_CTPoutFromCTPin_LTPoutFromCTPin();

    return status;
  }
};

//-----------------------------------------------------------------------------
class confLTPiCTPLTP : public MenuItem {
public:
  confLTPiCTPLTP() {setName("Configure LTPi such that CTP-link-in ==> CTP-link-out & LTP-link-in ==> LTP-link-out"); }
  int action() {
    int status = 0;

    status |= ltpi->BC_CTPoutFromCTPin_LTPoutFromLTPin();
    status |= ltpi->Orbit_CTPoutFromCTPin_LTPoutFromLTPin();
    status |= ltpi->L1A_CTPoutFromCTPin_LTPoutFromLTPin();
    status |= ltpi->TTR_CTPoutFromCTPin_LTPoutFromLTPin();
    status |= ltpi->BGO_CTPoutFromCTPin_LTPoutFromLTPin();
    status |= ltpi->TRT_CTPoutFromCTPin_LTPoutFromLTPin();

    return status;
  }
};

//-----------------------------------------------------------------------------
class confLTPiLTP : public MenuItem {
public:
  confLTPiLTP() {setName("Configure LTPi such that LTP-link-in ==> CTP-link-out & LTP-link-out"); }
  int action() {
    int status = 0;

    status |= ltpi->BC_CTPoutFromLTPin_LTPoutFromLTPin();
    status |= ltpi->Orbit_CTPoutFromLTPin_LTPoutFromLTPin();
    status |= ltpi->L1A_CTPoutFromLTPin_LTPoutFromLTPin();
    status |= ltpi->TTR_CTPoutFromLTPin_LTPoutFromLTPin();
    status |= ltpi->BGO_CTPoutFromLTPin_LTPoutFromLTPin();
    status |= ltpi->TRT_CTPoutFromLTPin_LTPoutFromLTPin();

    return status;
  }
};

//-----------------------------------------------------------------------------
class confLTPiNIM : public MenuItem {
public:
  confLTPiNIM() {setName("Configure LTPi such that NIM-in ==> CTP-link-out & LTP-link-out"); }
  int action() {
    int status = 0;

    status |= ltpi->BC_CTPoutFromNIMin_LTPoutFromNIMin();
    status |= ltpi->Orbit_CTPoutFromNIMin_LTPoutFromNIMin();
    status |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromNIMin();
    status |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromNIMin();
    status |= ltpi->BGO_CTPoutFromNIMin_LTPoutFromNIMin();
    status |= ltpi->TRT_CTPoutFromLocal_LTPoutFromLocal();

    return status;
  }
};

//-----------------------------------------------------------------------------
class confLTPiNIMLTP : public MenuItem {
public:
  confLTPiNIMLTP() {setName("Configure LTPi such that NIM-in ==> CTP-link-out & LTP-link-in ==> LTP-link-out"); }
  int action() {
    int status = 0;

    status |= ltpi->BC_CTPoutFromNIMin_LTPoutFromLTPin();
    status |= ltpi->Orbit_CTPoutFromNIMin_LTPoutFromLTPin();
    status |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromLTPin();
    status |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromLTPin();
    status |= ltpi->BGO_CTPoutFromNIMin_LTPoutFromLTPin();
    status |= ltpi->TRT_CTPoutFromLocal_LTPoutFromLTPin();

    return status;
  }
};

//-----------------------------------------------------------------------------
class startTestTRT : public MenuItem {
// the underlying assumption in this test is that the pattern is
// sending a LV1A (to trigger the receiving FIFO) followed by one
// combination of testtrigger 1 to 3. This sequence is repeted once
// for each test trigger 1-3 combination in the order 000 to 111.
public:
  startTestTRT() { setName("Start test"); }
  int action() {
    u_short w;
    u_short TRTvalue = 0;

    std::list<int> fifo;
    int status = 0;
    printf(" \n");
    printf(" \n");
    printf(" ******* start testing channels \n");
    status |= ltp_masterCTP->PG_runmode_singleshot_vme();
    status |= ltp_masterLTP->PG_runmode_singleshot_vme();
    status |= ltp_slave->TTYPE_reset_fifo();
    if (!ltp_slave->TTYPE_fifo_is_empty()) printf("Error fifo not empty! \n");
    status |= ltp_slave->TTYPE_fifotrigger_L1A();
    printf("FiFo triggered by L1A \n");
    status |= ltp_masterCTP->PG_start();
    status |= ltp_masterLTP->PG_start();
    if (ltp_slave->TTYPE_fifo_is_empty()) printf("Error fifo empty! \n");
    while (!ltp_slave->TTYPE_fifo_is_empty()) {
      ltp_slave->TTYPE_read_fifo(&w);
      status |= ltpi->GetTRT000(TRTvalue);
      cout << "fifo = " << w << endl;
      cout << "LTP = " <<TRTvalue << endl;
      if (w!=TRTvalue) {
        std::cout << "<<TEST FAILED>> received trigger type differs from the one set on the LTPi" << std::endl;
        status |=999;
      }
      fifo.push_back(w);
    }
    for (std::list<int>::const_iterator it = fifo.begin();
      it != fifo.end(); ++it) {
      std::printf(" 0x%02x\n",*it);
    }
    std::printf("Fifo contained %lu elements\n",fifo.size());
    std::printf("\n");

    return(status);
  }
};

//-----------------------------------------------------------------------------
class startTestCTP : public MenuItem {
public:
  startTestCTP() { setName("Start test"); }
  int action() {
    int status = 0;
    int pmax = 0;
    printf(" \n");
    printf(" \n");
    printf(" ******* start testing channels \n");
    status |= ltp_masterCTP->PG_runmode_singleshot_vme();
    pmax = enterInt("Enter the number of times you want to send the pattern:", 1, 1000);
    for (int i=0; i<pmax; ++i) {
    printf(" ....... L1A  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_L1A();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_L1A();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();

    printf(" ....... Test Trigger 1  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_TR1();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_TR1();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();
    printf(" ....... Test Trigger 2  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_TR2();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_TR2();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();
    printf(" ....... Test Trigger 3  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_TR3();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_TR3();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();
    printf(" ....... ORBIT  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_orb();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_orb();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();
    printf(" ....... BGO 0  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_BG0();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_BG0();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();
    printf(" ....... BGO 1  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_BG1();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_BG1();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();
    printf(" ....... BGO 2  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_BG2();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_BG2();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();
    printf(" ....... BGO 3  \n");
    status |= ltp_masterCTP->COUNTER_reset();
    status |= ltp_masterCTP->COUNTER_BG3();
    status |= ltp_slave->COUNTER_reset();
    status |= ltp_slave->COUNTER_BG3();
    status |= ltp_masterCTP->PG_start();
    while(!ltp_masterCTP->PG_is_idle()) {};
    status |= printCounters();

    if(status==999) printf("!!! TEST FAILED !!! for some channel there is a mismatch between signals sent and received. Check the list above. \n");
    else printf("!!! TEST PASSED !!!\n");
    }
    return status;
  }

private:
  int printCounters() {
    int status = 0;
    u_short m,l;
    u_int c;
    u_int f;
    wait(0.01,0);

    status |= ltp_slave->COUNTER_read_lsw(&l);
    status |= ltp_slave->COUNTER_read_msw(&m);
    c = m*0x10000 + l;
    std::printf("  LTP slave counter value:...................... %d \n",c);
    status |= ltp_masterCTP->COUNTER_read_lsw(&l);
    status |= ltp_masterCTP->COUNTER_read_msw(&m);
    f = m*0x10000 + l;
    std::printf("  LTP master CTP counter value:................. %d\n",f);
    std::printf("\n");

    if (c != f) {
      status = 999;
      printf("!!! ERROR !!! <<Counters do not match>> \n");
    }

    if (f == 0) {
      status = 666;
      printf("!!! ERROR !!! <<Zero signal sent>> \n");
    }
    return(status);
  }
  void wait(double sec=0.1, double nsec=0) {
     timespec wait_time;
     wait_time.tv_sec = sec;
     wait_time.tv_nsec = nsec;
     nanosleep(&wait_time, nullptr);
  }
};

//-----------------------------------------------------------------------------
class startTestLTP : public MenuItem {
public:
  startTestLTP() { setName("Start test"); }
  int action() {
    int status = 0;

    printf(" \n");
    printf(" \n");
    printf(" ******* start testing channels \n");
    status |= ltp_masterLTP->PG_runmode_singleshot_vme();

    int pmax = 1;
    pmax = enterInt("Enter the number of times you want to send the pattern:", 1, 1000);
    for (int i=0; i<pmax; ++i) {

      printf(" ....... L1A  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_L1A();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_L1A();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... Test Trigger 1  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_TR1();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_TR1();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... Test Trigger 2  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_TR2();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_TR2();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... Test Trigger 3  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_TR3();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_TR3();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... ORBIT  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_orb();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_orb();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... BGO 0  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_BG0();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_BG0();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... BGO 1  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_BG1();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_BG1();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... BGO 2  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_BG2();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_BG2();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();

      printf(" ....... BGO 3  \n");
      status |= ltp_masterLTP->COUNTER_reset();
      status |= ltp_masterLTP->COUNTER_BG3();
      status |= ltp_slave->COUNTER_reset();
      status |= ltp_slave->COUNTER_BG3();
      status |= ltp_masterLTP->PG_start();
      while(!ltp_masterLTP->PG_is_idle()) {};
      status |= printCounters();
      if      (status==999) printf("!!! TEST FAILED !!! for some channel there is a mismatch between signals sent and received. Check the list above. \n");
      else if (status==666) printf("!!! TEST FAILED !!! for some channel there were no signals sent. Check the list above. \n");
      else if (status==0) printf("!!! TEST PASSED !!!\n");
      return status;
    }

    if      (status==999) printf("!!! TEST FAILED !!! for some channel there is a mismatch between signals sent and received. Check the list above. \n");
    else if (status==666) printf("!!! TEST FAILED !!! for some channel there were no signals sent. Check the list above. \n");
    else if (status==0) printf("!!! TEST PASSED !!!\n");
    return status;
  }

private:
  int printCounters() {
    int status = 0;
    u_short m,l;
    u_int c;
    u_int f;
    wait(0.01,0);

    status |= ltp_slave->COUNTER_read_lsw(&l);
    status |= ltp_slave->COUNTER_read_msw(&m);
    c = m*0x10000 + l;
    std::printf("  LTP slave counter value:...................... %d \n",c);
    status |= ltp_masterLTP->COUNTER_read_lsw(&l);
    status |= ltp_masterLTP->COUNTER_read_msw(&m);
    f = m*0x10000 + l;
    std::printf("  LTP master CTP counter value:................. %d\n",f);
    std::printf("\n");

    if (c != f) {
      status = 999;
      printf("!!! ERROR !!! <<Counters do not match>> \n");
    }
    return(status);
  }
  void wait(double sec=0.1, double nsec=0) {
     timespec wait_time;
     wait_time.tv_sec = sec;
     wait_time.tv_nsec = nsec;
     nanosleep(&wait_time, nullptr);
  }
};

//------------------------------------------------------------------------------
class LTPClose : public MenuItem {
public:
  LTPClose() { setName("Close LTP"); }
  int action() {
    int status = ltpi->Close();
    delete ltpi;

    return(status);
  }
};

//-----------------------------------------------------------------------------
class reset : public MenuItem {
public:
  reset() { setName("Software reset"); }
  int action() {
    int ierr = 0;

    printf("\n");
    std::cout << ">>>>>>>>>>>>>>>>> software reset the module" << std::endl;
    printf("\n");
    ierr |= ltpi->Reset();
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class initialize : public MenuItem {
public:
  initialize() { setName("Initialize i2c"); }
  int action() {
    int ierr = 0;

    printf("\n");
    std::cout << ">>>>>>>>>>>>>>>>> set up i2c" << std::endl;
    printf("\n");
    ierr |= ltpi->setupI2C();
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class initializeDAC : public MenuItem {
public:
  initializeDAC() { setName("Initialize DAC"); }
  int action() {
    int ierr = 0;

    printf("\n");
    std::cout << ">>>>>>>>>>>>>>>>> set up equalization & gain to default" << std::endl;
    int sel = enterInt("0-QUIT, 1-short cable, 2-long cable", 0, 2);
    printf("\n");
    if (sel==1) ierr |= ltpi->setupDAC4shortCable();
    if (sel==2) ierr |= ltpi->setupDAC4longCable();
      ierr |= ltpi->printDACsettings();
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class confLTPiTRT : public MenuItem {
public:
  confLTPiTRT() { setName("Configure LTPi for trigger types"); }
  int action() {
    int ierr = 0;
    ierr = ltpi->TRT_CTPoutFromLocal_LTPoutFromLocal();
    printf("Select the test tigger input to derive the trigger types \n");
    int sel = enterInt("0-QUIT, 1-internal-register, 2-CTPLink, 3-LTPlink, 4-NIM", 0, 4);
    printf("make sure that the input selected corresponds to the input used....\n");
    switch (sel) {
    case 0: break;
    case 1: {
      ierr |= ltpi->TRT_FROM_LOCAL_REGISTER();
      int val = enterHex("Trigger Type word (0xff)", 0, 0xff);
      u_short value = static_cast<u_short>(val);
      ierr = ltpi->SetTRT000(value);
      ierr = ltpi->GetTRT000(value);
      std::printf("TRT000 (init value) = 0x%02x", value);
      break;
    }
    case 2: {
      ierr |= ltpi->BC_CTPoutFromCTPin_LTPoutFromCTPin();
      ierr |= ltpi->Orbit_CTPoutFromCTPin_LTPoutFromCTPin();
      ierr |= ltpi->L1A_CTPoutFromCTPin_LTPoutFromCTPin();
      ierr |= ltpi->TTR_CTPoutFromCTPin_LTPoutFromCTPin();
      ierr |= ltpi->TRT_DERIVED_FROM_CTP_TTR();
      break;
    }
    case 3: {
      ierr |= ltpi->BC_CTPoutFromLTPin_LTPoutFromLTPin();
      ierr |= ltpi->Orbit_CTPoutFromLTPin_LTPoutFromLTPin();
      ierr |= ltpi->L1A_CTPoutFromLTPin_LTPoutFromLTPin();
      ierr |= ltpi->TTR_CTPoutFromLTPin_LTPoutFromLTPin();
      ierr |= ltpi->TRT_DERIVED_FROM_LTP_TTR();
      break;
    }
    case 4: {
      ierr |= ltpi->BC_CTPoutFromNIMin_LTPoutFromNIMin();
      ierr |= ltpi->Orbit_CTPoutFromNIMin_LTPoutFromNIMin();
      ierr |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromNIMin();
      ierr |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromNIMin();
      ierr |= ltpi->TRT_DERIVED_FROM_NIM_TTR();
      break;
    }
    }

    int value = 0;
    value = 0x00;
    ierr |= ltpi->SetTRT000(value);
    value = 0x01;
    ierr |= ltpi->SetTRT001(value);
    value = 0x02;
    ierr |= ltpi->SetTRT010(value);
    value = 0x04;
    ierr |= ltpi->SetTRT011(value);
    value = 0x08;
    ierr |= ltpi->SetTRT100(value);
    value = 0x10;
    ierr |= ltpi->SetTRT101(value);
    value = 0x20;
    ierr |= ltpi->SetTRT110(value);
    value = 0x40;
    ierr |= ltpi->SetTRT111(value);

    // print trigger type table
    ierr = ltpi->dumpTRTTable();

    return(ierr);
  }
};

class testCal : public MenuItem {
public:
  testCal() { setName("Test calibration request path"); }
  int action() {
    int status = 0;
    printf("\n");
    printf("select 1. You should connect the slave LTP (0x%06x) to the CTP-link out of the LTPi \n", base_ltpSlave);
    printf("          Check the calibration test pad for the LTP slave (sender) and the LTP (ff1000) & (ff3000), pin by pin  they should be the same\n");
    printf("\n");
    printf("select 2. You should connect the slave LTP (0x%06x) to the LTP-link out of the LTPi \n", base_ltpSlave);
    printf("          Check the calibration test pad for the LTP slave (sender) and the LTP (ff1000) & (ff3000), pin by pin  they should be the same\n");
    printf("\n");
    printf("select 3. You should connect the slave LTP (0x%06x) to the CTP-link out of the LTPi \n", base_ltpSlave);
    printf("          Check the calibration test pad for the LTP slave (sender) and the LTP (ff1000), pin by pin  they should be the same\n. Note that the LTP (ff3000) should not recieve signals\n");
    printf("          You should connect the slave LTP (0x%06x) to the LTP-link out of the LTPi \n", base_ltpSlave);
    printf("          Check the calibration test pad for the LTP slave (sender) and the LTP (ff3000),  pin by pin  they should be the same\n. Note that the LTP (ff1000) should not receive signals\n");

    int sel = enterInt("Select Calibration Request Path: 0-QUIT, 1-CTPout, 2-LTPout, 3-CTPout-LTPout",0, 3);
    switch (sel)
      {
      case 0: break;
      case 1: ltpi->calCTPfromCTP_LTPFromCTP(); break;
      case 2: ltpi->calCTPfromLTP_LTPFromLTP(); break;
      case 3: ltpi->calCTPfromCTP_LTPFromLTP(); break;
      }
    return status;
  }
};

class testAutoCal : public MenuItem {
public:
  testAutoCal() { setName("Automatized test of Calibration requests"); }
  int action() {
    int status = 0;
    status |= ltp_slave->CAL_source_preset(); // set calibration request from preset
    status |= ltp_slave->CAL_set_preset_value(0);

    status |= ltp_slave->CAL_enable_testpath(); // enable test pad
    status |= ltp_masterLTP->CAL_enable_testpath();
    status |= ltp_masterCTP->CAL_enable_testpath();

    status |= ltp_masterCTP->CAL_source_link(); // read calibration from link
    status |= ltp_masterLTP->CAL_source_link();

    // to be finished !!!!!!!!!!!!!!!!!!!!!
    u_short w;
    int r1 = 0; int r2 = 0; int r3 = 0;
    int ierr = 0;

    printf("Starting testing the calibration request path:");
    printf("1 ---- checking CTP-link-to-CTP-link & CTP-link-to-LTP-link path");
    for(int i =0; i<8; ++i) {
      w = u_short(i);
      status |= ltp_slave->CAL_set_preset_value(w);
      ltpi->calCTPfromCTP_LTPFromCTP(); // first check the ctp connection
      ltp_slave->CAL_get_linkstatus(&r1);
      ltp_masterCTP->CAL_get_linkstatus(&r2);
      ltp_masterLTP->CAL_get_linkstatus(&r3);
      if (r1 != r2) ierr |= 606;
      if (r1 != r3) ierr |= 609;
      cout << " sender       receiver(CTP)" << endl;
      cout << "  r1 = " << r1 << ";        r2 = " << r2 << endl;
      cout << " sender       receiver(LTP)" << endl;
      cout << "  r1 = " << r1 << ";        r3 = " << r3 << endl;
    }

    printf("Starting testing the calibration request path:");
    printf("1 ---- checking LTP-link-to-CTP-link & LTP-link-to-LTP-link path");
    for(int i =0; i<8; ++i) {
      w = u_short(i);
      status |= ltp_slave->CAL_set_preset_value(w);
      ltpi->calCTPfromLTP_LTPFromLTP(); // then check the ltp connection
      ltp_slave->CAL_get_linkstatus(&r1);
      ltp_masterCTP->CAL_get_linkstatus(&r2);
      ltp_masterLTP->CAL_get_linkstatus(&r3);
      if (r1 != r2) ierr |= 906;
      if (r1 != r3) ierr |= 909;
      cout << " sender       receiver(CTP)" << endl;
      cout << "  r1 = " << r1 << ";        r2 = " << r2 << endl;
      cout << " sender       receiver(LTP)" << endl;
      cout << "  r1 = " << r1 << ";        r3 = " << r3 << endl;
    }

    if ((ierr & 606)==606) cout << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi CTP-link-out and the LTP acting as a CTP must be connected to the LTPi CTP-link-in" << endl;
    else if ((ierr & 609)==609) cout << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi CTP-link-out and the LTP acting as a LTP-master must be connected to the LTPi LTP-link-in" << endl;
    else if ((ierr & 906)==906) cout << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi LTP-link-out and the LTP acting as a CTP must be connected to the LTPi CTP-link-in" << endl;
    else if ((ierr & 909)==909) cout << "    <<TEST FAILED>>. check connections: LTP slave link-in must be connetct to LTPi LTP-link-out and the LTP acting as a LTP-master must be connected to the LTPi LTP-link-in" << endl;
    else if (ierr == 0) cout << "    <<TEST PASSED>>" << endl;

    return status;
  }
};

class confCal : public MenuItem {
public:
  confCal() { setName("Configure LTPs for Calibration request test"); }
  int action() {
    int status = 0;
    pgfilename = "pg1.dat";
    status |= ltp_slave->OM_master_patgen(pgfilename);
    status |= ltp_slave->PG_runmode_cont();
    status |= ltp_slave->ORB_local_pg();
    status |= ltp_slave->PG_start();
    status |= ltp_slave->CAL_source_pg(); // read calibration request from PG

    status |= ltp_slave->CAL_enable_testpath(); // enable test pad
    status |= ltp_masterLTP->CAL_enable_testpath();
    status |= ltp_masterCTP->CAL_enable_testpath();

    status |= ltp_masterCTP->CAL_source_link(); // read calibration from link
    status |= ltp_masterLTP->CAL_source_link();

    return status;
  }
};

class confBusy : public MenuItem {
public:
  confBusy() { setName("Configure LTPs' Busy"); }
  int action() {
    int status = 0;
    printf("Making the LTP slave (0x%06x) busy", base_ltpSlave);
    status |= ltp_slave->BUSY_enable_constant_level(); // assert a constant busy level
    status |= ltp_slave->BUSY_linkout_from_local(); // enable busy NIM
    status |= ltp_masterCTP->BUSY_local_with_linkin(); // enable busy from link in
    status |= ltp_masterLTP->BUSY_local_with_linkin(); // enable busy from link in
    return status;
  }
};

class busy2Ctp : public MenuItem {
public:
  busy2Ctp() { setName("Redirect busy signal to the CTP-link-in"); }
  int action() {
    int status = 0;
printf(" 1. First turn the busy off on the CTP-link-in: select 0 and make sure that the LTP (0x%06x) is not busy. \n 2. Connect the LTP salve (0x%06x) to the LTPi LTP-link-out, select 1 and make sure the LTP (0x%06x) gets busy. \n 3. Connect the LTP salve (0x%06x) to the LTPi CTP-link-out, select 2 and make sure the LTP (0x%06x) gets busy.). \n 4. Select 3 and make sure that connecting the slave to the LTP-link-out or the CTP-link-out the LTP (0x%06x) is busy in both the cases.\n", base_ltpMasterCTP, base_ltpSlave, base_ltpMasterCTP, base_ltpSlave,  base_ltpMasterCTP,  base_ltpMasterCTP);
printf("\n");
    int sel = enterInt("Select Busy input channel: 0-OFF, 1-LTP, 2-CTP, 3-LTP&CTP", 0, 3);
    switch (sel)
      {
      case 0: ltpi->busyCTP_OFF(); break;
      case 1: ltpi->busyCTP_FROM_LTP(); break;
      case 2: ltpi->busyCTP_FROM_CTP(); break;
      case 3: ltpi->busyCTP_FROM_CTP_AND_LTP(); break;
      }
    return status;
  }
};

class busy2Ltp : public MenuItem {
public:
  busy2Ltp() { setName("Redirect busy signal to the LTP-link-in"); }
  int action() {
    int status = 0;
    printf(" 1. First turn the busy off on the LTP-link-in: select 0 and make sure that the LTP (0x%06x) is not busy. \n 2. Connect the LTP salve (0x%06x) to the LTPi LTP-link-out, select 1 and make sure the LTP (0x%06x) gets busy. \n 3. Connect the LTP salve (0x%06x) to the LTPi CTP-link-out, select 2 and make sure the LTP (0x%06x) gets busy.). \n 4. Select 3 and make sure that connecting the slave to the LTP-link-out or the CTP-link-out the LTP (0x%06x) is busy in both the cases.\n", base_ltpMasterLTP, base_ltpSlave, base_ltpMasterLTP, base_ltpSlave,  base_ltpMasterLTP,  base_ltpMasterLTP);
    printf("\n");
    int sel = enterInt("Select Busy input channel: 0-OFF, 1-LTP, 2-CTP, 3-LTP&CTP", 0, 3);
    switch (sel)
      {
      case 0: ltpi->busyLTP_OFF(); break;
      case 1: ltpi->busyLTP_FROM_LTP(); break;
      case 2: ltpi->busyLTP_FROM_CTP(); break;
      case 3: ltpi->busyLTP_FROM_CTP_AND_LTP(); break;
      }
    return status;
  }
};

class busy2Nim : public MenuItem {
public:
  busy2Nim() { setName("Redirect busy signal to the Nim"); }
  int action() {
    int status = 0;
    printf(" Connect the LTP (0x%06x) busy-NIM-in to the LTPi busy-NIM-out. \n 1. First turn the busy off on  NIM: select 0 and make sure that the LTP (0x%06x) is not busy. \n 2. Connect the LTP salve (0x%06x) to the LTPi LTP-link-out, select 1 and make sure the LTP (0x%06x) gets busy. \n 3. Connect the LTP salve (0x%06x) to the LTPi CTP-link-out, select 2 and make sure the LTP (0x%06x) gets busy.). \n 4. Select 3 and make sure that connecting the slave to the LTP-link-out or the CTP-link-out the LTP (0x%06x) is busy in both the cases.", base_ltpMasterLTP, base_ltpMasterLTP, base_ltpSlave, base_ltpMasterLTP, base_ltpSlave,  base_ltpMasterLTP,  base_ltpMasterLTP);
    int sel = enterInt("Select Busy input channel: 0-OFF, 1-LTP, 2-CTP, 3-LTP&CTP", 0, 3);
    switch (sel)
      {
      case 0: ltpi->busyNIM_OFF(); break;
      case 1: ltpi->busyNIM_FROM_LTP(); break;
      case 2: ltpi->busyNIM_FROM_CTP(); break;
      case 3: ltpi->busyNIM_FROM_CTP_AND_LTP(); break;
      }
    return status;
  }
};

//------------------------------------------------------------------------------
class FillAllTRTTable : public MenuItem {
public:
  FillAllTRTTable() { setName("Fill All Entries in Trigger Type Table"); }
  int action() {
    int ierr = 0;

    int value = 0;
    std::printf("Enter Trigger Type sequentially \n");

    value = enterHex("Trigger Type word (0xff) for Test Trigger 000", 0, 0xff);
    ierr = ltpi->SetTRT000(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 001", 0, 0xff);
    ierr = ltpi->SetTRT001(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 010", 0, 0xff);
    ierr = ltpi->SetTRT010(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 011", 0, 0xff);
    ierr = ltpi->SetTRT011(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 100", 0, 0xff);
    ierr = ltpi->SetTRT100(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 101", 0, 0xff);
    ierr = ltpi->SetTRT101(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 110", 0, 0xff);
    ierr = ltpi->SetTRT110(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 111", 0, 0xff);
    ierr = ltpi->SetTRT111(value);

    std::printf("\n");
    ierr = ltpi->dumpTRTTable();
    std::printf("\n");

    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class enableCTPout : public MenuItem {
public:
  enableCTPout() { setName("Enable CTPout link"); }
  int action() {
    int status = 0;

    // Each DELAY25 chip has 4 channels, plus clock
    int sel = enterInt("0-Quit, 1-Enable, 2-Disable", 0, 2);
    switch (sel) {
    case 0: break;
    case 1: status = ltpi->enableCTPout(); break;
    case 2: status = ltpi->disableCTPout(); break;
    }

    return(status);
  }
};

//-----------------------------------------------------------------------------
class changePGfile : public MenuItem {
public:
  changePGfile() { setName("Change patter generator file"); }
  int action() {
    int status = 0;
    printf("The pattern file is in RCDLtpi/src/test/; make sure you execute the code from there.\n");
    int sel = enterInt("0-Quit, 1-All in phase, 2-random, 3-other file", 0, 3);
    switch (sel) {
    case 0: break;
    case 1: pgfilename = "pg11.dat";    break;
    case 2: pgfilename = "pg_rand.dat"; break;
    case 3: {
      string name;
      std::printf("Enter of the file name with respect to the current directory:");
      std::cin >> name;
      pgfilename = name; break;
    }
      // check that the file is actually here

    }

    return(status);
  }
};

//-----------------------------------------------------------------------------
class testI2CBus_Delay : public MenuItem {
public:
  testI2CBus_Delay() { setName("Test communication on the i2c bus"); }
  int action () {
    int status = 0;

    //std::vector<u_short> delayChannelAddress(0x38, 0x3a, 0x30, 0x31, 0x32, 0x33, 0x28, 0x29, 0x2a, 0x20, 0x21, 0x22, 0x32, 0x10, 0x11, 0x12, 0x13);
      //(ltpi->DEL25L1A, ltpi->DEL25ORB, ltpi->DEL25BGO1, ltpi->DEL25BGO2, ltpi->DEL25BGO3, ltpi->DEL25BGO4, ltpi->DEL25TTR1, ltpi->DEL25TTR2, ltpi->DEL25TTR3, ltpi->DEL25TRT4, ltpi->DEL25TRT5, ltpi->DEL25TRT6, ltpi->DEL25TRT7, ltpi->DEL25TRT0, ltpi->DEL25TRT1, ltpi->DEL25TRT2, ltpi->DEL25TRT3);
    return status;
  }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main() {

  // set defaults:

  Menu menu("LTPi Menu");

  // generate menu
  menu.init(new LTPOpen);

  menu.add(new reset);
  menu.add(new initialize);
  menu.add(new initializeDAC);
  menu.add(new enableCTPout);
  Menu menuI2cDelay("Test Delay Chips");
       menuI2cDelay.add(new testI2CBus_Delay);
  menu.add(new changePGfile);

  // LTPi in CTP mode
  Menu menuCTP("Test CTP-link-in - CTP mode");
  menuCTP.add(new confLTPiCTP);
  menuCTP.add(new confLTPs);
  Menu menuCTPCTP("Test CTP-link-out");
       menuCTPCTP.add(new connectLTP2CTPlinout);
       menuCTPCTP.add(new startTestCTP);
  menuCTP.add(&menuCTPCTP);
  Menu menuCTPLTPout("Test LTP-link-out");
       menuCTPLTPout.add(new connectLTP2LTPlinout);
       menuCTPLTPout.add(new startTestCTP);
  menuCTP.add(&menuCTPLTPout);
  menu.add(&menuCTP);

  // LTPi in LTP mode
  Menu menuLTP("Test LTP-link-in - LTP mode");
  menuLTP.add(new confLTPiLTP);
  menuLTP.add(new confLTPs);
  Menu menuLTPCTP("Test CTP-link-out");
       menuLTPCTP.add(new connectLTP2CTPlinout);
       menuLTPCTP.add(new startTestLTP);
  menuLTP.add(&menuLTPCTP);
  Menu menuLTPLTP("Test LTP-link-out");
       menuLTPLTP.add(new connectLTP2LTPlinout);
       menuLTPLTP.add(new startTestLTP);
  menuLTP.add(&menuLTPLTP);
  menu.add(&menuLTP);

  // LTPi in NIM mode
  Menu menuNIM("Test NIM-in - NIM mode");
  menuNIM.add(new confLTPiNIM);
  menuNIM.add(new confLTPs);
  Menu menuNIMCTP("Test CTP-link-out");
       menuNIMCTP.add(new connectLTP2CTPlinout);
       menuNIMCTP.add(new startTestCTP);
  menuNIM.add(&menuNIMCTP);
  Menu menuNIMLTPout("Test LTP-link-out");
       menuNIMLTPout.add(new connectLTP2LTPlinout);
       menuNIMLTPout.add(new startTestCTP);
  menuNIM.add(&menuNIMLTPout);
  menu.add(&menuNIM);

  // LTPi in CTP LTP mode
  Menu menuCTPLTP("Test CTP LTP mode");
  menuCTPLTP.add(new confLTPiCTPLTP);
  menuCTPLTP.add(new confLTPs);
  Menu menuCTPCTPLTP("Test CTP-link-in ==> CTP-link-out");
       menuCTPCTPLTP.add(new connectLTP2CTPlinout);
       menuCTPCTPLTP.add(new startTestCTP);
  menuCTPLTP.add(&menuCTPCTPLTP);
  Menu menuCTPLTPLTP("Test LTP-link-in ==> LTP-link-out");
       menuCTPLTPLTP.add(new connectLTP2LTPlinout);
       menuCTPLTPLTP.add(new startTestLTP);
  menuCTPLTP.add(&menuCTPLTPLTP);
  menu.add(&menuCTPLTP);

  // LTPi in NIM LTP mode
  Menu menuNIMLTP("Test NIM LTP mode");
  menuNIMLTP.add(new confLTPiNIMLTP);
  menuNIMLTP.add(new confLTPs);
  Menu menuNIMCTPLTP("Test NIM-in ==> CTP-link-out");
       menuNIMCTPLTP.add(new connectLTP2CTPlinout);
       menuNIMCTPLTP.add(new startTestCTP);
  menuNIMLTP.add(&menuNIMCTPLTP);
  Menu menuNIMLTPLTP("Test LTP-link-in ==> LTP-link-out");
       menuNIMLTPLTP.add(new connectLTP2LTPlinout);
       menuNIMLTPLTP.add(new startTestLTP);
  menuNIMLTP.add(&menuNIMLTPLTP);
  menu.add(&menuNIMLTP);

  Menu menuNIMOUT("Test NIM out");
       menuNIMOUT.add(new confLTPs4NIMout);
       menuNIMOUT.add(new startTestLTP);
  menu.add(&menuNIMOUT);

  // Test trigger types
  Menu menuTRT("Test trigger types");
  //Menu menuTRTlocal("Test trigger types assigned locally");
  menuTRT.add(new confLTPiTRT);
  //menuTRT.add(new FillAllTRTTable);
  menuTRT.add(new confLTPsTRT);
  menuTRT.add(new startTestTRT);
  menu.add(&menuTRT);

  Menu menuBusy("Test busy path");
  menuBusy.add(new confBusy);
  menuBusy.add(new busy2Ctp);
  menuBusy.add(new busy2Ltp);
  menuBusy.add(new busy2Nim);
  menu.add(&menuBusy);

  Menu menuCal("Test calibration request");
  menuCal.add(new testAutoCal());
  menuCal.add(new confCal);
  menuCal.add(new testCal);
  menu.add(&menuCal);

  menu.exit(new LTPClose);

  // start menu
  menu.execute();

  return 0;
}
