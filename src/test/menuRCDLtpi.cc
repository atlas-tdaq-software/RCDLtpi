//******************************************************************************
// file: menuRCDLtpi.cc
// desc: menu for test of library for LTPi
//******************************************************************************

#include <fstream>
#include <iostream>
#include <iomanip>
#include <list>
#include <cstdlib>
#include <cstdio>
#include <cerrno>

#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDVme.h"
#include "DFDebug/DFDebug.h"
#include "cmdl/cmdargs.h"


#include "RCDLtpi/RCDLtpi.h"


using namespace RCD;


// global variable
LTPi* ltpi;
int selchannel = 4; // all channel by default 
std::string conffile;
u_int base = 0;
bool expert(false);

class LTPOpen : public MenuItem {
public:
  LTPOpen() { setName("Open LTPi"); }
  int action() {
    int status = 0;

    ltpi = new LTPi();
    status |= ltpi->Open(base);

    u_int board_man = 0;
    u_int board_revision = 0;
    u_int board_id = 0;
    status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);

    printf("\n");
    printf(" Manufacturer: 0x%08x (CERN = 0x00080030)\n", board_man);
    printf(" Revision    : 0x%08x \n", board_revision);
    printf(" Id          : 0x%08x \n", board_id);
   
    // reset I2C
    // status |= ltpi->setupI2C();
    // status |= ltpi->setupDAC();
    
    return status;
  }
};
//------------------------------------------------------------------------------
class SetConfFileName    : public MenuItem {
public:

  SetConfFileName() { setName("Set Cofiguration File Name"); }
  int action() {

    int status = 0;
    
    std::printf("\n");
    std::printf("*** Set Configuratoin File Name\n\n");
    std::printf("Current filename: %s  (relative to current directory)\n", conffile.c_str());
    std::printf("\n");

    int sel = enterInt("0-quit, 1-enter file",0,1);
    switch (sel) {
    case 0: 
      break;
    case 1:
      std::printf("\n");
      std::printf("Please enter file name (relative to current directory): ");
      std::cin >> conffile;
      break;
    }
    std::printf("\n");
    std::printf("Selected filename: %s", conffile.c_str());

    return(status);
  }
};

//------------------------------------------------------------------------------
class DumpToFile : public MenuItem {
public:
  DumpToFile() { setName("Register Dump to File"); }
  int action() {
    int rtnv = 0;
      
    printf("\n");
    printf(" Current configuration file name: %s\n", conffile.c_str());
    printf("\n");

    int sel = enterInt("0-quit, 1-DumpToFile, 2-Change Filename",0,2);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      {
	// open file
	FILE* file;
	file = fopen(conffile.c_str(), "w");
	if (!file) {
	  printf("\n *** ERROR opening file %s for writing *** \n", conffile.c_str());
	  exit(-1);
	}

	// read values from registers

	u_short w = 0x0000;
	rtnv |= ltpi->getBC_Path(w);       u_short w_BC = w;
	rtnv |= ltpi->getOrbit_Path(w);    u_short w_Orbit = w;
	rtnv |= ltpi->getL1A_Path(w);      u_short w_L1A = w;
	rtnv |= ltpi->getBGO_Path(w);      u_short w_BGO = w;
	rtnv |= ltpi->getTRT_Path(w);      u_short w_TRT = w;
	rtnv |= ltpi->getBusyCTP_Path(w);  u_short w_BusyCTP = w;
	rtnv |= ltpi->getBusyNIM_Path(w);  u_short w_BusyNIM = w;
	rtnv |= ltpi->getBusyLTP_Path(w);  u_short w_BusyLTP = w;
	rtnv |= ltpi->getCAL_Path(w);      u_short w_CAL = w;
	rtnv |= ltpi->getTRT_SELECTION(w); u_short w_TRTSELECTION = w;

	rtnv |= ltpi->GetTRT000(w);      u_short w_TRT000 = w;
	rtnv |= ltpi->GetTRT001(w);      u_short w_TRT001 = w;
	rtnv |= ltpi->GetTRT010(w);      u_short w_TRT010 = w;
	rtnv |= ltpi->GetTRT011(w);      u_short w_TRT011 = w;
	rtnv |= ltpi->GetTRT100(w);      u_short w_TRT100 = w;
	rtnv |= ltpi->GetTRT101(w);      u_short w_TRT101 = w;
	rtnv |= ltpi->GetTRT110(w);      u_short w_TRT110 = w;
	rtnv |= ltpi->GetTRT111(w);      u_short w_TRT111 = w;
	
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25L1A); u_short d_L1A = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25ORB); u_short d_ORB = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25BGO1); u_short d_BGO1 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25BGO2); u_short d_BGO2 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25BGO3); u_short d_BGO3 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25BGO0); u_short d_BGO0 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TTR1); u_short d_TTR1 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TTR2); u_short d_TTR2 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TTR3); u_short d_TTR3 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT0); u_short d_TRT0 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT1); u_short d_TRT1 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT2); u_short d_TRT2 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT3); u_short d_TRT3 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT4); u_short d_TRT4 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT5); u_short d_TRT5 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT6); u_short d_TRT6 = w;
	rtnv |= ltpi->readDelaySingleChannel(w, ltpi->DEL25TRT7); u_short d_TRT7 = w;


	rtnv |= ltpi->getDACLTPequ(w); u_short d_ELTP = w;
	rtnv |= ltpi->getDACCTPequ(w); u_short d_ECTP = w;
	rtnv |= ltpi->getDACLTPgain(w); u_short d_GLTP = w;
	rtnv |= ltpi->getDACLTPgain(w); u_short d_GCTP = w;

	// write to file

	fprintf(file, "------------------\n");
	fprintf(file, "LTP Configuration\n");
	fprintf(file, "Do not change!!! \n");
	fprintf(file, "------------------\n");
	fprintf(file, "BC %04x\n", w_BC);
	fprintf(file, "Orbit %04x\n", w_Orbit);
	fprintf(file, "L1A %04x\n", w_L1A);
	fprintf(file, "BGO %04x\n", w_BGO);
	fprintf(file, "TRT %04x\n", w_TRT);
	fprintf(file, "Busy CTP %04x\n", w_BusyCTP);
	fprintf(file, "Busy NIM %04x\n", w_BusyNIM);
	fprintf(file, "Busy LTP %04x\n", w_BusyLTP);
	fprintf(file, "CALIBRATION %04x\n", w_CAL);
	fprintf(file, "Test Trigger selection  %04x\n", w_TRTSELECTION);
	fprintf(file, "TRT 000  %04x\n", w_TRT000);
	fprintf(file, "TRT 001  %04x\n", w_TRT001);
	fprintf(file, "TRT 010  %04x\n", w_TRT010);
	fprintf(file, "TRT 011  %04x\n", w_TRT011);
	fprintf(file, "TRT 100  %04x\n", w_TRT100);
	fprintf(file, "TRT 101  %04x\n", w_TRT101);
	fprintf(file, "TRT 110  %04x\n", w_TRT110);
	fprintf(file, "TRT 111  %04x\n", w_TRT111);

	fprintf(file, "Delay L1A %d\n", d_L1A);
	fprintf(file, "Delay ORB %d\n", d_ORB);
	fprintf(file, "Delay BGO1 %d\n", d_BGO1);
	fprintf(file, "Delay BGO2 %d\n", d_BGO2);
	fprintf(file, "Delay BGO3 %d\n", d_BGO3);
	fprintf(file, "Delay BGO0 %d\n", d_BGO0);
	fprintf(file, "Delay TTR1 %d\n", d_TTR1);
	fprintf(file, "Delay TTR2 %d\n", d_TTR2);
	fprintf(file, "Delay TTR3 %d\n", d_TTR3);
	fprintf(file, "Delay TRT0 %d\n", d_TRT0);
	fprintf(file, "Delay TRT1 %d\n", d_TRT1);
	fprintf(file, "Delay TRT2 %d\n", d_TRT2);
	fprintf(file, "Delay TRT3 %d\n", d_TRT3);
	fprintf(file, "Delay TRT4 %d\n", d_TRT4);
	fprintf(file, "Delay TRT5 %d\n", d_TRT5);
	fprintf(file, "Delay TRT6 %d\n", d_TRT6);
	fprintf(file, "Delay TRT7 %d\n", d_TRT7);

	fprintf(file, "LTP EQUALIZER %d\n", d_ELTP);
	fprintf(file, "CTP EQUALIZER %d\n", d_ECTP);
	fprintf(file, "LTP GAIN %d\n", d_GLTP);
	fprintf(file, "CTP GAIN %d\n", d_GCTP);


	// close file
	fclose(file);
	break;
      }
    case 2:
      SetConfFileName h;
      h.action();
      break;

    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class ConfigureFromFile : public MenuItem {
public:
  ConfigureFromFile() { setName("Configure from File"); }
  int action() {
    int rtnv = 0;

      
    // declare variables for registers

    u_short w_BC;
    u_short w_ORB;
    u_short w_L1A;
    u_short w_BGO;
    u_short w_TTR;
    u_short w_BusyCTP;
    u_short w_BusyNIM;
    u_short w_BusyLTP;
    u_short w_CAL;
    u_short w_TTRSELECTION;
    u_short w_TRT000;
    u_short w_TRT001;
    u_short w_TRT010;
    u_short w_TRT011;
    u_short w_TRT100;
    u_short w_TRT101;
    u_short w_TRT110;
    u_short w_TRT111;


    u_short d_L1A;
    u_short d_ORB;
    u_short d_BGO1;
    u_short d_BGO2;
    u_short d_BGO3;
    u_short d_BGO0;
    u_short d_TTR1;
    u_short d_TTR2;
    u_short d_TTR3;
    u_short d_TRT0;
    u_short d_TRT1;
    u_short d_TRT2;
    u_short d_TRT3;
    u_short d_TRT4;
    u_short d_TRT5;
    u_short d_TRT6;
    u_short d_TRT7;

    u_short d_ELTP;
    u_short d_ECTP;
    u_short d_GLTP;
    u_short d_GCTP;




    printf("\n");
    printf(" Current configuration file name: %s\n", conffile.c_str());
    printf("\n");

    int sel = enterInt("0-quit, 1-ConfigureFromFile, 2-Change Filename",0,2);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      {
	// open file
	std::ifstream tfile(conffile.c_str(), ios::in);
	if (!tfile.is_open()) {
	  exit(-1);
	}
	else {
	  std::cout << "Loading file " << conffile.c_str() << " into memory" << std::endl;

	  char buffer[200];
	  // ignore the first 4 lines
	  tfile.getline(buffer,200);
	  tfile.getline(buffer,200);
	  tfile.getline(buffer,200);
	  tfile.getline(buffer,200);

	  // read from file
	  u_int intread;
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BC %04x", &intread);
	  w_BC = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Orbit %04x", &intread);
	  w_ORB = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "L1A %04x", &intread);
	  w_L1A = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BGO %04x", &intread);
	  w_BGO = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TTR %04x", &intread);
	  w_TTR = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Busy CTP %04x", &intread);
	  tfile.getline(buffer,200);
	  w_BusyCTP = static_cast<u_short>(intread);
	  sscanf(buffer, "Busy NIM %04x", &intread);
	  tfile.getline(buffer,200);
	  w_BusyNIM = static_cast<u_short>(intread);
	  sscanf(buffer, "Busy LTP %04x", &intread);
	  tfile.getline(buffer,200);
	  w_BusyLTP = static_cast<u_short>(intread);
	  sscanf(buffer, "CALIBRATION %04x", &intread);
	  tfile.getline(buffer,200);
	  w_CAL = static_cast<u_short>(intread);
	  sscanf(buffer, "Test Trigger selection %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TTRSELECTION = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 000 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT000 = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 001 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT001 = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 010 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT010 = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 011 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT011 = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 100 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT100 = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 101 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT101 = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 110 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT110 = static_cast<u_short>(intread);
	  sscanf(buffer, "TRT 111 %04x", &intread);
	  tfile.getline(buffer,200);
	  w_TRT111 = static_cast<u_short>(intread);
	  sscanf(buffer, "Delay L1A %04x", &intread);
	  tfile.getline(buffer,200);
	  d_L1A = static_cast<u_short>(intread);
	  sscanf(buffer, "Delay ORB  %04x", &intread);
	  tfile.getline(buffer,200);
	  d_ORB = static_cast<u_short>(intread);
	  sscanf(buffer, "Delay BGO1 %04x", &intread);
	  tfile.getline(buffer,200);
	  d_BGO1 = static_cast<u_short>(intread);
	  sscanf(buffer, "Delay BGO2 %04x", &intread);
	  tfile.getline(buffer,200);
	  d_BGO2 = static_cast<u_short>(intread);
	  sscanf(buffer, "Delay BGO3 %04x", &intread);
	  tfile.getline(buffer,200);
	  d_BGO3 = static_cast<u_short>(intread);
	  sscanf(buffer, "Delay BGO0 %04x", &intread);
	  tfile.getline(buffer,200);
	  d_BGO0 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TTR1%04x", &intread);
	  d_TTR1 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TTR2 %04x", &intread);
	  d_TTR2 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TTR3 %04x", &intread);
	  d_TTR3 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT0 %04x", &intread);
	  d_TRT0 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT1 %04x", &intread);
	  d_TRT1 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT2 %04x", &intread);
	  d_TRT2 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT3 %04x", &intread);
	  d_TRT3 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT4 %04x", &intread);
	  d_TRT4 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT5 %04x", &intread);
	  d_TRT5 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT6 %04x", &intread);
	  d_TRT6 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Delay TRT7 %04x", &intread);
	  d_TRT7 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "LTP EQUALIZER %04x", &intread);
	  d_ELTP = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "CTP EQUALIZER %04x", &intread);
	  d_ECTP = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "LTP GAIN %04x", &intread);
	  d_GLTP = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "CTP GAIN %04x", &intread);
	  d_GCTP = static_cast<u_short>(intread);

	  printf("\n\n");
	  printf(" ----------------\n");
	  printf("  Read from File \n");
	  printf(" ----------------\n");
	  printf(" BC                      0x%04x \n", w_BC);
	  printf(" Orbit                   0x%04x \n", w_ORB);
	  printf(" BGO                     0x%04x \n", w_BGO);
	  printf(" TRT                     0x%04x \n", w_TTR);
	  printf(" Busy CTP                0x%04x \n", w_BusyCTP);
	  printf(" Busy NIM                0x%04x \n", w_BusyNIM);
	  printf(" Busy LTP                0x%04x \n", w_BusyLTP);
	  printf(" CALIBRATION             0x%04x \n", w_CAL);
	  printf(" Test Trigger election   0x%04x \n", w_TTRSELECTION);
	  printf(" TRT 000                 0x%04x \n", w_TRT000);
	  printf(" TRT 001                 0x%04x \n", w_TRT001);
	  printf(" TRT 010                 0x%04x \n", w_TRT010);
	  printf(" TRT 011                 0x%04x \n", w_TRT011);
	  printf(" TRT 100                 0x%04x \n", w_TRT100);
	  printf(" TRT 101                 0x%04x \n", w_TRT101);
	  printf(" TRT 110                 0x%04x \n", w_TRT110);
	  printf(" TRT 111                 0x%04x \n", w_TRT111);
	  printf(" Delay L1A               0x%04x \n", d_L1A);
	  printf(" Delay ORB               0x%04x \n", d_ORB);
	  printf(" Delay BGO1              0x%04x \n", d_BGO1);
	  printf(" Delay BGO2              0x%04x \n", d_BGO2);
	  printf(" Delay BGO3              0x%04x \n", d_BGO3);
	  printf(" Delay BGO0              0x%04x \n", d_BGO0);
	  printf(" Delay TTR1              0x%04x \n", d_TTR1);
	  printf(" Delay TTR2              0x%04x \n", d_TTR2);
	  printf(" Delay TTR3              0x%04x \n", d_TTR3);
	  printf(" Delay TRT0              0x%04x \n", d_TRT0);
	  printf(" Delay TRT1              0x%04x \n", d_TRT1);
	  printf(" Delay TRT2              0x%04x \n", d_TRT2);
	  printf(" Delay TRT3              0x%04x \n", d_TRT3);
	  printf(" Delay TRT4              0x%04x \n", d_TRT4);
	  printf(" Delay TRT5              0x%04x \n", d_TRT5);
	  printf(" Delay TRT6              0x%04x \n", d_TRT6);
	  printf(" Delay TRT7              0x%04x \n", d_TRT7);
	  printf(" LTP EQUALIZER           0x%04x \n", d_ELTP);
	  printf(" CTP EQUALIZER           0x%04x \n", d_ECTP);
	  printf(" LTP GAIN                0x%04x \n", d_GLTP);
	  printf(" CTP GAIN                0x%04x \n", d_GCTP);

	}
	tfile.close();

	// write to registers

	rtnv |= ltpi->setBC_Path(w_BC);
	rtnv |= ltpi->setOrbit_Path(w_ORB);
	rtnv |= ltpi->setL1A_Path(w_L1A);
	rtnv |= ltpi->setBGO_Path(w_BGO);
	rtnv |= ltpi->setTRT_Path(w_TTR);
	rtnv |= ltpi->setBusyCTP_Path(w_BusyCTP);
	rtnv |= ltpi->setBusyNIM_Path(w_BusyNIM);
	rtnv |= ltpi->setBusyLTP_Path(w_BusyLTP);
	rtnv |= ltpi->setCAL_Path(w_CAL);
	rtnv |= ltpi->setTRT_SELECTION(w_TTRSELECTION);

	rtnv |= ltpi->SetTRT000(w_TRT000);
	rtnv |= ltpi->SetTRT001(w_TRT001);
	rtnv |= ltpi->SetTRT010(w_TRT010);
	rtnv |= ltpi->SetTRT011(w_TRT011);
	rtnv |= ltpi->SetTRT100(w_TRT100);
	rtnv |= ltpi->SetTRT101(w_TRT101);
	rtnv |= ltpi->SetTRT110(w_TRT110);
	rtnv |= ltpi->SetTRT111(w_TRT111);
	
	rtnv |= ltpi->setDelaySingleChannel(d_L1A, ltpi->DEL25L1A); 
	rtnv |= ltpi->setDelaySingleChannel(d_ORB, ltpi->DEL25ORB); 
	rtnv |= ltpi->setDelaySingleChannel(d_BGO1, ltpi->DEL25BGO1);
	rtnv |= ltpi->setDelaySingleChannel(d_BGO2, ltpi->DEL25BGO2);
	rtnv |= ltpi->setDelaySingleChannel(d_BGO3, ltpi->DEL25BGO3);
	rtnv |= ltpi->setDelaySingleChannel(d_BGO0, ltpi->DEL25BGO0);
	rtnv |= ltpi->setDelaySingleChannel(d_TTR1, ltpi->DEL25TTR1);
	rtnv |= ltpi->setDelaySingleChannel(d_TTR2, ltpi->DEL25TTR2);
	rtnv |= ltpi->setDelaySingleChannel(d_TTR3, ltpi->DEL25TTR3);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT0, ltpi->DEL25TRT0);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT1, ltpi->DEL25TRT1);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT2, ltpi->DEL25TRT2);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT3, ltpi->DEL25TRT3);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT4, ltpi->DEL25TRT4);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT5, ltpi->DEL25TRT5);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT6, ltpi->DEL25TRT6);
	rtnv |= ltpi->setDelaySingleChannel(d_TRT7, ltpi->DEL25TRT7);

	rtnv |= ltpi->setDACLTPequ(d_ELTP);
	rtnv |= ltpi->setDACCTPequ(d_ECTP);
	rtnv |= ltpi->setDACLTPgain(d_ELTP);
	rtnv |= ltpi->setDACCTPgain(d_ECTP);


      }
    case 2:
      SetConfFileName h;
      h.action();
      break;
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class LTPiClose : public MenuItem {
public:
  LTPiClose() { setName("Close LTPi"); }
  int action() {
    int status = 0;
      status |= ltpi->Close();
    delete ltpi;

    return(status);
  }
};

//-----------------------------------------------------------------------------

class SetupDACShortCables : public MenuItem {
public:
  SetupDACShortCables() { setName("Setup DAC for short cables"); }
  int action() {
    int status = 0;

    std::cout << "Setup DAC" << std::endl;

    status |= ltpi->setupDAC4shortCable();
    status |= ltpi->printDACsettings();
   
    return(status);
  }
};

//-----------------------------------------------------------------------------

class SetupDACLongCables : public MenuItem {
public:
  SetupDACLongCables() { setName("Setup DAC for long cables"); }
  int action() {
    int status = 0;

    std::cout << "Setup DAC for long cables" << std::endl;

    status |= ltpi->setupDAC4longCable();
    status |= ltpi->printDACsettings();
   
    return(status);
  }
};

//-----------------------------------------------------------------------------
class PrintDAC : public MenuItem {
public:
  PrintDAC() { setName("PrintDAC"); }
  int action() {
    int status = 0;

    std::cout << "Print DAC Values" << std::endl;

    status |= ltpi->printDACsettings();
   
    return(status);
  }
};

//-----------------------------------------------------------------------------
class SetDACLTPgain : public MenuItem {
public:
  SetDACLTPgain() { setName("Set LTP Gain"); }
  int action() {
    int ierr = 0;
    int sel = enterHex("Gain in Hex", 0, 0xff);
    u_short gain = static_cast<u_short>(sel); 
    ierr |= ltpi->setDACLTPgain(gain);
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class SetDACLTPcable : public MenuItem {
public:
  SetDACLTPcable() { setName("Set LTP DAC giving cable length"); }
  int action() {
    int ierr = 0;
    float length = -1;
    while (length < 0) {
      std::cout << "LTP link in length [m]: ";
      std::cin >> length;
      std::cout << std::endl;
    }
    ierr |= ltpi->setupDAC4givenLTPCable(length);
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class SetDACCTPcable : public MenuItem {
public:
  SetDACCTPcable() { setName("Set CTP DAC giving cable length"); }
  int action() {
    int ierr = 0;
    float length = -1;
    while (length < 0) {
      std::cout << "LTP link in length [m]: ";
      std::cin >> length;
      std::cout << std::endl;
    }
    ierr |= ltpi->setupDAC4givenCTPCable(length);
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class SetDACCTPgain : public MenuItem {
public:
  SetDACCTPgain() { setName("Set CTP Gain"); }
  int action() {
    int ierr = 0;
    int sel = enterHex("Gain in Hex", 0, 0xff);
    u_short gain = static_cast<u_short>(sel); 
    ierr |= ltpi->setDACCTPgain(gain);
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class SetDACLTPequ : public MenuItem {
public:
  SetDACLTPequ() { setName("Set LTP equalization"); }
  int action() {
    int ierr = 0;
    int sel = enterHex("CTRL in Hex", 0, 0xff);
    u_short equ = static_cast<u_short>(sel); 
    ierr |= ltpi->setDACLTPequ(equ);
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class SetDACCTPequ : public MenuItem {
public:
  SetDACCTPequ() { setName("Set CTP equalization"); }
  int action() {
    int ierr = 0;
    int sel = enterHex("CTRL in Hex", 0, 0xff);
    u_short equ = static_cast<u_short>(sel); 
    ierr |= ltpi->setDACCTPequ(equ);
    return(ierr);
  }
};

//-----------------------------------------------------------------------------
class SetupDelay25SingleChannel : public MenuItem {
public:
  SetupDelay25SingleChannel() { setName("SetupDelay25 - Single Channel"); }
  int action() {
    int status = 0;

    u_short delay, channel;
    int seli = enterInt("Enter a channel a 0-QUIT, 1-LV1, 2-ORB, 3-BGO0, 4-BGO1, 5-BGO2, 6-BGO3, 7-TTR1, 8-TTR2, 9-TTR3, 10-TRT0, 11-TRT1, 12-TRT2, 13-TRT3, 14-TRT4, 15-TRT5, 16-TRT6, 17-TRT7 \n",0, 17);
    int sel = ltpi->DEL25L1A;
    if (seli==0) return(status);
    if(seli==1)  sel = ltpi->DEL25L1A;
    if(seli==2)  sel = ltpi->DEL25ORB;
    if(seli==3)  sel = ltpi->DEL25BGO0;
    if(seli==4)  sel = ltpi->DEL25BGO1;
    if(seli==5)  sel = ltpi->DEL25BGO2;
    if(seli==6)  sel = ltpi->DEL25BGO3;
    if(seli==7)  sel = ltpi->DEL25TTR1;
    if(seli==8)  sel = ltpi->DEL25TTR2;
    if(seli==9)  sel = ltpi->DEL25TTR3;
    if(seli==10)  sel = ltpi->DEL25TRT0;
    if(seli==11)  sel = ltpi->DEL25TRT1;
    if(seli==12)  sel = ltpi->DEL25TRT2;
    if(seli==13)  sel = ltpi->DEL25TRT3;
    if(seli==14)  sel = ltpi->DEL25TRT4;
    if(seli==15)  sel = ltpi->DEL25TRT5;
    if(seli==16)  sel = ltpi->DEL25TRT6;
    if(seli==17)  sel = ltpi->DEL25TRT7;

    channel = static_cast<u_short>(sel);
    sel = enterInt("Delay in steps of 0.5 ns", 0, 60);
    delay = static_cast<u_short>(sel);
    status |= ltpi->setDelaySingleChannel(delay, channel);

    return(status);
  }
};


class ResyncDelay25DLL : public MenuItem {
public:
  ResyncDelay25DLL() { setName("Resync DELAY25 DLL"); }
  int action() {
    int status = 0;
    status = ltpi->resetDelay25Pll();
    return(status);
  }
};

class ReadDelay25SingleChannel : public MenuItem {
public:
  ReadDelay25SingleChannel() { setName("ReadDelay25 - Single Channel"); }
  int action() {
    int status = 0;

    u_short delay, channel;

    int seli = enterInt("Enter a channel a 0-QUIT, 1-LV1, 2-ORB, 3-BGO0, 4-BGO1, 5-BGO2, 6-BGO3, 7-TTR1, 8-TTR2, 9-TTR3, 10-TRT0, 11-TRT1, 12-TRT2, 13-TRT3, 14-TRT4, 15-TRT5, 16-TRT6, 17-TRT7 \n",0, 17);
    int sel = ltpi->DEL25L1A;
    if (seli==0) return(status);
    if(seli==1)  sel = ltpi->DEL25L1A;
    if(seli==2)  sel = ltpi->DEL25ORB;
    if(seli==3)  sel = ltpi->DEL25BGO0;
    if(seli==4)  sel = ltpi->DEL25BGO1;
    if(seli==5)  sel = ltpi->DEL25BGO2;
    if(seli==6)  sel = ltpi->DEL25BGO3;
    if(seli==7)  sel = ltpi->DEL25TTR1;
    if(seli==8)  sel = ltpi->DEL25TTR2;
    if(seli==9)  sel = ltpi->DEL25TTR3;
    if(seli==10)  sel = ltpi->DEL25TRT0;
    if(seli==11)  sel = ltpi->DEL25TRT1;
    if(seli==12)  sel = ltpi->DEL25TRT2;
    if(seli==13)  sel = ltpi->DEL25TRT3;
    if(seli==14)  sel = ltpi->DEL25TRT4;
    if(seli==15)  sel = ltpi->DEL25TRT5;
    if(seli==16)  sel = ltpi->DEL25TRT6;
    if(seli==17)  sel = ltpi->DEL25TRT7;
    channel = static_cast<u_short>(sel);
    status |= ltpi->readDelaySingleChannel(delay, channel);
    printf("Delay = %d \n", delay);
    return(status);
  }
};

class ReadDelay25 : public MenuItem {
public:
  ReadDelay25() { setName("ReadDelay25"); }
  int action() {
    int status = 0;

    status |= ltpi->readDelayAllChannels();
    return(status);
  }
};

//-----------------------------------------------------------------------------
class SetupDelay25 : public MenuItem {
public:
  SetupDelay25() { setName("SetupDelay25"); }
  int action() {
    int status = 0;

    // Each DELAY25 chip has 4 channels, plus clock
    u_short delay;
    int sel = enterInt("Delay in steps of 0.5 ns", 0, 60);
    delay = static_cast<u_short>(sel);

    status |= ltpi->setDelayAllChannels(delay);

    return(status);
  }
};

//-----------------------------------------------------------------------------
class enableCTPout : public MenuItem {
public:
  enableCTPout() { setName("Enable CTPout link"); }
  int action() {
    int status = 0;
    
    // Each DELAY25 chip has 4 channels, plus clock
    int sel = enterInt("0-Quit, 1-Enable, 2-Disable", 0, 2);   
    switch (sel) {
    case 0: break;
    case 1: status |= ltpi->enableCTPout(); break;
    case 2: status |= ltpi->disableCTPout(); break;
    }
    
    return(status);
  }
};

//------------------------------------------------------------------------------
// IN THIS MODE ALL SIGNALS ARE GOING IN THE FOLLOWING WAY:
// CTP(CTP)
// LTP(LTP)
// NIM(CTP)
class CTP_LTP_Mode : public MenuItem {
public:
  CTP_LTP_Mode() { setName("CTP_LTP_Mode"); }
  int action() {
    int rtnv = 0;
    int sel = 0;
    std::cout << "" << std::endl;
    std::cout << "The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels" << std::endl;
    std::cout << "are routed as:" << std::endl;
    std::cout << "CTP-link out from CTP-link in" << std::endl;
    std::cout << "LTP-link out from LTP-link in" << std::endl;
    std::cout << "The CTP-link out is enabled" << std::endl;
    std::cout << "Each channel is delaied according to the configured delay or otherwise to the default" << std::endl;
    std::cout << "The DAC values for the LTP-link in are set as configured or otherwise the default" << std::endl;
    std::cout << "The DAC values for the CTP-link in are set as configured or otherwise the default" << std::endl;
    std::cout << "The CTP link in busy comes form CTP link out" << std::endl;
    std::cout << "The LTP link in busy comes from the LTP link out" << std::endl;
    std::cout << "" << std::endl;

    std::printf("\n");
    sel = enterInt("0-QUIT, 1-Configure", 0, 1);

    if (sel)
      rtnv = ltpi->ctpltp_mode();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
// IN THIS MODE ALL SIGNALS ARE GOING IN THE FOLLOWING WAY:
// CTP(CTP)
// LTP(CTP)
// NIM(CTP)
class CTP_Mode : public MenuItem {
public:
  CTP_Mode() { setName("CTP_Mode"); }
  int action() {
    int rtnv = 0;
    int sel = 0;

    std::cout << "" << std::endl;
    std::cout << "The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels" << std::endl;
    std::cout << "are routed as:" << std::endl;
    std::cout << "CTP-link out from CTP-link in" << std::endl;
    std::cout << "LTP-link out from CTP-link in" << std::endl;
    std::cout << "The CTP-link out is enabled" << std::endl;
    std::cout << "Each channel is delaied according to the configured delay or otherwise to the default" << std::endl;
    std::cout << "The DAC values for the CTP-link in are set as configured or otherwise the default" << std::endl;
    std::cout << "The DAC values for the LTP-link in are set to zero" << std::endl;
    std::cout << "The CTP link in busy is the or of the LTP link out and CTP link out" << std::endl;
    std::cout << "" << std::endl;

    std::printf("\n");
    sel = enterInt("0-QUIT, 1-Configure", 0, 1);

    if (sel)
      rtnv = ltpi->ctp_mode();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
// IN THIS MODE ALL SIGNALS ARE GOING IN THE FOLLOWING WAY:
// CTP(LTP)
// LTP(LTP)
// NIM(LTP)
class LTP_Mode : public MenuItem {
public:
  LTP_Mode() { setName("LTP_Mode"); }
  int action() {
    int rtnv = 0;
    int sel = 0;
    std::cout << "" << std::endl;
    std::cout << "The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels" << std::endl;
    std::cout << "are routed as:" << std::endl;
    std::cout << "CTP-link out from LTP-link in" << std::endl;
    std::cout << "LTP-link out from LTP-link in" << std::endl;
    std::cout << "The CTP-link out is enabled" << std::endl;
    std::cout << "Each channel is delaied according to the configured delay or otherwise to the default" << std::endl;
    std::cout << "The DAC values for the LTP-link in are set as configured or otherwise the default" << std::endl;
    std::cout << "The DAC values for the CTP-link in are set to zero" << std::endl;
    std::cout << "The LTP link in busy is the or of the LTP link out and CTP link out" << std::endl;
    std::cout << "" << std::endl;

    std::printf("\n");
    sel = enterInt("0-QUIT, 1-Configure", 0, 1);

    if (sel)
      rtnv = ltpi->ltp_mode();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
// IN THIS MODE ALL SIGNALS ARE GOING IN THE FOLLOWING WAY:
// CTP(NIM)
// LTP(NIM)
// NIM(NIM)
class NIM_Mode : public MenuItem {
public:
  NIM_Mode() { setName("NIM_Mode"); }
  int action() {
    int rtnv = 0;
    int sel = 0;

    std::cout << "" << std::endl;
    std::cout << "The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels" << std::endl;
    std::cout << "are routed as:" << std::endl;
    std::cout << "CTP-link out from NIM in" << std::endl;
    std::cout << "LTP-link out from NIM in" << std::endl;
    std::cout << "The CTP-link out is enabled" << std::endl;
    std::cout << "Each channel is delaied according to the configured delay or otherwise to the default" << std::endl;
    std::cout << "The DAC values for the CTP-link in are set to zero" << std::endl;
    std::cout << "The DAC values for the LTP-link in are set to zero" << std::endl;
    std::cout << "The NIM in busy is the or of the LTP link out and CTP link out" << std::endl;
    std::cout << "" << std::endl;

    std::printf("\n");
    sel = enterInt("0-QUIT, 1-Configure", 0, 1);

    if (sel)
      rtnv = ltpi->nim_mode();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
// IN THIS MODE ALL SIGNALS ARE GOING IN THE FOLLOWING WAY:
// CTP(NIM)
// LTP(LTP)
// NIM(??)
class NIM_LTP_Mode : public MenuItem {
public:
  NIM_LTP_Mode() { setName("NIM_LTP_Mode"); }
  int action() {
    int rtnv = 0;
    int sel = 0;

    std::cout << "" << std::endl;
    std::cout << "The BC, Orbit, L1A, Test Triggers, BGOs, Trigger types channels" << std::endl;
    std::cout << "are routed as:" << std::endl;
    std::cout << "CTP-link out from NIM in" << std::endl;
    std::cout << "LTP-link out from LTP-link in" << std::endl;
    std::cout << "The CTP-link out is enabled" << std::endl;
    std::cout << "Each channel is delaied according to the configured delay or otherwise to the default" << std::endl;
    std::cout << "The DAC values for the LTP-link in are set as configured or otherwise the default" << std::endl;
    std::cout << "The DAC values for the CTP-link in are set to zero" << std::endl;
    std::cout << "The NIN in busy comes form CTP link out" << std::endl;
    std::cout << "The LTP link in busy comes from the LTP link out" << std::endl;
    std::cout << "" << std::endl;

    std::printf("\n");
    sel = enterInt("0-QUIT, 1-Configure", 0, 1);

    if (sel)
      rtnv = ltpi->nimltp_mode();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class I2CWrite : public MenuItem {
public:
  I2CWrite() { setName("I2CWrite"); }
  int action() {
    int rtnv = 0;

    u_short addr;
    int address = enterHex("Address (hex)", 0, 0xff);
    addr = static_cast<u_short>(address);

    u_short dat;
    int data = enterHex("Data (hex)", 0, 0xff);
    dat = static_cast<u_short>(data);

    rtnv |= ltpi->i2c_write(addr, dat);

    printf("Written 0x%02x to 0x%02x\n", dat, addr);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class I2CRead : public MenuItem {
public:
  I2CRead() { setName("I2CRead"); }
  int action() {
    int rtnv = 0;

    u_short addr;
    int address = enterHex("Address (hex)", 0, 0xff);
    addr = static_cast<u_short>(address);

    u_short dat;
 
    rtnv |= ltpi->i2c_read(addr, dat);

    printf("Read 0x%02x from 0x%02x\n", dat, addr);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class I2CCheckFrequency : public MenuItem {
public:
  I2CCheckFrequency() { setName("I2CCheckFrequency"); }
  int action() {
    int ierr = 11;

    bool ok = ltpi->I2C_frequency_OK();
    if (!ok) {
      float freq = 0;
      freq = ltpi->readI2Cfreq();
      printf("\n");
      printf("Frequency = %5.1f kHz  (should be 100 kHz) \n", freq);
    } else {
      cout << endl << "Frequency OK (100 kHz)" << endl;
      ierr=0;
    }
    return (ierr);
  }
};

//------------------------------------------------------------------------------
class OFF_Mode : public MenuItem {
public:
  OFF_Mode() { setName("OFF_Mode"); }
  int action() {
    int rtnv = 0;
    int sel = 0;

    std::cout << "" << std::endl;
    std::cout << "No output going out of the module" << std::endl;
    std::cout << "CTP-link out, LTP-link out, NIM out disalbed" << std::endl;
    std::cout << "" << std::endl;
    std::printf("\n");
    sel = enterInt("0-QUIT, 1-Configure", 0, 1);

    if (sel)
      rtnv = ltpi->off_mode();


    return(rtnv);
  }
};



//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class resetI2C : public MenuItem {
public:
  resetI2C() { setName("reset I2C"); }
  int action() {
    int rtnv = 0;

     rtnv |= ltpi->resetI2C();

    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class setupI2C : public MenuItem {
public:
  setupI2C() { setName("setup I2C"); }
  int action() {
    int rtnv = 0;

    rtnv |=  ltpi->setupI2C();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPReset : public MenuItem {
public:
  LTPReset() { setName("Software reset"); }
  int action() {
    int rtnv = 0;

     rtnv |= ltpi->Reset();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class TRT_inputSelection : public MenuItem {
public:
  TRT_inputSelection() { setName("Get Test Trigger Input Selection"); }
  int action() {
    int ierr = ltpi->dumpTRTinput();
    return(ierr);
  }
};

//------------------------------------------------------------------------------
class TRTStatus : public MenuItem {
public:
  TRTStatus() { setName("Trigger Type Status"); }
  int action () {
  int ierr = ltpi->dumpTRTStatus();
  return(ierr);
  }
};

//------------------------------------------------------------------------------
class SelectTRTPath : public MenuItem {
public:
  SelectTRTPath() { setName("Select trigger type path"); }
  int action () {
    int ierr = 0;
    int sel  = 0;
    std::printf("\n");
    sel = enterInt("0-QUIT, 1-CTP, 2-LTP, 3-CTP-LTP, 4-Assigned-Locally, 5-Assigned-Locally-LTP", 0, 5);

    switch (sel) {
    case 0: break;
    case 1: ierr = ltpi->TRT_CTPoutFromCTPin_LTPoutFromCTPin(); break;
    case 2: ierr = ltpi->TRT_CTPoutFromLTPin_LTPoutFromLTPin(); break;
    case 3: ierr = ltpi->TRT_CTPoutFromCTPin_LTPoutFromLTPin(); break;
    case 4: ierr = ltpi->TRT_CTPoutFromLocal_LTPoutFromLocal(); break;
    case 5: ierr = ltpi->TRT_CTPoutFromLocal_LTPoutFromLTPin(); break;
    }
    return(ierr);
  }
};
/*******************/
class WriteProm : public MenuItem {
/*******************/
public:
  WriteProm() { setName("Write PROM"); }
  int action () {
    int status = 0;
    int sel = 0;
    u_int ma, bo, re;
    u_int board_man, board_revision, board_id;
    //u_short data;
    //char mode[9];
    
    printf("           WRITE IDPROM\n");
    //ltpi->m_vmm->ReadSafe(0x2,&data);
    //ltpi->get_B(0x2, &data);
    
    printf("The following values are programmed:\n");
    status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);
    cout << setbase(16) << "Manufacturer ID (CERN: 0x00080030): .......................................... 0x" << board_man << endl;
    cout << setbase(16) << "Revision (Firmware Date: 0xddmmyyyy): ........................................ 0x" << board_revision << endl;
    cout << setbase(16) << "Serial Number (0xmmyyT0NN; T=0 for successful test, 1 otherwise; NN=ID): ..... 0x" << board_id << endl;
    sel = enterInt("Enter [0] to keep value, [1] otherwise :", 0, 1);
    if (sel==0) return status;


    ma = enterHex("Manufacturer ID:", 0, 0xffffffff);
    re = enterHex("Revision:", 0, 0xffffffff);
    bo = enterHex("Serial Number:", 0,   0xffffffff);
    
    status |= ltpi->writeprom4(ltpi->MANUFID, ma);
    status |= ltpi->writeprom4(ltpi->BOARDID, bo);
    status |= ltpi->writeprom4(ltpi->REVISION, re);

    status |= ltpi->ReadConfigurationEEPROM(board_man, board_revision, board_id);

    printf("\n");
    printf(" Manufacturer: 0x%08x (CERN = 0x00080030)\n", board_man);
    printf(" Revision    : 0x%08x \n", board_revision);
    printf(" Id          : 0x%08x \n", board_id);

    return status;
  }
};

//------------------------------------------------------------------------------
class FillInitReg : public MenuItem {
public:
  FillInitReg() { setName("Fill Init Register To Read Trigger Type"); }
  int action () {
    int ierr = 0;
    
    int val = enterHex("Trigger Type word (0xff)", 0, 0xff);
    u_short value = static_cast<u_short>(val);
    ierr = ltpi->SetTRT000(value); 
    ierr = ltpi->GetTRT000(value);
    std::printf("TRT000 (init value) = 0x%02x", value);
    return(ierr);
  }
};

//------------------------------------------------------------------------------
class SelectTRTInput : public MenuItem {
public:
  SelectTRTInput() { setName("Select test trigger input to derive trigger type"); }
  int action () {
    int ierr = 0;
    int sel  = 0;
    std::printf("\n");
    sel = enterInt("0-QUIT, 1-internal-register, 2-CTPLink, 3-LTPlink, 4-NIM", 0, 4);

    switch (sel) {
    case 0: break;
    case 1: ierr = ltpi->TRT_FROM_LOCAL_REGISTER();   break;
    case 2: ierr = ltpi->TRT_DERIVED_FROM_CTP_TTR();  break;
    case 3: ierr = ltpi->TRT_DERIVED_FROM_LTP_TTR();  break;
    case 4: ierr = ltpi->TRT_DERIVED_FROM_NIM_TTR();  break;
    }
    return(ierr);
  }
};

//------------------------------------------------------------------------------
class FillTRTTable : public MenuItem {
public:
  FillTRTTable() { setName("Fill One Entry in Trigger Type Table"); } 
  int action() {
    int ierr = 0;
    int sel  = 0;

    ierr = ltpi->dumpTRTTable();
    std::printf("\n"); 
    std::printf("Select Test Trigger Combination: \n");
    sel = enterInt("0-QUIT, 1-TTR<3..1>=000, 2-TTR<3..1>=001, 3-TTR<3..1>=010, 3-TTR<3..1>=011, 5-TTR<3..1>=100, 6-TTR<3..1>=101, 7-TTR<3..1>=110, 8-TTR<3..1>=111", 0, 8); 

    int val = enterHex("Trigger Type word (0xff)", 0, 0xff);
    u_short value = static_cast<u_short>(val);
    switch (sel) {
    case 0: break;
    case 1: 
      ierr = ltpi->SetTRT000(value); 
      ierr = ltpi->GetTRT000(value);
      std::printf("TRT000 = 0x%02x", value);
      break;
    case 2: 
      ierr = ltpi->SetTRT001(value); 
      ierr = ltpi->GetTRT001(value);
      std::printf("TRT001 = 0x%02x", value);
      break;
    case 3: 
      ierr = ltpi->SetTRT010(value); 
      ierr = ltpi->GetTRT010(value);
      std::printf("TRT010 = 0x%02x", value);
      break;
    case 4: 
      ierr = ltpi->SetTRT011(value); 
      ierr = ltpi->GetTRT011(value);
      std::printf("TRT011 = 0x%02x", value);
      break;
    case 5:
      ierr = ltpi->SetTRT100(value);
      ierr = ltpi->GetTRT100(value);
      std::printf("TRT100 = 0x%02x", value);
      break;
    case 6: 
      ierr = ltpi->SetTRT101(value);
      ierr = ltpi->GetTRT101(value);
      std::printf("TRT101 = 0x%02x", value);
      break;
    case 7: 
      ierr = ltpi->SetTRT110(value);
      ierr = ltpi->GetTRT110(value);
      std::printf("TRT110 = 0x%02x", value);
      break;
    case 8: 
      ierr = ltpi->SetTRT111(value);
      ierr = ltpi->GetTRT111(value);
      std::printf("TRT111 = 0x%02x", value);
      break;
    }
    return(ierr); 
  }
};

//------------------------------------------------------------------------------
class FillAllTRTTable : public MenuItem {
public:
  FillAllTRTTable() { setName("Fill All Entries in Trigger Type Table"); } 
  int action() {
    int ierr = 0;

    std::printf("\n"); 
    ierr = ltpi->dumpTRTTable();
    std::printf("\n"); 

    int value = 0;
    std::printf("Enter Trigger Type sequentially \n");

    value = enterHex("Trigger Type word (0xff) for Test Trigger 000", 0, 0xff);
    ierr = ltpi->SetTRT000(value); 

    value = enterHex("Trigger Type word (0xff) for Test Trigger 001", 0, 0xff);
    ierr = ltpi->SetTRT001(value); 

    value = enterHex("Trigger Type word (0xff) for Test Trigger 010", 0, 0xff);
    ierr = ltpi->SetTRT010(value); 

    value = enterHex("Trigger Type word (0xff) for Test Trigger 011", 0, 0xff);
    ierr = ltpi->SetTRT011(value); 

    value = enterHex("Trigger Type word (0xff) for Test Trigger 100", 0, 0xff);
    ierr = ltpi->SetTRT100(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 101", 0, 0xff);
    ierr = ltpi->SetTRT101(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 110", 0, 0xff);
    ierr = ltpi->SetTRT110(value);

    value = enterHex("Trigger Type word (0xff) for Test Trigger 111", 0, 0xff);
    ierr = ltpi->SetTRT111(value);

    std::printf("\n"); 
    std::printf("Trigger Type Table:");
    ierr = ltpi->dumpTRTTable();
    std::printf("\n"); 

    return(ierr); 
  }
};

//------------------------------------------------------------------------------
class PrintTRTTable : public MenuItem {
public:
  PrintTRTTable() { setName("Print Trigger Type Table"); } 

  int action() {
    std::printf("\n"); 
    std::printf("Trigger Type Table:");
    int ierr = ltpi->dumpTRTTable();
    return(ierr);
  }
};

//------------------------------------------------------------------------------
class SetCalibrationPath : public MenuItem {
public:
  SetCalibrationPath() { setName("Set Calibration Path"); }

  int action() {
    int ierr = 0;

    int sel = enterInt("Select Calibration Request Path: 0-QUIT, 1-CTPout, 2-LTPout, 3-CTPout-LTPout", 0, 3);
    switch (sel)
      {
      case 0: break;
      case 1: ltpi->calCTPfromCTP_LTPFromCTP(); break;
      case 2: ltpi->calCTPfromLTP_LTPFromLTP(); break;
      case 3: ltpi->calCTPfromCTP_LTPFromLTP(); break;
      }

    return(ierr);
  }
};


//------------------------------------------------------------------------------
class CalibrationStatus : public MenuItem {
public:
  CalibrationStatus() { setName("Calibration Request Status"); }
  int action () {
  int ierr = ltpi->dumpCalibrationStatus();
  return(ierr);
  }
};


//------------------------------------------------------------------------------
class BusyStatus : public MenuItem {
public:
  BusyStatus() { setName("BUSY Status"); }
  int action () {
  int ierr = ltpi->dumpBusyStatus();
  return(ierr);
  }
};


//------------------------------------------------------------------------------
class SetBusyPath : public MenuItem {
public:
  SetBusyPath() { setName("Set Busy Path"); }

  int action() {
    int ierr = 0;
    int  sel = 0;
    int sel1 = 0;
    sel = enterInt("Redirect Busy to: 0-QUIT, 1-CTP, 2-NIM, 3-LTP", 0, 3);
    switch (sel) 
      {
      case 0: break;
      case 1:
	sel1 = enterInt("Accept Busy from: 0-OFF, 1-LTP, 2-CTP, 3-LTP&CTP", 0, 3);
	switch (sel1)
	  {
	  case 0: ierr |= ltpi->busyCTP_OFF(); break;
	  case 1: ierr |= ltpi->busyCTP_FROM_LTP(); break;
	  case 2: ierr |= ltpi->busyCTP_FROM_CTP(); break;
	  case 3: ierr |= ltpi->busyCTP_FROM_CTP_AND_LTP(); break;
	  }
	break;
      case 2:
	sel1 = enterInt("Select Busy input channel: 0-OFF, 1-LTP, 2-CTP, 3-LTP&CTP", 0, 3);
	switch (sel1)
	  {
	  case 0: ierr |= ltpi->busyNIM_OFF(); break;
	  case 1: ierr |= ltpi->busyNIM_FROM_LTP(); break;
	  case 2: ierr |= ltpi->busyNIM_FROM_CTP(); break;
	  case 3: ierr |= ltpi->busyNIM_FROM_CTP_AND_LTP(); break;
	  }
	break;
      case 3:
	sel1 = enterInt("Select Busy input channel: 0-OFF, 1-LTP, 2-CTP, 3-LTP&CTP", 0, 3);
	switch (sel1)
	  {
	  case 0: ierr |= ltpi->busyLTP_OFF(); break;
	  case 1: ierr |= ltpi->busyLTP_FROM_LTP(); break;
	  case 2: ierr |= ltpi->busyLTP_FROM_CTP(); break;
	  case 3: ierr |= ltpi->busyLTP_FROM_CTP_AND_LTP(); break;
	  }
	break;
      }
    return(ierr);
  }
};

class printStatusRegisterTable : public MenuItem {
public:
  printStatusRegisterTable() {setName("Print I/O signals status in a table");}
  int action() 
  {
    printf("\n");
    printf("The Following table presents the status of the different signals \n");
    printf("with the convention 1=ACTIVE signal, 0=NOT ACTIVE signal \n");
    printf("\n");
    printf("+--------------------------------------------------------+ \n");
    printf("| Signal | NIM input | CTP-Link output | LTP-Link output | \n");
    printf("+--------------------------------------------------------+ \n");
    printf("| BC      |        %d |               %d |               %d | \n", ltpi->isBC_NIM(), ltpi->isBC_CTPout(), ltpi->isBC_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| Orbit   |        %d |               %d |               %d | \n", ltpi->isORB_NIM(), ltpi->isORB_CTPout(), ltpi->isORB_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| L1A     |        %d |               %d |               %d | \n", ltpi->isL1A_NIM(), ltpi->isL1A_CTPout(), ltpi->isL1A_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TTR1    |        %d |               %d |               %d | \n", ltpi->isTTR1_NIM(), ltpi->isTTR1_CTPout(), ltpi->isTTR1_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TTR2    |        %d |               %d |               %d | \n", ltpi->isTTR2_NIM(), ltpi->isTTR2_CTPout(), ltpi->isTTR2_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TTR3    |        %d |               %d |               %d | \n", ltpi->isTTR3_NIM(), ltpi->isTTR3_CTPout(), ltpi->isTTR3_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| BGO0    |        %d |               %d |               %d | \n", ltpi->isBGO0_NIM(), ltpi->isBGO0_CTPout(), ltpi->isBGO0_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| BGO1    |        %d |               %d |               %d | \n", ltpi->isBGO1_NIM(), ltpi->isBGO1_CTPout(), ltpi->isBGO1_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| BGO2    |        %d |               %d |               %d | \n", ltpi->isBGO2_NIM(), ltpi->isBGO2_CTPout(), ltpi->isBGO2_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| BGO3    |        %d |               %d |               %d | \n", ltpi->isBGO3_NIM(), ltpi->isBGO3_CTPout(), ltpi->isBGO3_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| CAL0    |        - |               %d |               %d | \n", ltpi->isCAL0_CTPout(), ltpi->isCAL0_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| CAL1    |        - |               %d |               %d | \n", ltpi->isCAL1_CTPout(), ltpi->isCAL1_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| CAL2    |        - |               %d |               %d | \n", ltpi->isCAL2_CTPout(), ltpi->isCAL2_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT0    |        - |               %d |               %d | \n", ltpi->isTRT0_CTPout(), ltpi->isTRT0_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT1    |        - |               %d |               %d | \n", ltpi->isTRT1_CTPout(), ltpi->isTRT1_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT2    |        - |               %d |               %d | \n", ltpi->isTRT2_CTPout(), ltpi->isTRT2_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT3    |        - |               %d |               %d | \n", ltpi->isTRT3_CTPout(), ltpi->isTRT3_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT4    |        - |               %d |               %d | \n", ltpi->isTRT4_CTPout(), ltpi->isTRT4_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT5    |        - |               %d |               %d | \n", ltpi->isTRT5_CTPout(), ltpi->isTRT5_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT6    |        - |               %d |               %d | \n", ltpi->isTRT6_CTPout(), ltpi->isTRT6_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| TRT7    |        - |               %d |               %d | \n", ltpi->isTRT7_CTPout(), ltpi->isTRT7_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("| BUSY    |        - |               %d |               %d | \n", ltpi->isBUSY_CTPout(), ltpi->isBUSY_LTPout() );
    printf("+--------------------------------------------------------+ \n");
    printf("\n");
    return 0;
  }
};
  
class printStatusRegister : public MenuItem {
public:
  printStatusRegister() {setName("Read Status Register"); }
  int action()
  {
    string act;
    printf("\n");
    printf("\n");
    printf("OUTPUT on CTP: \n");
    ltpi->isBC_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BC on CTP is : %s \n",    act.c_str());
    ltpi->isORB_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("Orbit on CTP is : %s \n", act.c_str());
    ltpi->isL1A_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("L1A on CTP is : %s \n",   act.c_str());
    ltpi->isBUSY_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BUSY on CTP is : %s \n",  act.c_str());
    ltpi->isTTR1_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR1 on CTP is : %s \n",  act.c_str());
    ltpi->isTTR2_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR2 on CTP is : %s \n",  act.c_str());
    ltpi->isTTR3_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR3 on CTP is : %s \n",  act.c_str());
    ltpi->isCAL0_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("CAL0 on CTP is : %s \n",  act.c_str());
    ltpi->isCAL1_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("CAL1 on CTP is : %s \n",  act.c_str());
    ltpi->isCAL2_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("CAL2 on CTP is : %s \n",  act.c_str());
    ltpi->isBGO0_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO0 on CTP is : %s \n",  act.c_str());
    ltpi->isBGO1_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO1 on CTP is : %s \n",  act.c_str());
    ltpi->isBGO2_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO2 on CTP is : %s \n",  act.c_str());
    ltpi->isBGO3_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO3 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT0_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT0 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT1_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT1 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT2_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT2 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT3_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT3 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT4_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT4 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT5_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT5 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT6_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT6 on CTP is : %s \n",  act.c_str());
    ltpi->isTRT7_CTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT7 on CTP is : %s \n",  act.c_str());




    printf("\n");
    printf("\n");
    printf("OUTPUT on LTP: \n");
    ltpi->isBC_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BC on LTP is : %s \n",    act.c_str());
    ltpi->isORB_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("Orbit on LTP is : %s \n", act.c_str());
    ltpi->isL1A_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("L1A on LTP is : %s \n",   act.c_str());
    ltpi->isBUSY_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BUSY on LTP is : %s \n",  act.c_str());
    ltpi->isTTR1_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR1 on LTP is : %s \n",  act.c_str());
    ltpi->isTTR2_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR2 on LTP is : %s \n",  act.c_str());
    ltpi->isTTR3_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR3 on LTP is : %s \n",  act.c_str());
    ltpi->isCAL0_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("CAL0 on LTP is : %s \n",  act.c_str());
    ltpi->isCAL1_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("CAL1 on LTP is : %s \n",  act.c_str());
    ltpi->isCAL2_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("CAL2 on LTP is : %s \n",  act.c_str());
    ltpi->isBGO0_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO0 on LTP is : %s \n",  act.c_str());
    ltpi->isBGO1_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO1 on LTP is : %s \n",  act.c_str());
    ltpi->isBGO2_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO2 on LTP is : %s \n",  act.c_str());
    ltpi->isBGO3_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO3 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT0_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT0 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT1_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT1 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT2_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT2 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT3_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT3 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT4_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT4 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT5_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT5 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT6_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT6 on LTP is : %s \n",  act.c_str());
    ltpi->isTRT7_LTPout() == 1 ? act = "Active" : act = "Not Active";  
    printf("TRT7 on LTP is : %s \n",  act.c_str());

    printf("\n");
    printf("\n");
    printf("INPUTS from NIM: \n");
    ltpi->isBC_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("BC from NIM is : %s \n",    act.c_str());
    ltpi->isORB_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("Orbit from NIM is : %s \n", act.c_str());
    ltpi->isL1A_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("L1A from NIM is : %s \n",   act.c_str());
    ltpi->isTTR1_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR1 from NIM is : %s \n",  act.c_str());
    ltpi->isTTR2_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR2 from NIM is : %s \n",  act.c_str());
    ltpi->isTTR3_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("TTR3 from NIM is : %s \n",  act.c_str());
    ltpi->isBGO0_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO0 from NIM is : %s \n",  act.c_str());
    ltpi->isBGO1_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO1 from NIM is : %s \n",  act.c_str());
    ltpi->isBGO2_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO2 from NIM is : %s \n",  act.c_str());
    ltpi->isBGO3_NIM() == 1 ? act = "Active" : act = "Not Active";  
    printf("BGO3 from NIM is : %s \n",  act.c_str());
    printf("\n");
    printf("\n");
    return 0;
  }
};

//------------------------------------------------------------------------------
class statusBC : public MenuItem {
public:
  statusBC() {setName("BC Status"); }
  int action()
  {
    int status = 0;
    status = ltpi->printBC_status(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class BCCTP : public MenuItem {
public:
  BCCTP() {setName("CTP-Link out from CTP-Link in; LTP-Link out from CTP-Link in"); }
  int action()
  {
    int status =  ltpi->BC_CTPoutFromCTPin_LTPoutFromCTPin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class BCLTP : public MenuItem {
public:
  BCLTP() {setName("CTP-Link out from LTP-Link in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  ltpi->BC_CTPoutFromLTPin_LTPoutFromLTPin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class BCNIM : public MenuItem {
public:
  BCNIM() {setName("CTP-Link out from NIM in; LTP-Link out from NIM in"); }
  int action()
  {
    int status =  ltpi->BC_CTPoutFromNIMin_LTPoutFromNIMin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class BCCTPLTP : public MenuItem {
public:
  BCCTPLTP() {setName("CTP-Link out from CTP-Link in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  ltpi->BC_CTPoutFromCTPin_LTPoutFromLTPin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class BCNIMLTP : public MenuItem {
public:
  BCNIMLTP() {setName("CTP-Link out from NIM in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  ltpi->BC_CTPoutFromNIMin_LTPoutFromLTPin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class statusOrbit : public MenuItem {
public:
  statusOrbit() {setName("Orbit Status"); }
  int action()
  {
    int status = 0;
    status = ltpi->printOrbit_status();
    return status;
  }
};

//------------------------------------------------------------------------------
class OrbitCTP : public MenuItem {
public:
  OrbitCTP() {setName("CTP-Link out from CTP-Link in; LTP-Link out from CTP-Link in"); }
  int action()
  {
    int status =  ltpi->Orbit_CTPoutFromCTPin_LTPoutFromCTPin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class OrbitLTP : public MenuItem {
public:
  OrbitLTP() {setName("CTP-Link out from LTP-Link in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  ltpi->Orbit_CTPoutFromLTPin_LTPoutFromLTPin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class OrbitNIM : public MenuItem {
public:
  OrbitNIM() {setName("CTP-Link out from NIM in; LTP-Link out from NIM in"); }
  int action()
  {
    int status =  ltpi->Orbit_CTPoutFromNIMin_LTPoutFromNIMin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class OrbitCTPLTP : public MenuItem {
public:
  OrbitCTPLTP() {setName("CTP-Link out from CTP-Link in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  ltpi->Orbit_CTPoutFromCTPin_LTPoutFromLTPin(); 
    return status;
  }
};

//------------------------------------------------------------------------------
class OrbitNIMLTP : public MenuItem {
public:
  OrbitNIMLTP() {setName("CTP-Link out from NIM in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  ltpi->Orbit_CTPoutFromNIMin_LTPoutFromLTPin(); 
    return status;
  }
};


//------------------------------------------------------------------------------
class SelectChannel : public MenuItem {
public:
  SelectChannel() {setName("Select Channel"); }
  int action()
  {
    int status =  0;
    std::printf("\n");
    std::printf("*** Select Channel\n\n");
    std::printf("Current channel selection: %d \n", selchannel);
    
    int sel = enterInt("0-quit, 1-L1A, 2-Test Triggers, 3-BGos, 4-all",0,9);
    selchannel = sel;
    return status;
  }
};

//------------------------------------------------------------------------------
class statusChannel : public MenuItem {
public:
  statusChannel() {setName("Channel Status"); }
  int action()
  {
    int status =  0;
    switch (selchannel) {
    case 1: status |= ltpi->printL1A_status(); break;
    case 2: status |= ltpi->printTTR_status(); break;
    case 3: status |= ltpi->printBGO_status(); break;
    case 4:
          status |= ltpi->printL1A_status();
          status |= ltpi->printTTR_status();
          status |= ltpi->printBGO_status();
	  break;
    }
    return status;
  }
};

//------------------------------------------------------------------------------
class ChannelCTP : public MenuItem {
public:
  ChannelCTP() {setName("CTP-Link out from CTP-Link in; LTP-Link out from CTP-Link in"); }
  int action()
  {
    int status =  0;
    switch (selchannel) {
    case 1: status |= ltpi->L1A_CTPoutFromCTPin_LTPoutFromCTPin(); break;
    case 2: status |= ltpi->TTR_CTPoutFromCTPin_LTPoutFromCTPin(); break;
    case 3: status |= ltpi->BGO_CTPoutFromCTPin_LTPoutFromCTPin(); break;
    case 4: 
      status |= ltpi->L1A_CTPoutFromCTPin_LTPoutFromCTPin();
      status |= ltpi->TTR_CTPoutFromCTPin_LTPoutFromCTPin();
      status |= ltpi->BGO_CTPoutFromCTPin_LTPoutFromCTPin();
      break;    
    }
    return status;
  }
};

//------------------------------------------------------------------------------
class ChannelLTP : public MenuItem {
public:
  ChannelLTP() {setName("CTP-Link out from LTP-Link in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  0;
    switch (selchannel) {
    case 1: status |= ltpi->L1A_CTPoutFromLTPin_LTPoutFromLTPin(); break;
    case 2: status |= ltpi->TTR_CTPoutFromLTPin_LTPoutFromLTPin(); break;
    case 3: status |= ltpi->BGO_CTPoutFromLTPin_LTPoutFromLTPin(); break;
    case 4: 
      status |= ltpi->L1A_CTPoutFromLTPin_LTPoutFromLTPin();
      status |= ltpi->TTR_CTPoutFromLTPin_LTPoutFromLTPin();
      status |= ltpi->BGO_CTPoutFromLTPin_LTPoutFromLTPin();
      break;    
    }
    return status;
  }
};

//------------------------------------------------------------------------------
class ChannelNIM : public MenuItem {
public:
  ChannelNIM() {setName("CTP-Link out from NIM in; LTP-Link out from NIM in"); }
  int action()
  {
    int status =  0;
    switch (selchannel) {
    case 1: status |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromNIMin(); break;
    case 2: status |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromNIMin(); break;
    case 3: status |= ltpi->BGO_CTPoutFromNIMin_LTPoutFromNIMin(); break;
    case 4: 
      status |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromNIMin();
      status |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromNIMin();
      status |= ltpi->BGO_CTPoutFromNIMin_LTPoutFromNIMin();
      break;    
    }
    return status;
  }
};

//------------------------------------------------------------------------------
class ChannelCTPLTP : public MenuItem {
public:
  ChannelCTPLTP() {setName("CTP-Link out from CTP-Link in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int status =  0; 
    switch (selchannel) {
    case 1: status = ltpi->L1A_CTPoutFromCTPin_LTPoutFromLTPin(); break;
    case 2: status = ltpi->TTR_CTPoutFromCTPin_LTPoutFromLTPin(); break;
    case 3: status = ltpi->BGO_CTPoutFromCTPin_LTPoutFromLTPin(); break;
    case 4: 
      status = ltpi->L1A_CTPoutFromCTPin_LTPoutFromLTPin();
      status = ltpi->TTR_CTPoutFromCTPin_LTPoutFromLTPin();
      status = ltpi->BGO_CTPoutFromCTPin_LTPoutFromLTPin();
      break;    
    }
    return status;
  }
};

//------------------------------------------------------------------------------
class ChannelNIMLTP : public MenuItem {
public:
  ChannelNIMLTP() {setName("CTP-Link out from NIM in; LTP-Link out from LTP-Link in"); }
  int action()
  {
    int ierr = 0;
    switch (selchannel) {
    case 1: ierr |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromLTPin(); break;
    case 2: ierr |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromLTPin(); break;
    case 3: ierr |= ltpi->BGO_CTPoutFromNIMin_LTPoutFromLTPin(); break;
    case 4: 
      ierr |= ltpi->L1A_CTPoutFromNIMin_LTPoutFromLTPin();
      ierr |= ltpi->TTR_CTPoutFromNIMin_LTPoutFromLTPin();
      ierr |= ltpi->BGO_CTPoutFromNIMin_LTPoutFromLTPin();
      break;    
    }
    return ierr;
  }
};

//------------------------------------------------------------------------------
class moduleStatus : public MenuItem {
public:
  moduleStatus() {setName("Print Module Status"); }
  int action()
  {
    int ierr = 0;
    ierr |= ltpi->printI2Cfreq();
    ierr |= ltpi->printBC_status();
    ierr |= ltpi->printOrbit_status();
    ierr |= ltpi->printL1A_status();
    ierr |= ltpi->printTTR_status();
    ierr |= ltpi->printBGO_status();
    ierr |= ltpi->dumpTRTinput();
    ierr |= ltpi->dumpTRTStatus(); 
    ierr |= ltpi->dumpTRTTable();
    ierr |= ltpi->dumpCalibrationStatus();
    ierr |= ltpi->dumpBusyStatus(); 
    return ierr;
  }
};

//------------------------------------------------------------------------------
class dumpModuleCR : public MenuItem {
public:
  dumpModuleCR() {setName("Print Module Registers"); }
  int action()
  {
    return ltpi->dumpModuleRegisters();
  }
};

//------------------------------------------------------------------------------

class readBaseAddr : public MenuItem {
public:
  readBaseAddr() { setName("Read the base address from the serial flash"); }
  int action()
  {
    int rtnv;
    u_short addr;
    if ((rtnv = ltpi->read_base_addr(addr))) {
      printf("failed to read base address, return status = %d\n", rtnv);
      return rtnv;
    }
    printf("Base address = 0x%04x00\n", addr);
    return rtnv;
  }
};

//------------------------------------------------------------------------------

class writeBaseAddr : public MenuItem {
public:
  writeBaseAddr() { setName("Write the base address to the serial flash"); }
  int action()
  {
    int rtnv = 0;
    u_short addr = enterHex("Base address (hex)", 0, 0xffff);
    int cont = enterInt("Write new base address? (1-yes,0-abort)", 0, 1);
    if (!cont) return 0;
    if ((rtnv = ltpi->write_base_addr(addr))) {
      printf("failed to write base address, return status = %d\n", rtnv);
      return rtnv;
    } 
    printf("wrote new base address 0x%04x00\n", addr);
    return rtnv;
  }
};

//------------------------------------------------------------------------------
int main(int argc, char* argv[]) {

  CmdArgStr  base_cmd('a', "address", "vme-base-address",  "VME base address in hex", CmdArg::isOPT);
//   CmdArgBool expert_cmd('e', "expert", "expert mode", CmdArg::isOPT | CmdArg::isHIDDEN);
  CmdArgBool expert_cmd('e', "expert", "expert mode", CmdArg::isOPT);

  base_cmd = "";
  expert_cmd = false;


  CmdLine  cmd(*argv, &base_cmd, &expert_cmd, nullptr);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.description("Command-line driven program to communicate with an LTPi board");
  cmd.parse(arg_iter);

  if (base_cmd=="") {
    // no argument given, ask for base address
    std::string in;
    std::cout << "<base>?" << std::endl;
    getline(cin,in);
    if(sscanf(in.c_str(),"%x",&base) == EOF) exit(EINVAL);
    if(base < 0x100) base = base << 16;
  } else {
    // get first argument, the base address
    std::string in(base_cmd);
    sscanf(argv[1], "%x", &base);
    if(base < 0x100) base = base << 16;
  }

  expert = expert_cmd;

  std::cout << std::endl;
  if (expert) {
    std::cout << ">> started in expert mode" << endl;
  }
  printf(">> vme base address = 0x%06x\n", base);
  std::cout << std::endl;
  

  // set defaults:
  // set default filename
  conffile = "conf.dat";

  Menu	menu("LTPi Menu");
  
  // generate menu
  menu.init(new LTPOpen);
  
  Menu menuStatus("Module Global configuration");
  menuStatus.add(new moduleStatus);
  menuStatus.add(new dumpModuleCR);
  menuStatus.add(new DumpToFile);
  menuStatus.add(new ConfigureFromFile);
  menu.add(&menuStatus);

  Menu menuCases("Predefined Modes/Actions");
  menuCases.add(new OFF_Mode);
  menuCases.add(new CTP_LTP_Mode);
  menuCases.add(new CTP_Mode);
  menuCases.add(new LTP_Mode);
  menuCases.add(new NIM_Mode);
  menuCases.add(new NIM_LTP_Mode);
  menu.add(&menuCases);

  Menu menuBC("Bunch Clock");
  menuBC.add(new statusBC);
  menuBC.add(new BCCTP);
  menuBC.add(new BCLTP);
  menuBC.add(new BCNIM);
  menuBC.add(new BCCTPLTP);
  menuBC.add(new BCNIMLTP);
  menu.add(&menuBC);

  Menu menuOrbit("Orbit");
  menuOrbit.add(new statusOrbit);
  menuOrbit.add(new OrbitCTP);
  menuOrbit.add(new OrbitLTP);
  menuOrbit.add(new OrbitNIM);
  menuOrbit.add(new OrbitCTPLTP);
  menuOrbit.add(new OrbitNIMLTP);
  menu.add(&menuOrbit);

 Menu menuChannelPath("Select Channel Path: L1A, Test Trigger, BGO");
 menuChannelPath.add(new SelectChannel);
 menuChannelPath.add(new statusChannel);
 menuChannelPath.add(new ChannelCTP);
 menuChannelPath.add(new ChannelLTP);
 menuChannelPath.add(new ChannelNIM);
 menuChannelPath.add(new ChannelCTPLTP);
 menuChannelPath.add(new ChannelNIMLTP);
 menu.add(&menuChannelPath);


  Menu menuDelay("CTP-Link out");
  menuDelay.add(new enableCTPout);
  menuDelay.add(new SetupDelay25);
  menuDelay.add(new ReadDelay25);
  menuDelay.add(new SetupDelay25SingleChannel);
  menuDelay.add(new ReadDelay25SingleChannel);
  menuDelay.add(new ResyncDelay25DLL);
  menu.add(&menuDelay);

  Menu menuBusy("Busy");
  menuBusy.add(new BusyStatus());
  menuBusy.add(new SetBusyPath());
  menu.add(&menuBusy);

  Menu menuCal("Calibration Request");
  menuCal.add(new CalibrationStatus());
  menuCal.add(new SetCalibrationPath());
  menu.add(&menuCal);
  
  Menu menuTRType("Trigger Type");
  menuTRType.add(new TRTStatus);
  menuTRType.add(new TRT_inputSelection);
  menuTRType.add(new FillInitReg);
  menuTRType.add(new SelectTRTPath);
  menuTRType.add(new SelectTRTInput);
  menuTRType.add(new PrintTRTTable);
  menuTRType.add(new FillTRTTable);
  menuTRType.add(new FillAllTRTTable);
  menu.add(&menuTRType);

  Menu menuSt("Signals Status");
  menuSt.add(new printStatusRegister);
  menuSt.add(new printStatusRegisterTable);
  menu.add(&menuSt);

  menu.add(new LTPReset);

  Menu menuI2C("I2C");
  menuI2C.add(new resetI2C);
  menuI2C.add(new setupI2C);
  menuI2C.add(new I2CWrite);
  menuI2C.add(new I2CRead);
  menuI2C.add(new I2CCheckFrequency);
  menu.add(&menuI2C);

  Menu menuDAC("DAC");
  menuDAC.add(new PrintDAC);
  //menuDAC.add(new SetupDACShortCables);
  //menuDAC.add(new SetupDACLongCables);
  menuDAC.add(new SetDACCTPcable);
  menuDAC.add(new SetDACLTPcable);
  if (expert) menuDAC.add(new SetDACLTPgain);
  if (expert) menuDAC.add(new SetDACCTPgain);
  if (expert) menuDAC.add(new SetDACLTPequ);
  if (expert) menuDAC.add(new SetDACCTPequ);
  menu.add(&menuDAC);
  menu.add(new readBaseAddr());
  
  if (expert) menu.add(new writeBaseAddr());
  if (expert) menu.add(new WriteProm);


  menu.exit(new LTPiClose);

  // start menu
  menu.execute();

  return 0;
}
