#ifndef TEST_TESTLTPI_H
#define TEST_TESTLTPI_H

#include <cstdlib>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <list>
#include <cerrno>
#include <string>
#include <vector>
#include <cmath>
#include <bitset>
#include <sstream>
#include <time.h>
#include <unistd.h>
#include <pwd.h>

#include "RCDUtilities/RCDUtilities.h"
#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDVme.h"
#include "DFDebug/DFDebug.h"
#include "RCDLtpi/LTPI.h"
#include "RCDLtp/RCDLtp.h"

using namespace RCD;
using namespace std;

//----------------------------------------------------------------------------------------------------------

typedef enum { MASTER_CTP, MASTER_LTP, SLAVE_CTP, SLAVE_LTP }LTP_TYPE;
static const unsigned int LTP_TYPE_NUMBER = SLAVE_LTP + 1;
static const string LTP_TYPE_NAME[LTP_TYPE_NUMBER]={"Master CTP", "Master LTP", "Slave CTP", "Slave LTP"};

//----------------------------------------------------------------------------------------------------------

class LTPextension : public LTP
{
 public:
  LTPextension() : m_status(0), m_last_pg_file("") {}
  ~LTPextension(){}
   
  // used in ConnectionTest:
  int LTPCounterSource( LTPI::SIGNAL );  // and in TTYP test
  int SetupSender( string );             // also in Delay Scan Test
  int SetupReceiver( int );
  
  // used in BusyTest:
  int setupLTPForBusy( bool );

  // used in TriggerTypeTesr
  int LTPFifoDelay( int );
  int setupSenderForTTYP( string ); // also in Delay Scan Test
  int setupReceiverForTTYP( int );
  
  int LoadPatGenFile(const string &); // see if it can be REPAIRED
  
  // used in calibration request 
  int setupMasterForCalreq();
  int setupSlaveForCalreq();
  
 private:
  int m_status; 
  string m_last_pg_file;
};

//--------------------------------------------------------------------------------------------------------

class MainProgram : public MenuItem
{
 public:
  MainProgram()  : m_input_filename("test_ltpi.in"), m_vme(0), m_base_ltpi(0xee1000), 
                   m_base_ltpi_helper(0xee2000), m_ltpi(0), m_ltpi_helper(0), 
                   m_status(0), m_ltpi_cfg(0), m_ltpi_helper_cfg(0),
                   m_sleep_period(1), m_ttyp_iter(1), m_output_filename(""),
                   m_pg_filename("pg_rand.dat"), m_pg_ttyp_ext("pg_trt.dat"), m_pg_ttyp_loc("pg1.dat")
   {
    setName("Main Program");
     
    m_vme = VME::Open(); 
   
    for(unsigned int i=0; i<LTP_TYPE_NUMBER; i++) 
      m_ltp[i]=0;

    m_base_ltp[0] = 0xff1000;
    m_base_ltp[1] = 0xff3000;
    m_base_ltp[2] = 0xff2000;
    m_base_ltp[3] = 0xff4000;
   }
  
  ~MainProgram()
   {
    for(unsigned int i=0; i<LTP_TYPE_NUMBER; i++) if(m_ltp[i]) delete m_ltp[i];
    if(m_ltpi_helper_cfg) delete m_ltpi_helper_cfg;
    if(m_ltpi_cfg)        delete m_ltpi_cfg;
    if(m_ltpi_helper)     delete m_ltpi_helper;
    if(m_ltpi)            delete m_ltpi;
    VME::Close();
   }
  
  int                  action(); 
  LTPI*                getLTPI()         const { return m_ltpi        ; }
  LTPI*                getLTPIhelper()   const { return m_ltpi_helper ; }
  LTPextension*        getLTP( int t )   const { return m_ltp[t]      ; }
  LTPI::Configuration* getLTPIConfig()   const { return m_ltpi_cfg    ; }
  unsigned int         getSleepPeriod()  const { return m_sleep_period; }
  void                 setSleepPeriod(unsigned int p) {m_sleep_period = p; }
  unsigned int         getTTYPiter()     const { return m_ttyp_iter;}
  void                 setTTYPiter(  unsigned int p ) {m_ttyp_iter = p;}
  int                  checkForFile( string );
  string               getPGFile()       const { return m_pg_filename; }
  string               getPGForTTYP(int l)    const { if(l<0) return  m_pg_ttyp_ext; else return m_pg_ttyp_loc; }
  
 private:
  // Data members
  string               m_input_filename;
  VME*                 m_vme;
  u_int                m_base_ltpi;
  u_int                m_base_ltpi_helper;
  LTPI*                m_ltpi;
  LTPI*                m_ltpi_helper;
  int                  m_status;
  LTPI::Configuration* m_ltpi_cfg;
  LTPI::Configuration* m_ltpi_helper_cfg;
  unsigned int         m_sleep_period;
  unsigned int         m_ttyp_iter;
  string               m_output_filename;
  string               m_pg_filename;
  string               m_pg_ttyp_ext;
  string               m_pg_ttyp_loc;
  LTPextension*        m_ltp[LTP_TYPE_NUMBER];
  u_int                m_base_ltp[LTP_TYPE_NUMBER];
  static const unsigned int DEFAULT_DELAY = 36;
  static const string MON[12];
  
  void printInfo(  ostream& = cout ) const;
  int  readInputFile(  ostream& = cout );
  void tokenize( const string& str, vector<string>& tokens, const string& delimiters = " " ); 
};

//--------------------------------------------------------------------------------------------------

const string MainProgram::MON[12] 
 = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

//--------------------------------------------------------------------------------------------------------

class Test : public MenuItem
{
 public:
  Test(MainProgram* prog, const char *str) : m_prog(prog), m_status(0) {setName(str);}
  virtual ~Test() {}
  
  static const int SUCCESS = 0;
  static const int ERROR = -1;
  static const int ERROR_TIMEOUT = -2;
  
 protected:
  int readAndCompareCounters( LTPextension* , LTPextension* , LTPI::SIGNAL , int& );
  int testSignal( LTPextension* , LTPextension* , LTPI::SIGNAL ); 
  int readAndCompareFIFO( int, int , LTPextension*, LTPextension* );
  int setTable( int, LTPextension* );
  int testTTYPsignal( LTPextension* , LTPextension* , int );
  
  MainProgram*              m_prog;
  int                       m_status;
  static const unsigned int TIMEOUT = 100000;
  //static const unsigned int TTYP_INDEX_NUMBER = 4;
};

//--------------------------------------------------------------------------------------------------------

class printConfiguration : public Test
{ 
public: 
  printConfiguration( MainProgram* prog) : Test(prog, "Print configuration") {}
  virtual ~printConfiguration() {}
  int action();
};

  
class LongShortCable : public Test
{
 public:  
  LongShortCable( MainProgram* prog ) : Test(prog, "Set cable length") {}
  virtual ~LongShortCable() {}
  int action();
};


class SetDelayTime : public Test
{
 public:  
  SetDelayTime( MainProgram* prog ) : Test(prog, "Set delay time") {} 
  virtual ~SetDelayTime() {} 
  int action(); 
};


class SetSleepPeriod : public Test
{
 public: 
  SetSleepPeriod( MainProgram* prog ) : Test(prog, "Set sleep period") {}
  virtual ~SetSleepPeriod() {}
  int action();
};


class SetTTYPiteration : public Test
{
 public:  
  SetTTYPiteration( MainProgram* prog ) : Test(prog, "Set TTYP iteration number") {}
  virtual ~SetTTYPiteration() {}
  int action();
};

//--------------------------------------------------------------------------------------------------------

class CalibrationTest : public Test
{
 public:
  CalibrationTest( MainProgram* prog, const char *str, unsigned int c) : Test( prog, str ), m_cal(c) {}
  virtual ~CalibrationTest(){}
  void subTest( LTPI::CALREQ_SELECTION ); // TODO: make int
  int  action();
  
 private:
  unsigned int m_cal;
  unsigned int m_result[LTPI::CALREQ_SELECTION_NUMBER][8][2];
  void printTableHeader( ostream& = cout ) const ;
  void printTableLine( LTPI::CALREQ_SELECTION, ostream& = cout ) const ;
  static const string desc[LTPI::CALREQ_SELECTION_NUMBER][2];
};

//--------------------------------------------------------------------------------------------------------

class BusyTest : public Test
{
 public:
  BusyTest( MainProgram* prog, const char *str, unsigned int b ) : Test( prog, str ), m_busy(b) {}
  virtual ~BusyTest(){}
  int subTest( LTPI::BUSY_SELECTION );
  int action();
  
 private:
  void printTableHeader( ostream& = cout ) const ;
  void printTableLine( LTPI::BUSY_SELECTION, ostream& = cout ) const ;
  int checkBusyStatus( bool, bool, bool );
  int enableBusy( LTP_TYPE, bool ); 
  int setLTPIBusySelection( LTPI::BUSY_SELECTION, LTPI::BUSY_SELECTION, LTPI::BUSY_SELECTION );
  unsigned int m_busy;
  bool m_result[LTPI::BUSY_SELECTION_NUMBER][LTPI::LINK_NUMBER][3];
};

//-------------------------------------------------------------------------------------------------------

class ConnectionTest : public Test
{
 public:
  ConnectionTest( MainProgram* prog, const char *str, unsigned int s ) : Test( prog, str ), m_sig(s) {}
  virtual ~ConnectionTest() {}
  void subTest( LTPI::SIGNAL_SELECTION ); // TODO: make int
  int  action();
  
 private:
  unsigned int m_sig;
  bool m_result[LTPI::SELECT_SIGNAL_NUMBER][3][LTPI::SIGNAL_NUMBER];
  void printTableHeader( ostream& = cout ) const ;
  void printTableLine( LTPI::SIGNAL_SELECTION, ostream& = cout ) const;
  static const string desc[LTPI::SIGNAL_SELECTION_NUMBER][3];
};

//--------------------------------------------------------------------------------------------------------

class TriggerTypeTest : public Test
{
 public:
  TriggerTypeTest( MainProgram* prog, const char *str, LTPI::TTYP_SELECTION m0, int m1 , bool a) : 
    Test( prog, str ),  m_ttyp_ext(m0), m_ttyp_loc(m1), m_all(a) {}
  virtual ~TriggerTypeTest() {}
  int subTest( LTPI::TTYP_SELECTION, int );
  int action();
  
 private:
  LTPI::TTYP_SELECTION m_ttyp_ext;
  int                  m_ttyp_loc;
  bool                 m_all;
  int testTTYPFromLocalRegister( LTPextension* );
};

//---------------------------------------------------------------------------------------------------------

class DelayScan : public Test
{
 public: 
  DelayScan( MainProgram *prog, const char *str, unsigned int m1, unsigned int m2, bool m3) 
    : Test( prog, str ), sig(m1), grp(m2), all_combinations(m3) {}
  virtual ~DelayScan() {}
  void subTest( LTP_TYPE, LTP_TYPE, unsigned int, bool ); // TODO: make int
  int action();
  
 private:
  static const u_short DELAY_MAX = 50;
  bool m_result[LTPI::DELAY_SIGNAL_NUMBER-7][DELAY_MAX+1]; // ( "-7" because of the trigger types )
  void printResults(  unsigned int, unsigned int, LTP_TYPE, LTP_TYPE, ostream& = cout ) const;
  void singleCombination( LTP_TYPE, LTP_TYPE );
  unsigned int sig;
  unsigned int grp;
  bool all_combinations;
};

//--------------------------------------------------------------------------------------------------------

class SuperTest : public Test
{
 public: 
  SuperTest( MainProgram *prog ) : Test(prog, "Super Test") {}
  virtual ~SuperTest() {}
  int action();
}; 

#endif
