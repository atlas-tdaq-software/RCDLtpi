#ifndef LTPI_H
#define LTPI_H

//******************************************************************************
// file: LTPI.h
// desc: library for LTPI module
// auth: 08-JUL-2013, R. Spiwoks (rewrite from exisiting RCDLtpi.h)
//******************************************************************************

#include <string>
#include <iostream>
#include "RCDVme/RCDVme.h"

namespace RCD {

class LTPI {

  public:

    // public type declarations

    // general modes
    typedef enum                { MODE_DISABLED, MODE_FROM_CTP, MODE_FROM_LTP, MODE_FROM_NIM, MODE_PARALLEL, MODE_PARALLEL_FROM_NIM } MODE_SELECTION;
    static const unsigned int   MODE_SELECTION_NUMBER = MODE_PARALLEL_FROM_NIM + 1;
    static const std::string    MODE_SELECTION_NAME[MODE_SELECTION_NUMBER];

    // general signals
    typedef enum                { ORB, L1A, TTRG1, TTRG2, TTRG3, BGO0, BGO1, BGO2, BGO3 } SIGNAL;
    static const unsigned int   SIGNAL_NUMBER = BGO3 + 1;
    static const std::string    SIGNAL_NAME[SIGNAL_NUMBER];
    static const unsigned int   TTRG_NUMBER = 3;
    static const unsigned int   BGO_NUMBER = 4;
    static const unsigned int   TTYP_NUMBER = 8;
    static const unsigned int   CALREQ_NUMBER = 3;

    // signal (BC, ORB, L1A, TTRG, BGO)  selection
    typedef enum                { SELECT_BC, SELECT_ORB, SELECT_L1A, SELECT_TTRG, SELECT_BGO } SELECT_SIGNAL;
    static const unsigned int   SELECT_SIGNAL_NUMBER = SELECT_BGO + 1;
    static const std::string    SELECT_SIGNAL_NAME[SELECT_SIGNAL_NUMBER];

    typedef enum                { SIGNAL_DISABLED, SIGNAL_PARALLEL, SIGNAL_FROM_CTP, SIGNAL_FROM_LTP, SIGNAL_FROM_NIM, SIGNAL_PARALLEL_FROM_NIM } SIGNAL_SELECTION;
    static const unsigned int   SIGNAL_SELECTION_NUMBER = SIGNAL_PARALLEL_FROM_NIM + 1;
    static const std::string    SIGNAL_SELECTION_NAME[SIGNAL_SELECTION_NUMBER];
    static const std::string    SIGNAL_SELECTION_DESC[SIGNAL_SELECTION_NUMBER];

    // TTYP and local TTYP selection
    typedef enum                { TTYP_DISABLED, TTYP_PARALLEL, TTYP_FROM_CTP, TTYP_FROM_LTP, TTYP_FROM_LOCAL, TTYP_PARALLEL_FROM_LOCAL } TTYP_SELECTION;
    static const unsigned int   TTYP_SELECTION_NUMBER = TTYP_PARALLEL_FROM_LOCAL + 1;
    static const std::string    TTYP_SELECTION_NAME[TTYP_SELECTION_NUMBER];
    static const std::string    TTYP_SELECTION_DESC[TTYP_SELECTION_NUMBER];

    typedef enum                { LOCTTYP_FROM_TTYP_WORD0, LOCTTYP_FROM_CTP_TTRG, LOCTTYP_FROM_LTP_TTRG, LOCTTYP_FROM_NIM_TTRG } LOCTTYP_SELECTION;
    static const unsigned int   LOCTTYP_SELECTION_NUMBER = LOCTTYP_FROM_NIM_TTRG + 1;
    static const std::string    LOCTTYP_SELECTION_NAME[LOCTTYP_SELECTION_NUMBER];
    static const unsigned int   LOCTTYP_NUMBER = 8;
    static const u_short        LOCTTYP_MASK = 0x00ff;

    // CALREQ selection
    typedef enum                { CALREQ_DISABLED, CALREQ_PARALLEL, CALREQ_FROM_CTP, CALREQ_FROM_LTP } CALREQ_SELECTION;
    static const unsigned int   CALREQ_SELECTION_NUMBER = CALREQ_FROM_LTP + 1;
    static const std::string    CALREQ_SELECTION_NAME[CALREQ_SELECTION_NUMBER];
    static const std::string    CALREQ_SELECTION_DESC[CALREQ_SELECTION_NUMBER];

    // link definition
    typedef enum                { CTP, LTP, NIM } LINK;
    static const unsigned int   LINK_NUMBER = NIM + 1;
    static const std::string    LINK_NAME[LINK_NUMBER];

    // BUSY selection
    typedef enum                { BUSY_DISABLED, BUSY_FROM_CTP, BUSY_FROM_LTP, BUSY_FROM_CTP_OR_LTP } BUSY_SELECTION;
    static const unsigned int   BUSY_SELECTION_NUMBER = BUSY_FROM_CTP_OR_LTP + 1;
    static const std::string    BUSY_SELECTION_NAME[BUSY_SELECTION_NUMBER];
    static const std::string    BUSY_SELECTION_DESC[BUSY_SELECTION_NUMBER];

    // DELAY selection
    typedef enum                { DELAY_ORB, DELAY_L1A, DELAY_TTRG1, DELAY_TTRG2, DELAY_TTRG3, DELAY_BGO0, DELAY_BGO1, DELAY_BGO2, DELAY_BGO3,
                                  DELAY_TTYP0, DELAY_TTYP1, DELAY_TTYP2, DELAY_TTYP3, DELAY_TTYP4, DELAY_TTYP5, DELAY_TTYP6, DELAY_TTYP7 } DELAY_SIGNAL;
    static const unsigned int   DELAY_SIGNAL_NUMBER = DELAY_TTYP7 + 1;
    static const std::string    DELAY_SIGNAL_NAME[DELAY_SIGNAL_NUMBER];
    typedef enum                { DELAY_GROUP_ORB, DELAY_GROUP_L1A, DELAY_GROUP_TTRG, DELAY_GROUP_BGO, DELAY_GROUP_TTYP } DELAY_GROUP;
    static const unsigned int   DELAY_GROUP_NUMBER = DELAY_GROUP_TTYP + 1;
    static const std::string    DELAY_GROUP_NAME[DELAY_GROUP_NUMBER];
    static const unsigned int   DELAY_GROUP_LO[DELAY_GROUP_NUMBER];
    static const unsigned int   DELAY_GROUP_HI[DELAY_GROUP_NUMBER];
    static const u_short        DELAY_MAXIMUM = 60;
    static const double         DELAY_STEP;

    // STATUS definitions
    typedef enum                { STATUS_BC, STATUS_ORB, STATUS_L1A, STATUS_TTRG1, STATUS_TTRG2, STATUS_TTRG3,
                                  STATUS_BGO0, STATUS_BGO1, STATUS_BGO2, STATUS_BGO3,
                                  STATUS_TTYP0, STATUS_TTYP1, STATUS_TTYP2, STATUS_TTYP3, STATUS_TTYP4, STATUS_TTYP5, STATUS_TTYP6, STATUS_TTYP7,
                                  STATUS_CALREQ0, STATUS_CALREQ1, STATUS_CALREQ2, STATUS_BUSY } STATUS_SIGNAL;
    static const unsigned int   STATUS_SIGNAL_NUMBER = STATUS_BUSY + 1;
    static const std::string    STATUS_SIGNAL_NAME[STATUS_SIGNAL_NUMBER];

    // EQUALIZER selection
    typedef enum                { EQUAL_CTP, EQUAL_LTP } EQUAL_LINK;
    static const unsigned int   EQUAL_LINK_NUMBER = EQUAL_LTP + 1;
    static const std::string    EQUAL_LINK_NAME[EQUAL_LINK_NUMBER];

    static const u_short        EQUAL_MAXIMUM = 0x00ff;
    static const double         EQUAL_LENGTH;

    // print selection
    typedef enum                { PRINT_SIG, PRINT_TTYP, PRINT_CALREQ, PRINT_BUSY, PRINT_ALL } PRINT_SELECTION;

    // I2C default frequency
    static const double         I2C_DEFAULT_FREQUENCY;

    // EEPROM definitions
    static const unsigned int   EEPROM_MANUF_MASK       = 0x00ffffff;
    static const unsigned int   EEPROM_BOARD_MASK       = 0xffffffff;
    static const unsigned int   EEPROM_REVISION_MASK    = 0xffffffff;

    // serial flash definition
    static const u_short        SFL_BASE_MASK           = 0xffff;

    // return codes
    static const int            SUCCESS                 = 0;
    static const int            ERROR                   = -1;
    static const int            ERROR_NOTEXIST          = -2;
    static const int            ERROR_INVALID           = -3;
    static const int            ERROR_TIMEOUT           = -4;

    // time-out value
    static const unsigned int   TIMEOUT = 500000;

    // public type definition: configuration class
    class Configuration {
      public:

        // public constructor and destructor
        Configuration();
       ~Configuration();

        // link equalization
        bool                    equal_enable[EQUAL_LINK_NUMBER];
        bool                    equal_short_cable[EQUAL_LINK_NUMBER];

        // signal selection
        SIGNAL_SELECTION        signal_select[SELECT_SIGNAL_NUMBER];
        TTYP_SELECTION          ttyp_select;
        LOCTTYP_SELECTION       locttyp_select;
        u_short                 locttyp_word[LOCTTYP_NUMBER];
        CALREQ_SELECTION        calreq_select;
        BUSY_SELECTION          busy_select[LINK_NUMBER];

        // delay/enable selection
        bool                    ctpout_enable[DELAY_SIGNAL_NUMBER];
        u_short                 ctpout_delay[DELAY_SIGNAL_NUMBER];

        // methods
        void reset();
        void print(std::ostream& = std::cout) const;
        int read(const std::string&);
        int write(const std::string&) const;
        void setMode(const MODE_SELECTION);
        void setSignalDelay(const DELAY_GROUP, const u_short);
        void setSignalEnable(const DELAY_GROUP, const bool);
        void setSignalDelay(const u_short);
        void setSignalEnable(const bool);

      private:

        // private method
        static int readNextRecord(std::istream&, std::string&, std::string&);
    };

    // public constructor and destructor
    LTPI(RCD::VME*, unsigned int);
   ~LTPI();

    // VME methods
    RCD::VME* getVme() const                    { return(m_vme); }
    RCD::VMEMasterMap* getVmeMasterMap() const  { return(m_vmm); }
    unsigned int getBaseAddress() const         { return(m_base); }
    int operator()() const                      { return(m_status); }
    static unsigned int addr()                  { return(VMM_ADDR); }
    static unsigned int size()                  { return(VMM_SIZE); }
    static unsigned int amod()                  { return(VMM_AMOD); }
    static std::string code()                   { return(VMM_CODE); }
    static void printSignalSelectionLegend(const PRINT_SELECTION, std::ostream& = std::cout);

    // reset
    int reset();
    int ready(bool&);
    int init();

    // configuration methods
    int read(Configuration&);
    int readLinkEqualization(Configuration&);
    int readSignalSelection(Configuration&);
    int readSignalDelay(Configuration&);
    int write(const Configuration&);
    int writeLinkEqualization(const Configuration&);
    int writeSignalSelection(const Configuration&);
    int writeSignalDelay(const Configuration&);

    // read/write signal configuration methods
    int readSignalSelection(const SELECT_SIGNAL, SIGNAL_SELECTION&);
    int writeSignalSelection(const SELECT_SIGNAL, const SIGNAL_SELECTION);
    int writeSignalSelection(const SIGNAL_SELECTION);

    int readTriggerTypeSelection(TTYP_SELECTION&, LOCTTYP_SELECTION&);
    int writeTriggerTypeSelection(const TTYP_SELECTION, const LOCTTYP_SELECTION);
    int readLocalTriggerTypeWord(const unsigned int, u_short&);
    int writeLocalTriggerTypeWord(const unsigned int, const u_short);

    int readCalibrationRequestSelection(CALREQ_SELECTION&);
    int writeCalibrationRequestSelection(const CALREQ_SELECTION);

    int readBusySelection(const LINK, BUSY_SELECTION&);
    int writeBusySelection(const LINK, const BUSY_SELECTION);

    int printSignalSelection(std::ostream& = std::cout);

    int readSignalStatus(const STATUS_SIGNAL, const LINK, bool&);

    int readSignalDelay(const DELAY_SIGNAL, u_short&, bool&);
    int writeSignalDelay(const DELAY_SIGNAL, const u_short, const bool = true);
    int writeSignalDelay(const DELAY_GROUP, const u_short, const bool = true);
    int writeSignalDelay(const u_short, const bool = true);
    int writeSignalEnable(const DELAY_SIGNAL, const bool);
    int writeSignalEnable(const DELAY_GROUP, const bool);
    int writeSignalEnable(const bool);
    int printSignalDelay(std::ostream& = std::cout);

    int readLinkEqualization(const EQUAL_LINK, u_short&, u_short&);
    int writeLinkEqualization(const EQUAL_LINK, const u_short, const u_short);
    int readLinkEqualization(const EQUAL_LINK, bool&, bool&);
    int writeLinkEqualization(const EQUAL_LINK, const bool, const bool);
    int writeLinkEqualization(const EQUAL_LINK, const bool, const double);
    int printLinkEqualization(std::ostream& = std::cout);

    // I2C methods
    int resetI2C();
    int initI2C();
    int readyI2C(bool&);
    int readI2CFrequency(double&);
    int writeI2CFrequency(const double);

    // EEPROM methods
    int readEEPROM(unsigned int&, unsigned int&, unsigned int&);
    int writeEEPROM(const unsigned int, const unsigned int, const unsigned int);

    // base address methods
    int readBaseAddress(u_short&);
    int writeBaseAddress(const u_short);

  private:

    // private members
    RCD::VME*                   m_vme;
    RCD::VMEMasterMap*          m_vmm;
    unsigned int                m_base;
    int                         m_status;

    // private methods
    int readBitField(const u_short, const u_short, const u_short, u_short&);
    int writeBitField(const u_short, const u_short, const u_short, const u_short);
    int readBitMask(const u_short, const u_short, const bool, bool&);

    int readI2CDEL(const u_short, u_short&);
    int writeI2CDEL(const u_short, const u_short);
    int readI2CDAC(const u_short, u_short&);
    int writeI2CDAC(const u_short, const u_short);
    int waitI2C();
    int checkI2C();

    int readFlash(const u_short, u_short&);
    int writeFlash(const u_short, const u_short);
    int eraseFlash();
    int waitFlash();

    // private constants
    static const u_int          VMM_ADDR                = 0x00000000;
    static const u_int          VMM_SIZE                = 0x00000100;
    static const u_int          VMM_AMOD                = 1;
    static const std::string    VMM_CODE;

    static const unsigned int   STRING_LENGTH           = 1024;

    static const u_short        SELECT_ADDR[SELECT_SIGNAL_NUMBER];
    static const u_short        SELECT_MASK[SELECT_SIGNAL_NUMBER];
    static const u_short        SELECT_SHFT[SELECT_SIGNAL_NUMBER];
    static const u_short        SELECT_DATA[SIGNAL_SELECTION_NUMBER];

    static const u_short        TTYP_DATA[TTYP_SELECTION_NUMBER];
    static const u_short        LOCTTYP_DATA[LOCTTYP_SELECTION_NUMBER];

    static const u_short        CALREQ_DATA[CALREQ_SELECTION_NUMBER];

    static const u_short        BUSY_ADDR[LINK_NUMBER];
    static const u_short        BUSY_MASK[LINK_NUMBER];
    static const u_short        BUSY_SHFT[LINK_NUMBER];
    static const u_short        BUSY_DATA[BUSY_SELECTION_NUMBER];

    static const u_short        STATUS_ADDR[LINK_NUMBER][STATUS_SIGNAL_NUMBER];
    static const u_short        STATUS_MASK[LINK_NUMBER][STATUS_SIGNAL_NUMBER];
    static const u_short        STATUS_INVT[LINK_NUMBER][STATUS_SIGNAL_NUMBER];

    static const u_short        DELAY_ADDR[DELAY_SIGNAL_NUMBER];
    static const u_short        DELAY_MASK_VAL          = 0x003f;
    static const u_short        DELAY_MASK_ENA          = 0x0040;

    typedef enum                { EQUAL_DISABLED, EQUAL_SHORT_CABLE, EQUAL_LONG_CABLE } EQUAL_SELECTION;
    static const unsigned int   EQUAL_SELECTION_NUMBER = EQUAL_LONG_CABLE + 1;
    static const std::string    EQUAL_SELECTION_NAME[EQUAL_SELECTION_NUMBER];
    static const u_short        EQUAL_GAIN_DATA[EQUAL_SELECTION_NUMBER];
    static const u_short        EQUAL_CTRL_DATA[EQUAL_SELECTION_NUMBER];
    static const u_short        EQUAL_GAIN_ADDR[EQUAL_LINK_NUMBER];
    static const u_short        EQUAL_CTRL_ADDR[EQUAL_LINK_NUMBER];
    static const u_short        DAC_ADDR                = 0x0098;
    static const u_short        DAC_MASK                = 0x00ff;
    static const u_short        DAC_SHFT                = 8;

    // EEPROM
    static const u_int          EEPROM_ADDR             = 0x00;
    static const u_int          EEPROM_SIZE             = 0x100;

    // register CS1
    static const u_short        CSR1_ADDR               = 0x80;
    static const u_short        CSR1_MASK_BC            = 0x0007;
    static const u_short        CSR1_SHFT_BC            = 0;
    static const u_short        CSR1_MASK_ORB           = 0x0070;
    static const u_short        CSR1_SHFT_ORB           = 4;
    static const u_short        CSR1_MASK_L1A           = 0x0700;
    static const u_short        CSR1_SHFT_L1A           = 8;
    static const u_short        CSR1_MASK_TTRG          = 0x7000;
    static const u_short        CSR1_SHFT_TTRG          = 12;

    // register CSR2
    static const u_short        CSR2_ADDR               = 0x82;
    static const u_short        CSR2_MASK_BGO           = 0x0007;
    static const u_short        CSR2_SHFT_BGO           = 0;
    static const u_short        CSR2_MASK_TTYP          = 0x0070;
    static const u_short        CSR2_SHFT_TTYP          = 4;
    static const u_short        CSR2_MASK_CALREQ        = 0x0700;
    static const u_short        CSR2_SHFT_CALREQ        = 8;
    static const u_short        CSR2_MASK_LOCTTYP       = 0x7000;
    static const u_short        CSR2_SHFT_LOCTTYP       = 12;

    // register CSR3
    static const u_short        CSR3_ADDR               = 0x84;
    static const u_short        CSR3_MASK_BUSY_CTP      = 0x0003;
    static const u_short        CSR3_SHFT_BUSY_CTP      = 0;
    static const u_short        CSR3_MASK_BUSY_NIM      = 0x0030;
    static const u_short        CSR3_SHFT_BUSY_NIM      = 4;
    static const u_short        CSR3_MASK_BUSY_LTP      = 0x0300;
    static const u_short        CSR3_SHFT_BUSY_LTP      = 8;

    // register CSR4
    static const u_short        CSR4_ADDR               = 0x86;

    // register local TTYP
    static const u_short        LOCTTYP1_ADDR           = 0x88;
    static const u_short        LOCTTYP2_ADDR           = 0x8a;
    static const u_short        LOCTTYP3_ADDR           = 0x8c;
    static const u_short        LOCTTYP4_ADDR           = 0x8e;
    static const u_short        LOCTTYP_NMOD            = 2;
    static const u_short        LOCTTYP_SHFT            = 8;

    // register STAT1
    static const u_short        STAT1_ADDR              = 0x90;
    static const u_short        STAT1_MASK_BC_CTP       = 0x0001;
    static const u_short        STAT1_MASK_ORB_CTP      = 0x0002;
    static const u_short        STAT1_MASK_L1A_CTP      = 0x0004;
    static const u_short        STAT1_MASK_BC_LTP       = 0x0010;
    static const u_short        STAT1_MASK_ORB_LTP      = 0x0020;
    static const u_short        STAT1_MASK_L1A_LTP      = 0x0040;
    static const u_short        STAT1_MASK_BC_NIM       = 0x0100;
    static const u_short        STAT1_MASK_ORB_NIM      = 0x0200;
    static const u_short        STAT1_MASK_L1A_NIM      = 0x0400;
    static const u_short        STAT1_MASK_BUSY_CTP     = 0x1000;
    static const u_short        STAT1_MASK_BUSY_LTP     = 0x2000;

    // register STAT2
    static const u_short        STAT2_ADDR              = 0x92;
    static const u_short        STAT2_MASK_TTRG1_CTP    = 0x0001;
    static const u_short        STAT2_MASK_TTRG2_CTP    = 0x0002;
    static const u_short        STAT2_MASK_TTRG3_CTP    = 0x0004;
    static const u_short        STAT2_MASK_TTRG1_LTP    = 0x0010;
    static const u_short        STAT2_MASK_TTRG2_LTP    = 0x0020;
    static const u_short        STAT2_MASK_TTRG3_LTP    = 0x0040;
    static const u_short        STAT2_MASK_TTRG1_NIM    = 0x0100;
    static const u_short        STAT2_MASK_TTRG2_NIM    = 0x0200;
    static const u_short        STAT2_MASK_TTRG3_NIM    = 0x0400;
    static const u_short        STAT2_MASK_CALREQ0_CTP  = 0x1000;
    static const u_short        STAT2_MASK_CALREQ1_CTP  = 0x2000;
    static const u_short        STAT2_MASK_CALREQ2_CTP  = 0x4000;

    // register STAT3
    static const u_short        STAT3_ADDR              = 0x94;
    static const u_short        STAT3_MASK_BGO0_CTP     = 0x0001;
    static const u_short        STAT3_MASK_BGO1_CTP     = 0x0002;
    static const u_short        STAT3_MASK_BGO2_CTP     = 0x0004;
    static const u_short        STAT3_MASK_BGO3_CTP     = 0x0008;
    static const u_short        STAT3_MASK_BGO0_LTP     = 0x0010;
    static const u_short        STAT3_MASK_BGO1_LTP     = 0x0020;
    static const u_short        STAT3_MASK_BGO2_LTP     = 0x0040;
    static const u_short        STAT3_MASK_BGO3_LTP     = 0x0080;
    static const u_short        STAT3_MASK_BGO0_NIM     = 0x0100;
    static const u_short        STAT3_MASK_BGO1_NIM     = 0x0200;
    static const u_short        STAT3_MASK_BGO2_NIM     = 0x0400;
    static const u_short        STAT3_MASK_BGO3_NIM     = 0x0800;
    static const u_short        STAT3_MASK_CALREQ0_LTP  = 0x1000;
    static const u_short        STAT3_MASK_CALREQ1_LTP  = 0x2000;
    static const u_short        STAT3_MASK_CALREQ2_LTP  = 0x4000;

    // register STAT4
    static const u_short        STAT4_ADDR              = 0x96;
    static const u_short        STAT4_MASK_TTYP0_CTP    = 0x0001;
    static const u_short        STAT4_MASK_TTYP1_CTP    = 0x0002;
    static const u_short        STAT4_MASK_TTYP2_CTP    = 0x0004;
    static const u_short        STAT4_MASK_TTYP3_CTP    = 0x0008;
    static const u_short        STAT4_MASK_TTYP4_CTP    = 0x0010;
    static const u_short        STAT4_MASK_TTYP5_CTP    = 0x0020;
    static const u_short        STAT4_MASK_TTYP6_CTP    = 0x0040;
    static const u_short        STAT4_MASK_TTYP7_CTP    = 0x0080;
    static const u_short        STAT4_MASK_TTYP0_LTP    = 0x0100;
    static const u_short        STAT4_MASK_TTYP1_LTP    = 0x0200;
    static const u_short        STAT4_MASK_TTYP2_LTP    = 0x0400;
    static const u_short        STAT4_MASK_TTYP3_LTP    = 0x0800;
    static const u_short        STAT4_MASK_TTYP4_LTP    = 0x1000;
    static const u_short        STAT4_MASK_TTYP5_LTP    = 0x2000;
    static const u_short        STAT4_MASK_TTYP6_LTP    = 0x4000;
    static const u_short        STAT4_MASK_TTYP7_LTP    = 0x8000;

    // register SWRST
    static const u_short        SWRST_ADDR              = 0x98;
    static const u_short        SWRST_DATA              = 0x0001;

    // register I2C
    static const u_short        I2C_PLO_ADDR            = 0xa0;
    static const u_short        I2C_PHI_ADDR            = 0xa2;
    static const u_short        I2C_PRE_MASK            = 0x00ff;
    static const u_short        I2C_PRE_SHFT            = 8;
    static const double         I2C_PRE_FACT;

    static const u_short        I2C_CTR_ADDR            = 0xa4;
    static const u_short        I2C_CTR_MASK_EN         = 0x0080;
    static const u_short        I2C_CTR_MASK_IEN        = 0x0040;

    static const u_short        I2C_TXT_ADDR            = 0xa6;
    static const u_short        I2C_TXT_MASK_ADDR       = 0x007f;
    static const u_short        I2C_TXT_SHFT_ADDR       = 1;
    static const u_short        I2C_TXT_MASK_READ       = 0x0001;

    static const u_short        I2C_RCV_ADDR            = 0xa6;

    static const u_short        I2C_CMD_ADDR            = 0xa8;
    static const u_short        I2C_CMD_MASK_STA        = 0x0080;
    static const u_short        I2C_CMD_MASK_STO        = 0x0040;
    static const u_short        I2C_CMD_MASK_RD         = 0x0020;
    static const u_short        I2C_CMD_MASK_WR         = 0x0010;
    static const u_short        I2C_CMD_MASK_ACK        = 0x0008;
    static const u_short        I2C_CMD_MASK_IACK       = 0x0001;

    static const u_short        I2C_STA_ADDR            = 0xa8;
    static const u_short        I2C_STA_MASK_RXACK      = 0x0080;
    static const u_short        I2C_STA_MASK_BUSY       = 0x0040;
    static const u_short        I2C_STA_MASK_AL         = 0x0020;
    static const u_short        I2C_STA_MASK_TIP        = 0x0002;
    static const u_short        I2C_STA_MASK_IF         = 0x0002;

    static const u_short        I2C_RST_ADDR            = 0xaa;

    // addresses EEPROM
    static const unsigned int   EEPROM_MAN_NUMBER = 3;
    static const u_short        EEPROM_MAN_ADDR[EEPROM_MAN_NUMBER];

    static const unsigned int   EEPROM_BRD_NUMBER = 4;
    static const u_short        EEPROM_BRD_ADDR[EEPROM_BRD_NUMBER];

    static const unsigned int   EEPROM_REV_NUMBER = 4;
    static const u_short        EEPROM_REV_ADDR[EEPROM_REV_NUMBER];

    static const u_short        EEPROM_MASK = 0x00ff;
    static const u_short        EEPROM_SHFT = 8;

    // register SFL (serial flash)
    static const u_short        SFL_CSR_ADDR            = 0xb0;
    static const u_short        SFL_CSR_MASK_READ       = 0x0001;
    static const u_short        SFL_CSR_MASK_WRITE      = 0x0002;
    static const u_short        SFL_CSR_MASK_ERASE      = 0x0004;
    static const u_short        SFL_CSR_MASK_BUSY       = 0x0008;

    static const u_short        SFL_ADDR_ADDR           = 0xb2;
    static const u_short        SFL_ADDR_MASK           = 0x00ff;
    static const u_short        SFL_ADDR_MASK_BASE_LO   = 0x00fe;
    static const u_short        SFL_ADDR_MASK_BASE_HI   = 0x00ff;
    static const u_short        SFL_ADDR_SHFT_BASE      = 8;

    static const u_short        SFL_WDAT_ADDR           = 0xb4;
    static const u_short        SFL_RDAT_ADDR           = 0xb6;
    static const u_short        SFL_DATA_MASK           = 0xff;

};      // class LTPI

}       // namespace RCD

#endif  // LTPI_H
