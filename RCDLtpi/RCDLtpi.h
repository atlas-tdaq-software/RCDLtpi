#ifndef RCDLTPI_H
#define RCDLTPI_H

//******************************************************************************
  // file: RCDLtpi.h
  // desc: library for Ltp module
  //
  // Author: Andrea Messina (andrea.messina@cern.ch)
  //         Thilo Pauly  (thilo.pauly@cern.ch)
  //
  //******************************************************************************

#include <string>
#include <vector>
#include "RCDVme/RCDVme.h"

  using namespace std;

namespace RCD {

  // Ltpi class -----------------------------------------------------------------
  class LTPi {

  public:
    LTPi() {
      debug=0;
      // DAC settings
      static const u_short VGain_shortCTP = 0x80;
      static const u_short VGain_shortLTP = 0x80;
      static const u_short VCTRL_shortCTP = 0x00;
      static const u_short VCTRL_shortLTP = 0x00;
      m_ctp_gain=VGain_shortCTP;
      m_ctp_equ=VCTRL_shortCTP;
      m_ltp_gain=VGain_shortLTP;
      m_ltp_equ=VCTRL_shortLTP;
      // DELAY settings
      m_delay_L1A=0;
      m_delay_ORB=0;
      m_delay_TTR1=0;
      m_delay_TTR2=0;
      m_delay_TTR3=0;
      m_delay_BGO0=0;
      m_delay_BGO1=0;
      m_delay_BGO2=0;
      m_delay_BGO3=0;
      m_delay_TRT0=0;
      m_delay_TRT1=0;
      m_delay_TRT2=0;
      m_delay_TRT3=0;
      m_delay_TRT4=0;
      m_delay_TRT5=0;
      m_delay_TRT6=0;
      m_delay_TRT7=0;
    }
    ~LTPi() {}

    // low-level interface:
    std::string GetError(int errcode);
    int Reset();
    int Open(u_int base);
    int Close();
    int ReadConfigurationEEPROM(u_int& board_man, u_int& board_revision, u_int& board_id);
    int readprom4(u_int offset, u_int *concat);
    int writeprom4(u_int offset, u_int concat);
    bool CheckCERNID();
    enum error_codes {
      ERROR_INVALID_INTERRUPT_VECTOR = 9901
    };

    // temporary write method
    int write(u_short addr, u_short data);

    // I2C
    int resetI2C();
    int setupI2C();
    bool I2C_frequency_OK();
    float readI2Cfreq();
    int setupDAC();
    int setupDAC4shortCTPCable();
    int setupDAC_LTPCable_toZero();
    int setupDAC_CTPCable_toZero();
    int setupDAC4shortLTPCable();
    int setupDAC4longCTPCable();
    int setupDAC4longLTPCable();
    int setupDAC4longCable();
    int setupDAC4shortCable();
    int setupDAC4givenCTPCable(float length);
    int setupDAC4givenLTPCable(float length);
    bool checkDAC4givenCTPCable(float length);
    bool checkDAC4givenLTPCable(float length);

    int setDAC();
    bool checkDAC4shortCTPCable();
    bool checkDAC4shortLTPCable();
    bool checkDAC4longCTPCable();
    bool checkDAC4longLTPCable();
    bool isShortCable(int length);
    bool isLongCable(int length);

    int setDacLtp(u_short gain, u_short equ);
    int setDacCtp(u_short gain, u_short equ);

    int setDACLTPgain(u_short gain);
    int setDACCTPgain(u_short gain);
    int setDACLTPequ(u_short gain);
    int setDACCTPequ(u_short gain);

    int getDACLTPgain(u_short& gain);
    int getDACCTPgain(u_short& gain);
    int getDACLTPequ(u_short& gain);
    int getDACCTPequ(u_short& gain);

    int enableCtpInput(bool ena);
    int enableLtpInput(bool ena);
    //     void setDefaultCtpGain(u_short gain) { m_ctp_gain = gain; }
    //     void setDefaultCtpEqu(u_short equ)   { m_ctp_equ = equ; }
    //     void setDefaultLtpGain(u_short gain) { m_ltp_gain = gain; }
    //     void setDefaultLtpEqu(u_short equ)   { m_ltp_equ = equ; }

    int setDelayAllChannels(u_short delay);
    int setDelayAllChannels();
    int readDelayAllChannels();
    int setDelaySingleChannel(u_short delay, u_short channel);
    int storeChannelDelay(u_short delay, u_short channel);
    int readDelaySingleChannel(u_short &delay, u_short channel);
    int resetDelay25Pll();
    int enableCTPout(int delay = 10);
    int disableCTPout();

    int i2c_write(u_short addr, u_short dat);
    int i2c_writeDAC(u_short addr, u_short dat);
    int i2c_readDAC(u_short daccr, u_short& dacval);
    int i2c_read(u_short addr, u_short& dat);

    // Module base address programming
    int read_base_addr(u_short &baseaddr);
    int write_base_addr(u_short baseaddr);

  private:
    int waitTransfer();
    // debug print out
    int debug;

  public:

    // Read methods
    int getBC_Path(u_short &status);
    int getOrbit_Path(u_short &status);
    int getL1A_Path(u_short &status);
    int getTTR_Path(u_short &status);
    int getBGO_Path(u_short &status);
    int getTRT_Path(u_short &status);
    int getCAL_Path(u_short &status);
    int getBusyCTP_Path(u_short &status);
    int getBusyNIM_Path(u_short &status);
    int getBusyLTP_Path(u_short &status);

    string getBC_Path();
    string getOrbit_Path();
    string getL1A_Path();
    string getTTR_Path();
    string getBGO_Path();
    string getTRT_Path();
    string getCAL_Path();

    string CTPout_BUSY();
    string LTPout_BUSY();
    string NIM_BUSY();

    int getTRT_SELECTION(u_short &status);
    bool isTRT_FROM_CTP_LTPOUT_FROM_LTP();
    bool isTRT_FROM_CTP();
    bool isTRT_FROM_LTP();
    bool isTRT_FROM_LOCAL();
    bool isTRT_FROM_LOCAL_LTPOUT_FROM_LTP();

    // set methods
    int setBC_Path(u_short value);
    int setOrbit_Path(u_short value);
    int setL1A_Path(u_short value);
    int setTTR_Path(u_short value);
    int setBGO_Path(u_short value);
    int setTRT_Path(u_short value);
    int setCAL_Path(u_short value);
    int setBusyCTP_Path(u_short value);
    int setBusyNIM_Path(u_short value);
    int setBusyLTP_Path(u_short value);
    int setTRT_SELECTION(u_short value);

    //test status register

    //    ---Status register 1
    bool isBC_CTPout();
    bool isORB_CTPout();
    bool isL1A_CTPout();

    bool isBC_LTPout();
    bool isORB_LTPout();
    bool isL1A_LTPout();

    bool isBC_NIM();
    bool isORB_NIM();
    bool isL1A_NIM();

    bool isBUSY_CTPout();
    bool isBUSY_LTPout();
    //    ---Status register 2
    bool isTTR1_CTPout();
    bool isTTR2_CTPout();
    bool isTTR3_CTPout();

    bool isTTR1_LTPout();
    bool isTTR2_LTPout();
    bool isTTR3_LTPout();

    bool isTTR1_NIM();
    bool isTTR2_NIM();
    bool isTTR3_NIM();

    bool isCAL0_CTPout();
    bool isCAL1_CTPout();
    bool isCAL2_CTPout();
    //    ---Status register 3
    bool isBGO0_CTPout();
    bool isBGO1_CTPout();
    bool isBGO2_CTPout();
    bool isBGO3_CTPout();

    bool isBGO0_LTPout();
    bool isBGO1_LTPout();
    bool isBGO2_LTPout();
    bool isBGO3_LTPout();

    bool isBGO0_NIM();
    bool isBGO1_NIM();
    bool isBGO2_NIM();
    bool isBGO3_NIM();

    bool isCAL0_LTPout();
    bool isCAL1_LTPout();
    bool isCAL2_LTPout();
    //    ---Status register 4
    bool isTRT0_CTPout();
    bool isTRT1_CTPout();
    bool isTRT2_CTPout();
    bool isTRT3_CTPout();
    bool isTRT4_CTPout();
    bool isTRT5_CTPout();
    bool isTRT6_CTPout();
    bool isTRT7_CTPout();

    bool isTRT0_LTPout();
    bool isTRT1_LTPout();
    bool isTRT2_LTPout();
    bool isTRT3_LTPout();
    bool isTRT4_LTPout();
    bool isTRT5_LTPout();
    bool isTRT6_LTPout();
    bool isTRT7_LTPout();

    // global mode of operation
    int off_mode();
    int ctp_mode();
    int ltp_mode();
    int nim_mode();
    int ctpltp_mode();
    int nimltp_mode();

    // Signal selection
    int BC_CTPoutFromCTPin_LTPoutFromCTPin();  // CTP-slave
    int Orbit_CTPoutFromCTPin_LTPoutFromCTPin();
    int L1A_CTPoutFromCTPin_LTPoutFromCTPin();
    int TTR_CTPoutFromCTPin_LTPoutFromCTPin();
    int BGO_CTPoutFromCTPin_LTPoutFromCTPin();
    int TRT_CTPoutFromCTPin_LTPoutFromCTPin();

    int BC_CTPoutFromLTPin_LTPoutFromLTPin();  // LTP-slave
    int Orbit_CTPoutFromLTPin_LTPoutFromLTPin();
    int L1A_CTPoutFromLTPin_LTPoutFromLTPin();
    int TTR_CTPoutFromLTPin_LTPoutFromLTPin();
    int BGO_CTPoutFromLTPin_LTPoutFromLTPin();
    int TRT_CTPoutFromLTPin_LTPoutFromLTPin();

    int BC_CTPoutFromNIMin_LTPoutFromNIMin();  // NIM-master
    int Orbit_CTPoutFromNIMin_LTPoutFromNIMin();
    int L1A_CTPoutFromNIMin_LTPoutFromNIMin();
    int TTR_CTPoutFromNIMin_LTPoutFromNIMin();
    int BGO_CTPoutFromNIMin_LTPoutFromNIMin();
    int TRT_CTPoutFromLocal_LTPoutFromLocal();

    int BC_CTPoutFromCTPin_LTPoutFromLTPin();  // CTP-master, parallel bypass
    int Orbit_CTPoutFromCTPin_LTPoutFromLTPin();
    int L1A_CTPoutFromCTPin_LTPoutFromLTPin();
    int TTR_CTPoutFromCTPin_LTPoutFromLTPin();
    int BGO_CTPoutFromCTPin_LTPoutFromLTPin();
    int TRT_CTPoutFromCTPin_LTPoutFromLTPin();

    int BC_CTPoutFromNIMin_LTPoutFromLTPin(); // Local-master, parallel bypass
    int Orbit_CTPoutFromNIMin_LTPoutFromLTPin();
    int L1A_CTPoutFromNIMin_LTPoutFromLTPin();
    int TTR_CTPoutFromNIMin_LTPoutFromLTPin();
    int BGO_CTPoutFromNIMin_LTPoutFromLTPin();
    int TRT_CTPoutFromLocal_LTPoutFromLTPin();

    int BC_Off(); // OFF
    int Orbit_Off();
    int L1A_Off();
    int TTR_Off();
    int BGO_Off();
    int TRT_Off();

    // busy selection
    int  busyCTP_OFF();
    int  busyCTP_FROM_LTP();
    int  busyCTP_FROM_CTP();
    int  busyCTP_FROM_CTP_AND_LTP();
    int  busyNIM_OFF();
    int  busyNIM_FROM_LTP();
    int  busyNIM_FROM_CTP();
    int  busyNIM_FROM_CTP_AND_LTP();
    int  busyLTP_OFF();
    int  busyLTP_FROM_LTP();
    int  busyLTP_FROM_CTP();
    int  busyLTP_FROM_CTP_AND_LTP();

    // calibration request selection
    int calCTPfromCTP_LTPFromLTP();
    int calCTPfromCTP_LTPFromCTP();
    int calCTPfromLTP_LTPFromLTP();

    // Trigger type selection when generated locally
    int TRT_FROM_LOCAL_REGISTER();
    int TRT_DERIVED_FROM_CTP_TTR();
    int TRT_DERIVED_FROM_LTP_TTR();
    int TRT_DERIVED_FROM_NIM_TTR();

    // Set Trigger type word given the trigger test pattern
    int SetTRT(int, u_short);
    int SetTRT000(u_short value=0);
    int SetTRT001(u_short value=1);
    int SetTRT010(u_short value=2);
    int SetTRT011(u_short value=3);
    int SetTRT100(u_short value=4);
    int SetTRT101(u_short value=5);
    int SetTRT110(u_short value=6);
    int SetTRT111(u_short value=7);

    int GetTRT(int, u_short&);
    int GetTRT000(u_short& value);
    int GetTRT001(u_short& value);
    int GetTRT010(u_short& value);
    int GetTRT011(u_short& value);
    int GetTRT100(u_short& value);
    int GetTRT101(u_short& value);
    int GetTRT110(u_short& value);
    int GetTRT111(u_short& value);

    int dumpTRTTable();
    int dumpTRTStatus();
    int dumpTRTinput();
    int dumpModuleRegisters();
    int dumpCalibrationStatus();

    int printI2Cfreq();
    int printDACsettings();
    int printBC_status();
    int printOrbit_status();
    int printL1A_status();
    int printTTR_status();
    int printBGO_status();
    int dumpBusyStatus();

    // helper methods
    int  pow(int a, int k);
    int  bintoint(char* a);
    int  bin4toint(char* a1,char* a2,char* a3,char* a4);
    int  setBits(u_int reg, u_short mask, u_short bits);
    int  getBitsAndPrint(u_int reg, u_short mask, u_short* bits);
    int  getBits(u_int reg, u_short mask, u_short* bits);
    int  testBits(u_int reg, u_short mask, bool* result);
    std::string printBinary16(u_short n);

    static const int minimalLongLength  = 20;
    static const int maximalShortLength = 21;

    // delay25 chip addresses
    static const u_short L1AORB = 0x38;
    // chip address + channel addresses
    static const u_short DEL25L1A = 0x38;
    static const u_short DEL25ORB = 0x3a;

    static const u_short BGO    = 0x30;
    // channel addresses
    static const u_short DEL25BGO0 = 0x30;
    static const u_short DEL25BGO1 = 0x31;
    static const u_short DEL25BGO2 = 0x32;
    static const u_short DEL25BGO3 = 0x33;

    static const u_short TTR    = 0x28;
    // channel addresses
    static const u_short DEL25TTR1 = 0x28;
    static const u_short DEL25TTR2 = 0x29;
    static const u_short DEL25TTR3 = 0x2a;

    static const u_short TRT2   = 0x20;
    // channel addresses
    static const u_short DEL25TRT4 = 0x20;
    static const u_short DEL25TRT5 = 0x21;
    static const u_short DEL25TRT6 = 0x22;
    static const u_short DEL25TRT7 = 0x23;

    static const u_short TRT1   = 0x10;
    // channel addresses
    static const u_short DEL25TRT0 = 0x10;
    static const u_short DEL25TRT1 = 0x11;
    static const u_short DEL25TRT2 = 0x12;
    static const u_short DEL25TRT3 = 0x13;

    // DAC
    static const u_short addGain_CTP = 0x10;
    static const u_short addGain_LTP = 0x12;
    static const u_short addCTRL_CTP = 0x14;
    static const u_short addCTRL_LTP = 0x16;
    // DAC values
    //  -- long cables
    static const u_short VGain_longCTP  = 0xA0;
    static const u_short VGain_longLTP  = 0xA0;
    static const u_short VCTRL_longCTP  = 0x10;
    static const u_short VCTRL_longLTP  = 0x10;
    //  -- short cables
    static const u_short VGain_shortCTP = 0x80;
    static const u_short VGain_shortLTP = 0x80;
    static const u_short VCTRL_shortCTP = 0x00;
    static const u_short VCTRL_shortLTP = 0x00;

    // -- cable length to DAC correspondence
    u_short VGain_CTP (float length) {
      if      ( length <= 20 ) return 0x80;
      else if ( length >  20 ) return 0xA0;
      else return 0x00;
    };
    u_short VGain_LTP (float length) {
      if      ( length <= 20 ) return 0x80;
      else if ( length >  20 ) return 0xA0;
      else return 0x00;
    };
    u_short VCTRL_CTP (float length) {
      if      ( length <= 20 ) return 0x00;
      else if ( length >  20 ) return 0x10;
      else return 0x00;
    };
    u_short VCTRL_LTP (float length) {
      if      ( length <= 20 ) return 0x00;
      else if ( length >  20 ) return 0x10;
      else return 0x00;
    };

    // masks - busy
    static const u_short MSK_BUSY_CTP   = 0x0003;
    static const u_short MSK_BUSY_NIM   = 0x0030;
    static const u_short MSK_BUSY_LTP   = 0x0300;

    static const u_int MANUFID          = 0x20;
    static const u_int BOARDID          = 0x30;
    static const u_int REVISION         = 0x40;

  private:

    string decodeCodeA(u_short p);
    string decodeCodeB(u_short p);

    // register addresses
    static const u_short REG_I2C_RESET          = 0xaa;
    static const u_short REG_I2C_COMMAND_STATUS = 0xa8;
    static const u_short REG_I2C_TRANS_RECEIVE  = 0xa6;
    static const u_short REG_I2C_CTR            = 0xa4;
    static const u_short REG_I2C_PER_HIGH       = 0xa2;
    static const u_short REG_I2C_PER_LOW        = 0xa0;
    static const u_short REG_SW_RST             = 0x98;
    static const u_short REG_STATUS4            = 0x96;
    static const u_short REG_STATUS3            = 0x94;
    static const u_short REG_STATUS2            = 0x92;
    static const u_short REG_STATUS1            = 0x90;
    // TRIGGER TYPE REGISTERS
    static const u_short REG_TTYPE4             = 0x8e;
    static const u_short REG_TTYPE3             = 0x8c;
    static const u_short REG_TTYPE2             = 0x8a;
    static const u_short REG_TTYPE1             = 0x88;

    // CONTROL REGISTERS
    static const u_short REG_CSR4               = 0x86;
    static const u_short REG_CSR3               = 0x84;
    static const u_short REG_CSR2               = 0x82;
    static const u_short REG_CSR1               = 0x80;
    static const u_short REG_CONF_EEPROM        = 0x00;

    // Masks -
    static const u_short MSK_BC            = 0x0007;
    static const u_short MSK_ORB           = 0x0070;
    static const u_short MSK_L1A           = 0x0700;
    static const u_short MSK_TTR           = 0x7000;

    static const u_short MSK_BGO           = 0x0007;
    static const u_short MSK_TRT           = 0x0070;
    static const u_short MSK_CAL           = 0x0300;
    static const u_short MSK_TRT_SELECTION = 0x3000;

    // TRIGGER TYPE MASKS
    static const u_short MSK_TTYPE0 = 0x00ff;
    static const u_short MSK_TTYPE1 = 0xff00;
    static const u_short MSK_TTYPE2 = 0x00ff;
    static const u_short MSK_TTYPE3 = 0xff00;
    static const u_short MSK_TTYPE4 = 0x00ff;
    static const u_short MSK_TTYPE5 = 0xff00;
    static const u_short MSK_TTYPE6 = 0x00ff;
    static const u_short MSK_TTYPE7 = 0xff00;

    //       -- STATUS REGISTER 1
    static const u_short MSK_SR_BC_CTP         = 0x0001;
    static const u_short MSK_SR_ORB_CTP        = 0x0002;
    static const u_short MSK_SR_L1A_CTP        = 0x0004;

    static const u_short MSK_SR_BC_LTP         = 0x0010;
    static const u_short MSK_SR_ORB_LTP        = 0x0020;
    static const u_short MSK_SR_L1A_LTP        = 0x0040;

    static const u_short MSK_SR_BC_NIM         = 0x0100;
    static const u_short MSK_SR_ORB_NIM        = 0x0200;
    static const u_short MSK_SR_L1A_NIM        = 0x0400;

    static const u_short MSK_SR_BUSY_CTP       = 0x1000;
    static const u_short MSK_SR_BUSY_LTP       = 0x2000;

    //       -- STATUS REGISTER 2
    static const u_short MSK_SR_TTR1_CTP       = 0x0001;
    static const u_short MSK_SR_TTR2_CTP       = 0x0002;
    static const u_short MSK_SR_TTR3_CTP       = 0x0004;

    static const u_short MSK_SR_TTR1_LTP       = 0x0010;
    static const u_short MSK_SR_TTR2_LTP       = 0x0020;
    static const u_short MSK_SR_TTR3_LTP       = 0x0040;

    static const u_short MSK_SR_TTR1_NIM       = 0x0100;
    static const u_short MSK_SR_TTR2_NIM       = 0x0200;
    static const u_short MSK_SR_TTR3_NIM       = 0x0400;

    static const u_short MSK_SR_CAL0_CTP       = 0x1000;
    static const u_short MSK_SR_CAL1_CTP       = 0x2000;
    static const u_short MSK_SR_CAL2_CTP       = 0x4000;

    //       -- STATUS REGISTER 3
    static const u_short MSK_SR_BGO0_CTP       = 0x0001;
    static const u_short MSK_SR_BGO1_CTP       = 0x0002;
    static const u_short MSK_SR_BGO2_CTP       = 0x0004;
    static const u_short MSK_SR_BGO3_CTP       = 0x0008;

    static const u_short MSK_SR_BGO0_LTP       = 0x0010;
    static const u_short MSK_SR_BGO1_LTP       = 0x0020;
    static const u_short MSK_SR_BGO2_LTP       = 0x0040;
    static const u_short MSK_SR_BGO3_LTP       = 0x0080;

    static const u_short MSK_SR_BGO0_NIM       = 0x0100;
    static const u_short MSK_SR_BGO1_NIM       = 0x0200;
    static const u_short MSK_SR_BGO2_NIM       = 0x0400;
    static const u_short MSK_SR_BGO3_NIM       = 0x0800;

    static const u_short MSK_SR_CAL0_LTP       = 0x1000;
    static const u_short MSK_SR_CAL1_LTP       = 0x2000;
    static const u_short MSK_SR_CAL2_LTP       = 0x4000;

    //       -- STATUS REGISTER 4
    static const u_short MSK_SR_TRT0_CTP       = 0x0001;
    static const u_short MSK_SR_TRT1_CTP       = 0x0002;
    static const u_short MSK_SR_TRT2_CTP       = 0x0004;
    static const u_short MSK_SR_TRT3_CTP       = 0x0008;
    static const u_short MSK_SR_TRT4_CTP       = 0x0010;
    static const u_short MSK_SR_TRT5_CTP       = 0x0020;
    static const u_short MSK_SR_TRT6_CTP       = 0x0040;
    static const u_short MSK_SR_TRT7_CTP       = 0x0080;

    static const u_short MSK_SR_TRT0_LTP       = 0x0100;
    static const u_short MSK_SR_TRT1_LTP       = 0x0200;
    static const u_short MSK_SR_TRT2_LTP       = 0x0400;
    static const u_short MSK_SR_TRT3_LTP       = 0x0800;
    static const u_short MSK_SR_TRT4_LTP       = 0x1000;
    static const u_short MSK_SR_TRT5_LTP       = 0x2000;
    static const u_short MSK_SR_TRT6_LTP       = 0x4000;
    static const u_short MSK_SR_TRT7_LTP       = 0x8000;

    // Bits -
    //         -- BC
    static const u_short BC_OFF                         = 0x0;
    static const u_short BC_FROM_CTP_LTPOUT_FROM_LTP    = 0x0001;
    static const u_short BC_FROM_CTP                    = 0x0002;
    static const u_short BC_FROM_LTP                    = 0x0003;
    static const u_short BC_FROM_NIM                    = 0x0004;
    static const u_short BC_FROM_NIM_LTPOUT_FROM_LTP    = 0x0005;
    //         -- ORBIT
    static const u_short ORB_OFF                        = 0x0;
    static const u_short ORB_FROM_CTP_LTPOUT_FROM_LTP   = 0x0010;
    static const u_short ORB_FROM_CTP                   = 0x0020;
    static const u_short ORB_FROM_LTP                   = 0x0030;
    static const u_short ORB_FROM_NIM                   = 0x0040;
    static const u_short ORB_FROM_NIM_LTPOUT_FROM_LTP   = 0x0050;
    //         -- L1A
    static const u_short L1A_OFF                        = 0x0;
    static const u_short L1A_FROM_CTP_LTPOUT_FROM_LTP   = 0x0100;
    static const u_short L1A_FROM_CTP                   = 0x0200;
    static const u_short L1A_FROM_LTP                   = 0x0300;
    static const u_short L1A_FROM_NIM                   = 0x0400;
    static const u_short L1A_FROM_NIM_LTPOUT_FROM_LTP   = 0x0500;
    //         -- TEST TRIGGER
    static const u_short TTR_OFF                        = 0x0;
    static const u_short TTR_FROM_CTP_LTPOUT_FROM_LTP   = 0x1000;
    static const u_short TTR_FROM_CTP                   = 0x2000;
    static const u_short TTR_FROM_LTP                   = 0x3000;
    static const u_short TTR_FROM_NIM                   = 0x4000;
    static const u_short TTR_FROM_NIM_LTPOUT_FROM_LTP   = 0x5000;
    //         -- BGO
    static const u_short BGO_OFF                        = 0x0;
    static const u_short BGO_FROM_CTP_LTPOUT_FROM_LTP   = 0x0001;
    static const u_short BGO_FROM_CTP                   = 0x0002;
    static const u_short BGO_FROM_LTP                   = 0x0003;
    static const u_short BGO_FROM_NIM                   = 0x0004;
    static const u_short BGO_FROM_NIM_LTPOUT_FROM_LTP   = 0x0005;
    //        -- TRIGGER TYPE
    static const u_short TRT_OFF                        = 0x0;
    static const u_short TRT_FROM_CTP_LTPOUT_FROM_LTP   = 0x0010;
    static const u_short TRT_FROM_CTP                   = 0x0020;
    static const u_short TRT_FROM_LTP                   = 0x0030;
    static const u_short TRT_FROM_LOCAL                 = 0x0040;
    static const u_short TRT_FROM_LOCAL_LTPOUT_FROM_LTP = 0x0050;
    //        -- BUSY
    static const u_short BUSY_CTP_OFF                   = 0x0000;
    static const u_short BUSY_CTP_FROM_LTP              = 0x0003;
    static const u_short BUSY_CTP_FROM_CTP              = 0x0001;
    static const u_short BUSY_CTP_FROM_CTP_AND_LTP      = 0X0002;
    static const u_short BUSY_NIM_OFF                   = 0x0000;
    static const u_short BUSY_NIM_FROM_LTP              = 0x0030;
    static const u_short BUSY_NIM_FROM_CTP              = 0x0010;
    static const u_short BUSY_NIM_FROM_CTP_AND_LTP      = 0X0020;
    static const u_short BUSY_LTP_OFF                   = 0x0000;
    static const u_short BUSY_LTP_FROM_LTP              = 0x0300;
    static const u_short BUSY_LTP_FROM_CTP              = 0x0100;
    static const u_short BUSY_LTP_FROM_CTP_AND_LTP      = 0X0200;
    //        -- CALIBRATION REQUEST
    static const u_short CAL_CTP_FROM_CTP_LTP_FROM_LTP  = 0x0100;
    static const u_short CAL_FROM_CTP                   = 0x0200;
    static const u_short CAL_FROM_LTP                   = 0x0300;
    //        -- SELECTION TEST TRIGGER TO GENEATE TRIGGER TYPE
    static const u_short SEL_TTR_TEST                   = 0x0000;
    static const u_short SEL_TTR_CTP                    = 0x1000;
    static const u_short SEL_TTR_LTP                    = 0x2000;
    static const u_short SEL_TTR_NIM                    = 0x3000;

    // Serial flash access
    int flash_read(u_short addr, u_short &data);
    int flash_write(u_short addr, u_short data);

    // Regisgers for programming the base address in the serial flash
    static const u_int SFL_CSR_REG       = 0xB0;
    static const u_int SFL_ADDR_REG      = 0xB2;
    static const u_int SFL_WDAT_REG      = 0xB4;
    static const u_int SFL_RDAT_REG      = 0xB6;

    // Serial flash control/status register bits
    static const u_short SFL_READ        = 0x1;
    static const u_short SFL_WRITE       = 0x2;
    static const u_short SFL_ERASE       = 0x4;
    static const u_short SFL_BUSY        = 0x8;

    static const u_short BASE_ADDR_OFFSET = 0xFE;
    /////////////////////////////////////////////

      // -- VMEbus driver/library and master mapping
      VME*                      m_vme;
      VMEMasterMap*             m_vmm;
      static const u_int        VME_SIZE = 0x0100;

      // -- general usage parameters
      u_short                   m_dats;
      // DAC settings
      u_short                   m_ctp_gain;
      u_short                   m_ctp_equ;
      u_short                   m_ltp_gain;
      u_short                   m_ltp_equ;
      // DELAY settings
      u_short                   m_delay_L1A;
      u_short                   m_delay_ORB;
      u_short                   m_delay_TTR1;
      u_short                   m_delay_TTR2;
      u_short                   m_delay_TTR3;
      u_short                   m_delay_BGO0;
      u_short                   m_delay_BGO1;
      u_short                   m_delay_BGO2;
      u_short                   m_delay_BGO3;
      u_short                   m_delay_TRT0;
      u_short                   m_delay_TRT1;
      u_short                   m_delay_TRT2;
      u_short                   m_delay_TRT3;
      u_short                   m_delay_TRT4;
      u_short                   m_delay_TRT5;
      u_short                   m_delay_TRT6;
      u_short                   m_delay_TRT7;
  };

}       // namespace RCD

#endif // RCDLTPI_H
